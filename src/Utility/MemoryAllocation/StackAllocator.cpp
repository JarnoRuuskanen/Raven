#include "Utility/MemoryAllocation/StackAllocator.h"
#include "Utility/MemoryAllocation/MemoryUtils.h"

#include <stdlib.h>
#include <algorithm>

namespace Raven
{
    StackAllocator::StackAllocator(const std::size_t totalSize) : Allocator(totalSize), m_offset(0) {}

    StackAllocator::~StackAllocator()
    {
        free(m_startPtr);
        m_startPtr = nullptr;
    }

    void StackAllocator::init()
    {
        if (m_startPtr != nullptr)
        {
            free(m_startPtr);
        }
        m_startPtr = malloc(m_totalSize);
        m_offset = 0;
    }

    void* StackAllocator::allocate(const std::size_t size, const std::size_t alignment)
    {
        const std::size_t currentAddress = (std::size_t)m_startPtr + m_offset;
        std::size_t padding = MemoryUtility::calculatePaddingWithHeader(currentAddress, alignment, sizeof(AllocationHeader));
       
        if (m_offset + padding + size > m_totalSize)
        {
            return nullptr;
        }
        m_offset += padding;

        const std::size_t nextAddress = currentAddress + padding;
        const std::size_t headerAddress = nextAddress - sizeof(AllocationHeader);

        AllocationHeader header{padding};
        AllocationHeader* headerPtr = (AllocationHeader * )headerAddress;
        headerPtr = &header;

        m_offset += size;
        m_sizeUsed = m_offset;
        m_peakSize = std::max(m_peakSize, m_sizeUsed);

        return (void*)nextAddress;
    }

    void StackAllocator::free(void* ptr)
    {
        const std::size_t currentAddress = (std::size_t) ptr;
        const std::size_t headerAddress = currentAddress - sizeof(AllocationHeader);
        const AllocationHeader* header{(AllocationHeader*)headerAddress};

        m_offset = currentAddress - header->padding - (std::size_t)m_startPtr;
        m_sizeUsed = m_offset;
    }

    void StackAllocator::reset()
    {
        m_offset = 0;
        m_sizeUsed = 0;
        m_peakSize = 0;
    }
}