#ifndef RAVEN_IMAGE_UTILITY_H_INCLUDED
#define RAVEN_IMAGE_UTILITY_H_INCLUDED
#include "GraphicsApi/Vulkan/Common.h"

namespace Raven
{
    namespace ImageUtility
    {
        VkSampler createDefaultLinearSampler(const VkDevice& device);
        VkImageSubresourceRange getDefaultImageSubresourceRange(VkFormat format, VkImageLayout newLayout);

        bool createMemoryBackedImage(const VkDevice& device, const VkPhysicalDevice& physicalDevice, const VkImageCreateInfo& createInfo,
                                     VkMemoryPropertyFlags properties, VkDeviceMemory& imageMemory, VkImage& image);
        bool createImageView(const VkDevice& device, VkImageViewCreateInfo& createInfo, VkImageView& imageView);
        bool transitionImageLayout(const VkDevice& device, const VkQueue& queue, VkImageLayout oldLayout,
                                   VkImageLayout newLayout, VkImageSubresourceRange& subresourceRange, VkImage& image);
    }
}

#endif
