#include "GraphicsApi/Vulkan/Subpasses/BaseSubpass.h"
#include "Utility/DelegateManager.h"
#include "GraphicsApi/Vulkan/VulkanUtility/UtilityFunctions.h"
#include "GraphicsApi/Vulkan/VulkanUtility/DescriptorUtility.h"
#include "GraphicsApi/Vulkan/VulkanUtility/StructInitializer.h"

namespace Raven
{
    bool BaseSubpass::initPipeline(const VkDevice& device, const VkRenderPass& parentRenderPass, uint32_t subpassIndex)
    {
        assert(subpassIndex >= 0);
        m_orderNumberInRenderPass = subpassIndex;
        assert(m_subpassDescriptionCreated);
        assert(!m_shaderStages.empty());

        //Vertex input state.
        VkPipelineVertexInputStateCreateInfo vertexInputState;
        initializeVertexInputState(vertexInputState);

        VkPipelineInputAssemblyStateCreateInfo inputAssembly;
        initializeInputAssemblyState(inputAssembly);

        //Viewport state.
        VkPipelineViewportStateCreateInfo viewportState;
        initializeViewportState(viewportState);

        //Rasterizer
        VkPipelineRasterizationStateCreateInfo rasterizer;
        initializeRasterizerState(rasterizer);

        //Multisampling.
        VkPipelineMultisampleStateCreateInfo multisampling;
        initializeMultisampleState(multisampling);

        //Depth stencil testing.
        VkPipelineDepthStencilStateCreateInfo depthStencil;
        initializeDepthStencilState(depthStencil);

        VkPipelineColorBlendStateCreateInfo colorBlend;
        initializeColorBlendState(colorBlend);

        VkPipelineDynamicStateCreateInfo dynamicState;
        initializeDynamicState(dynamicState);

        VkGraphicsPipelineCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        createInfo.pNext = nullptr;
        createInfo.flags = 0;
        createInfo.stageCount = static_cast<uint32_t>(m_shaderStages.size());
        createInfo.pStages = m_shaderStages.data();
        createInfo.pVertexInputState = &vertexInputState;
        createInfo.pInputAssemblyState = &inputAssembly;
        createInfo.pTessellationState = nullptr;
        createInfo.pViewportState = &viewportState;
        createInfo.pRasterizationState = &rasterizer;
        createInfo.pMultisampleState = &multisampling;
        createInfo.pDepthStencilState = &depthStencil;
        createInfo.pColorBlendState = &colorBlend;
        createInfo.layout = m_renderpassPipeline.layout;
        createInfo.pDynamicState = &dynamicState;
        createInfo.renderPass = parentRenderPass;
        createInfo.subpass = subpassIndex;
        createInfo.basePipelineHandle = VK_NULL_HANDLE;

        //Pipeline cache
        VkPipelineCacheCreateInfo pipelineCacheCreateInfo = {};
        pipelineCacheCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
        VkResult result = vkCreatePipelineCache(device, &pipelineCacheCreateInfo, nullptr, &m_renderpassPipeline.cache);
        assert(result == VK_SUCCESS);

        result = vkCreateGraphicsPipelines(device, m_renderpassPipeline.cache, 1, &createInfo, nullptr, &m_renderpassPipeline.pipeline);
        if (result != VK_SUCCESS)
        {
            assert(result == VK_SUCCESS && "Failed to create a subpass pipeline!");
            return false;
        }
        return true;
    }

    void BaseSubpass::createSubpassDescription(const std::vector<VkAttachmentReference>& inputReferences, const std::vector<VkAttachmentReference>& colorReferences, 
                                               const std::vector<VkAttachmentReference>& depthReferences, const std::vector<VkAttachmentDescription>& attachments,
                                               const std::vector<uint32_t>& preservedAttachmentIndices)
    {
        for (const auto& reference : inputReferences)
        {
            m_inputAttachmentReferences.push_back(reference);
        }

        for (const auto& reference : colorReferences)
        {
            m_colorAttachmentReferences.push_back(reference);
        }

        //There is usually only a single depth attachment if any.
        for (const auto& reference : depthReferences)
        {
            m_depthAttachmentReferences.push_back(reference);
        }

        //NOTE, ACCORDING TO THE SPECS, THE PRESERVED ATTACHMENTS MUST NOT BE 
        //REFERENCES BY ANY OTHER ATTACHMENTS SAVED TO THE SUBPASS DESCRIPTION.
        //https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkSubpassDescription.html#_members
        for (const auto& index : preservedAttachmentIndices)
        {
            std::cout << "Subpass \"" << m_passName.c_str() << "\" is preserving attachment with index: " << index << " for further use.\n";
            m_preservedAttachmentsIndices.push_back(index);
        }

        for (const auto& attachment : attachments)
        {
            m_attachmentDescriptions.push_back(attachment);
        }

        //Initialize the subpass description.
        m_subpassDescription.flags = 0;
        m_subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        m_subpassDescription.colorAttachmentCount = static_cast<uint32_t>(m_colorAttachmentReferences.size());
        m_subpassDescription.pColorAttachments = m_colorAttachmentReferences.size() > 0 ? m_colorAttachmentReferences.data() : nullptr;
        m_subpassDescription.inputAttachmentCount = static_cast<uint32_t>(m_inputAttachmentReferences.size());
        m_subpassDescription.pInputAttachments = m_inputAttachmentReferences.size() > 0 ? m_inputAttachmentReferences.data() : nullptr; //Attachments that are read from in a shader
        m_subpassDescription.pResolveAttachments = nullptr;
        //Subpass can only use a single depth attachment but it's easy to copy to a vector so that's why I'm using vectors here too.
        m_subpassDescription.pDepthStencilAttachment = m_depthAttachmentReferences.size() > 0 ? &m_depthAttachmentReferences[0] : nullptr;
        m_subpassDescription.preserveAttachmentCount = static_cast<uint32_t>(m_preservedAttachmentsIndices.size());
        m_subpassDescription.pPreserveAttachments = m_preservedAttachmentsIndices.size() > 0 ? m_preservedAttachmentsIndices.data() : nullptr;

        const RavenVulkanContext& context = DelegateManager::getInstance().getVulkanContextDelegates().getVulkanContextFunc.Invoke();
        VkResult layoutInitialized = initializePipelineLayout(context.device);
        assert(layoutInitialized == VK_SUCCESS);

        m_subpassDescriptionCreated = true;
    }

    void BaseSubpass::setSubpassDependency(uint32_t src, uint32_t dst, VkPipelineStageFlags srcStageMask,
                                           VkPipelineStageFlags dstStageMask, VkAccessFlags srcAccessMask, 
                                           VkAccessFlags dstAccessMask, VkDependencyFlags flags)
    {
        m_subpassDependency = {};
        m_subpassDependency.srcSubpass = src;
        m_subpassDependency.dstSubpass = dst;
        m_subpassDependency.srcStageMask = srcStageMask;
        m_subpassDependency.srcAccessMask = srcAccessMask;
        m_subpassDependency.dstStageMask = dstStageMask;
        m_subpassDependency.dstAccessMask = dstAccessMask;
        m_subpassDependency.dependencyFlags = flags;

        m_subpassDependencySet = true;
    }

    void BaseSubpass::destroyPipeline(const VkDevice& device)
    {
        vkDestroyPipeline(device, m_renderpassPipeline.pipeline, nullptr);
        vkDestroyPipelineCache(device, m_renderpassPipeline.cache, nullptr);
        vkDestroyPipelineLayout(device, m_renderpassPipeline.layout, nullptr);
    }

    VkResult BaseSubpass::describePipeline(const VkDevice& device, const std::vector<ShaderLoadInformation>& shadersToLoad)
    {
        m_shaderStages.clear();
        m_shaderStages.shrink_to_fit();
        m_shaderStageDatas.clear();
        m_shaderStageDatas.shrink_to_fit();
        //First we compile the spirv.
        UtilityFunctions::fillShaderStageData(device, shadersToLoad, m_shaderStageDatas);
        assert(m_shaderStageDatas.size() > 0);
        for (size_t i = 0; i < m_shaderStageDatas.size(); i++)
        {
            m_shaderStages.push_back(m_shaderStageDatas[i].createInfo);
        }

        //Using reflection we generate the descriptor layout bindings so that I don't have to modify these values every time I make 
        //changes in the shaders.
        DescriptorLayoutBindingsPerDescriptorSet descriptorLayoutBindings;
        ShaderUtility::generatePipelineDescriptionFromShaders(m_shaderStageDatas, descriptorLayoutBindings, m_shaderPushConstantInfos);

        //Generate descriptor set layouts for each descriptor set.
        std::vector<VkDescriptorSetLayoutBinding> layoutBindings;
        for (const auto& layoutBindingsPerDescriptorSet : descriptorLayoutBindings)
        {
            auto& binds = layoutBindingsPerDescriptorSet.second;
            for (auto& layoutBinding : binds)
            {
                layoutBindings.push_back(layoutBinding.layoutBinding);
                m_descriptorSetBindingInfos.push_back(layoutBinding);
            }
            m_renderpassPipeline.descriptorSetLayouts.push_back({});
            VkResult result = DescriptorUtility::createDescriptorSetLayout(device, layoutBindings, m_renderpassPipeline.descriptorSetLayouts.back());
            assert(result == VK_SUCCESS);
            layoutBindings.clear();
            layoutBindings.shrink_to_fit();
        }

        m_renderpassPipeline.pushConstantRanges.clear();
        m_renderpassPipeline.pushConstantRanges.shrink_to_fit();
        for (auto& info : m_shaderPushConstantInfos)
        {
            m_renderpassPipeline.pushConstantRanges.push_back(info.pushConstantRange);
        }

        VkPipelineLayoutCreateInfo pipelineLayoutInfo = StructInitializer::getPipelineLayoutCreateInfo(m_renderpassPipeline.descriptorSetLayouts, m_renderpassPipeline.pushConstantRanges);

        VkResult result = vkCreatePipelineLayout(device, &pipelineLayoutInfo, nullptr, &m_renderpassPipeline.layout);
        CHECK_RESULT_FOR_ERRORS(result, "Failed to create a pipeline layout! \n");

        return result;
    }
}