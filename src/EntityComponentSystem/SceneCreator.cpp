#include "EntityComponentSystem/SceneCreator.h"
#include "GraphicsApi/Vulkan/VulkanUtility/UtilityFunctions.h"
#include "GraphicsApi/Vulkan/VulkanUtility/StructInitializer.h"

namespace Raven
{
    void SceneCreator::createSceneGraphFromAssets(EntityManager* entityManager, const std::vector<AssetGroup>& assets)
    {
        DelegateManager& manager = DelegateManager::getInstance();
        const auto& vulkanManagerDelegates = manager.getVulkanManagerDelegates();

        std::vector<AssetGraph> assetGraphs;
        for (auto& assetGroup : assets)
        {
            AssetGraph graph;

            //Create all the meshes.
            createMeshComponents(entityManager, vulkanManagerDelegates, assetGroup, graph);

            //Create all the materials:
            createMaterialComponents(entityManager, vulkanManagerDelegates, assetGroup, graph);

            //Create and initialize components.
            createAnimationComponents(entityManager, assetGroup, graph);

            //Create joints
            createSkeletonComponents(entityManager, assetGroup, graph);

            //Now that all entities, mesh and material components have been created by the engine and added to the AssetGraph,
            //it is time to assemble all the entities to use correct components.
            createEntitiesFromAssetGroup(entityManager, vulkanManagerDelegates, assetGroup, graph);

            assetGraphs.emplace_back(graph);
        }

    }

    void SceneCreator::createMeshComponents(EntityManager* entityManager, 
                                            const RavenVulkanManagerDelegates& vulkanManagerDelegates,  
                                            const AssetGroup& group, AssetGraph& graph)
    {
        for (auto& mesh : group.meshes)
        {
            MeshGeometry geometry;
            for (auto& primitive : mesh.primitives)
            {
                MeshPrimitive pair;
                Mesh* meshComponent = entityManager->createComponent<Mesh>();
                //Create the vertex buffer.
                vulkanManagerDelegates.createVertexBufferFunc.Invoke((void*)primitive.vertices.data(), sizeof(primitive.vertices[0]), primitive.vertices.size(), meshComponent->m_vertexBuffer);

                //Create the index buffer
                vulkanManagerDelegates.createIndexBufferFunc.Invoke((void*)primitive.indices.data(), sizeof(primitive.indices[0]), primitive.indices.size(), meshComponent->m_indexBuffer);

                size_t serialization = UtilityFunctions::hashVertexVertices(primitive.vertices);
                meshComponent->m_serialization = serialization;
                meshComponent->init(nullptr);

                pair.mesh = meshComponent;
                pair.materialIndex = primitive.material;

                geometry.primitives.push_back(pair);
            }
            graph.meshes.push_back(geometry);
        }
    }

    void SceneCreator::createMaterialComponents(EntityManager* entityManager, 
                                                const RavenVulkanManagerDelegates& vulkanManagerDelegates, 
                                                const AssetGroup& group, AssetGraph& graph)
    {
        for (auto& material : group.materials)
        {
            if (material.materialName.empty())
            {
                std::cerr << "MATERIAL HAS NO NAME!\n";
            }
            Material* newMaterial = entityManager->createComponent<Material>();
            //Remember to initialize all the values as memory initialization doesn't actually
            //initialize the object.
            newMaterial->init((void*)&material);
            vulkanManagerDelegates.createRavenMaterialFunc.Invoke(material, *newMaterial);
            uint32_t serialization = RavenUtility::generateHash(material.materialName.c_str());
            newMaterial->m_serialization = serialization;
            graph.materials.emplace_back(newMaterial);
        }
    }

    void SceneCreator::createAnimationComponents(EntityManager* entityManager,
                                                 const AssetGroup& group, AssetGraph& graph)
    {
        for (auto& anim : group.animations)
        {
            Animation* animation = entityManager->createComponent<Animation>();
            animation->init((void*)&anim);
            graph.animations.emplace_back(animation);
        }
    }

    void SceneCreator::createSkeletonComponents(EntityManager* entityManager, const AssetGroup& group, AssetGraph& graph)
    {
        for (auto& skeletonData : group.skeletons)
        {
            assert(skeletonData.jointNodes.size() <= MAX_JOINT_COUNT);
            Skeleton* skeleton = entityManager->createAndInitializeSkeleton();
            skeleton->init((void*)&skeletonData);
            graph.skeletons.emplace_back(skeleton);
        }
    }

    void SceneCreator::createEntitiesFromAssetGroup(EntityManager* entityManager,
                                                  const RavenVulkanManagerDelegates& vulkanManagerDelegates,
                                                  const AssetGroup& group, AssetGraph& graph)
    {
        Entity* groupRootEntity = entityManager->createEntity();
        TransformData rootData;
        rootData.scale = glm::vec3(1.0f);
        rootData.translation = glm::vec4(1.0f);
        rootData.rotation = glm::quat(glm::vec3(glm::radians(0.0f), glm::radians(0.0f), glm::radians(0.0f)));
        createTransformComponent(entityManager, rootData, groupRootEntity);

        TagData groupTagData;
        groupTagData.name = "Asset root";
        Tag* groupTag = entityManager->createComponent<Tag>();
        groupTag->init((void*)&groupTagData);
        groupRootEntity->addComponent<Tag>(groupTag);

        std::vector<EntityId> meshEntities;
        size_t nodeIndex = 0;
        for (auto& node : group.nodes)
        {
            //Currently not supporting camera or skeleton nodes as entities.
            if (node.mesh > -1)
            {
                Entity* meshEntity = createMeshEntity(entityManager, node, graph);
                if (!node.animations.empty())
                {
                    //TODO: ADD SUPPORT FOR MULTIPLE ANIMATIONS IN A SINGLE ENTITY.
                    //Currently an entity can have only a single animation.
                    int animationIndex = node.animations[0];
                    if (animationIndex < graph.animations.size())
                    {
                        Animation* animation = graph.animations[animationIndex];
                        meshEntity->addComponent<Animation>(animation);
                    }
                }
                if (node.skeleton > -1)
                {
                    //Todo: This feels a bit stupid, maybe assign skeleton directly to the animation instead of 
                    //assigning it to mesh?
                    Renderable* renderable = meshEntity->getComponent<Renderable>();
                    if (renderable)
                    {
                        Mesh* mesh = renderable->m_mesh;
                        if (mesh)
                        {
                            //FIXME: DOES NOT MAP PERFECTLY BUT USING FOR DEBUG!
                            mesh->skeleton = graph.skeletons[node.skeleton];
                            int test = 0;
                        }
                    }
                }
                entityManager->notifySystems(meshEntity);
                meshEntities.push_back(meshEntity->getId());
            }
            nodeIndex++;
        }

        entityManager->setChildrenForEntity(groupRootEntity->getId(), meshEntities);
        entityManager->notifySystems(groupRootEntity);
    }

    void SceneCreator::createTransformComponent(EntityManager* manager, const TransformData& data, Entity* entity)
    {
        Transform* transform = manager->createComponent<Transform>();
        transform->init((void*)&data);
        entity->addComponent<Transform>(transform);
    }

    Entity* SceneCreator::createMeshEntity(EntityManager* entityManager, const NodeData& node, const AssetGraph& graph)
    {
        Entity* entity = entityManager->createEntity();
        createTransformComponent(entityManager, node.transformData, entity);

        TagData rootNodeTagData;
        rootNodeTagData.name = "Mesh root node";
        if (!node.name.empty())
        {
            rootNodeTagData.name = "Mesh root node, name: " + node.name;
        }
        Tag* tag = entityManager->createComponent<Tag>();
        tag->init((void*)&rootNodeTagData);
        entity->addComponent<Tag>(tag);

        auto& meshComponents = graph.meshes[node.mesh];

        //If node has only one mesh primitive, add it directly.
        //If it has many, create separate entities and add them as children.
        if (meshComponents.primitives.size() == 1)
        {
            MeshPrimitive prim = meshComponents.primitives[0];
            Material* material = nullptr;
            if ((prim.materialIndex > -1) && (prim.materialIndex < graph.materials.size()))
            {
                material = graph.materials[prim.materialIndex];
            }
            Renderable* renderable = entityManager->createAndInitializeRenderable(prim.mesh, material);
            entity->addComponent<Renderable>(renderable);
        }
        else
        {
            std::vector<EntityId> children;
            for (auto& mesh : meshComponents.primitives)
            {
                Entity* meshEntity = entityManager->createEntity();
                Material* material = nullptr;
                if ((mesh.materialIndex > -1) && (mesh.materialIndex < graph.materials.size()))
                {
                    material = graph.materials[mesh.materialIndex];
                }
                Renderable* renderable = entityManager->createAndInitializeRenderable(mesh.mesh, material);
                meshEntity->addComponent<Renderable>(renderable);
                createTransformComponent(entityManager, node.transformData, meshEntity);

                children.push_back(meshEntity->getId());
                entityManager->notifySystems(meshEntity);
            }
            entityManager->setChildrenForEntity(entity->getId(), children);
        }
        entityManager->notifySystems(entity);
        return entity;
    }
}