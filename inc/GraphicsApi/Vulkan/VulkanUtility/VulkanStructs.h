#ifndef RAVEN_VULKAN_STRUCTS_H_INCLUDED
#define RAVEN_VULKAN_STRUCTS_H_INCLUDED

#include "GraphicsApi/Vulkan/Common.h"
#include "Utility/GeneralUtility.h"

#include <string>
#include <vector>
#include <array>
#include <unordered_map>
#include <functional>
namespace Raven
{
    #define MAX_JOINT_COUNT 256

    struct Joint
    {
        int jointNodeIndex;
        char jointName[64];
        //This is in model/local space. This data should be uploaded to the 
        //shader in a uniform array and it will tell the vertices where they should
        //currently be located.
        glm::mat4 jointLocalTransform;
    };

    struct VulkanQueueInfo
    {
        uint32_t queueFamilyIndex;
        //Family priorities. The number of queues in this family is priorities.size().
        std::vector<float> priorities;
    };

    struct SwapchainSupportInfo
    {
        VkSurfaceCapabilitiesKHR capabilities;
        std::vector<VkSurfaceFormatKHR> formats;
        std::vector<VkPresentModeKHR> presentModes;
    };

    struct Vertex
    {
        glm::vec4 pos;
        glm::vec3 color;
        glm::vec3 normal;
        glm::vec2 texCoord1;
        glm::vec2 texCoord2;
        glm::vec4 joint0;
        glm::vec4 weight0;

        static VkVertexInputBindingDescription getBindingDescription()
        {
            VkVertexInputBindingDescription bindingDescription = {};
            bindingDescription.binding = 0;
            bindingDescription.stride = sizeof(Vertex);
            bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
            return bindingDescription;
        }

        //How to extract vertex attributes from a vertex data chunk.
        //Currently there is position and color so two descriptions are needed.
        static std::array<VkVertexInputAttributeDescription, 7> getAttributeDescriptions()
        {
            std::array<VkVertexInputAttributeDescription, 7> attributeDescriptions = {};
            attributeDescriptions[0].binding = 0;
            attributeDescriptions[0].location = 0;
            attributeDescriptions[0].format = VK_FORMAT_R32G32B32A32_SFLOAT;
            attributeDescriptions[0].offset = offsetof(Vertex,pos);

            attributeDescriptions[1].binding = 0;
            attributeDescriptions[1].location = 1;
            attributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
            attributeDescriptions[1].offset = offsetof(Vertex,color);

            attributeDescriptions[2].binding = 0;
            attributeDescriptions[2].location = 2;
            attributeDescriptions[2].format = VK_FORMAT_R32G32B32_SFLOAT;
            attributeDescriptions[2].offset = offsetof(Vertex, normal);

            attributeDescriptions[3].binding = 0;
            attributeDescriptions[3].location = 3;
            attributeDescriptions[3].format = VK_FORMAT_R32G32_SFLOAT;
            attributeDescriptions[3].offset = offsetof(Vertex, texCoord1);

            attributeDescriptions[4].binding = 0;
            attributeDescriptions[4].location = 4;
            attributeDescriptions[4].format = VK_FORMAT_R32G32_SFLOAT;
            attributeDescriptions[4].offset = offsetof(Vertex, texCoord2);

            attributeDescriptions[5].binding = 0;
            attributeDescriptions[5].location = 5;
            attributeDescriptions[5].format = VK_FORMAT_R32G32B32A32_SFLOAT;
            attributeDescriptions[5].offset = offsetof(Vertex, joint0);

            attributeDescriptions[6].binding = 0;
            attributeDescriptions[6].location = 6;
            attributeDescriptions[6].format = VK_FORMAT_R32G32B32A32_SFLOAT;
            attributeDescriptions[6].offset = offsetof(Vertex, weight0);

            return attributeDescriptions;
        }

        //Custom comparator for Vertex to remove duplicates.
        bool operator==(const Vertex& other) const {
            return pos == other.pos && color == other.color && normal == other.normal && texCoord1 == other.texCoord1;
        }
    };

    //A hash function for Vertex.
    //https://en.cppreference.com/w/cpp/utility/hash
    //and
    //https://vulkan-tutorial.com/Loading_models
    struct VertexHash
    {
        std::size_t operator()(Vertex const& vertex) const noexcept
        {
            std::size_t h1 = std::hash<glm::vec4>{}(vertex.pos);
            std::size_t h2 = std::hash<glm::vec3>{}(vertex.color);
            std::size_t h3 = std::hash<glm::vec3>{}(vertex.normal);
            std::size_t h4 = std::hash<glm::vec2>{}(vertex.texCoord1);
            
            std::size_t combinedHash = 0;
            hash_combine(combinedHash, h1, h2, h3, h4);
            return combinedHash;
        }
    };

    struct UniformBufferObject
    {
        glm::mat4 model;
        glm::mat4 view;
        glm::mat4 proj;
    };

    struct LightUniformBufferObject
    {
        glm::vec4 position;
        glm::vec3 color;
        float radius;
    };

    //Information passed to the shader for animation.
    struct SkeletonUniform
    {
        glm::mat4 matrix;
        glm::mat4 jointMatrices[MAX_JOINT_COUNT];
        float jointCount;
    };

    //ShaderLoadInformation and ShaderStageData should be used in conjunction.
    struct ShaderLoadInformation
    {
        std::string filepath;
        VkShaderStageFlagBits shaderStage;
    };

    struct ShaderStageData
    {
        std::vector<uint32_t> spirv;
        VkShaderStageFlagBits stage;
        VkShaderModule module;
        VkPipelineShaderStageCreateInfo createInfo;
    };

    //Used by whoever creates descriptor sets.
    struct ShaderDescriptorBindingInfo
    {
        VkDescriptorSetLayoutBinding layoutBinding;
        std::string primaryName;
        std::string secondaryName;
    };

    //Used to match resources to the descriptor sets 
    //defined by ShaderDescriptorBindingInfo.
    struct ShaderDescriptorMatchInfo
    {
        std::string name;
        VkDescriptorType type;
    };

    struct ShaderPushConstantInfo
    {
        std::string name;
        std::string fallbackName;
        VkPushConstantRange pushConstantRange;
    };

    struct PushConstantData
    {
        void* data;
        uint32_t dataSize;
        uint32_t dataOffset;
    };

    struct VulkanExtensionFunctionPointers
    {
        //Function pointers for loading and storing extension functions.
        //Currently Vulkan 1.2 timeline semaphores don't seem to work 
        //correctly so using 1.1 with extensions instead. This is 
        //not the optimal way but will have to do for now.
        PFN_vkGetDeviceProcAddr pfnGetDeviceProcAddr;
        PFN_vkWaitSemaphoresKHR pfnWaitSemaphoresKHR;
        PFN_vkSignalSemaphoreKHR pfnSignalSemaphoreKHR;
        PFN_vkGetSemaphoreCounterValueKHR pfnGetSemaphoreCounterValueKHR;
#ifdef RAVEN_DEBUG
        PFN_vkDebugMarkerSetObjectNameEXT pfnDebugMarkerSetObjectNameEXT;
        PFN_vkCreateDebugUtilsMessengerEXT pfnCreateDebugUtilsMessengexEXT;
#endif
    };

    struct RavenVulkanContext
    {
        VkInstance instance = VK_NULL_HANDLE;
        VkDevice device = VK_NULL_HANDLE;
        VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
        VkPhysicalDeviceFeatures2 extraDeviceFeatures;
        VkPhysicalDeviceTimelineSemaphoreFeatures timelineSemaphoreFeatures;
        VkDebugUtilsMessengerEXT debugMessenger;
        //TODO: RavenVulkanManager should probably not allocate the queues.
        std::vector<VkQueue> queues;
        uint32_t primaryQueueFamilyIndex;
        VulkanExtensionFunctionPointers funcPointers;
    };

    struct GraphicsPipelineDescriptorInfo
    {
        std::string descriptorName;
        uint32_t binding;
        uint32_t descriptorSetIndex;
        VkDescriptorType descriptorType;
    };

    //Pool and buffers that were allocated from the pool.
    //This struct exists because Vulkan currently doesn't 
    //allow recording to multiple command buffers from 
    //a single pool at once. So instead create a buffer 
    //per pool.
    struct PoolAndBuffers
    {
        VkCommandPool pool;
        std::vector<VkCommandBuffer> buffers;
    };
}

#endif