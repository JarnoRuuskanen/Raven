#ifndef RAVEN_APPLICATION_WINDOW_H_INCLUDED
#define RAVEN_APPLICATION_WINDOW_H_INCLUDED

#include "GraphicsApi/Vulkan/Common.h"
#include <vector>

namespace Raven
{
    struct WindowCreateInfo
    {
        uint32_t height;
        uint32_t width;
    };

    struct SurfaceCreateInfo
    {
        VkInstance instance;
        VkPhysicalDevice physicalDevice;
        uint32_t queueFamilyIndex;
        GLFWframebuffersizefun framebufferResizeCallback;
    };

    class ApplicationWindow
    {
    public:
        ApplicationWindow();
        ~ApplicationWindow();

        void initialize(const WindowCreateInfo &info);
        bool createWindowSurface(const SurfaceCreateInfo& info);
        GLFWwindow* getWindow();
        VkSurfaceKHR& getSurface();
        inline uint32_t getWidth() {return m_width;}
        inline uint32_t getHeight(){return m_height;}
    private:
        bool createWindow(const WindowCreateInfo& info);

        //Window stuff
        GLFWwindow* m_window = nullptr;
        VkSurfaceKHR m_windowSurface = VK_NULL_HANDLE;
        uint32_t m_width;
        uint32_t m_height;
    };
}
#endif