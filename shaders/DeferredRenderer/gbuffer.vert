//Reference with some changes: 
//https://github.com/SaschaWillems/Vulkan/blob/master/data/shaders/subpasses/gbuffer.vert

#version 450

layout(location = 0) in vec4 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in vec2 inTexCoord;

layout(binding = 0) uniform UniformBufferObject
{
    mat4 model;
    mat4 view;
    mat4 proj;
}ubo;

layout (location = 0) out vec3 outNormal;
layout (location = 1) out vec3 outColor;
layout (location = 2) out vec3 outWorldPos;
layout (location = 3) out vec2 outTexCoord;

out gl_PerVertex
{
	vec4 gl_Position;
};

void main() 
{
    gl_Position = ubo.proj * ubo.view * ubo.model * inPosition;

    //Position in world space.
    outWorldPos = vec3(ubo.model * inPosition);
    //Invert y axis because of OpenGL/Vulkan differences.
    //outWorldPos.y = -outWorldPos.y;

    //Normal in world space.
    mat3 mNormal = transpose(inverse(mat3(ubo.model)));
    outNormal = mNormal * normalize(inNormal);

    outColor = inColor;
    outTexCoord = inTexCoord;
}