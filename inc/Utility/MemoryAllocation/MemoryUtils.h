#ifndef RAVEN_MEMORY_UTILITY_H_INCLUDED
#define RAVEN_MEMORY_UTILITY_H_INCLUDED
#include <cstddef>

//Reference https://github.com/mtrebi/memory-allocators/blob/master/includes/Utils.h
namespace Raven
{
    namespace MemoryUtility
    {
        const std::size_t calculatePadding(const std::size_t baseAddress, const std::size_t alignment)
        {
            const std::size_t multiplier = (baseAddress / alignment) + 1;
            const std::size_t alignedAddress = multiplier * alignment;
            const std::size_t padding = alignedAddress - baseAddress;
            return padding;
        }

        const std::size_t calculatePaddingWithHeader(const std::size_t baseAddress, const std::size_t alignment, const std::size_t headerSize)
        {
            std::size_t padding = calculatePadding(baseAddress, alignment);
            std::size_t neededSpace = headerSize;

            //Check if header fits.
            if (padding < neededSpace)
            {
                neededSpace -= padding;

                if (neededSpace % alignment > 0)
                {
                    padding += alignment * (1 + (neededSpace / alignment));
                }
                else
                {
                    padding += alignment * (neededSpace / alignment);
                }
            }
            return padding;
        }
    }
}
#endif