#include "EntityComponentSystem/RavenSystems/LightSystem.h"
#include "GraphicsApi/Vulkan/VulkanUtility/StructInitializer.h"

#include "Utility/DelegateManager.h"
namespace Raven
{
    LightSystem::LightSystem(EntityManager* pGameWorld) : BaseType(pGameWorld)
    {
        m_vulkanResourceManagerDelegates = DelegateManager::getInstance().getVulkanResourceManagerDelegates();
        std::cerr << "Currently there can only be a single light, update this and change the size of the uniform buffer!\n";

        VkBufferCreateInfo uboCreateInfo = StructInitializer::getBufferCreateInfo({}, sizeof(m_uboLights), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);
        DelegateManager& manager = DelegateManager::getInstance();
        manager.getVulkanResourceManagerDelegates().createAllocatedBufferFunc.Invoke(uboCreateInfo, VMA_MEMORY_USAGE_CPU_TO_GPU, m_lightsUniformBuffer);

        //Register light system delegates.
        LightSystemDelegates lightSystemDelegates;
        lightSystemDelegates.getLightBufferFunc.Bind<LightSystem, &LightSystem::getLightBuffer>(this);
        manager.registerLightSystemDelegates(lightSystemDelegates);
    }

	void LightSystem::update(const Timer& timer)
	{
        //Update lights.
        size_t lightIndex = 0;
        assert(m_components.size() <= 2);
        for (auto& compTuple : m_components)
        {
            Light* light = std::get<Light*>(compTuple);
            Transform* transform = std::get<Transform*>(compTuple);
            //TODO: Add lazy evaluation so that these calculations 
            //are done at the last possible moment (= when value is used).
            m_uboLights.lights[lightIndex].position = glm::vec4(transform->m_position, 1.0f);
            m_uboLights.lights[lightIndex].color = light->lightColor;
            m_uboLights.lights[lightIndex].radius = light->lightIntensity;
            lightIndex++;
        }

        //Lastly update uniform buffer. This should be done only if changes occured.
        if (m_components.size() > 0)
        {
            m_uboLights.viewPosition = glm::vec4(m_viewTransform->m_position, 1.0f);
            void* data;
            m_vulkanResourceManagerDelegates.mapAllocatedMemoryFunc.Invoke(m_lightsUniformBuffer.memoryAllocation, &data);
            std::memcpy(data, &m_uboLights, sizeof(m_uboLights));
            m_vulkanResourceManagerDelegates.unmapAllocatedMemoryFunc.Invoke(m_lightsUniformBuffer.memoryAllocation);
        }
	}

    void LightSystem::entityAddedToSystem(Entity* entity)
    {
        //Allocate new descriptor set for the light entity.

    }
}
