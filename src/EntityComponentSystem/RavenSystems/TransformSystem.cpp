#include <EntityComponentSystem/RavenSystems/TransformSystem.h>
#include "EntityComponentSystem/EntityManager.h"
namespace Raven
{
    TransformSystem::TransformSystem(EntityManager* pGameWorld) : BaseType(pGameWorld)
    {

    }

    void TransformSystem::update(const Timer& timer)
    {
        for (auto& compTuple : m_components)
        {
            Transform* transform = std::get<Transform*>(compTuple);
            //TODO: Add lazy evaluation so that these calculations 
            //are done at the last possible moment (= when value is used).
            if (transform->dirty)
            {
                updateTransform(transform);
            }
        }
    }

    void TransformSystem::updateTransform(Transform*& transform)
    {
        glm::mat4 basisMatrix{ 1.0f };
        glm::mat4 rotation = glm::toMat4(transform->m_rotation);
        glm::mat4 translation = glm::translate(basisMatrix, transform->m_position);
        glm::mat4 scale = glm::scale(basisMatrix, transform->m_scale);
        //Apply scale, then rotation, then position.
        transform->m_localTransform = translation * rotation * scale;

        //Update children.
        EntityId id = transform->getEntityId();
        const Entity& entity = m_pGameWorld->getEntity(id);

        const EntityId parentId = entity.getParent();
        if (parentId != INVALID_ENTITY_ID)
        {
            const Entity& parent = m_pGameWorld->getEntity(parentId);
            //Update world matrix by multiplying it with parent's world matrix to get
            //this transform in the world space.
            Transform* parentTransform = parent.getComponent<Transform>();
            if (parentTransform)
            {
                transform->m_worldTransform = parentTransform->m_localTransform * transform->m_localTransform;
            }
        }

        //Now that we have updated the transform, check if it has children and set them to be updated.
        auto& children = entity.getChildren();
        if (!children.empty())
        {
            for (auto& child : children)
            {
                const Entity& childEntity = m_pGameWorld->getEntity(child);
                Transform* childTransform = childEntity.getComponent<Transform>();
                if (childTransform)
                {
                    childTransform->dirty = true;
                }
            }
        }
        transform->dirty = false;
    }

    void TransformSystem::entityAddedToSystem(Entity* entity)
    {

    }
}