#include "Utility/FileIO.h"
#include <fstream>

namespace Raven
{
    namespace FileIO
    {
        std::vector<char> readBinaryFile(std::string filename)
        {
            //Open the file.
            std::ifstream file(filename, std::ios::ate | std::ios::binary);
            if(!file.is_open())
            {
                throw std::runtime_error("Failed to open shader file.");
            }

            //Get the size of the file and resize the vector accordingly.
            size_t fileSize = (size_t) file.tellg();
            std::vector<char> fileContents(fileSize);

            //Go back to the beginning of the file and read all the bytes in the file at once.
            file.seekg(0);
            file.read(fileContents.data(), fileSize);

            //Close the file.
            file.close();
            return fileContents;
        }
    }
}