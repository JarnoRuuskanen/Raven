#include "GraphicsApi/Vulkan/VulkanUtility/StructInitializer.h"

namespace Raven
{
    namespace StructInitializer
    {
        VkPipelineShaderStageCreateInfo getShaderStageCreateInfo(VkShaderStageFlagBits shaderStage,
                                                                 const VkShaderModule &shaderModule)
        {
            VkPipelineShaderStageCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
            createInfo.pNext = nullptr;
            createInfo.flags = 0;
            createInfo.stage = shaderStage;
            createInfo.pName = "main";
            createInfo.module = shaderModule;

            //createInfo.pSpecializationInfo
            //"allows you to specify values for shader constants.
            //You can use a single shader module where its behavior can be configured at pipeline creation by
            //specifying different values for the constants used in it. This is more efficient than configuring the
            // shader using variables at render time, because the compiler can do optimizations like eliminating if
            // statements that depend on these values. If you don't have any constants like that, then you can set
            // the member to nullptr, which our struct initialization does automatically." - vulkan-tutorial.

            return createInfo;
        }

        VkBufferCreateInfo getBufferCreateInfo(const std::vector<uint32_t>& queueFamilyIndices, VkDeviceSize size,
                                               VkBufferUsageFlags usage, VkSharingMode sharingMode)
        {
            VkBufferCreateInfo info = {};
            info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = 0;
            info.queueFamilyIndexCount = static_cast<uint32_t>(queueFamilyIndices.size());
            //IGNORED IF SHARING MODE IS NOT VK_SHARING_MODE_CONCURRENT. YOU CAN SEND {} TO queueFamilyIndices.
            info.pQueueFamilyIndices = queueFamilyIndices.empty() ? nullptr : queueFamilyIndices.data();
            info.size = size;
            info.usage = usage;
            info.sharingMode = sharingMode;
            return info;
        };

        VkImageCreateInfo getImageCreateInfo(uint32_t width, uint32_t height, VkImageUsageFlags usage, VkFormat format)
        {
            VkImageCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
            createInfo.imageType = VK_IMAGE_TYPE_2D;
            createInfo.extent.width = width;
            createInfo.extent.height = height;
            createInfo.extent.depth = 1;
            createInfo.mipLevels = 1;
            createInfo.arrayLayers = 1;
            createInfo.format = format;
            createInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
            createInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
            createInfo.usage = usage;
            createInfo.samples= VK_SAMPLE_COUNT_1_BIT;
            createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

            return createInfo;
        }

        VkImageViewCreateInfo getImageViewCreateInfo(VkImage& image, VkFormat format, VkImageAspectFlags aspectFlags, VkImageViewType type)
        {
            VkImageViewCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
            createInfo.image = image;
            createInfo.viewType = type;
            createInfo.format = format;
            createInfo.subresourceRange.aspectMask = aspectFlags;
            createInfo.subresourceRange.baseMipLevel = 0;
            createInfo.subresourceRange.levelCount = 1;
            createInfo.subresourceRange.baseArrayLayer = 0;
            createInfo.subresourceRange.layerCount = 1;
            return createInfo;
        }

        VkSamplerCreateInfo getSamplerCreateInfo()
        {
            VkSamplerCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
            createInfo.pNext = nullptr;
            createInfo.flags = 0;
            createInfo.magFilter = VK_FILTER_LINEAR;
            createInfo.minFilter = VK_FILTER_LINEAR;
            createInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
            createInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
            createInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
            createInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
            createInfo.mipLodBias = 0.0f;
            createInfo.maxAnisotropy = 8;
            createInfo.compareOp = VK_COMPARE_OP_NEVER;
            createInfo.minLod = 0.0f;
            createInfo.maxLod = 1.0f;
            createInfo.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
            return createInfo;
        }

        VkWriteDescriptorSet getWriteDescriptorSet(VkDescriptorSet dstSet, VkDescriptorType type, uint32_t binding,
                                                   VkDescriptorBufferInfo* info, uint32_t count)
        {
            VkWriteDescriptorSet write = {};
            write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            write.dstSet = dstSet;
            write.descriptorType = type;
            write.dstBinding = binding;
            write.pBufferInfo = info;
            write.descriptorCount = count;

            return write;
        }

        VkWriteDescriptorSet getWriteDescriptorSet(VkDescriptorSet dstSet, VkDescriptorType type, uint32_t binding,
                                                   VkDescriptorImageInfo* info, uint32_t count)
        {
            VkWriteDescriptorSet write = {};
            write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            write.dstSet = dstSet;
            write.descriptorType = type;
            write.dstBinding = binding;
            write.pImageInfo = info;
            write.descriptorCount = count;

            return write;
        }

        VkPipelineVertexInputStateCreateInfo getPipelineVertexInputStateCreateInfo(const std::vector<VkVertexInputBindingDescription>& bindings,
                                                                                   const std::vector<VkVertexInputAttributeDescription>& attributes)
        {
            VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
            vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
            vertexInputInfo.vertexBindingDescriptionCount = static_cast<uint32_t>(bindings.size());
            vertexInputInfo.pVertexBindingDescriptions = bindings.empty() ? nullptr : bindings.data();
            vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributes.size());
            vertexInputInfo.pVertexAttributeDescriptions = attributes.empty() ? nullptr : attributes.data();
            return vertexInputInfo;
        }

        VkPipelineInputAssemblyStateCreateInfo getPipelineInputAssemblyStateCreateInfo(VkPrimitiveTopology topology, VkBool32 primitiveRestartEnable)
        {
            VkPipelineInputAssemblyStateCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
            createInfo.pNext = nullptr;
            createInfo.topology = topology;
            createInfo.primitiveRestartEnable = primitiveRestartEnable;
            return createInfo;
        }

        VkPipelineViewportStateCreateInfo getPipelineViewportStateCreateInfo(const std::vector<VkViewport>& viewports, const std::vector<VkRect2D>& scissors)
        {
            VkPipelineViewportStateCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
            createInfo.pNext = nullptr;
            createInfo.viewportCount = static_cast<uint32_t>(viewports.size());
            createInfo.pViewports = viewports.empty() ? nullptr : viewports.data();
            createInfo.scissorCount = static_cast<uint32_t>(scissors.size());
            createInfo.pScissors = scissors.empty() ? nullptr : scissors.data();
            return createInfo;
        }

        VkPipelineRasterizationStateCreateInfo getPipelineRasterizationStateCreateInfo(VkBool32 depthClampEnable, VkBool32 rasterizerDiscardEnable, VkPolygonMode polygonMode,
                                                                                       float lineWidth, VkCullModeFlags cullMode, VkFrontFace frontFace, VkBool32 depthBiasEnable,
                                                                                       float depthBiasConstantFactor, float depthBiasClamp, float depthBiasSlopeFactor)
        {
            VkPipelineRasterizationStateCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
            createInfo.pNext = nullptr;
            createInfo.depthClampEnable = depthClampEnable; //Enabling this requires also enabling a gpu feature.
            createInfo.rasterizerDiscardEnable = rasterizerDiscardEnable;
            createInfo.polygonMode = polygonMode;
            createInfo.lineWidth = lineWidth;
            createInfo.cullMode = cullMode;
            createInfo.frontFace = frontFace;
            createInfo.depthBiasEnable = depthBiasEnable;
            createInfo.depthBiasConstantFactor = depthBiasConstantFactor;
            createInfo.depthBiasClamp = depthBiasClamp;
            createInfo.depthBiasSlopeFactor = depthBiasSlopeFactor;
            return createInfo;
        }

        VkPipelineDepthStencilStateCreateInfo getPipelineDepthStencilStateCreateInfo(VkBool32 depthTestEnable, VkBool32 depthWriteEnable,
                                                                                     VkCompareOp depthCompareOp, VkBool32 depthBoundsTestEnable,
                                                                                     float minDepthBounds, float maxDepthBounds,
                                                                                     VkBool32 stencilTestEnable, const VkStencilOpState& front,
                                                                                     const VkStencilOpState& back)
        {
            VkPipelineDepthStencilStateCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
            createInfo.pNext = nullptr;
            createInfo.depthTestEnable = depthTestEnable;
            createInfo.depthWriteEnable = depthWriteEnable;
            createInfo.depthCompareOp = depthCompareOp;
            createInfo.depthBoundsTestEnable = depthBoundsTestEnable;
            createInfo.minDepthBounds = minDepthBounds;
            createInfo.maxDepthBounds = maxDepthBounds;
            createInfo.stencilTestEnable = stencilTestEnable;
            createInfo.front = front;
            createInfo.back = back;
            return createInfo;
        }

        VkPipelineMultisampleStateCreateInfo getPipelineMultisampleStateCreateInfo(VkBool32 sampleShadingEnable, VkSampleCountFlagBits rasterizationSamples,
                                                                                   float minSampleShading, const VkSampleMask* sampleMask,
                                                                                   VkBool32 alphaToCoverageEnable, VkBool32 alphaToOneEnable)
        {
            VkPipelineMultisampleStateCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
            createInfo.pNext = nullptr;
            createInfo.sampleShadingEnable = sampleShadingEnable;
            createInfo.rasterizationSamples = rasterizationSamples;
            createInfo.minSampleShading = minSampleShading;
            createInfo.pSampleMask = sampleMask;
            createInfo.alphaToCoverageEnable = alphaToCoverageEnable;
            createInfo.alphaToOneEnable = alphaToOneEnable;
            return createInfo;
        }

        VkPipelineColorBlendStateCreateInfo getPipelineColorBlendStateCreateInfo(VkBool32 logicOpEnable, VkLogicOp logicOp,
                                                                                 const std::vector<VkPipelineColorBlendAttachmentState>& attachments)
        {
            VkPipelineColorBlendStateCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
            createInfo.pNext = nullptr;
            createInfo.logicOpEnable = logicOpEnable;
            createInfo.logicOp = logicOp;
            createInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
            createInfo.pAttachments = attachments.empty() ? nullptr : attachments.data();
            return createInfo;
        }

        VkPipelineDynamicStateCreateInfo getPipelineDynamicStateCreateInfo(const std::vector<VkDynamicState>& dynamicStates)
        {
            VkPipelineDynamicStateCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
            createInfo.pNext = nullptr;
            createInfo.dynamicStateCount = static_cast<uint32_t>(dynamicStates.size());
            createInfo.pDynamicStates = dynamicStates.empty() ? nullptr : dynamicStates.data();
            return createInfo;
        }

        VkPipelineLayoutCreateInfo getPipelineLayoutCreateInfo(const std::vector<VkDescriptorSetLayout>& descriptorSetLayouts,
                                                               const std::vector<VkPushConstantRange>& pushConstantRanges)
        {
            VkPipelineLayoutCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
            createInfo.pNext = nullptr;
            createInfo.flags = 0;
            createInfo.setLayoutCount = static_cast<uint32_t>(descriptorSetLayouts.size());
            createInfo.pSetLayouts = descriptorSetLayouts.empty() ? nullptr : descriptorSetLayouts.data();
            createInfo.pushConstantRangeCount = static_cast<uint32_t>(pushConstantRanges.size());
            createInfo.pPushConstantRanges = pushConstantRanges.empty() ? nullptr : pushConstantRanges.data();
            return createInfo;
        }

        VkVertexInputBindingDescription getVertexInputBindingDescription(uint32_t binding, uint32_t stride, VkVertexInputRate inputRate)
        {
            VkVertexInputBindingDescription description = {};
            description.binding = binding;
            description.stride = stride;
            description.inputRate = inputRate;
            return description;
        }

        VkVertexInputAttributeDescription getVertexInputAttributeDescription(uint32_t binding, uint32_t location,
                                                                             VkFormat format, uint32_t offset)
        {
            VkVertexInputAttributeDescription description = {};
            description.binding = binding;
            description.location = location;
            description.format = format;
            description.offset = offset;
            return description;
        }
    }
}

