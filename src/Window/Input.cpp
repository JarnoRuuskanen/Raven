#include "Window/Input.h"

namespace Raven
{
    void cursor_position_callback_func(GLFWwindow* window, double xpos, double ypos);
    void key_callback_func(GLFWwindow* window, int key, int scancode, int action, int mods);
    void mouse_button_callback_func(GLFWwindow* window, int button, int action, int mods);
    void character_callback(GLFWwindow* window, unsigned int glfwCodePoint);

    Delegate<void(int button, int action, int mods)> mouseKeyDelegate;
    Delegate<void(float xpos, float ypos)> mouseMovedDelegate;
    Delegate<void(int key, int scancode, int action, int mods)> keyboardDelegate;
    Delegate<void(unsigned int codepoint)> characterDelegate;

    void Input::initialize(GLFWwindow* window)
    {
        if(m_initialized)
        {
            return;
        }

        assert(window);
        m_window = window;

        glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

        //Set callbacks
        glfwSetCursorPosCallback(m_window, cursor_position_callback_func);
        glfwSetMouseButtonCallback(m_window, mouse_button_callback_func);
        glfwSetKeyCallback(m_window, key_callback_func);
        glfwSetCharCallback(m_window, character_callback);

        glfwSetWindowUserPointer(m_window, this);

        //Initialize all the delegates.
        mouseKeyDelegate.Bind<Input, &Input::handleMouseKeyCallbacks>(this);
        mouseMovedDelegate.Bind<Input, &Input::handleMouseMovementCallbacks>(this);
        keyboardDelegate.Bind<Input, &Input::handleKeyboardKeyCallbacks>(this);
        characterDelegate.Bind<Input, & Input::handleCharacterCallbacks>(this);

        m_initialized = true;
    }

    void Input::handleMouseKeyCallbacks(int button, int action, int mods)
    {
        MouseKeyEvent event;
        event.mouseKey = button;
        if(action == GLFW_PRESS || action == GLFW_REPEAT)
        {
            event.state = MouseKeyState::PRESS;
        }
        else
        {
            event.state = MouseKeyState::RELEASE;
        }
        event.glfwModifierKeys = mods;

        for(const auto& callback : m_mouseKeyCallbacks)
        {
            bool shouldContinue = callback.Invoke(event);
            if(!shouldContinue)
            {
                return;
            }
        }
    }

    void Input::handleMouseMovementCallbacks(float xpos, float ypos)
    {
        for(const auto& callbacks : m_mouseMovementCallbacks)
        {
            bool shouldContinue = callbacks.Invoke(xpos, ypos);
            if(!shouldContinue)
            {
                return;
            }
        }
    }

    unsigned int Input::getMatchingKey(int glfwKey)
    {
#define COMPARE_KEYS(key, output)                   \
    case GLFW_##key:                                \
        output = (unsigned int)KeyboardKey::key;    \
        break;

        int correctKey = (unsigned int)KeyboardKey::LAST;
        switch(glfwKey)
        {
            COMPARE_KEYS(KEY_A, correctKey);
            COMPARE_KEYS(KEY_W, correctKey);
            COMPARE_KEYS(KEY_S, correctKey);
            COMPARE_KEYS(KEY_D, correctKey);
            COMPARE_KEYS(KEY_SPACE, correctKey);
            COMPARE_KEYS(KEY_LEFT_CONTROL, correctKey);
            COMPARE_KEYS(KEY_LEFT_ALT, correctKey);
            COMPARE_KEYS(KEY_ESCAPE, correctKey);
            COMPARE_KEYS(KEY_LEFT_SHIFT, correctKey);
            COMPARE_KEYS(KEY_TAB, correctKey);
            COMPARE_KEYS(KEY_BACKSPACE, correctKey);
            default:
                break;
        }
        return correctKey;
    }

    void Input::handleKeyboardKeyCallbacks(int key, int scancode, int action, int mods)
    {
        bool keyDown = action == GLFW_PRESS || action == GLFW_REPEAT;
        unsigned int matchingKey = getMatchingKey(key);

        //Handle event callbacks.
        KeyEvent event;
        event.ravenKey = matchingKey;
        if(action == GLFW_PRESS || action == GLFW_REPEAT)
        {
            event.state = KeyboardKeyState::PRESS;
        }
        else
        {
            event.state = KeyboardKeyState::RELEASE;
        }
        event.glfwModifierKeys = mods;

        auto it = m_keyboardKeyEventCallbacks.find(event);
        if(it != m_keyboardKeyEventCallbacks.end())
        {
            for(const auto& callbacks : it->second)
            {
                bool shouldContinue = callbacks.Invoke(event);
                if(!shouldContinue)
                {
                    return;
                }
            }
        }

        for(auto& keyCallback : m_keyboardKeyCallbacks)
        {
            bool shouldContinue = keyCallback.Invoke(key, scancode, action, mods);
            if(!shouldContinue)
            {
                return;
            }
        }

        //Update key states last.
        if(matchingKey != (unsigned int)KeyboardKey::LAST)
        {
            m_keyboardKeyStates[matchingKey] = keyDown;
        }
        else
        {
            std::cout << "No matching key!\n";
        }
    }

    void Input::handleCharacterCallbacks(unsigned int glfwCodePoint)
    {
        //Handle general character callbacks. 
        //TODO: For now this is used only for imgui and imgui 
        //wants glfw raw inputs. Creating a special case KeyEvent, 
        //which will be using glfw key instead.
        for (auto& characterCallback : m_characterCallbacks)
        {
            bool shouldContinue = characterCallback.Invoke(glfwCodePoint);
            if (!shouldContinue)
            {
                return;
            }
        }
    }

    //Functions triggered by glfw. These should call the Input class functions which will in
    //turn handle the keys/events from there.
    void key_callback_func(GLFWwindow* window, int key, int scancode, int action, int mods)
    {
        //Go through all the registered listeners and send them information about the key state(s).
        keyboardDelegate.Invoke(key, scancode, action, mods);
    }

    void cursor_position_callback_func(GLFWwindow* window, double xpos, double ypos)
    {
        //Go through all the registered listeners and send them information about the mouse state.
        mouseMovedDelegate.Invoke(static_cast<float>(xpos), static_cast<float>(ypos));
    }

    void mouse_button_callback_func(GLFWwindow* window, int button, int action, int mods)
    {
        mouseKeyDelegate.Invoke(button, action, mods);
    }

    void character_callback(GLFWwindow* window, unsigned int codepoint)
    {
        characterDelegate.Invoke(codepoint);
    }
}