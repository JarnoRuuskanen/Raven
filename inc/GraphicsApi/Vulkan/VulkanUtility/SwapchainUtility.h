#ifndef RAVEN_SWAPCHAIN_UTILITY_H_INCLUDED
#define RAVEN_SWAPCHAIN_UTILITY_H_INCLUDED

#include "GraphicsApi/Vulkan/VulkanObjects/Swapchain.h"
#include "GraphicsApi/Vulkan/Common.h"
#include "VulkanStructs.h"
#include "StructInitializer.h"
#include "ImageUtility.h"

namespace Raven
{
    namespace SwapchainUtility
    {
        bool getDefaultSwapchain(const VkPhysicalDevice& physicalDevice, const VkDevice& device,
                                 const VkSurfaceKHR& surface, GLFWwindow* appWindow, Swapchain& swapchain);

        bool createSwapchain(const VkDevice& device, const SwapchainSupportInfo& swapchainInfo, const VkSurfaceKHR& surface,
                             VkPresentModeKHR selectedPresentMode, VkExtent2D swapchainExtent, VkSurfaceFormatKHR surfaceFormat,
                             uint32_t imageCount, VkSwapchainKHR& swapchain);
        bool getSwapchainImages(const VkDevice& device, const VkSwapchainKHR& swapchain, std::vector<VkImage>& images);

        bool createSwapchainImageViews(const VkDevice& device, VkFormat imageFormat, std::vector<VkImage>& images, std::vector<VkImageView>& imageViews);
    }
}

#endif