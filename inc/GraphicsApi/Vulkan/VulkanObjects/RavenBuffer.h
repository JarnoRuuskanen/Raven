#ifndef RAVEN_BUFFER_H_INCLUDED
#define RAVEN_BUFFER_H_INCLUDED

#include "GraphicsApi/Vulkan/Common.h"
#include "GraphicsApi/Vulkan/ResourceManager/vk_mem_alloc.h"

namespace Raven
{
    struct RavenBuffer
    {
        //How many objects are there in the buffer.
        uint32_t count;
        VkBuffer buffer;
        VkBufferView bufferView;
        //Allocation is an object that represents memory assigned to this buffer.
        //It can be queried for parameters like Vulkan memory handle and offset.
        VmaAllocation memoryAllocation;
        //Pointer to the beginning of data. Used when mapping host visible data.
        void* mapped = nullptr;
    };
}

#endif