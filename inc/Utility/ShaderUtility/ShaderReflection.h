#ifndef RAVEN_SHADER_REFLECTION_H_INCLUDED
#define RAVEN_SHADER_REFLECTION_H_INCLUDED
#include <spirv_glsl.hpp>
#include <vector>
#include <utility>
#include "GraphicsApi/Vulkan/Common.h"

//Reference for spirv stuff was taken from:
//https://github.com/KhronosGroup/SPIRV-Cross and
//https://github.com/KhronosGroup/SPIRV-Cross/wiki/Reflection-API-user-guide
namespace Raven
{
    namespace ShaderUtility
    {
        struct StageIOReflection
        {
            uint32_t location;
            uint32_t vecsize;
            uint32_t width;
            uint32_t columns;
            std::string name;
        };

        struct BufferMemberReflection
        {
            uint32_t vecsize;
            uint32_t arrayByteStride;
            //Over one if array.
            uint32_t arrayComponentCount;
            //Over 1 if member is a matrix.
            uint32_t columns;
            uint32_t matrixByteStride;
            uint32_t size;
            std::string name;
        };

        struct BufferReflection
        {
            uint32_t set;
            uint32_t binding;
            std::string name;
            std::string fallbackName;
            std::vector<BufferMemberReflection> members;
        };

        struct CombinedImageSamplerReflection
        {
            uint32_t binding;
            uint32_t set;
            std::string name;
        };

        struct PushConstantMemberRange
        {
            int index;
            int offset;
            int range;
        };

        struct PushConstantDescriptor
        {
            std::string name;
            std::string fallbackName;
            uint32_t offset;
            uint32_t size;
            //Each ACTIVE(=used inside the shader) push constant member inside a push constant block.
            std::vector<PushConstantMemberRange> pushConstantMembers;
        };

        struct InputAttachmentDescriptor
        {
            uint32_t set;
            uint32_t binding;
            std::string name;
            std::string fallbackName;
        };

        struct Descriptor
        {
            uint32_t binding;
            uint32_t set;
            VkDescriptorType descriptorType;
            std::string name;
            std::string secondaryName;
        };

        struct VertexAttributeDescriptor
        {

        };

        struct VertexBindingDescriptor
        {

        };

        class ShaderReflection
        {
        public:
            ShaderReflection(const uint32_t* spirv, size_t codeLength);
            ~ShaderReflection();

            const std::vector<VertexAttributeDescriptor>& getVertexAttributeDescriptors() const { assert(!"Not yet implemented");  return m_attributeDescriptors; }
            const std::vector<VertexBindingDescriptor>& getVertexBindingDescriptors() const { assert(!"Not yet implemented"); return m_bindingDescriptors; }
            const std::vector<Descriptor>& getShaderStageDescriptors() const { return m_stageDescriptors; }
            const std::vector<PushConstantDescriptor>& getShaderStagePushConstantDescriptors() const { return m_pushConstantDescriptor; }
            void findStageIOData(const spirv_cross::CompilerGLSL& glsl, const spirv_cross::SmallVector<spirv_cross::Resource>& resources, std::vector<StageIOReflection>& target);
            void findCombinedImageSamplers(const spirv_cross::CompilerGLSL& glsl, const spirv_cross::SmallVector<spirv_cross::Resource>& resources, std::vector<CombinedImageSamplerReflection>& target);
            void findUniformBuffers(const spirv_cross::CompilerGLSL& glsl, const spirv_cross::SmallVector<spirv_cross::Resource>& resources, std::vector<BufferReflection>& target);
            void findPushConstants(const spirv_cross::CompilerGLSL& glsl, const spirv_cross::SmallVector<spirv_cross::Resource>& resources, std::vector<PushConstantDescriptor>& target);
            void findInputAttachments(const spirv_cross::CompilerGLSL& glsl, const spirv_cross::SmallVector<spirv_cross::Resource>& resources, std::vector<InputAttachmentDescriptor>& target);

            void generateVertexAttributeDescriptors();
            void generateVertexBindingDescriptors();
            void generateShaderStageDescriptors();

            //Corresponds to VkVertexInputAttributeDescription concept.
            std::vector<StageIOReflection> m_inputs;
            std::vector<StageIOReflection> m_outputs;
            //Sampler2D
            std::vector<CombinedImageSamplerReflection> m_combinedImageSamplers;
            std::vector<BufferReflection> m_uniformBuffers;

            VkShaderStageFlags m_shaderStage;

            //Data that is required for graphics pipeline creation.
            std::vector<VertexAttributeDescriptor> m_attributeDescriptors;
            std::vector<VertexBindingDescriptor> m_bindingDescriptors;
            std::vector<Descriptor> m_stageDescriptors;
            std::vector<PushConstantDescriptor> m_pushConstantDescriptor;
            std::vector<InputAttachmentDescriptor> m_subpassInputAttachments;
        };
    }
}

#endif