#ifndef RAVEN_DELEGATE_MANAGER_H_INCLUDED
#define RAVEN_DELEGATE_MANAGER_H_INCLUDED

#include "Delegate.h"
#include "DelegateTypes.h"

namespace Raven
{
    //Meant to be a singleton class. Keeps a record of all the function pointers
    //for different systems to avoid decoupling. Systems can record their functions
    //to the delegate manager and other classes can then use said functions without
    //the need to include the said class header. Should also make it easier to avoid
    //friend classes etc.
    class DelegateManager
    {
    public:
        static DelegateManager& getInstance()
        {
            static DelegateManager manager;
            return manager;
        }

        //TODO: This feels a bit cumbersome. Perhaps use templates?
        void registerRendererDelegates(const RendererDelegates& delegates);
        void registerSystemDelegates(const WindowDelegates& delegates);
        void registerVulkanManagerDelegates(const RavenVulkanManagerDelegates& delegates);
        void registerVulkanResourceManagerDelegates(const VulkanResourceManagerDelegates& delegates);
        void registerVulkanContextDelegates(const VulkanContextDelegates& delegates);
        void registerEntityManagerDelegates(const EntityManagerDelegates& delegates);
        void registerLightSystemDelegates(const LightSystemDelegates& delegates);

        const RendererDelegates& getRendererDelegates() const;
        const WindowDelegates& getWindowDelegates() const;
        const RavenVulkanManagerDelegates& getVulkanManagerDelegates() const;
        const VulkanResourceManagerDelegates& getVulkanResourceManagerDelegates() const;
        const VulkanContextDelegates& getVulkanContextDelegates() const;
        const EntityManagerDelegates& getEntityManagerDelegates() const;
        const LightSystemDelegates& getLightSystemDelegates() const;

    private:
        DelegateManager() : m_rendererDelegatesInitialized(false), m_systemDelegatesInitialized(false),
                            m_vulkanManagerDelegatesInitialized(false), m_vulkanResourceManagerDelegatesInitialized(false),
                            m_vulkanContextDelegatesInitialized(false), m_entityManagerDelegatesInitialized(false),
                            m_lightSystemDelegatesInitialized(false){}

        RendererDelegates m_rendererDelegates;
        WindowDelegates m_systemDelegates;
        RavenVulkanManagerDelegates m_ravenVulkanManagerDelegates;
        VulkanResourceManagerDelegates m_vulkanResourceManagerDelegates;
        VulkanContextDelegates m_vulkanContextDelegates;
        EntityManagerDelegates m_entityManagerDelegates;
        LightSystemDelegates m_lightSystemDelegates;

        bool m_rendererDelegatesInitialized;
        bool m_systemDelegatesInitialized;
        bool m_vulkanManagerDelegatesInitialized;
        bool m_vulkanResourceManagerDelegatesInitialized;
        bool m_vulkanContextDelegatesInitialized;
        bool m_entityManagerDelegatesInitialized;
        bool m_lightSystemDelegatesInitialized;

    public:
        DelegateManager(DelegateManager const&) = delete;
        void operator=(DelegateManager const&) = delete;
    };
}

#endif