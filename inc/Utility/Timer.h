#ifndef RAVEN_TIMER_H_INCLUDED
#define RAVEN_TIMER_H_INCLUDED

namespace Raven
{
    struct Timer
    {
        double time;
        double deltaTime;
    };
}
#endif