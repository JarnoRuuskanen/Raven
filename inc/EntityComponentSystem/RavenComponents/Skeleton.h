#ifndef RAVEN_SKELETON_COMPONENT_H_INCLUDED
#define RAVEN_SKELETON_COMPONENT_H_INCLUDED
#include "EntityComponentSystem/RavenComponents/Component.h"
namespace Raven
{
    class Skeleton : public Component
    {
    public:
        static constexpr ComponentId ID = RavenUtility::generateHash("Skeleton");
        Skeleton() :m_jointCount(-1) {}
        virtual bool init(void* data) override final
        {
            SkeletonData* skeletonData = static_cast<SkeletonData*>(data);
            m_jointCount = static_cast<int>(skeletonData->jointNodes.size());
            for (uint32_t i = 0; i < skeletonData->joints.size(); i++)
            {
                m_joints[i] = skeletonData->joints[i];
            }
            for (uint32_t i = 0; i < skeletonData->inverseBindMatrices.size(); i++)
            {
                m_inverseBindMatrices[i] = skeletonData->inverseBindMatrices[i];
            }
            return true;
        }
        std::array<Joint, MAX_JOINT_COUNT> m_joints;
        std::array<glm::mat4, MAX_JOINT_COUNT> m_inverseBindMatrices;
        SkeletonUniform m_skeletonUniform;
        int m_jointCount;
        //Used to pass joint matrices to shaders.
        RavenBuffer m_uniformBuffer;
        //Write uniform buffer to this descriptor set.
        VkDescriptorSet m_descriptorSet;
    };
}

#endif