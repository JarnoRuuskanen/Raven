#ifndef COMMON_HEADERS_H_INCLUDED
#define COMMON_HEADERS_H_INCLUDED

#define GLFW_INCLUDE_VULKAN
#include<GLFW/glfw3.h>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/hash.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

#include <iostream>

//There are a billion Vulkan checks in the program so might as well create a
//function for them.
#define CHECK_RESULT_FOR_ERRORS(r, message)     \
{                                               \
    VkResult res = (r);                         \
    if(res != VK_SUCCESS)                       \
    {                                           \
        std::cerr << message << "\n";           \
        assert(res == VK_SUCCESS);              \
        return res;                             \
    }                                           \
}

#endif