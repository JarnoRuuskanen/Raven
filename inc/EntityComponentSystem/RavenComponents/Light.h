#ifndef RAVEN_LIGHT_COMPONENT_H_INCLUDED
#define RAVEN_LIGHT_COMPONENT_H_INCLUDED

#include "Utility/StringHash.h"
#include "Component.h"

namespace Raven
{
    enum class LIGHT_TYPE
    {
        POINT,
        SUN
    };

    struct Light : public Component
    {
        static constexpr ComponentId ID = RavenUtility::generateHash("Light");
        Light() : lightColor(glm::vec3{1.0f}), lightIntensity(1.0f), lightType(LIGHT_TYPE::POINT), hasChanged(true) {}
        virtual bool init(void* data) override final
        {
            return true;
        }
        glm::vec3 lightColor;
        float lightIntensity;
        LIGHT_TYPE lightType;
        bool hasChanged;
    };
}

#endif