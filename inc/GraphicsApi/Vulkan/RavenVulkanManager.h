#ifndef RAVEN_VULKAN_MANAGER_H_INCLUDED
#define RAVEN_VULKAN_MANAGER_H_INCLUDED

#include <vector>
#include <Utility/DelegateTypes.h>
#include <EntityComponentSystem/RavenComponents/Material.h>
#include <GraphicsApi/Vulkan/VulkanUtility/DebugCallbacks.h>
#include "AssetLoader/AssetGroup.h"
#include "GraphicsApi/Vulkan/VulkanUtility/VulkanStructs.h"
#include "GraphicsApi/Vulkan/ResourceManager/VulkanResourceManager.h"
#include "Common.h"


namespace Raven
{
    class RavenVulkanManager
    {
    public:
        RavenVulkanManager();
        ~RavenVulkanManager();

        bool initialize();
        void shutdown();

        bool createRavenVertexBuffer(void* data, size_t elementSize, size_t elementCount, RavenBuffer& buffer);
        bool createRavenIndexBuffer(void* data, size_t elementSize, size_t elementCount, RavenBuffer& buffer);
        bool createRavenMaterial(const MaterialData& data, Material& material);
        bool createTextureImage(const TextureData& textureData, const VkImageCreateInfo& createInfo, RavenImage& image);
        bool createRavenImages(VkFormat format, VkExtent2D extent, VkImageUsageFlags imageUsages,
                               VkImageAspectFlagBits imageAspectFlags, uint32_t allocationCount, std::vector<RavenImage>& images);
        bool createAlbedoOnlyMaterial(VkFormat format, const TextureData& textureData, Material& material);

        const RavenVulkanContext& getVulkanContext() const { return m_context; }

        static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
                VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
                VkDebugUtilsMessageTypeFlagsEXT messageType,
                const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
                void* pUserData) {

            std::cerr << "validation layer: " << pCallbackData->pMessage << std::endl;

            return VK_FALSE;
        }

    private:
        bool initializeVulkanContext();
        void createDelegates();

        bool createDeviceLocalBuffer(void* dataToCopy, VkDeviceSize bufferSize, VkBufferUsageFlags bufferUsage, RavenBuffer& buffer);
        void copyBufferToImage(VkBuffer& srcBuffer, VkImage& dstImage, uint32_t width, uint32_t height);
        void copyBufferToCubemap(VkBuffer& srcBuffer, VkImage& dstImage, std::vector<VkBufferImageCopy>& copyRegions);
        bool copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size);
        bool addNewTextureToMaterial(Material& materials, const TextureData* data, MaterialDefinition definition);

        VulkanResourceManager m_resourceManager;

        bool initializeVulkanInstance();
        bool initializePhysicalDevice();
        bool initializeLogicalDevice();
        bool initializeDeviceQueues(const VulkanQueueInfo& queueInfo);
        bool createDebugMessenger();

        RavenVulkanContext m_context;
        bool m_enableValidationLayers;

        std::vector<const char*> desiredDeviceExtensions =
        {
                VK_KHR_SWAPCHAIN_EXTENSION_NAME,
                VK_KHR_DEDICATED_ALLOCATION_EXTENSION_NAME,
                VK_KHR_TIMELINE_SEMAPHORE_EXTENSION_NAME,
                VK_KHR_IMAGELESS_FRAMEBUFFER_EXTENSION_NAME,
                VK_KHR_IMAGE_FORMAT_LIST_EXTENSION_NAME,
        };
    };


}

#endif