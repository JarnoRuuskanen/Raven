#ifndef RAVEN_MODEL_LOADER_H_INCLUDED
#define RAVEN_MODEL_LOADER_H_INCLUDED
#include "tiny_gltf.h"
#include "AssetGroup.h"
#include <string>

namespace Raven
{
    namespace AssetLoader
    {
        bool fillAssetGroup(const std::string &path, AssetGroup& group);
        bool loadTexture(const std::string& filepath, TextureData& textureData);
        void releaseTextureData(TextureData& data);
    }
}
#endif