#ifndef RAVEN_RENDER_TARGET_UTILITY_H_INCLUDED
#define RAVEN_RENDER_TARGET_UTILITY_H_INCLUDED
#include "GraphicsApi/Vulkan/Common.h"
#include "GraphicsApi/Vulkan/VulkanObjects/RenderTarget.h"

namespace Raven
{
	bool initializeRenderTarget(RenderTarget& targetToInitialize);
}

#endif