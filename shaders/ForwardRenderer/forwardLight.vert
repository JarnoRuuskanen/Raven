#version 450

//Skinning related things follow this tutorial: https://github.com/SaschaWillems/Vulkan-glTF-PBR/blob/master/data/shaders/pbr.vert

layout(location = 0) in vec4 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in vec2 inTexCoord;
layout(location = 4) in vec2 inTexCoord1;
layout(location = 5) in vec4 inJoint0;
layout(location = 6) in vec4 inWeight0;

layout(set=0, binding = 0) uniform UniformBufferObject
{
    mat4 model;
    mat4 view;
    mat4 proj;
}ubo;

layout(location = 0) out vec2 fragTexCoordOut;
layout(location = 1) out vec3 fragPositionOut;
layout(location = 2) out vec3 normalOut;

void main() 
{
    fragPositionOut = vec3(ubo.model * vec4(inPosition.xyz, 1.0));
    normalOut = mat3(transpose(inverse(ubo.model))) * inNormal;
    fragTexCoordOut = inTexCoord;
    gl_Position = ubo.proj * ubo.view * ubo.model * inPosition;
}