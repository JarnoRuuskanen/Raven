#include <EntityComponentSystem/RavenComponents/Tag.h>
#include <GraphicsApi/Vulkan/VulkanUtility/UtilityFunctions.h>
#include <EntityComponentSystem/RavenComponents/Light.h>
#include "Utility/DelegateManager.h"
#include "GraphicsApi/Vulkan/VulkanUtility/StructInitializer.h"
#include "GraphicsApi/Vulkan/VulkanUtility/ImageUtility.h"
#include "GraphicsApi/Vulkan/VulkanUtility/DescriptorUtility.h"
#include "EntityComponentSystem/RavenComponents/Transform.h"
#include "GraphicsApi/Vulkan/UI/RavenUI.h"
#include "EntityComponentSystem/RavenComponents/ComponentUtility/ComponentUtility.h"
#include <glm/gtx/matrix_decompose.hpp>

namespace Raven
{
    void RavenUI::initialize()
    {
        m_firstVertexBufferInitialized = false;
        m_firstIndexBufferInitialized = false;

        saveWindowExtent();
        initImGui();

        const auto& manager = DelegateManager::getInstance();
        const Swapchain& swapchain = manager.getRendererDelegates().getSwapchainFunc.Invoke();
        const VkDevice& device = manager.getVulkanContextDelegates().getVulkanContextFunc.Invoke().device;

        m_colorAttachmentFormat = swapchain.swapchainImageFormat;

        //Main swapchain attachment.
        VkAttachmentDescription colorAttachment = {};
        colorAttachment.format = m_colorAttachmentFormat;
        colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
        colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
        colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        colorAttachment.initialLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR; //What layout the image will have before presentation.
        colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR; // What layout to switch when the render pass finishes.
        m_attachments.push_back(colorAttachment);

        // Depth attachment
        VkAttachmentDescription depthAttachment = {};
        depthAttachment.format = VK_FORMAT_D32_SFLOAT; //TODO: optimize.
        depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
        depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
        depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        depthAttachment.initialLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
        depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
        m_attachments.push_back(depthAttachment);

        std::vector<VkAttachmentReference> colorReferences(1);
        colorReferences[0] = { 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };

        std::vector<VkAttachmentReference> depthReferences(1);
        depthReferences[0] = { 1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL };

        m_uiSubpass.createSubpassDescription({}, colorReferences, depthReferences, m_attachments, {});

        m_swapchainExtent = swapchain.swapChainExtent;

        bool success = createRenderpass();
        assert(success);

        success = createDepthResources();
        assert(success);

        success = createFramebuffers();
        assert(success);

        saveSystemDelegates();

        m_uiSubpass.initPipeline(device, m_uiRenderPass, 0);

        bindDelegates();

        updateSceneData();
        generateComponentTypeStrings();

        createCommandPoolsAndBuffers(device, static_cast<uint32_t>(swapchain.swapchainImageViews.size()));
    }

    void RavenUI::createCommandPoolsAndBuffers(const VkDevice& device, uint32_t frameCount)
    {
        //A command buffer for each frame.
        assert(frameCount > 0);
        m_commandBuffers.resize(frameCount);
        std::cout << "Remove hard coded queueFamilyIndex!\n";

        for (size_t i = 0; i < frameCount; i++)
        {
            VkCommandPoolCreateInfo poolInfo = {};
            poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
            poolInfo.pNext = nullptr;
            poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
            poolInfo.queueFamilyIndex = 0;
            VkResult result = vkCreateCommandPool(device, &poolInfo, nullptr, &m_commandBuffers[i].pool);
            assert(result == VK_SUCCESS);

            VkCommandBuffer buffer;
            VkCommandBufferAllocateInfo allocInfo = {};
            allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
            allocInfo.commandBufferCount = 1;
            allocInfo.commandPool = m_commandBuffers[i].pool;
            allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
            allocInfo.pNext = nullptr;
            result = vkAllocateCommandBuffers(device, &allocInfo, &buffer);
            assert(result == VK_SUCCESS);

            m_commandBuffers[i].buffers.push_back(buffer);
        }
    }
    void RavenUI::update(float deltaTime)
    {
        updateImGuiIO(deltaTime);
        newFrame();
        updateBuffers();
    }

    bool RavenUI::draw(VkCommandBuffer cmdBuffer, uint32_t imageIndex)
    {
        std::array<VkClearValue,2> clearValues = {};
        clearValues[0].color = {1.0f, 1.0f, 1.0f, 0.0f};
        clearValues[1].depthStencil = {1.0f,0};

        VkCommandBufferBeginInfo beginInfo = {};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
        beginInfo.pInheritanceInfo = nullptr;

        if(vkBeginCommandBuffer(cmdBuffer, &beginInfo) != VK_SUCCESS)
        {
            std::cerr << "Failed to begin command buffer recording for buffer with index << " << imageIndex << "\n";
            return false;
        }

        VkRenderPassBeginInfo renderPassInfo = {};
        renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfo.renderPass = m_uiRenderPass;
        renderPassInfo.renderArea.offset = {0,0};
        renderPassInfo.renderArea.extent = m_swapchainExtent;
        renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
        renderPassInfo.pClearValues = clearValues.data();
        renderPassInfo.framebuffer = m_targetFramebuffers[imageIndex];

        vkCmdBeginRenderPass(cmdBuffer, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

        m_uiSubpass.draw(m_vertexBuffer, m_indexBuffer, m_descriptorSets, {}, cmdBuffer);

        vkCmdEndRenderPass(cmdBuffer);

        vkEndCommandBuffer(cmdBuffer);

        return true;
    }

    void RavenUI::updateImGuiIO(float deltaTime)
    {
        ImGuiIO& io = ImGui::GetIO();
        IM_ASSERT(io.Fonts->IsBuilt());

        int width, height;
        int displayWidth, displayHeight;
        const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        displayWidth = mode->width;
        displayHeight = mode->height;

        glfwGetWindowSize(m_window, &width, &height);

        io.DisplaySize = ImVec2(static_cast<float>(width), static_cast<float>(height));
        if(width > 0 && height > 0)
        {
            io.DisplayFramebufferScale = ImVec2(static_cast<float>(displayWidth/width), static_cast<float>(displayHeight/height));
        }

        io.DeltaTime = deltaTime;

        double xpos, ypos;
        glfwGetCursorPos(m_window, &xpos, &ypos);
        io.MousePos = ImVec2(static_cast<float>(xpos), static_cast<float>(ypos));
    }

    void RavenUI::newFrame()
    {
        if(m_updateCounter >= 60)
        {
            updateSceneData();
            m_updateCounter = 0;
        }
        m_updateCounter++;

        ImGui::NewFrame();

        updateEntityWindow();
        //ImGui::ShowDemoWindow();
        //Imgui render generates the draw buffers.
        //Next they need to be passed to vulkan buffers.
        ImGui::Render();

        updateSelectedObjectRenderable();
    }

    void RavenUI::updateBuffers()
    {
        ImDrawData* drawData = ImGui::GetDrawData();

        VkDeviceSize vertexBufferSize = drawData->TotalVtxCount * sizeof(ImDrawVert);
        VkDeviceSize indexBufferSize = drawData->TotalIdxCount * sizeof(ImDrawIdx);

        if((vertexBufferSize == 0) || (indexBufferSize == 0))
        {
            return;
        }

        bool success = false;
        if((m_vertexBuffer.buffer == VK_NULL_HANDLE) || (m_vertexBuffer.count != drawData->TotalVtxCount))
        {
            //Destroy previous buffer.
            if (m_vertexBuffer.buffer != VK_NULL_HANDLE && m_firstVertexBufferInitialized)
            {
                m_resourceManagerDelegates.unmapAllocatedMemoryFunc.Invoke(m_vertexBuffer.memoryAllocation);
                m_resourceManagerDelegates.destroyAllocatedBufferFunc.Invoke(m_vertexBuffer);
            }

            VkBufferCreateInfo createInfo = StructInitializer::getBufferCreateInfo({}, vertexBufferSize, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);
            success = m_resourceManagerDelegates.createAllocatedBufferFunc.Invoke(createInfo, VMA_MEMORY_USAGE_CPU_TO_GPU, m_vertexBuffer);
            assert(success && "Failed to create an allocated vertex buffer for UI.");
            m_vertexBuffer.count = drawData->TotalVtxCount;
            m_resourceManagerDelegates.mapAllocatedMemoryFunc.Invoke(m_vertexBuffer.memoryAllocation, &m_vertexBuffer.mapped);

            m_firstVertexBufferInitialized = true;
        }

        if((m_indexBuffer.buffer == VK_NULL_HANDLE) || (m_indexBuffer.count != drawData->TotalIdxCount))
        {
            //Destroy previous buffer.
            if (m_indexBuffer.buffer != VK_NULL_HANDLE && m_firstIndexBufferInitialized)
            {
                m_resourceManagerDelegates.unmapAllocatedMemoryFunc.Invoke(m_indexBuffer.memoryAllocation);
                m_resourceManagerDelegates.destroyAllocatedBufferFunc.Invoke(m_indexBuffer);
            }

            VkBufferCreateInfo createInfo = StructInitializer::getBufferCreateInfo({}, indexBufferSize, VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);
            success = m_resourceManagerDelegates.createAllocatedBufferFunc.Invoke(createInfo, VMA_MEMORY_USAGE_CPU_TO_GPU, m_indexBuffer);
            assert(success && "Failed to create an allocated index buffer for UI.");
            m_indexBuffer.count = drawData->TotalIdxCount;
            m_resourceManagerDelegates.mapAllocatedMemoryFunc.Invoke(m_indexBuffer.memoryAllocation, &m_indexBuffer.mapped);

            m_firstIndexBufferInitialized = true;
        }

        ImDrawVert* vtxDst = (ImDrawVert*)m_vertexBuffer.mapped;
        ImDrawIdx* idxDst = (ImDrawIdx*)m_indexBuffer.mapped;

        for(size_t i = 0; i < drawData->CmdListsCount; i++)
        {
            const ImDrawList* cmdList = drawData->CmdLists[i];
            memcpy(vtxDst, cmdList->VtxBuffer.Data, cmdList->VtxBuffer.Size*sizeof(ImDrawVert));
            memcpy(idxDst, cmdList->IdxBuffer.Data, cmdList->IdxBuffer.Size*sizeof(ImDrawIdx));
            vtxDst += cmdList->VtxBuffer.Size;
            idxDst += cmdList->IdxBuffer.Size;
        }

        m_resourceManagerDelegates.flushAllocatedMemoryFunc.Invoke(m_vertexBuffer.memoryAllocation);
        m_resourceManagerDelegates.flushAllocatedMemoryFunc.Invoke(m_indexBuffer.memoryAllocation);
    }

    void RavenUI::updateSceneData()
    {
        const auto& sceneEntities = m_entityManagerDelegates.getAllEntitiesFunc.Invoke();
        m_entities.clear();
        m_entities.shrink_to_fit();
        for(const auto& entity : sceneEntities)
        {
            std::string entityIdAsString = std::to_string(static_cast<size_t>(entity.first));
            Tag* tag = entity.second.getComponent<Tag>();
            if (tag)
            {
                std::string tagAsString = std::string(tag->m_name);
                entityIdAsString += "(" + tagAsString + ")";
            }
            m_entities.push_back(entityIdAsString);
        }
        //TODO: This might not be wise.
        std::sort(m_entities.begin(), m_entities.end());

        //Refresh the list of available mesh components.
        m_meshHashes = m_entityManagerDelegates.getMeshComponentHashesFunc.Invoke();
        m_availableMeshes.clear();
        m_availableMeshes.shrink_to_fit();
        for(const auto& mesh : m_meshHashes)
        {
            if (mesh.first != "Terrain")
            {
                m_availableMeshes.push_back(mesh.first);
            }
        }
    }

    void RavenUI::updateEntityWindow()
    {
        float xPos = static_cast<float>(m_windowWidth) / 5 * 4;
        float windowWidth = static_cast<float>(m_windowWidth)/5;
        ImGui::SetNextWindowPos(ImVec2(xPos, 0.0f));
        ImGui::SetNextWindowSize(ImVec2(windowWidth, static_cast<float>(m_windowHeight)));
        ImGui::Begin("Scene");

        updateEntityListUI();
        updateEntityAttributesUI();

        ImGui::End();
    }

    void RavenUI::updateEntityListUI()
    {
        if(m_entities.size() > 0)
        {
            ImGui::Text("Entity IDs:");
            ImGui::Separator();

            //Right align the box and set width to match parent window.
            ImGui::PushItemWidth(-1);
            auto vectorAdapter =
                    [](void* vec, int idx, const char** outText)
                    {
                        std::string& id = ((std::vector<std::string>*)vec)->at(idx);
                        *outText = id.c_str();
                        return true;
                    };
            ImGui::ListBox("##entityListBox", &m_currentEntityListSelection, vectorAdapter, (void*)&m_entities, static_cast<int>(m_entities.size()), 5);
            ImGui::PopItemWidth();

            if(m_currentEntityListSelection != -1 && m_selectedEntityId != m_currentEntityListSelection)
            {
                int idStringAsInt = std::stoi(m_entities[m_currentEntityListSelection]);
                if(idStringAsInt != m_selectedEntityId)
                {
                    std::cout << "Updating selected object\n";
                    updateSelectedObject(idStringAsInt);
                }
            }
        }

        if(ImGui::Button("Add new entity"))
        {
            m_entityManagerDelegates.addNewEntityFunc.Invoke();
        }
    }

    void RavenUI::updateEntityAttributesUI()
    {
        auto windowSize = ImGui::GetWindowSize();
        //Setting cursor position determines where the next element is going to be drawn.
        ImGui::SetCursorPos(ImVec2(windowSize.x * 0.02f, ImGui::GetWindowSize().y*0.3f));
        if(m_objectSelected)
        {
            ImGuiTabBarFlags tabFlags = ImGuiTabBarFlags_None;
            if (ImGui::BeginTabBar("ObjectTabs", tabFlags))
            {
                if(ImGui::BeginTabItem("General"))
                {
                    ImGui::Text("EntityID: %d", m_selectedEntityId);
                    auto it = m_selectedObjectComponents.find("Renderable");
                    if(it != m_selectedObjectComponents.end())
                    {
                        Renderable* renderable = static_cast<Renderable*>(it->second);
                        ImGui::Checkbox("Visible", &renderable->m_isVisible);
                    }

                    ImGui::EndTabItem();
                }
                if(ImGui::BeginTabItem("Components"))
                {
                    //Right align the box and set width to match parent window.
                    ImGui::PushItemWidth(-1);
                    auto vectorAdapter =
                            [](void* vec, int idx, const char** outText)
                            {
                                std::string& id = ((std::vector<std::string>*)vec)->at(idx);
                                *outText = id.c_str();
                                return true;
                            };

                    ImGui::ListBox("##componentListBox", &m_currentComponentSelection, vectorAdapter, (void*)&m_selectedObjectComponentNames, static_cast<int>(m_selectedObjectComponentNames.size()), 5);
                    ImGui::PopItemWidth();
                    if(ImGui::Button("Add new component"))
                    {
                        ImGui::OpenPopup("componentPopup");
                    }
                    if (ImGui::BeginPopup("componentPopup"))
                    {
                        ImGui::Text("Component types");
                        ImGui::Separator();
                        for (size_t i = 0; i < m_availableComponentTypes.size(); i++)
                            if (ImGui::Selectable(m_availableComponentTypes[i].c_str()))
                            {
                                ComponentId componentId = ComponentUtility::getComponentIdFromString(m_availableComponentTypes[i]);
                                addNewComponentToEntity(componentId);
                            }
                        ImGui::EndPopup();
                    }
                    ImGui::Spacing();

                    //TODO:Remove this check. Currently terrain uses a custom pipeline and since the pipeline selection is
                    //a bit wonky (=pipeline selection is not set to the current one when switching between entities),
                    //this check exists. Terrain is always entity number 0.
                    if (!m_selectedObjectComponents.empty())
                    {
                        bool isTerrainRenderable = (m_selectedEntityId == 0 && m_selectedObjectComponentNames[m_currentComponentSelection] == "Renderable");
                        if (!isTerrainRenderable)
                            showComponentInformation(m_selectedObjectComponentNames[m_currentComponentSelection]);
                    }
                    ImGui::EndTabItem();
                }
                ImGui::EndTabBar();
            }
        }
    }

    void RavenUI::updateSelectedObjectRenderable()
    {
        if(m_objectSelectionChanged)
        {
            m_objectSelectionChanged = false;
            m_rendererDelegates.highlightSelectedEntityFunc.Invoke(m_selectedEntityId);
        }
        if(!m_meshToSet.empty())
        {
            uint32_t meshHash = m_meshToSet.front();
            m_entityManagerDelegates.changeEntityMeshFunc.Invoke(m_selectedEntityId, meshHash);
            m_meshToSet.pop();
        }
    }

    void RavenUI::updateSelectedObject(const EntityId& id)
    {
        if(id != m_selectedEntityId)
        {
            m_objectSelectionChanged = true;
        }

        m_objectSelected = id != INVALID_ENTITY_ID;
        m_selectedEntityId = id;
        //Selected component is always reset back to 0.
        m_currentComponentSelection = 0;
        m_selectedObjectComponents.clear();
        m_selectedObjectComponentNames.clear();
        m_selectedObjectComponentNames.shrink_to_fit();
        if(m_objectSelected)
        {
            const Entity& entity = m_entityManagerDelegates.getEntityDelegateFunc.Invoke(id);

            //Update entity list to highlight the selected entity.
            //TODO: This is really dumb. Investigate the possibility to use numbers instead of strings.
            std::string entityIdAsString = std::to_string(static_cast<size_t>(m_selectedEntityId));
            auto it = std::find(m_entities.begin(), m_entities.end(), entityIdAsString);
            if(it != m_entities.end())
            {
                m_currentEntityListSelection = static_cast<int>(std::distance(m_entities.begin(), it));
            }

            const auto& components = entity.getComponents();
            for(const auto& component : components)
            {
                std::string componentName = ComponentUtility::getComponentName(component.first);
                m_selectedObjectComponentNames.push_back(componentName);
                m_selectedObjectComponents[componentName] = component.second;

                if(component.first == Renderable::ID)
                {
                    Renderable* renderable = static_cast<Renderable*>(component.second);
                    updateCurrentSelectedPipeline(renderable);
                    updateCurrentSelectedMesh(renderable);
                }
            }
        }
        else
        {
            m_currentEntityListSelection = -1;
            m_currentPipelineSelection = 0;
            m_currentMeshSelection = 0;
        }
    }

    void RavenUI::updateCurrentSelectedPipeline(Renderable* renderable)
    {
        int index = 0;
       /* for(auto& serialization : m_pipelineHashes)
        {
            if(renderable->m_pipeline)
            {
                auto hash = renderable->m_pipeline->serialization;
                if(serialization.second == hash)
                {
                    m_currentPipelineSelection = index;
                }
            }
            index++;
        }*/
    }

    void RavenUI::updateCurrentSelectedMesh(Renderable* renderable)
    {
        int index = 0;
        for (auto& serialization : m_meshHashes)
        {
            if (renderable->m_mesh)
            {
                auto hash = renderable->m_mesh->m_serialization;
                if (serialization.second == hash)
                {
                    m_currentMeshSelection = index;
                }
            }
            index++;
        }
    }

    void RavenUI::generateComponentTypeStrings()
    {
        m_availableComponentTypes.clear();
        m_availableComponentTypes.shrink_to_fit();

        //World components lists all the possible component types in the world
        //even if there are zero of the said components. So it can be used for this
        //purpose when determining what kind of components can be added to entities.
        const auto worldComponents = m_entityManagerDelegates.getAllComponentsFunc.Invoke();
        if(!worldComponents.empty())
        {
            for(const auto component : worldComponents)
            {
                std::string componentString = ComponentUtility::getComponentName(component.first);
                m_availableComponentTypes.push_back(componentString);
            }
        }
    }

    void RavenUI::showComponentInformation(const std::string &component)
    {
        if(m_selectedObjectComponentNames.empty())
        {
            return;
        }

        if(component == "Transform")
        {
            updateObjectTransformUI();
        }
        else if(component == "Renderable")
        {
            updateObjectRenderableUI();
        }
        else if(component == "Tag")
        {
            updateObjectTagUI();
        }
        else if(component == "Light")
        {
            updateObjectLightUI();
        }
    }

    void RavenUI::updateObjectTransformUI()
    {
        Transform* transform = (Transform*)m_selectedObjectComponents["Transform"];
        if(transform)
        {
            updateObjectLocalTransformUI(transform);
            updateObjectWorldTransformUI(transform);
            //TODO: Add lazy evaluation. Currently the selected object transform is being updated every cycle
            //even if it doesn't change as long as it is selected.
            transform->dirty = true;
        }
        else
        {
            std::cout << "Entity doesn't have a transform\n";
        }
    }

    void RavenUI::updateObjectLocalTransformUI(Transform* transform)
    {
        ImGui::TextColored(ImVec4(0.0f, 0.8f, 0.0f, 1.0f), "Local transform");

        ImGui::Separator();

        ImGui::Text("Position");
        ImGui::PushID(0);
        ImGui::AlignTextToFramePadding();
        ImGui::Text("X:");
        ImGui::SameLine();
        ImGui::DragFloat("##hidelabel", &transform->m_position.x, 0.1f);
        ImGui::PopID();

        ImGui::PushID(1);
        ImGui::AlignTextToFramePadding();
        ImGui::Text("Y:");
        ImGui::SameLine();
        ImGui::DragFloat("##hidelabel", &transform->m_position.y, 0.1f);
        ImGui::PopID();

        ImGui::PushID(2);
        ImGui::AlignTextToFramePadding();
        ImGui::Text("Z:");
        ImGui::SameLine();
        ImGui::DragFloat("##hidelabel", &transform->m_position.z, 0.1f);
        ImGui::PopID();

        ImGui::Spacing();

        ImGui::Text("Scale");
        ImGui::PushID(3);
        ImGui::AlignTextToFramePadding();
        ImGui::Text("X:");
        ImGui::SameLine();
        ImGui::DragFloat("##hidelabel", &transform->m_scale.x, 0.01f);
        ImGui::PopID();

        ImGui::PushID(4);
        ImGui::AlignTextToFramePadding();
        ImGui::Text("Y:");
        ImGui::SameLine();
        ImGui::DragFloat("##hidelabel", &transform->m_scale.y, 0.01f);
        ImGui::PopID();

        ImGui::PushID(5);
        ImGui::AlignTextToFramePadding();
        ImGui::Text("Z:");
        ImGui::SameLine();
        ImGui::DragFloat("##hidelabel", &transform->m_scale.z, 0.01f);
        ImGui::PopID();

        glm::vec3 euler = glm::eulerAngles(transform->m_rotation);

        //Showing the values in radians for ease of use.
        euler.x = glm::degrees(euler.x);
        euler.y = glm::degrees(euler.y);
        euler.z = glm::degrees(euler.z);

        ImGui::Text("Rotation (Euler)");
        ImGui::PushID(6);
        ImGui::AlignTextToFramePadding();
        ImGui::Text("Pitch:");
        ImGui::SameLine();
        ImGui::DragFloat("##hidelabel", &euler.x);
        ImGui::PopID();

        ImGui::PushID(7);
        ImGui::AlignTextToFramePadding();
        ImGui::Text("Yaw:  ");
        ImGui::SameLine();
        ImGui::DragFloat("##hidelabel", &euler.y);
        ImGui::PopID();

        ImGui::PushID(8);
        ImGui::AlignTextToFramePadding();
        ImGui::Text("Roll: ");
        ImGui::SameLine();
        ImGui::DragFloat("##hidelabel", &euler.z);
        ImGui::PopID();

        transform->m_rotation = glm::quat(glm::vec3(glm::radians(euler.x), glm::radians(euler.y), glm::radians(euler.z)));

    }

    void RavenUI::updateObjectWorldTransformUI(Transform* transform)
    {
        ImGui::TextColored(ImVec4(0.0f, 0.8f, 0.0f, 1.0f), "World transform");

        glm::vec3 scale;
        glm::quat rotation;
        glm::vec3 translation;
        glm::vec3 skew;
        glm::vec4 perspective;
        glm::decompose(transform->m_worldTransform, scale, rotation, translation, skew, perspective);

        ImGui::Separator();

        ImGui::Text("Position");
        ImGui::PushID(9);
        ImGui::AlignTextToFramePadding();
        ImGui::Text("X:");
        ImGui::SameLine();
        ImGui::DragFloat("##hidelabel", &translation.x, 0.1f);
        ImGui::PopID();

        ImGui::PushID(10);
        ImGui::AlignTextToFramePadding();
        ImGui::Text("Y:");
        ImGui::SameLine();
        ImGui::DragFloat("##hidelabel", &translation.y, 0.1f);
        ImGui::PopID();

        ImGui::PushID(11);
        ImGui::AlignTextToFramePadding();
        ImGui::Text("Z:");
        ImGui::SameLine();
        ImGui::DragFloat("##hidelabel", &translation.z, 0.1f);
        ImGui::PopID();

        ImGui::Spacing();

        ImGui::Text("Scale");
        ImGui::PushID(12);
        ImGui::AlignTextToFramePadding();
        ImGui::Text("X:");
        ImGui::SameLine();
        ImGui::DragFloat("##hidelabel", &scale.x, 0.01f);
        ImGui::PopID();

        ImGui::PushID(13);
        ImGui::AlignTextToFramePadding();
        ImGui::Text("Y:");
        ImGui::SameLine();
        ImGui::DragFloat("##hidelabel", &scale.y, 0.01f);
        ImGui::PopID();

        ImGui::PushID(14);
        ImGui::AlignTextToFramePadding();
        ImGui::Text("Z:");
        ImGui::SameLine();
        ImGui::DragFloat("##hidelabel", &scale.z, 0.01f);
        ImGui::PopID();

        glm::vec3 euler = glm::eulerAngles(rotation);

        //Showing the values in radians for ease of use.
        euler.x = glm::degrees(euler.x);
        euler.y = glm::degrees(euler.y);
        euler.z = glm::degrees(euler.z);

        ImGui::Text("Rotation (Euler)");
        ImGui::PushID(15);
        ImGui::AlignTextToFramePadding();
        ImGui::Text("Pitch:");
        ImGui::SameLine();
        ImGui::DragFloat("##hidelabel", &euler.x);
        ImGui::PopID();

        ImGui::PushID(16);
        ImGui::AlignTextToFramePadding();
        ImGui::Text("Yaw:  ");
        ImGui::SameLine();
        ImGui::DragFloat("##hidelabel", &euler.y);
        ImGui::PopID();

        ImGui::PushID(17);
        ImGui::AlignTextToFramePadding();
        ImGui::Text("Roll: ");
        ImGui::SameLine();
        ImGui::DragFloat("##hidelabel", &euler.z);
        ImGui::PopID();

        //transform->m_rotation = glm::quat(glm::vec3(glm::radians(euler.x), glm::radians(euler.y), glm::radians(euler.z)));

    }

    void RavenUI::updateObjectRenderableUI()
    {
        Renderable* renderable = (Renderable*)m_selectedObjectComponents["Renderable"];
        if(renderable)
        {
            auto vectorAdapter =
                    [](void* vec, int idx, const char** outText)
                    {
                        std::string& id = ((std::vector<std::string>*)vec)->at(idx);
                        *outText = id.c_str();
                        return true;
                    };

            //if(m_availableGraphicsPipelines.size() > 0)
            //{
            //    ImGui::Text("Current pipeline");
            //    ImGui::Combo("##pipelineDropdown", &m_currentPipelineSelection, vectorAdapter, (void*)&m_availableGraphicsPipelines, m_availableGraphicsPipelines.size());
            //    /*uint32_t currentSelection = m_pipelineHashes[m_availableGraphicsPipelines[m_currentPipelineSelection]];
            //    if(renderable->m_pipeline)
            //    {
            //        if(renderable->m_pipeline->serialization != currentSelection)
            //        {
            //            std::string newPipeline = std::string(m_availableGraphicsPipelines[m_currentPipelineSelection]);
            //            m_graphicsPipelineToSet.push(newPipeline);
            //        }
            //    }*/
            //}
            if(m_availableMeshes.size() > 0)
            {
                ImGui::Text("Current mesh");
                ImGui::Combo("##meshDropdown", &m_currentMeshSelection, vectorAdapter, (void*)&m_availableMeshes, static_cast<int>(m_availableMeshes.size()));
                uint32_t currentSelection = m_meshHashes[m_availableMeshes[m_currentMeshSelection]];
                if(renderable->m_mesh)
                {
                    if(renderable->m_mesh->m_serialization != currentSelection)
                    {
                        m_meshToSet.push(currentSelection);
                    }
                }
            }
        }
    }

    void RavenUI::updateObjectTagUI()
    {
        Tag* tag = (Tag*)m_selectedObjectComponents["Tag"];
        ImGui::InputText("Label: ", tag->m_name, 64);
    }

    void RavenUI::updateObjectLightUI()
    {
        Light* light = (Light*)m_selectedObjectComponents["Light"];
        ImGui::Text("Light color: ");
        ImGui::ColorPicker3("color", (float*)&light->lightColor);
        ImGui::Separator();

        ImGui::Text("Light intensity: ");
        ImGui::SameLine();
        ImGui::DragFloat("##hidelabel", (float*)&light->lightIntensity, 0.1f);
    }

    void RavenUI::addNewComponentToEntity(Raven::ComponentId id)
    {
        if(id == Tag::ID)
        {
            TagData data;
            data.name = "No label";
            m_entityManagerDelegates.addComponentToEntityFunc.Invoke(id, m_selectedEntityId, (void*)&data);
            updateSelectedObject(m_selectedEntityId);
        }
        if(id == Renderable::ID)
        {
            m_entityManagerDelegates.addComponentToEntityFunc.Invoke(id, m_selectedEntityId, nullptr);
            //Renderable currently has a nullptr mesh and material which also need to be set. Set to default for now.
            updateSelectedObject(m_selectedEntityId);
        }
    }

    bool RavenUI::createRenderpass()
    {
        VkAttachmentReference colorAttachmentRef = {};
        colorAttachmentRef.attachment = 0;
        colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        VkAttachmentReference depthAttachmentRef = {};
        depthAttachmentRef.attachment = 1;
        depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        VkSubpassDescription subpass = {};
        subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpass.colorAttachmentCount = 1;
        subpass.pColorAttachments = &colorAttachmentRef;
        subpass.pInputAttachments = nullptr;
        subpass.pResolveAttachments = nullptr;
        subpass.pDepthStencilAttachment = &depthAttachmentRef;
        subpass.pPreserveAttachments = nullptr;

        //Describe a dependency.
        // This makes the render pass wait until VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT stage before executing.
        VkSubpassDependency dependency = {};
        dependency.srcSubpass = VK_SUBPASS_EXTERNAL; // implicit subpass before or after the render pass depending if it's specified in srcSubpass. of dstSubpass
        dependency.dstSubpass = 0; // our subpass.
        //dstSubpass must always be higher than srcSubpass to prevent cycles in the dependency graph.
        dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependency.srcAccessMask = 0;
        dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

        VkRenderPassCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        createInfo.attachmentCount = static_cast<uint32_t>(m_attachments.size());
        createInfo.pAttachments = m_attachments.data();
        createInfo.subpassCount = 1;
        createInfo.pSubpasses = &subpass;
        createInfo.dependencyCount = 1;
        createInfo.pDependencies = &dependency;

        const RavenVulkanContext& context = DelegateManager::getInstance().getVulkanContextDelegates().getVulkanContextFunc.Invoke();
        VkResult result = vkCreateRenderPass(context.device, &createInfo, nullptr, &m_uiRenderPass);
        assert(result == VK_SUCCESS);
        return result == VK_SUCCESS;
    }

    void RavenUI::saveWindowExtent()
    {
        const WindowDelegates& windowDelegates = DelegateManager::getInstance().getWindowDelegates();
        m_windowWidth = windowDelegates.getWindowWidthFunc.Invoke();
        m_windowHeight = windowDelegates.getWindowHeightFunc.Invoke();
    }

    void RavenUI::saveSystemDelegates()
    {
        DelegateManager& manager = DelegateManager::getInstance();
        m_systemDelegates = manager.getWindowDelegates();
        m_resourceManagerDelegates = manager.getVulkanResourceManagerDelegates();
        m_rendererDelegates = manager.getRendererDelegates();
        m_entityManagerDelegates = manager.getEntityManagerDelegates();
        m_updateCounter = 0;
        m_currentEntityListSelection = -1;
        m_currentPipelineSelection = 0;
        m_currentMeshSelection = 0;
        m_selectedObjectComponents = {};

        m_window = m_systemDelegates.getNativeWindowFunc.Invoke();
        assert(m_window != nullptr);
    }

    void RavenUI::initImGui()
    {
        //Initialize imgui:
        IMGUI_CHECKVERSION();
        ImGui::CreateContext();

        //Select ImGui style:
        //ImGui::StyleColorsLight();

        ImGuiIO& io = ImGui::GetIO();

        ///////////////////////////////////////////////////////////
        ImGuiStyle* style = &ImGui::GetStyle();
        float hspacing = 8.0f;
        float vspacing = 6.0f;
        style->DisplaySafeAreaPadding = ImVec2(0, 0);
        style->WindowPadding = ImVec2(hspacing / 2, vspacing);
        style->FramePadding = ImVec2(hspacing, vspacing);
        style->ItemSpacing = ImVec2(hspacing, vspacing);
        style->ItemInnerSpacing = ImVec2(hspacing, vspacing);
        style->IndentSpacing = 20.0f;

        style->WindowRounding = 0.0f;
        style->FrameRounding = 0.0f;

        style->WindowBorderSize = 0.0f;
        style->FrameBorderSize = 1.0f;
        style->PopupBorderSize = 1.0f;

        style->ScrollbarSize = 20.0f;
        style->ScrollbarRounding = 0.0f;
        style->GrabMinSize = 5.0f;
        style->GrabRounding = 0.0f;

        ImVec4 white = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
        ImVec4 transparent = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
        ImVec4 dark = ImVec4(0.00f, 0.00f, 0.00f, 0.20f);
        ImVec4 darker = ImVec4(0.00f, 0.00f, 0.00f, 0.50f);

        ImVec4 background = ImVec4(0.95f, 0.95f, 0.95f, 1.00f);
        ImVec4 text = ImVec4(0.10f, 0.10f, 0.10f, 1.00f);
        ImVec4 border = ImVec4(0.60f, 0.60f, 0.60f, 1.00f);
        ImVec4 grab = ImVec4(0.69f, 0.69f, 0.69f, 1.00f);
        ImVec4 header = ImVec4(0.86f, 0.86f, 0.86f, 1.00f);
        ImVec4 active = ImVec4(0.00f, 0.47f, 0.84f, 1.00f);
        ImVec4 hover = ImVec4(0.00f, 0.47f, 0.84f, 0.20f);

        style->Colors[ImGuiCol_Text] = text;
        style->Colors[ImGuiCol_WindowBg] = background;
        style->Colors[ImGuiCol_ChildBg] = background;
        style->Colors[ImGuiCol_PopupBg] = white;

        style->Colors[ImGuiCol_Border] = border;
        style->Colors[ImGuiCol_BorderShadow] = transparent;

        style->Colors[ImGuiCol_Button] = header;
        style->Colors[ImGuiCol_ButtonHovered] = hover;
        style->Colors[ImGuiCol_ButtonActive] = active;

        style->Colors[ImGuiCol_FrameBg] = white;
        style->Colors[ImGuiCol_FrameBgHovered] = hover;
        style->Colors[ImGuiCol_FrameBgActive] = active;

        style->Colors[ImGuiCol_MenuBarBg] = header;
        style->Colors[ImGuiCol_Header] = header;
        style->Colors[ImGuiCol_HeaderHovered] = hover;
        style->Colors[ImGuiCol_HeaderActive] = active;

        style->Colors[ImGuiCol_CheckMark] = text;
        style->Colors[ImGuiCol_SliderGrab] = grab;
        style->Colors[ImGuiCol_SliderGrabActive] = darker;

        style->Colors[ImGuiCol_ScrollbarBg] = header;
        style->Colors[ImGuiCol_ScrollbarGrab] = grab;
        style->Colors[ImGuiCol_ScrollbarGrabHovered] = dark;
        style->Colors[ImGuiCol_ScrollbarGrabActive] = darker;
        ///////////////////////////////////////////////////////////

        // Keyboard mapping. ImGui will use those indices to peek into the io.KeysDown[] array.
        io.KeyMap[ImGuiKey_Tab] = GLFW_KEY_TAB;
        io.KeyMap[ImGuiKey_LeftArrow] = GLFW_KEY_LEFT;
        io.KeyMap[ImGuiKey_RightArrow] = GLFW_KEY_RIGHT;
        io.KeyMap[ImGuiKey_UpArrow] = GLFW_KEY_UP;
        io.KeyMap[ImGuiKey_DownArrow] = GLFW_KEY_DOWN;
        io.KeyMap[ImGuiKey_PageUp] = GLFW_KEY_PAGE_UP;
        io.KeyMap[ImGuiKey_PageDown] = GLFW_KEY_PAGE_DOWN;
        io.KeyMap[ImGuiKey_Home] = GLFW_KEY_HOME;
        io.KeyMap[ImGuiKey_End] = GLFW_KEY_END;
        io.KeyMap[ImGuiKey_Insert] = GLFW_KEY_INSERT;
        io.KeyMap[ImGuiKey_Delete] = GLFW_KEY_DELETE;
        io.KeyMap[ImGuiKey_Backspace] = GLFW_KEY_BACKSPACE;
        io.KeyMap[ImGuiKey_Space] = GLFW_KEY_SPACE;
        io.KeyMap[ImGuiKey_Enter] = GLFW_KEY_ENTER;
        io.KeyMap[ImGuiKey_Escape] = GLFW_KEY_ESCAPE;
        io.KeyMap[ImGuiKey_KeyPadEnter] = GLFW_KEY_KP_ENTER;
        io.KeyMap[ImGuiKey_A] = GLFW_KEY_A;
        io.KeyMap[ImGuiKey_C] = GLFW_KEY_C;
        io.KeyMap[ImGuiKey_V] = GLFW_KEY_V;
        io.KeyMap[ImGuiKey_X] = GLFW_KEY_X;
        io.KeyMap[ImGuiKey_Y] = GLFW_KEY_Y;
        io.KeyMap[ImGuiKey_Z] = GLFW_KEY_Z;

        io.DisplaySize = ImVec2(static_cast<float>(m_windowWidth), static_cast<float>(m_windowHeight));
        io.DisplayFramebufferScale = ImVec2(1.0f, 1.0f);
    }

    void RavenUI::bindDelegates()
    {
        Input& input = Input::getInstance();
        m_mouseKeyCallbackDelegate.Bind<RavenUI, &RavenUI::mouseKeyCallback>(this);
        input.registerMouseKeyCallback(m_mouseKeyCallbackDelegate);
        m_characterCallbackDelegate.Bind<RavenUI, &RavenUI::keyboardCharacterCallback>(this);
        input.registerCharacterCallback(m_characterCallbackDelegate);
        m_keyboardCallbackDelegate.Bind<RavenUI, &RavenUI::keyboardKeyCallback>(this);
        input.registerKeyboardKeyCallback(m_keyboardCallbackDelegate);
    }

    bool RavenUI::mouseKeyCallback(const MouseKeyEvent& event)
    {
        ImGuiIO& io = ImGui::GetIO();
        bool pressed = event.state == MouseKeyState::PRESS;
        if(event.mouseKey == GLFW_MOUSE_BUTTON_LEFT)
        {
            io.MouseDown[0] = pressed;
        }
        if(event.mouseKey == GLFW_MOUSE_BUTTON_RIGHT)
        {
            io.MouseDown[1] = pressed;
        }

        bool wantsToCapture = io.WantCaptureMouse;
        //If Imgui wants to capture the input, do not pass it to the rest of the code.
        return !wantsToCapture;
    }

    bool RavenUI::keyboardCharacterCallback(unsigned int glfwCodePoint)
    {
        ImGuiIO& io = ImGui::GetIO();
        io.AddInputCharacter(glfwCodePoint);
        bool wantsToCapture = io.WantCaptureKeyboard;
        return !wantsToCapture;
    }

    bool RavenUI::keyboardKeyCallback(int key, int scancode, int action, int mods)
    {
        ImGuiIO& io = ImGui::GetIO();
        bool keyIsDown = action == GLFW_PRESS;
        io.KeysDown[key] = keyIsDown;

        switch(key)
        {
            case GLFW_KEY_LEFT_SHIFT :
                io.KeyShift = keyIsDown;
                break;
            case GLFW_KEY_LEFT_CONTROL :
                io.KeyCtrl = keyIsDown;
                break;
            case GLFW_KEY_LEFT_ALT :
                io.KeyAlt = keyIsDown;
                break;
        }

        bool wantsToCapture = io.WantCaptureKeyboard;
        return !wantsToCapture;
    }

    void RavenUI::setTimelineWaitAndSignalValue(uint64_t waitForTimelineValue, uint64_t signalTimelineValue)
    {
        m_waitForTimelineValue = waitForTimelineValue;
        m_signalTimelineValue = signalTimelineValue;

        m_submitInfo = {};
        m_submitInfo.sType = VK_STRUCTURE_TYPE_TIMELINE_SEMAPHORE_SUBMIT_INFO_KHR;
        m_submitInfo.pNext = nullptr;
        m_submitInfo.waitSemaphoreValueCount = 1;
        m_submitInfo.pWaitSemaphoreValues = &m_waitForTimelineValue;
        m_submitInfo.signalSemaphoreValueCount = 1;
        m_submitInfo.pSignalSemaphoreValues = &m_signalTimelineValue;
    }

    bool RavenUI::createFramebuffers()
    {
        auto& manager = DelegateManager::getInstance();
        const Swapchain& swapchain = manager.getRendererDelegates().getSwapchainFunc.Invoke();
        const auto& context = manager.getVulkanContextDelegates().getVulkanContextFunc.Invoke();

        m_targetFramebuffers.resize(swapchain.swapchainImageViews.size());
        for(size_t i = 0; i < m_targetFramebuffers.size(); i++)
        {
            std::array<VkImageView,2> attachments = { swapchain.swapchainImageViews[i], m_depthImageView };

            VkFramebufferCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
            createInfo.renderPass = m_uiRenderPass;
            createInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
            createInfo.pAttachments = attachments.data();
            createInfo.width = swapchain.swapChainExtent.width;
            createInfo.height = swapchain.swapChainExtent.height;
            createInfo.layers = 1;

            VkResult result = vkCreateFramebuffer(context.device, &createInfo, nullptr, &m_targetFramebuffers[i]);
            if(result != VK_SUCCESS)
            {
                m_targetFramebuffers.clear();
                std::cerr << "Failed to create swapchain framebuffers!" << std::endl;
                return false;
            }
        }
        return true;
    }

    bool RavenUI::createDepthResources()
    {
        DelegateManager& manager = DelegateManager::getInstance();
        const auto& context = manager.getVulkanContextDelegates().getVulkanContextFunc.Invoke();

        VkFormat depthFormat = VK_FORMAT_D32_SFLOAT;

        VkImageCreateInfo createInfo = StructInitializer::getImageCreateInfo(m_swapchainExtent.width, m_swapchainExtent.height,VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, depthFormat);
        if(!ImageUtility::createMemoryBackedImage(context.device, context.physicalDevice, createInfo, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, m_depthImageMemory, m_depthImage))
        {
            return false;
        }

        VkImageViewCreateInfo viewInfo = StructInitializer::getImageViewCreateInfo(m_depthImage, depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT, VK_IMAGE_VIEW_TYPE_2D);
        if(!ImageUtility::createImageView(context.device, viewInfo, m_depthImageView))
        {
            return false;
        }

        VkImageSubresourceRange depthRange = ImageUtility::getDefaultImageSubresourceRange(depthFormat, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
        if(!ImageUtility::transitionImageLayout(context.device, context.queues[0], VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, depthRange, m_depthImage))
        {
            return false;
        }
        return true;
    }
}