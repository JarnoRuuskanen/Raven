#include "Utility/DelegateManager.h"

namespace Raven
{
    void DelegateManager::registerRendererDelegates(const RendererDelegates& delegates)
    {
        if(!m_rendererDelegatesInitialized)
        {
            m_rendererDelegates = delegates;
            m_rendererDelegatesInitialized = true;
        }
    }

    void DelegateManager::registerSystemDelegates(const WindowDelegates& delegates)
    {
        if(!m_systemDelegatesInitialized)
        {
            m_systemDelegates = delegates;
            m_systemDelegatesInitialized = true;
        }
    }

    void DelegateManager::registerVulkanManagerDelegates(const RavenVulkanManagerDelegates& delegates)
    {
        if(!m_vulkanManagerDelegatesInitialized)
        {
            m_ravenVulkanManagerDelegates = delegates;
            m_vulkanManagerDelegatesInitialized = true;
        }
    }

    void DelegateManager::registerVulkanResourceManagerDelegates(const VulkanResourceManagerDelegates& delegates)
    {
        if(!m_vulkanResourceManagerDelegatesInitialized)
        {
            m_vulkanResourceManagerDelegates = delegates;
            m_vulkanResourceManagerDelegatesInitialized = true;
        }
    }

    void DelegateManager::registerVulkanContextDelegates(const Raven::VulkanContextDelegates &delegates)
    {
        if(!m_vulkanContextDelegatesInitialized)
        {
            m_vulkanContextDelegates = delegates;
            m_vulkanContextDelegatesInitialized = true;
        }
    }

    void DelegateManager::registerEntityManagerDelegates(const Raven::EntityManagerDelegates &delegates)
    {
        if(!m_entityManagerDelegatesInitialized)
        {
            m_entityManagerDelegates = delegates;
            m_entityManagerDelegatesInitialized = true;
        }
    }

    void DelegateManager::registerLightSystemDelegates(const Raven::LightSystemDelegates& delegates)
    {
        if (!m_lightSystemDelegatesInitialized)
        {
            m_lightSystemDelegates = delegates;
            m_lightSystemDelegatesInitialized = true;
        }
    }

    const RendererDelegates& DelegateManager::getRendererDelegates() const
    {
        if(!m_rendererDelegatesInitialized)
        {
            assert(!"Renderer delegates not initialized!");
        }
        return m_rendererDelegates;
    }

    const WindowDelegates& DelegateManager::getWindowDelegates() const
    {
        if(!m_systemDelegatesInitialized)
        {
            assert(!"System delegates not initialized!");
        }
        return m_systemDelegates;
    }

    const RavenVulkanManagerDelegates& DelegateManager::getVulkanManagerDelegates() const
    {
        if(!m_vulkanManagerDelegatesInitialized)
        {
            assert(!"Vulkan manager delegates not initialized!");
        }
        return m_ravenVulkanManagerDelegates;
    }

    const VulkanResourceManagerDelegates& DelegateManager::getVulkanResourceManagerDelegates() const
    {
        if(!m_vulkanResourceManagerDelegatesInitialized)
        {
            assert(!"Vulkan resource manager delegates not initialized!");
        }
        return m_vulkanResourceManagerDelegates;
    }

    const VulkanContextDelegates& DelegateManager::getVulkanContextDelegates() const
    {
        if(!m_vulkanContextDelegatesInitialized)
        {
            assert(!"Vulkan context delegates not initialized!");
        }
        return m_vulkanContextDelegates;
    }

    const EntityManagerDelegates& DelegateManager::getEntityManagerDelegates() const
    {
        if(!m_entityManagerDelegatesInitialized)
        {
            assert(!"Entity manager delegates not initialized!");
        }
        return m_entityManagerDelegates;
    }

    const LightSystemDelegates& DelegateManager::getLightSystemDelegates() const
    {
        if (!m_lightSystemDelegatesInitialized)
        {
            assert(!"Light system delegates not initialized!");
        }
        return m_lightSystemDelegates;
    }
}