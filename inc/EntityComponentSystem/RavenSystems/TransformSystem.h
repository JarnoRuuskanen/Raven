#ifndef TRANSFORM_SYSTEM_H_INCLUDED
#define TRANSFORM_SYSTEM_H_INCLUDED
#include <EntityComponentSystem/RavenSystems/System.h>
#include <EntityComponentSystem/RavenComponents/Transform.h>

namespace Raven
{
    class TransformSystem : public System<Transform>
    {
        using BaseType = System<Transform>;
    public:
        TransformSystem(EntityManager* pGameWorld);
        void update(const Timer& timer) override;
    private:
        void entityAddedToSystem(Entity* entity) override;
        void updateTransform(Transform*& transform);
    };
}
#endif