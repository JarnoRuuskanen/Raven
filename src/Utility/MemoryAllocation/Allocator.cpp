#include "Utility/MemoryAllocation/Allocator.h"

namespace Raven
{
    Allocator::Allocator(const std::size_t totalSize) : m_totalSize(totalSize), m_sizeUsed(0), m_peakSize(0){}
    Allocator::~Allocator(){}
}
