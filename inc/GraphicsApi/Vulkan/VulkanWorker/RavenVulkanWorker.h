#ifndef RAVEN_VULKAN_WORKER_H_INCLUDED
#define RAVEN_VULKAN_WORKER_H_INCLUDED

#include <queue>
#include "GraphicsApi/Vulkan/Common.h"
#include "GraphicsApi/Vulkan/VulkanWorker/ThreadPool.h"

namespace Raven
{
    //This class is supposed to handle the multithreaded, Vulkan related work.

    //The way that I want this class to work is:
    //1. This class should submit work to multiple queues all the time.
    //2. Rest of the program can tell this class that a task needs to be sent to a/the queue.
    //3. This class will handle all the semaphore/fence-related synchronization.
    //4. Should possibly also handle command buffer recording to some extent.

    //Some of the stuff here is closely following the examples from Vulkan Cookbook and GpuZen book.
    using RecordingFunction = Delegate<bool(VkCommandBuffer, uint32_t)>;
    struct CommandBufferRecordingThreadParameters
    {
        VkTimelineSemaphoreSubmitInfo timelineInfo;
        VkCommandBuffer commandBuffer;
        RecordingFunction recordingFunction;
    };

    struct FirstAndLastTaskIndex
    {
        size_t first;
        size_t last;
    };

    class RavenVulkanWorker
    {
    public:
        RavenVulkanWorker();
        ~RavenVulkanWorker();
        bool initialize();
        //Each thread can work on one command pool/buffer at a time.
        void recordAndSubmitCommands(const std::vector<CommandBufferRecordingThreadParameters> &threadParams, VkSemaphore timeline, VkSemaphore firstTaskNeedsToWaitForThis, VkSemaphore allTasksSubmitted, size_t frameIndex);
        std::atomic_uint64_t m_submitCount;
    private:
        void recordCommandBuffers(const CommandBufferRecordingThreadParameters& threadParams, size_t frameIndex);
        void submitCommandBuffers(const std::vector<CommandBufferRecordingThreadParameters>& threadParams, VkSemaphore timeline, VkSemaphore firstTaskSemaphore, VkSemaphore lastTaskSemaphore);
        size_t m_pipelineLength;

        //Hard coded debug functions, remove
        void firstTaskSubmit(const CommandBufferRecordingThreadParameters& threadParam, VkSemaphore timeline, VkSemaphore firstTaskSemaphore, uint32_t queueIndex);
        void midTaskSubmit(const CommandBufferRecordingThreadParameters& threadParam, VkSemaphore timeline, uint32_t queueIndex);
        void lastTaskSubmit(const CommandBufferRecordingThreadParameters& threadParam, VkSemaphore timeline, VkSemaphore lastTaskSemaphore, uint32_t queueIndex);
        void singleTaskSubmit(const CommandBufferRecordingThreadParameters& threadParam, VkSemaphore timeline, VkSemaphore firstTaskSemaphore, VkSemaphore lastTaskSemaphore);

        std::vector<VkQueue> m_queues;
        ThreadPool m_threadPool;
    };
}

#endif