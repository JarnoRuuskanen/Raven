#ifndef RAVEN_VULKAN_INITIALIZATION_H_INCLUDED
#define RAVEN_VULKAN_INITIALIZATION_H_INCLUDED
#include "GraphicsApi/Vulkan/Common.h"
#include "GraphicsApi/Vulkan/VulkanUtility/VulkanStructs.h"

#include <vector>

namespace Raven
{
    //This api is meant for initializating Vulkan. That includes helper functions for selecting
    //queue families etc.
    namespace VulkanInitialization
    {
        bool getPhysicalDevices(VkInstance &instance, std::vector<VkPhysicalDevice> &physicalDevices);
        bool arePhysicalDeviceExtensionsSupported(const VkPhysicalDevice &physicalDevice,
                                                  std::vector<char const*> const& desiredExtensions);
        bool isExtensionSupported(std::vector<VkExtensionProperties> &availableExtensions,
                                  const char* desiredExtension);
        uint32_t getQueueFamilyIndex(std::vector<VkQueueFamilyProperties> &queueFamilies,
                                     VkQueueFlags desiredQueueFamily);
        bool getPhysicalDeviceQueuesWithProperties(const VkPhysicalDevice &physicalDevice,
                                                   std::vector<VkQueueFamilyProperties> &queueFamilies);
        uint32_t getPresentationQueueFamilyIndex(const VkPhysicalDevice& device,
                                                 VkSurfaceKHR& surface, const std::vector<VkQueueFamilyProperties>& queueFamilies);
        bool getPhysicalDevicePrimaryQueueFamily(const VkPhysicalDevice& physicalDevice, VulkanQueueInfo &familyInfo);
        std::vector<VkPresentModeKHR> getPhysicalDevicePresentationModes(const VkPhysicalDevice& physicalDevice, const VkSurfaceKHR& surface);
        std::vector<VkSurfaceFormatKHR> getPhysicalDeviceSurfaceFormats(const VkPhysicalDevice& physicalDevice, const VkSurfaceKHR& surface);
        SwapchainSupportInfo getSwapchainSupportInfo(const VkPhysicalDevice& physicalDevice, const VkSurfaceKHR& surface);

        bool createVulkanInstance(const std::vector<const char*>& extensions, const std::vector<const char*>& enabledLayers, VkInstance& instance);
        bool createLogicalDevice(const VkPhysicalDevice& physicalDevice, const std::vector<const char*>& desiredDeviceExtensions, const VulkanQueueInfo& queueInfo, VkDevice& device);
    }

}

#endif
