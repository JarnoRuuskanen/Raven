#ifndef RAVEN_DELEGATE_TYPES_H_INCLUDED
#define RAVEN_DELEGATE_TYPES_H_INCLUDED

#include <GraphicsApi/Vulkan/ResourceManager/VulkanResourceManager.h>
#include <AssetLoader/AssetGroup.h>
#include <EntityComponentSystem/RavenComponents/Renderable.h>
#include <EntityComponentSystem/RavenEntities/Entity.h>
#include <GraphicsApi/Vulkan/VulkanObjects/Swapchain.h>
#include "Window/ApplicationWindow.h"
#include "Utility/Delegate.h"
#include "GraphicsApi/Vulkan/Common.h"
#include "GraphicsApi/Vulkan/VulkanUtility/VulkanStructs.h"

namespace Raven
{
    //RavenVulkanManager delegates.
    using CreateRavenVertexBufferDelegate = Delegate<bool(void*, size_t, size_t, RavenBuffer&)>;
    using CreateRavenIndexBufferDelegate = Delegate<bool(void*, size_t, size_t, RavenBuffer&)>;
    //using CreateCubemapDelegate = Delegate<bool(const gli::texture_cube&, VkFormat , RavenImage&)>;
    using CreateTextureImageDelegate = Delegate<bool(const TextureData&, const VkImageCreateInfo&, RavenImage&)>;
    using CreateRavenImagesDelegate = Delegate<bool(VkFormat, VkExtent2D, VkImageUsageFlags, VkImageAspectFlagBits, uint32_t, std::vector<RavenImage>&)>;
    using CreateRavenMaterialDelegate = Delegate<bool(const MaterialData&, Material&)>;

    //VulkanResourceManager delegates.
    using CreateAllocatedBufferDelegate = Delegate <bool(const VkBufferCreateInfo&, VmaMemoryUsage, RavenBuffer&)>;
    using DestroyAllocatedBufferDelegate = Delegate <void(RavenBuffer&)>;
    using CreateAllocatedImageDelegate = Delegate <bool(const VkImageCreateInfo&, VmaMemoryUsage, RavenImage&)>;
    using DestroyAllocatedImageDelegate = Delegate <void(RavenImage&)>;
    using MapAllocatedMemoryDelegate = Delegate<VkResult(const VmaAllocation&, void**)>;
    using UnmapAllocatedMemoryDelegate = Delegate<void(const VmaAllocation&)>;
    using FlushAllocatedMemoryDelegate = Delegate<void(const VmaAllocation&)>;

    //General system delegates.
    using CreateWindowSurfaceDelegate = Delegate<bool(const SurfaceCreateInfo& info)>;
    using GetWindowSurfaceDelegate = Delegate<VkSurfaceKHR&(void)>;
    using GetWindowWidthDelegate = Delegate<uint32_t(void)>;
    using GetWindowHeightDelegate = Delegate<uint32_t(void)>;
    using GetNativeWindowDelegate = Delegate<GLFWwindow*(void)>;
    using GetVulkanContextDelegate = Delegate<const RavenVulkanContext&(void)>;
    using GetLightBufferDelegate = Delegate<const RavenBuffer&(void)>;

    //Renderer system delegates.
    using HighlightSelectedEntityDelegate = Delegate<void(EntityId id)>;
    using GetSwapchainImageCountDelegate = Delegate<size_t(void)>;
    using GetSwapchainDelegate = Delegate<const Swapchain&(void)>;

    //Entity manager delegates.
    using GetAllEntitiesDelegate = Delegate<const std::unordered_map<EntityId, Entity>&(void)>;
    using GetEntityDelegate = Delegate<const Entity&(const EntityId)>;
    using GetMeshComponentHashesDelegate = Delegate<const std::unordered_map<std::string,uint32_t>&(void)>;
    using AddNewEntityDelegate = Delegate<Entity*(void)>;
    using GetAllComponentsDelegate = Delegate<const std::unordered_map<ComponentId, std::vector<Component*>>&(void)>;
    using AddComponentToEntityDelegate = Delegate<void(ComponentId, EntityId, void*)>;
    using ChangeEntityMeshDelegate = Delegate<void(EntityId, uint32_t)>;

    //Command recorder function type.
    using RecordRenderpassDelegate = Delegate<bool(VkCommandBuffer&, uint32_t)>;

    struct RavenVulkanManagerDelegates
    {
        CreateRavenVertexBufferDelegate createVertexBufferFunc;
        CreateRavenIndexBufferDelegate  createIndexBufferFunc;
        CreateTextureImageDelegate  createTextureImageFunc;
        CreateRavenImagesDelegate createRavenImagesFunc;
        CreateRavenMaterialDelegate createRavenMaterialFunc;
    };

    struct VulkanResourceManagerDelegates
    {
        CreateAllocatedBufferDelegate createAllocatedBufferFunc;
        DestroyAllocatedBufferDelegate destroyAllocatedBufferFunc;
        CreateAllocatedImageDelegate createAllocatedImageFunc;
        DestroyAllocatedImageDelegate  destroyAllocatedImageFunc;
        MapAllocatedMemoryDelegate mapAllocatedMemoryFunc;
        UnmapAllocatedMemoryDelegate unmapAllocatedMemoryFunc;
        FlushAllocatedMemoryDelegate flushAllocatedMemoryFunc;
    };

    struct WindowDelegates
    {
        CreateWindowSurfaceDelegate createWindowSurfaceFunc;
        GetWindowSurfaceDelegate getSurfaceFunc;
        GetWindowWidthDelegate getWindowWidthFunc;
        GetWindowHeightDelegate getWindowHeightFunc;
        GetNativeWindowDelegate getNativeWindowFunc;
    };

    struct VulkanContextDelegates
    {
        GetVulkanContextDelegate getVulkanContextFunc;
    };

    struct RendererDelegates
    {
        HighlightSelectedEntityDelegate highlightSelectedEntityFunc;
        GetSwapchainImageCountDelegate getSwapchainImageCountFunc;
        GetSwapchainDelegate getSwapchainFunc;
    };

    struct LightSystemDelegates
    {
        GetLightBufferDelegate getLightBufferFunc;
    };

    struct EntityManagerDelegates
    {
        GetAllEntitiesDelegate getAllEntitiesFunc;
        GetEntityDelegate getEntityDelegateFunc;
        GetMeshComponentHashesDelegate getMeshComponentHashesFunc;
        AddNewEntityDelegate addNewEntityFunc;
        GetAllComponentsDelegate getAllComponentsFunc;
        AddComponentToEntityDelegate addComponentToEntityFunc;
        ChangeEntityMeshDelegate changeEntityMeshFunc;
    };
}

#endif