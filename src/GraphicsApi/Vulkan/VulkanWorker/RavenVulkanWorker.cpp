#include <Utility/DelegateManager.h>
#include <GraphicsApi/Vulkan/VulkanUtility/CommandBufferUtility.h>
#include "GraphicsApi/Vulkan/VulkanWorker/RavenVulkanWorker.h"
#include <atomic>

namespace Raven
{
    RavenVulkanWorker::RavenVulkanWorker()
    {
        m_submitCount = 0;
    }

    RavenVulkanWorker::~RavenVulkanWorker()
    {
    }

    bool RavenVulkanWorker::initialize()
    {
        DelegateManager& manager = DelegateManager::getInstance();
        m_pipelineLength = manager.getRendererDelegates().getSwapchainImageCountFunc.Invoke();
        assert(m_pipelineLength > 0);

        m_queues = manager.getVulkanContextDelegates().getVulkanContextFunc.Invoke().queues;

        return true;
    }

    FirstAndLastTaskIndex findFirstAndLastTaskByTimelineSignal(const std::vector<CommandBufferRecordingThreadParameters>& threadParams)
    {
        FirstAndLastTaskIndex firstAndLastTaskIndex;
        //Initializing first task to the first threadParams.
        firstAndLastTaskIndex.first = 0;
        firstAndLastTaskIndex.last = threadParams.size() - 1;
        return firstAndLastTaskIndex;
    }

    void RavenVulkanWorker::recordAndSubmitCommands(const std::vector<CommandBufferRecordingThreadParameters> &threadParams,
                                                    VkSemaphore timeline, VkSemaphore firstTaskNeedsToWaitForThis,
                                                    VkSemaphore allTasksSubmitted, size_t frameIndex)
    {
        std::vector<std::future<void>> futures;
        for( size_t taskIndex = 0; taskIndex < threadParams.size(); ++taskIndex)
        {
            size_t currentFrameIndex = frameIndex;
            CommandBufferRecordingThreadParameters params = threadParams[taskIndex];
            auto task = [this, params, currentFrameIndex]()
            {
                recordCommandBuffers(params, currentFrameIndex);
            };
            futures.push_back(m_threadPool.queueTask(task));
        }

        //Wait for all tasks to finish. This is suboptimal as I would like to be able to submit
        //work in multiple threads, not just record it.
        for(size_t i = 0; i < futures.size(); i++)
        {
            futures[i].get();
        }

        submitCommandBuffers(threadParams, timeline, firstTaskNeedsToWaitForThis, allTasksSubmitted);
    }

    void RavenVulkanWorker::recordCommandBuffers(const CommandBufferRecordingThreadParameters &threadParams,
                                                 size_t frameIndex)
    {
        //Invoke the record function.
        threadParams.recordingFunction.Invoke(threadParams.commandBuffer, frameIndex);
    }

    void RavenVulkanWorker::submitCommandBuffers(const std::vector<CommandBufferRecordingThreadParameters> &threadParams,
                                                 VkSemaphore timeline, VkSemaphore firstTaskSemaphore, VkSemaphore lastTaskSemaphore)
    {
        m_submitCount = 0;
        size_t lastTaskIndex = threadParams.size() - 1;
        size_t queuesAvailable = m_queues.size();
        size_t taskSubmitQueue = 0;

#ifdef RAVEN_MULTITHREADED_SUBMIT
        std::cerr << "Multithreaded submit implementation missing.";
        std::exit()
        while(m_submitCount < threadParams.size());
#else
        if(threadParams.size() == 1)
        {
            singleTaskSubmit(threadParams[0], timeline, firstTaskSemaphore, lastTaskSemaphore);
        }
        else
        {
            for(size_t i = 0; i < threadParams.size(); i++)
            {
                if(i == 0)
                {
                    firstTaskSubmit(threadParams[i], timeline, firstTaskSemaphore, taskSubmitQueue);
                }
                else if(i == lastTaskIndex)
                {
                    lastTaskSubmit(threadParams[i], timeline, lastTaskSemaphore, taskSubmitQueue);
                }
                else
                {
                    midTaskSubmit(threadParams[i], timeline, taskSubmitQueue);
                }
                taskSubmitQueue = (taskSubmitQueue + 1) % queuesAvailable;
            }
        }
#endif
    }

    void RavenVulkanWorker::firstTaskSubmit(const CommandBufferRecordingThreadParameters& threadParam, VkSemaphore timeline, VkSemaphore firstTaskSemaphore, uint32_t queueIndex)
    {
        std::vector<VkPipelineStageFlags> waitStages = { VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT };
        const VkSemaphore firstTaskWaitSemaphores [2] = {timeline, firstTaskSemaphore};
        const uint64_t firstTaskWaitValues [2] = {threadParam.timelineInfo.pWaitSemaphoreValues[0], 1};

        VkTimelineSemaphoreSubmitInfo timelineInfo;
        timelineInfo.sType = VK_STRUCTURE_TYPE_TIMELINE_SEMAPHORE_SUBMIT_INFO;
        timelineInfo.pNext = NULL;
        timelineInfo.waitSemaphoreValueCount = 2;
        timelineInfo.pWaitSemaphoreValues = firstTaskWaitValues;
        timelineInfo.signalSemaphoreValueCount = 1;
        timelineInfo.pSignalSemaphoreValues = threadParam.timelineInfo.pSignalSemaphoreValues;

        VkSubmitInfo submitInfo;
        submitInfo.pWaitDstStageMask = waitStages.data();
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.pNext = &timelineInfo;
        submitInfo.waitSemaphoreCount = 2;
        submitInfo.pWaitSemaphores = firstTaskWaitSemaphores;
        submitInfo.signalSemaphoreCount  = 1;
        submitInfo.pSignalSemaphores = &timeline;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &threadParam.commandBuffer;

        vkQueueSubmit(m_queues[queueIndex], 1, &submitInfo, VK_NULL_HANDLE);
        m_submitCount++;
    }

    void RavenVulkanWorker::midTaskSubmit(const CommandBufferRecordingThreadParameters& threadParam, VkSemaphore timeline, uint32_t queueIndex)
    {
        std::vector<VkPipelineStageFlags> waitStages = { VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT };

        VkTimelineSemaphoreSubmitInfo timelineInfo;
        timelineInfo.sType = VK_STRUCTURE_TYPE_TIMELINE_SEMAPHORE_SUBMIT_INFO;
        timelineInfo.pNext = NULL;
        timelineInfo.waitSemaphoreValueCount = 1;
        timelineInfo.pWaitSemaphoreValues = threadParam.timelineInfo.pWaitSemaphoreValues;
        timelineInfo.signalSemaphoreValueCount = 1;
        timelineInfo.pSignalSemaphoreValues = threadParam.timelineInfo.pSignalSemaphoreValues;

        VkSubmitInfo submitInfo;
        submitInfo.pWaitDstStageMask = waitStages.data();
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.pNext = &timelineInfo;
        submitInfo.waitSemaphoreCount = 1;
        submitInfo.pWaitSemaphores = &timeline;
        submitInfo.signalSemaphoreCount  = 1;
        submitInfo.pSignalSemaphores = &timeline;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &threadParam.commandBuffer;

        vkQueueSubmit(m_queues[queueIndex], 1, &submitInfo, VK_NULL_HANDLE);
        m_submitCount++;
    }

    void RavenVulkanWorker::lastTaskSubmit(const CommandBufferRecordingThreadParameters& threadParam, VkSemaphore timeline, VkSemaphore lastTaskSemaphore, uint32_t queueIndex)
    {
        std::vector<VkPipelineStageFlags> waitStages = {VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT };
        const VkSemaphore lastTaskSignalSemaphores[2] = {timeline, lastTaskSemaphore};
        const uint64_t lastTaskSignalValues [2] = {threadParam.timelineInfo.pSignalSemaphoreValues[0], 1};

        VkTimelineSemaphoreSubmitInfo timelineInfo;
        timelineInfo.sType = VK_STRUCTURE_TYPE_TIMELINE_SEMAPHORE_SUBMIT_INFO;
        timelineInfo.pNext = NULL;
        timelineInfo.waitSemaphoreValueCount = 1;
        timelineInfo.pWaitSemaphoreValues = threadParam.timelineInfo.pWaitSemaphoreValues;
        timelineInfo.signalSemaphoreValueCount = 2;
        timelineInfo.pSignalSemaphoreValues = lastTaskSignalValues;

        VkSubmitInfo submitInfo;
        submitInfo.pWaitDstStageMask = waitStages.data();
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.pNext = &timelineInfo;
        submitInfo.waitSemaphoreCount = 1;
        submitInfo.pWaitSemaphores = &timeline;
        submitInfo.signalSemaphoreCount  = 2;
        submitInfo.pSignalSemaphores = lastTaskSignalSemaphores;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &threadParam.commandBuffer;

        vkQueueSubmit(m_queues[queueIndex], 1, &submitInfo, VK_NULL_HANDLE);
        m_submitCount++;
    }

    void RavenVulkanWorker::singleTaskSubmit(const Raven::CommandBufferRecordingThreadParameters &threadParam,
                                             VkSemaphore timeline, VkSemaphore firstTaskSemaphore,
                                             VkSemaphore lastTaskSemaphore)
    {
        std::vector<VkPipelineStageFlags> waitStages = {VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT };

        const VkSemaphore firstTaskWaitSemaphores [2] = {timeline, firstTaskSemaphore};
        const uint64_t firstTaskWaitValues [2] = {threadParam.timelineInfo.pWaitSemaphoreValues[0], 1};

        const VkSemaphore lastTaskSignalSemaphores[2] = {timeline, lastTaskSemaphore};
        const uint64_t lastTaskSignalValues [2] = {threadParam.timelineInfo.pSignalSemaphoreValues[0], 1};

        VkTimelineSemaphoreSubmitInfo timelineInfo;
        timelineInfo.sType = VK_STRUCTURE_TYPE_TIMELINE_SEMAPHORE_SUBMIT_INFO;
        timelineInfo.pNext = NULL;
        timelineInfo.waitSemaphoreValueCount = 2;
        timelineInfo.pWaitSemaphoreValues = firstTaskWaitValues;
        timelineInfo.signalSemaphoreValueCount = 2;
        timelineInfo.pSignalSemaphoreValues = lastTaskSignalValues;

        VkSubmitInfo submitInfo;
        submitInfo.pWaitDstStageMask = waitStages.data();
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.pNext = &timelineInfo;
        submitInfo.waitSemaphoreCount = 2;
        submitInfo.pWaitSemaphores = firstTaskWaitSemaphores;
        submitInfo.signalSemaphoreCount  = 2;
        submitInfo.pSignalSemaphores = lastTaskSignalSemaphores;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &threadParam.commandBuffer;

        vkQueueSubmit(m_queues[0], 1, &submitInfo, VK_NULL_HANDLE);
        m_submitCount++;
    }
}