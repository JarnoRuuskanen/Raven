#ifndef RAVEN_VULKAN_DESCRIPTOR_UTILITY_H_INCLUDED
#define RAVEN_VULKAN_DESCRIPTOR_UTILITY_H_INCLUDED
#include "GraphicsApi/Vulkan/Common.h"
#include <vector>

namespace Raven
{
    namespace DescriptorUtility
    {
        VkResult createDescriptorSetLayout(const VkDevice& device, const std::vector<VkDescriptorSetLayoutBinding>& bindings, VkDescriptorSetLayout& layout);
        VkResult createDescriptorPool(const VkDevice& device, const std::vector<VkDescriptorPoolSize>& poolSizes, uint32_t maxSets, bool releasable, VkDescriptorPool& pool);
        VkResult allocateDescriptorSets(const VkDevice& device, const VkDescriptorPool& pool, const std::vector<VkDescriptorSetLayout>& layouts,
                                    uint32_t allocationCount, std::vector<VkDescriptorSet>& descriptorSets);
        VkResult allocateDescriptorSet(const VkDevice& device, const VkDescriptorPool& pool, const std::vector<VkDescriptorSetLayout>& layouts, VkDescriptorSet& descriptorSets);
    }
}

#endif