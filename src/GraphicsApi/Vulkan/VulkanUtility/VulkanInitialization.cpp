#include "GraphicsApi/Vulkan/VulkanUtility/VulkanInitialization.h"

#include <iostream>
#include <cstring>
namespace Raven
{
    namespace VulkanInitialization
    {
        //Enumerates through all physical, vulkan-capable devices and stores them inside a vector
        bool getPhysicalDevices(VkInstance &instance, std::vector<VkPhysicalDevice> &physicalDevices)
        {
            uint32_t deviceCount = 0;
            VkResult result;
            //First enumerate physical devices to find out how many devices there are
            result = vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);

            //Check that enumeration was successful.
            if(result != VK_SUCCESS || deviceCount == 0)
            {
                std::cerr << "Failed to enumerate physical device count! \n";
                return false;
            }

            //deviceCount has now been updated, resize physicalDevies accordingly
            physicalDevices.resize(deviceCount);

            //Next get the physical devices by calling the same function again
            result = vkEnumeratePhysicalDevices(instance, &deviceCount, physicalDevices.data());

            //Check that enumeration was successful.
            if(result != VK_SUCCESS || physicalDevices.empty())
            {
                std::cerr << "Failed to get physical devices!\n";
                return false;
            }
            return true;
        }

        bool arePhysicalDeviceExtensionsSupported(const VkPhysicalDevice &physicalDevice,
                                                  std::vector<char const*> const& desiredExtensions)
        {
            uint32_t extensionCount = 0;
            std::vector<VkExtensionProperties> availableExtensions;
            VkResult result;

            //First find out how many extensions are supported by the physical device
            result = vkEnumerateDeviceExtensionProperties(physicalDevice, nullptr, &extensionCount, nullptr);
            if(result != VK_SUCCESS || extensionCount == 0)
                return false;

            //Next resize the container and enumerate again to get the extensions
            availableExtensions.resize(extensionCount);
            result = vkEnumerateDeviceExtensionProperties(physicalDevice,
                                                          nullptr,
                                                          &extensionCount,
                                                          availableExtensions.data());

            if(result != VK_SUCCESS || availableExtensions.empty())
                return false;

            //Now that we have the array of extensions check if the desiredProperty is supported
            for(auto& extension : desiredExtensions)
            {
                if(!isExtensionSupported(availableExtensions, extension))
                {
                    std::cerr << "Extension: " << extension << " not available!\n";
                    return false;
                }
            }
            return true;
        }

        bool isExtensionSupported(std::vector<VkExtensionProperties> &availableExtensions,
                                  const char* desiredExtension)
        {
            //Loop through the whole extension vector and check, if the desired extension is supported
            for(auto& extension : availableExtensions)
            {
                //Compare the two extension names
                if(std::strcmp(extension.extensionName,desiredExtension) == 0)
                    return true;
            }
            //Otherwise the extension is not supported
            return false;
        }

        uint32_t getQueueFamilyIndex(std::vector<VkQueueFamilyProperties> &queueFamilies,
                                     VkQueueFlags desiredQueueFamily)
        {
            uint32_t queueFamilyIndex = UINT32_MAX;
            //After acquiring all the queue families, find the correct one for our needs
            for(uint32_t i = 0; i < static_cast<uint32_t>(queueFamilies.size()); i++)
            {
                if((queueFamilies[i].queueCount > 0) && (queueFamilies[i].queueFlags & desiredQueueFamily))
                {
                    queueFamilyIndex = i;
                    break;
                }
            }
            return queueFamilyIndex;
        }

        bool getPhysicalDeviceQueuesWithProperties(const VkPhysicalDevice &physicalDevice,
                                                   std::vector<VkQueueFamilyProperties> &queueFamilies)
        {
            if(physicalDevice == VK_NULL_HANDLE)
            {
                std::cerr << "No valid physical device for queue family information fetching!\n";
                return false;
            }

            uint32_t queueFamilyCount = 0;
            //First get the count of queue families
            vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, nullptr);

            //Check wether there are any queue families or not
            if(queueFamilyCount == 0)
            {
                std::cerr << "Failed to acquire the number of queue families!\n";
                return false;
            }

            //Get the actual queue families
            queueFamilies.resize(queueFamilyCount);
            vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice,
                                                     &queueFamilyCount,
                                                     queueFamilies.data());
            if(queueFamilies.empty())
            {
                std::cerr << "Failed to acquire queue family properties!\n";
                return false;
            }

            //There are currently four kinds of graphics families:
            //graphics, compute, transfer and sparse.
            return true;
        }

        uint32_t getPresentationQueueFamilyIndex(const VkPhysicalDevice& device,
                                                 VkSurfaceKHR& surface, const std::vector<VkQueueFamilyProperties>& queueFamilies)
        {
            uint32_t i = 0;
            for(const auto& family : queueFamilies)
            {
                VkBool32 presentSupport = false;
                vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport);
                if(family.queueCount > 0 && presentSupport)
                {
                    return i;
                }
                i++;
            }
            return UINT32_MAX;
        }

        std::vector<VkPresentModeKHR> getPhysicalDevicePresentationModes(const VkPhysicalDevice& physicalDevice, const VkSurfaceKHR& surface)
        {
            uint32_t presentModesCount;
            std::vector<VkPresentModeKHR> presentModes;
            vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice,surface, &presentModesCount, nullptr);
            if(presentModesCount > 0)
            {
                presentModes.resize(presentModesCount);
                vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice,surface, &presentModesCount, presentModes.data());
            }
            return presentModes;
        }

        std::vector<VkSurfaceFormatKHR> getPhysicalDeviceSurfaceFormats(const VkPhysicalDevice& physicalDevice, const VkSurfaceKHR& surface)
        {
            uint32_t formatCount;
            vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatCount, nullptr);
            std::vector<VkSurfaceFormatKHR> surfaceFormats;
            if(formatCount > 0)
            {
                surfaceFormats.resize(formatCount);
                vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatCount,surfaceFormats.data());
            }
            return surfaceFormats;
        }

        SwapchainSupportInfo getSwapchainSupportInfo(const VkPhysicalDevice& physicalDevice, const VkSurfaceKHR& surface)
        {
            SwapchainSupportInfo info = {};

            vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, &info.capabilities);
            info.formats = getPhysicalDeviceSurfaceFormats(physicalDevice, surface);
            info.presentModes = getPhysicalDevicePresentationModes(physicalDevice, surface);

            return info;
        }

        bool createVulkanInstance(const std::vector<const char*>& extensions, const std::vector<const char*>& enabledLayers, VkInstance& instance)
        {
            VkApplicationInfo appInfo = {};
            appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
            appInfo.pNext = nullptr;
            appInfo.pApplicationName = "Raven";
            appInfo.applicationVersion = 1;
            appInfo.pEngineName = "Raven";
            appInfo.engineVersion = 1;
            appInfo.apiVersion = VK_API_VERSION_1_1;

            VkInstanceCreateInfo instInfo = {};
            instInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
            instInfo.pNext = nullptr;
            instInfo.flags = 0;
            instInfo.pApplicationInfo = &appInfo;
            instInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
            instInfo.ppEnabledExtensionNames = !extensions.empty() ? extensions.data() : nullptr;
            instInfo.enabledLayerCount = static_cast<uint32_t>(enabledLayers.size());
            instInfo.ppEnabledLayerNames = !enabledLayers.empty() ? enabledLayers.data() : nullptr;

            VkResult result = vkCreateInstance(&instInfo, nullptr, &instance);
            assert(result == VK_SUCCESS);

            return result == VK_SUCCESS;
        }

        bool createLogicalDevice(const VkPhysicalDevice& physicalDevice, const std::vector<const char*>& desiredDeviceExtensions, const VulkanQueueInfo& queueInfo, VkDevice& device)
        {
            assert(physicalDevice != VK_NULL_HANDLE && "Physical device was VK_NULL_HANDLE!");
            VkDeviceQueueCreateInfo queueCreateInfo = {};
            queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            queueCreateInfo.flags = 0;
            queueCreateInfo.pNext = nullptr;
            queueCreateInfo.pQueuePriorities = queueInfo.priorities.data();
            queueCreateInfo.queueFamilyIndex = queueInfo.queueFamilyIndex;
            queueCreateInfo.queueCount = static_cast<uint32_t>(queueInfo.priorities.size());

            //Query timeline semaphore feature support.
            VkPhysicalDeviceTimelineSemaphoreFeatures timelineSemaphoreFeatures;
            timelineSemaphoreFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_FEATURES;
            timelineSemaphoreFeatures.pNext = nullptr;

            VkPhysicalDeviceFeatures basicFeatures{};
            VkPhysicalDeviceFeatures2 features{};
            features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
            features.pNext = &timelineSemaphoreFeatures;
            features.features = basicFeatures;


            VkPhysicalDeviceProperties2 physicalDeviceProperties2{VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2};
            vkGetPhysicalDeviceProperties2(physicalDevice, &physicalDeviceProperties2);

            vkGetPhysicalDeviceFeatures2(physicalDevice, &features);

            VkDeviceCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
            createInfo.flags = 0;
            createInfo.pNext = &features; //Enabling some extra features so sending all the enabled features to pNext.
            createInfo.enabledExtensionCount = static_cast<uint32_t>(desiredDeviceExtensions.size());
            createInfo.ppEnabledExtensionNames = !desiredDeviceExtensions.empty() ? desiredDeviceExtensions.data() : nullptr;
            createInfo.pEnabledFeatures = nullptr;
            createInfo.queueCreateInfoCount = 1;
            createInfo.pQueueCreateInfos = &queueCreateInfo;

            VkResult result = vkCreateDevice(physicalDevice, &createInfo, nullptr, &device);
            assert(result == VK_SUCCESS);
            return result == VK_SUCCESS;
        }

        bool getPhysicalDevicePrimaryQueueFamily(const VkPhysicalDevice& physicalDevice, VulkanQueueInfo &familyInfo)
        {
            std::vector<VkQueueFamilyProperties> queueFamilies;
            //Get all queue families of the physical device.
            if(!VulkanInitialization::getPhysicalDeviceQueuesWithProperties(physicalDevice, queueFamilies))
                return false;

            //Find a queue family that suits our needs.
            uint32_t primaryQueueIndex = VulkanInitialization::getQueueFamilyIndex(queueFamilies,
                                                                                    VK_QUEUE_GRAPHICS_BIT |
                                                                                    VK_QUEUE_COMPUTE_BIT  |
                                                                                    VK_QUEUE_TRANSFER_BIT |
                                                                                    VK_QUEUE_SPARSE_BINDING_BIT);

            assert(primaryQueueIndex != UINT32_MAX);

            familyInfo.queueFamilyIndex = primaryQueueIndex;
            //Track all queues in the family.
            for(uint32_t i = 0; i < queueFamilies[primaryQueueIndex].queueCount; i++)
            {
                //Every queue is as valuable.
                familyInfo.priorities.push_back(1.0f);
            }

            return true;
        }
    }
}
