#ifndef RAVEN_ENTITY_MANAGER_H_INCLUDED
#define RAVEN_ENTITY_MANAGER_H_INCLUDED

#include "EntityComponentSystem/RavenSystems/RenderSystem.h"
#include "EntityComponentSystem/RavenSystems/PlayerControlSystem.h"
#include "EntityComponentSystem/RavenSystems/TransformSystem.h"
#include "EntityComponentSystem/RavenSystems/LightSystem.h"
#include "EntityComponentSystem/RavenSystems/AnimationSystem.h"
#include "EntityComponentSystem/RavenComponents/Tag.h"
#include "EntityComponentSystem/RavenComponents/Skeleton.h"

#include "AssetLoader/AssetGroup.h"
#include "RavenEntities/Entity.h"
#include "EntityComponentSystem/ECSTypes.h"
#include "Utility/MemoryAllocation/Allocator.h"
#include "GraphicsApi/Vulkan/RavenVulkanManager.h"
#include "Utility/VertexUtility/TerrainUtility.h"
#include "Window/Input.h"

#include <vector>
#include <unordered_map>
#include <Utility/DelegateTypes.h>

namespace Raven
{
    //This ECS implementation follows a tutorial series made by Youtube username Rez Bot.
    class Component;
    class EntityManager
    {
    using Components = std::unordered_map<ComponentId, std::vector<Component*>>;
    using Systems = std::vector<System<>>;

    public:
        EntityManager();
        ~EntityManager();

        //Deleting default copy/move.
        EntityManager(const EntityManager&) = delete;
        EntityManager& operator=(const EntityManager&) = delete;
        EntityManager(EntityManager&&) = delete;
        EntityManager& operator=(EntityManager&&) = delete;

        //Let all systems know of an entity. This way they can check 
        //if current entity needs to be processed by them or not.
        void notifySystems(Entity* entity);

        void initialize(bool loadUI);
        void shutdown();
        void update(const Timer& timer);

        Entity* createEntity();
        void destroyEntity(EntityId id);
        const Entity& getEntity(const EntityId id) const;

        //TODO: Consider moving these functions to another file perhaps?
        /*bool loadAssetGroupIntoWorld(const AssetGroup& group);
        void createTestEntities();*/
        void generateTerrain(const TextureData& heightMap);
        void createTestLights();
        //Convenience functions for creating specific entities faster.
        Entity* createPlayerEntity();
        //Sets error material if material was not present.
        Renderable* createAndInitializeRenderable(Mesh* mesh, Material* material);
        Skeleton* createAndInitializeSkeleton();
        inline bool renderingUI() const { return m_renderSystem->renderingUI(); }
        /*const EntityId selectClickedEntity(const glm::vec3& ray) const;*/
        const Entities& getAllEntities() const { return m_entities; }
        inline const Entity& getEntity(EntityId id);
        const std::unordered_map<std::string, uint32_t>& getMeshComponentNames() const{ return m_meshSerializations; }
        const std::unordered_map<ComponentId, std::vector<Component*>>& getAllComponents() const { return m_worldComponents; }
        void setParentForEntity(EntityId entityId, EntityId parentEntity);
        void setChildrenForEntity(EntityId entityId, const std::vector<EntityId>& children);

        template <typename ComponentType>
        ComponentType* createComponent()
        {
            ComponentId id = ComponentType::ID;
            assert(m_poolAllocators.find(id) != m_poolAllocators.end() && "Trying to allocate stack object from a pool. Use function which requires size as parameter instead!");
            m_worldComponents[id].push_back((ComponentType*)m_poolAllocators[id]->allocate(sizeof(ComponentType), 8));
            return static_cast<ComponentType*>(m_worldComponents[id].back());
        }

        template <typename ComponentType>
        ComponentType* createComponent(size_t allocationSize)
        {
            ComponentId id = ComponentType::ID;
            assert(m_stackAllocators.find(id) != m_stackAllocators.end() && "Given type does not exist in stack allocators! Did you mean to allocate from a pool(same function but without size parameter)?");
            m_worldComponents[id].push_back((ComponentType*)m_stackAllocators[id]->allocate(allocationSize, 8));
            return static_cast<ComponentType*>(m_worldComponents[id].back());
        }

        template <typename ComponentType>
        const Component* getEntityComponent(const EntityId id) const
        {
            if (m_entities.find(id) != m_entities.end())
            {
                return m_entities.at(id).getComponent<ComponentType>();
            }
            return nullptr;
        }

        template<typename ComponentType>
        const std::vector<Component*>& getComponents() const
        {
            ComponentId id = ComponentType::ID;
            const auto& iterator = m_worldComponents.find(id);
            if (iterator != m_worldComponents.end())
            {
                return iterator->second;
            }
            return m_invalidComponents;
        }
    private:
        void createSystems();
        void createRootEntity();
        void allocateComponentMemoryContainers();
        /*void generateMaterialsFromAssetGroup(const AssetGroup& group, AssetGroupHierarchy& assetGroupHierarchy);
        void generateMeshesFromAssetGroup(const AssetGroup& group, AssetGroupHierarchy& assetGroupHierarchy);
        void generateRenderablesFromAssetGroup(const AssetGroup& group, AssetGroupHierarchy& assetGroupHierarchy);*/
        void createErrorAssets();
        //void changeEntityMesh(EntityId id, uint32_t name);
        ////Convenience functions.
        void addComponentToEntity(ComponentId componentId, EntityId entityId, void* data);
        void addChildForEntity(Entity& entity, EntityId childId);

        Entities m_entities;
        Entity* m_root;
        //All the components in the whole world.
        Components m_worldComponents;

        RenderSystem* m_renderSystem = nullptr;
        PlayerControlSystem* m_playerControlSystem = nullptr;
        TransformSystem* m_transformSystem = nullptr;
        LightSystem* m_lightSystem = nullptr;
        AnimationSystem* m_animationSystem = nullptr;
        std::array<BaseSystem*, 5> m_systems;

        size_t m_entityCount;

        //Invalid objects.
        Entity m_invalidEntity;
        std::vector<Component*> m_invalidComponents;

        //Experimental:
        std::unordered_map<ComponentId, Allocator*> m_poolAllocators;
        std::unordered_map<ComponentId, Allocator*> m_stackAllocators;

        RavenVulkanManager m_vulkanManager;

        //Serializations and their corresponding names. Before allocating new memory, check if component already exists.
        std::unordered_map<std::string, uint32_t> m_materialSerializations;
        std::unordered_map<std::string, uint32_t> m_meshSerializations;

        //Components corresponding to a serialization. Keeping two lists might be overkill and redundant. Instead store
        //component pointers by name?
        std::unordered_map<uint32_t, Component*> m_materialComponentPointers;
        std::unordered_map<uint32_t, Component*> m_meshComponentPointers;

        std::unordered_map<EntityId, std::vector<EntityId>> m_entityRelations;

        //Duplicate information, this same data is also in RenderSystem.h.
        Transform* m_playerTransformComponent = nullptr;
        Camera* m_playerCameraComponent = nullptr;

        //Objects that point missing assets and special items.
        Material m_missingMaterial;
        MaterialData m_missingMaterialData;
        TextureData m_missingAlbedo;
        TextureData m_missingNormal;

        Material m_lightMaterial;
        TextureData m_lightTexture;

        //Remove
        glm::vec3 m_terrainColor = glm::vec3(1.0);

        std::unordered_map<EntityId, PushConstantData> m_entityPushConstantDatas;
    };
}
#endif