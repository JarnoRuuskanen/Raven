#ifndef RAVEN_SWAPCHAIN_H_INCLUDED
#define RAVEN_SWAPCHAIN_H_INCLUDED
#include "GraphicsApi/Vulkan/Common.h"
#include "GraphicsApi/Vulkan/VulkanUtility/VulkanStructs.h"

namespace Raven
{
    struct Swapchain
    {
        VkSwapchainKHR swapchain;
        std::vector<VkImage> swapchainImages;
        std::vector<VkImageView> swapchainImageViews;
        VkFormat swapchainImageFormat;
        VkExtent2D swapChainExtent;
    };
}

#endif