#ifndef RAVEN_PLAYER_CONTROL_SYSTEM_H_INCLUDED
#define RAVEN_PLAYER_CONTROL_SYSTEM_H_INCLUDED

#include "EntityComponentSystem/RavenComponents/Transform.h"
#include "EntityComponentSystem/RavenComponents/Camera.h"
#include "EntityComponentSystem/RavenSystems/System.h"

//Do these need to be entangled?
#include <GLFW/glfw3.h>

namespace Raven
{
    class PlayerControlSystem : public System<Transform, Camera>
    {
    public:
        using BaseInputType = System<Transform, Camera>;
        PlayerControlSystem(EntityManager* pGameWorld);
        void update(const Timer& timer) override;
    private:
        void entityAddedToSystem(Entity* entity) override;
        void moveForwards(Transform* transform, Camera* camera, float speed);
        void moveBackwards(Transform* transform, Camera* camera, float speed);
        void moveLeft(Transform* transform, Camera* camera, float speed);
        void moveRight(Transform* transform, Camera* camera, float speed);
        void moveUp(Transform* transform, Camera* camera, float speed);
        void moveDown(Transform* transform, Camera* camera, float speed);
        bool handleMouseMovementCallback(float xpos, float ypos);
    };
}

#endif