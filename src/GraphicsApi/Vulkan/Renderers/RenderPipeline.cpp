#include "GraphicsApi/Vulkan/Renderers/RenderPipeline.h"
#include "Utility/DelegateManager.h"
#include "GraphicsApi/Vulkan/VulkanUtility/StructInitializer.h"
#include "GraphicsApi/Vulkan/VulkanUtility/DescriptorUtility.h"
#include "GraphicsApi/Vulkan/DescriptorWriteHelper.h"

#include <vector>
#include <GraphicsApi/Vulkan/VulkanUtility/ImageUtility.h>

namespace Raven
{
    RenderPipeline::~RenderPipeline()
    {
        const auto& context = DelegateManager::getInstance().getVulkanContextDelegates().getVulkanContextFunc.Invoke();
        destroyPipeline(context.device);
    }

    bool RenderPipeline::initialize(const VkDevice& device, RenderTargetType type, const std::vector<BaseSubpass*> &subpasses, 
        const std::vector<VkAttachmentDescription>& renderPassAttachments, const std::vector<VkClearValue>& attachmentClearValues, const VkImageView* depthFromPreviousPass)
    {
        assert(!attachmentClearValues.empty());
        for (auto& val : attachmentClearValues)
        {
            m_clearValues.push_back(val);
        }
        assert(!renderPassAttachments.empty());
        for (auto& attachmentDescription : renderPassAttachments)
        {
            m_renderPassAttachments.push_back(attachmentDescription);
        }

        const WindowDelegates& delegates = DelegateManager::getInstance().getWindowDelegates();

        uint32_t windowWidth = delegates.getWindowWidthFunc.Invoke();
        uint32_t windowHeight = delegates.getWindowHeightFunc.Invoke();
        m_extent = {windowWidth, windowHeight};
        const Swapchain& swapchain = DelegateManager::getInstance().getRendererDelegates().getSwapchainFunc.Invoke();
        m_swapchainLength = static_cast<uint32_t>(swapchain.swapchainImages.size());
        m_swapchainImageFormat = swapchain.swapchainImageFormat;
        m_currentTargetType = type;

        bool success = initializeRenderPass(device, subpasses);
        assert(success && "Failed to create a renderpass for render pipeline!");

        //Now that render pass is initialized, initialize subpass pipelines
        success = initializeSubpassPipelines(device, subpasses);
        assert(success && "Failed to initialize subpass pipelines!");

        gatherSubpassDescriptorSetLayoutBindings(subpasses);
        generateDescriptorPool(device);
        createCommandPoolAndBuffers(device);

        success = createRenderTarget(device, depthFromPreviousPass);
        assert(success);

        return success;
    }

    void RenderPipeline::gatherSubpassDescriptorSetLayoutBindings(const std::vector<BaseSubpass*>& subpasses)
    {
        m_subpassDescriptorSetLayoutBindings.clear();
        m_subpassDescriptorSetLayoutBindings.shrink_to_fit();
        
        for (const auto& subpass : subpasses)
        {
            auto& descriptorInfos = subpass->getShaderDescriptorBindingInfos();
            for (auto& descriptorInfo : descriptorInfos)
            {
                bool alreadyExists = false;
                for (auto& existingBinding : m_subpassDescriptorSetLayoutBindings)
                {
                    if (ShaderUtility::descriptorsMatch(existingBinding, descriptorInfo.layoutBinding))
                    {
                        alreadyExists = true;
                        existingBinding.stageFlags |= descriptorInfo.layoutBinding.stageFlags;
                        existingBinding.descriptorCount++;
                        break;
                    }
                }
                if (!alreadyExists)
                {
                    m_subpassDescriptorSetLayoutBindings.push_back(descriptorInfo.layoutBinding);
                }
            }
        }
    }

    void RenderPipeline::destroyPipeline(const VkDevice& device)
    {
        for (size_t i = 0; i < m_renderTarget.targetFramebuffers.size(); i++)
        {
            VkFramebuffer& frameBuffer = m_renderTarget.targetFramebuffers[i];
            if (frameBuffer != VK_NULL_HANDLE)
            {
                vkDestroyFramebuffer(device, frameBuffer, nullptr);
            }
        }

        for (auto& poolBufferPair : m_commandBuffers)
        {
            vkFreeCommandBuffers(device, poolBufferPair.pool, 
                                 static_cast<uint32_t>(poolBufferPair.buffers.size()), 
                                 poolBufferPair.buffers.data());
        }

        for (auto& subpass : m_subPasses)
        {
            subpass->destroyPipeline(device);
        }

        vkDestroyRenderPass(device, m_renderPass, nullptr);
    }

    void RenderPipeline::generateDescriptorPool(const VkDevice& device)
    {
        std::vector<VkDescriptorPoolSize> poolSizes = {};
        for (const auto& binding : m_subpassDescriptorSetLayoutBindings)
        {
            bool typeExists = false;
            for (auto& poolSize : poolSizes)
            {
                if (poolSize.type == binding.descriptorType)
                {
                    typeExists = true;
                    break;
                }
            }

            if (!typeExists)
            {
                poolSizes.push_back({ binding.descriptorType, 1000 });
            }
        }

        VkResult result = DescriptorUtility::createDescriptorPool(device, poolSizes, 1000, true, m_descriptorPool);
        assert(result == VK_SUCCESS);
    }

    void RenderPipeline::gatherSubpassAttachmentInfo(const std::vector<BaseSubpass*>& subpasses)
    {
        m_subpassDescriptions.clear();
        m_subpassDescriptions.shrink_to_fit();

        m_subpassDependencies.clear();
        m_subpassDependencies.shrink_to_fit();

        m_subPasses.clear();
        m_subPasses.shrink_to_fit();

        for(auto& subpass : subpasses)
        {
            m_subpassDescriptions.push_back(subpass->getSubpassDescription());
            m_subpassDependencies.push_back(subpass->getSubpassDependency());
            m_subPasses.push_back(subpass);
        }
    }

    bool RenderPipeline::initializeRenderPass(const VkDevice& device, const std::vector<BaseSubpass*> &subpasses)
    {
        assert(!subpasses.empty());

        gatherSubpassAttachmentInfo(subpasses);
        resolveRenderPassAttachmentReferences();

        VkRenderPassCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        createInfo.attachmentCount = static_cast<uint32_t>(m_renderPassAttachments.size());;
        createInfo.pAttachments = m_renderPassAttachments.empty() ? nullptr : m_renderPassAttachments.data();
        createInfo.subpassCount = static_cast<uint32_t>(m_subpassDescriptions.size());
        createInfo.pSubpasses = m_subpassDescriptions.empty() ? nullptr : m_subpassDescriptions.data();
        createInfo.dependencyCount = static_cast<uint32_t>(m_subpassDependencies.size());
        createInfo.pDependencies = m_subpassDependencies.empty() ? nullptr : m_subpassDependencies.data();

        VkResult result = vkCreateRenderPass(device, &createInfo, nullptr, &m_renderPass);
        if(result != VK_SUCCESS)
        {
            std::cerr << "Failed to create a render pass!\n";
            assert(result == VK_SUCCESS);
            return false;
        }

        return true;
    }

    bool RenderPipeline::initializeSubpassPipelines(const VkDevice &device,
                                                  const std::vector<BaseSubpass *> &subpasses)
    {
        for(size_t subpassIndex = 0; subpassIndex < m_subPasses.size(); subpassIndex++)
        {
            if(!m_subPasses[subpassIndex]->initPipeline(device, m_renderPass, subpassIndex))
            {
                std::cerr << "Failed to create a pipeline for subpass!\n";
                return false;
            }
        }
        return true;
    }

    bool RenderPipeline::draw(VkCommandBuffer cmdBuffer, uint32_t imageIndex)
    {
        VkCommandBufferBeginInfo beginInfo = {};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
        beginInfo.pInheritanceInfo = nullptr;

        VkViewport viewport = {};
        viewport.x = 0.0f;
        viewport.y = 0.0f;
        viewport.width = (float)m_extent.width;
        viewport.height = (float)m_extent.height;
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;
        std::array<VkViewport, 4> viewports;
        viewports[0] = viewport;
        viewports[1] = viewport;
        viewports[2] = viewport;
        viewports[3] = viewport;

        if(vkBeginCommandBuffer(cmdBuffer, &beginInfo) != VK_SUCCESS)
        {
            std::cerr << "Failed to begin command buffer recording for buffer with index << " << imageIndex << "\n";
            return false;
        }

        VkRenderPassBeginInfo renderPassInfo = {};
        renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfo.renderPass = m_renderPass;
        renderPassInfo.renderArea.offset = {0,0};
        renderPassInfo.renderArea.extent = m_extent;
        renderPassInfo.clearValueCount = static_cast<uint32_t>(m_clearValues.size());
        renderPassInfo.pClearValues = m_clearValues.data();
        renderPassInfo.framebuffer = m_renderTarget.targetFramebuffers[imageIndex];

        vkCmdBeginRenderPass(cmdBuffer, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

        vkCmdSetViewport(cmdBuffer, 0, static_cast<uint32_t>(viewports.size()), viewports.data());

        for(size_t i = 0; i < m_subPasses.size(); i++)
        {
            auto& subpass = m_subPasses[i];
            if(i != 0)
            {
                vkCmdNextSubpass(cmdBuffer, VK_SUBPASS_CONTENTS_INLINE);
            }
            for (size_t k = 0; k < m_renderables.size(); k++)
            {
                auto& renderable = m_renderables[k];
                if (renderable->m_isVisible)
                {
                    //Pass renderable descriptor sets to subpasses. They can either use them or not use them.
                    //NOTE: Since renderables are unique, wouldn't it be wiser to switch back to renderables holding 
                    //their own descriptors? They know the best what kind of data is held within.
                    //Here we could query for all the descriptor sets that the renderable's entity has, pass them all 
                    //to the subpass draw call and subpass could then choose which ones of those descriptors it wants 
                    //to bind. This would require some sort of mapping probably.

                    std::vector<VkDescriptorSet> descriptorSets = m_renderableDescriptorSets[k];
                    //Additional descriptor sets can be queried from inside the renderable.
                    Skeleton* skeleton = renderable->m_mesh->skeleton;
                    if (skeleton)
                    {
                        descriptorSets.push_back(skeleton->m_descriptorSet);
                    }
                    const std::vector<PushConstantData*>& pushConstantDatas = m_renderablePushConstants[k];

                    //Call subpass draw for each renderable.
                    subpass->draw(renderable->m_mesh->m_vertexBuffer, renderable->m_mesh->m_indexBuffer, descriptorSets, pushConstantDatas, cmdBuffer);
                }
            }
        }
        vkCmdEndRenderPass(cmdBuffer);

        vkEndCommandBuffer(cmdBuffer);
        return true;
    }

    void RenderPipeline::registerDataForDrawing(const VkDevice& device, const std::vector<PushConstantData*>& pushConstants, Renderable* renderable)
    {
        //Only add renderable if it doesn't already exist in this bundle.
        auto it = std::find(m_renderables.begin(), m_renderables.end(), renderable);
        if(it == m_renderables.end())
        {
            if (allocateDescriptorSetsForRenderable(device, renderable))
            {
                m_renderables.push_back(renderable);
                storePushConstantPointers(pushConstants);
            }
        } else
        {
            std::cout << "Renderable was already assigned to this render pipeline.\n";
        }
    }

    void RenderPipeline::removeRenderableFromRenderPipeline(const VkDevice& device, Renderable* renderable)
    {
        if (renderable)
        {
            for (size_t i = 0; i < m_renderables.size(); i++)
            {
                if(m_renderables[i] == renderable)
                {
                    m_renderables.erase(m_renderables.begin() + i);
                    //Remember to also free the descriptor sets and resize the vector.
                    auto& descriptorSets = m_renderableDescriptorSets[i];
                    VkResult result = vkFreeDescriptorSets(device, m_descriptorPool, static_cast<uint32_t>(descriptorSets.size()), descriptorSets.data());
                    assert(result == VK_SUCCESS);
                    m_renderableDescriptorSets.erase(m_renderableDescriptorSets.begin() + i);
                    m_renderablePushConstants.erase(m_renderablePushConstants.begin() + i);
                    break;
                }
            }
            std::cout << "Size after: " << m_renderables.size();
        }
    }

    void RenderPipeline::clearAllRenderables(const VkDevice &device)
    {
        m_renderables.clear();
        m_renderables.shrink_to_fit();
        for(size_t i = 0; i < m_renderableDescriptorSets.size(); i++)
        {
            auto& descriptorSets = m_renderableDescriptorSets[i];
            VkResult result = vkFreeDescriptorSets(device, m_descriptorPool, static_cast<uint32_t>(descriptorSets.size()), descriptorSets.data());
            assert(result == VK_SUCCESS);
        }
        m_renderableDescriptorSets.clear();
        m_renderableDescriptorSets.shrink_to_fit();
        m_renderablePushConstants.clear();
        m_renderablePushConstants.shrink_to_fit();
    }

    bool RenderPipeline::allocateDescriptorSetsForRenderable(const VkDevice& device, Renderable*& renderable)
    {
        //This descriptor set -vector will be stored to a vector of vectors and will correspond to the same index in m_renderables.
        std::vector<ShaderDescriptorMatchInfo> matchInfos;
        getRenderableShaderData(renderable, matchInfos);
        if (matchInfos.empty())
        {
            return false;
        }

        std::vector<VkDescriptorSet> descriptorSets;
        bool success = true;
        for (auto& subpass : m_subPasses)
        {
            //Binding is used as key here.
            std::unordered_map<uint32_t,VkDescriptorBufferInfo> matchingBufferInfos{};
            std::unordered_map<uint32_t,VkDescriptorImageInfo> matchingImageInfos{};

            const auto& shaderDescriptorInfos = subpass->getShaderDescriptorBindingInfos();
            //Go through all descriptor bindings and see if this renderable has descriptors with 
            //a matching name for this subpass. Each shader names its descriptors and those names
            //are compared here to the ShadreMatchInfo of the renderable + its material.
            for (auto& info : shaderDescriptorInfos)
            {
                for (auto& matchInfo : matchInfos)
                {
                    if ((matchInfo.name == info.secondaryName || matchInfo.name == info.primaryName) 
                        && matchInfo.type == info.layoutBinding.descriptorType)
                    {
                        if (info.layoutBinding.descriptorType == VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER)
                        {
                            VkDescriptorBufferInfo bufferInfo = {};
                            bufferInfo.buffer = renderable->m_uniformBuffer.buffer;
                            bufferInfo.offset = 0;
                            bufferInfo.range = sizeof(UniformBufferObject);
                            matchingBufferInfos.insert(std::make_pair(info.layoutBinding.binding, bufferInfo));
                        }
                        if (info.layoutBinding.descriptorType == VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER)
                        {
                            VkDescriptorImageInfo imageInfo = {};
                            imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
                            //This basically means that materials have to be always bound to the same index. Albedo to 0, normal to 1 etc.
                            uint32_t imageIndex = getImageIndexBasedOnName(matchInfo.name);
                            imageInfo.imageView = renderable->m_material->m_images[imageIndex].imageView;
                            imageInfo.sampler = renderable->m_material->m_images[imageIndex].sampler;
                            matchingImageInfos.insert(std::make_pair(info.layoutBinding.binding, imageInfo));
                        }
                    }
                }
            }
            if (!matchingBufferInfos.empty() || !matchingImageInfos.empty())
            {
                std::vector<VkWriteDescriptorSet> writes = {};

                //Layouts will hold renderable descriptor layouts at index 0, light descriptors at index 1.
                const auto& layouts = subpass->getDescriptorSetLayouts();
                std::vector<VkDescriptorSetLayout> renderableLayout = {layouts[0]};
                descriptorSets.emplace_back(VkDescriptorSet());
                VkDescriptorSet& descriptorSet = descriptorSets.back();
                VkResult result = DescriptorUtility::allocateDescriptorSet(device, m_descriptorPool, renderableLayout, descriptorSet);

                if (result != VK_SUCCESS)
                {
                    assert(result == VK_SUCCESS);
                    return false;
                }

                for (auto& bufferInfo : matchingBufferInfos)
                {
                    VkWriteDescriptorSet bufferWrite =
                                    StructInitializer::getWriteDescriptorSet(descriptorSet, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, bufferInfo.first, &bufferInfo.second);
                    writes.push_back(bufferWrite);
                }

                for (auto& imageInfo : matchingImageInfos)
                {
                    VkWriteDescriptorSet imageWrite = 
                        StructInitializer::getWriteDescriptorSet(descriptorSet, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, imageInfo.first, &imageInfo.second);
                    writes.push_back(imageWrite);
                }

                if (!writes.empty())
                {
                    vkUpdateDescriptorSets(device, static_cast<uint32_t>(writes.size()), writes.data(), 0, nullptr);
                }
            }
        }
        if (!descriptorSets.empty())
        {
            m_renderableDescriptorSets.push_back(descriptorSets);
        }
        return success;
    }

    void RenderPipeline::storePushConstantPointers(const std::vector<PushConstantData*>& pushConstants)
    {
        m_renderablePushConstants.push_back(pushConstants);
    }

    void RenderPipeline::createCommandPoolAndBuffers(const VkDevice& device)
    {
        //A command buffer for each frame.
        assert(m_swapchainLength > 0);
        m_commandBuffers.resize(m_swapchainLength);
        std::cout << "Remove hard coded queueFamilyIndex!\n";

        for (size_t i = 0; i < m_swapchainLength; i++)
        {
            VkCommandPoolCreateInfo poolInfo = {};
            poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
            poolInfo.pNext = nullptr;
            poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
            poolInfo.queueFamilyIndex = 0;
            VkResult result = vkCreateCommandPool(device, &poolInfo, nullptr, &m_commandBuffers[i].pool);
            assert(result == VK_SUCCESS);

            VkCommandBuffer buffer;
            VkCommandBufferAllocateInfo allocInfo = {};
            allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
            allocInfo.commandBufferCount = 1;
            allocInfo.commandPool = m_commandBuffers[i].pool;
            allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
            allocInfo.pNext = nullptr;
            result = vkAllocateCommandBuffers(device, &allocInfo, &buffer);
            assert(result == VK_SUCCESS);

            m_commandBuffers[i].buffers.push_back(buffer);
        }
    }

    void RenderPipeline::setTimelineWaitAndSignalValue(uint64_t waitForTimelineValue, uint64_t signalTimelineValue)
    {
        m_waitForTimelineValue = waitForTimelineValue;
        m_signalTimelineValue = signalTimelineValue;

        m_submitInfo = {};
        m_submitInfo.sType = VK_STRUCTURE_TYPE_TIMELINE_SEMAPHORE_SUBMIT_INFO_KHR;
        m_submitInfo.pNext = nullptr;
        m_submitInfo.waitSemaphoreValueCount = 1;
        m_submitInfo.pWaitSemaphoreValues = &m_waitForTimelineValue;
        m_submitInfo.signalSemaphoreValueCount = 1;
        m_submitInfo.pSignalSemaphoreValues = &m_signalTimelineValue;
    }

    bool RenderPipeline::createRenderTarget(const VkDevice &device, const VkImageView* depthFromPreviousPass)
    {
        //1. Go through each subpass attachment description.
        //2. Gather their respective information and look for duplicate bindings
        //3. Create resources that are required for drawing throughout all the stages.
        //4. TODO: Figure out if swap chain framebuffer should be added as one of the outputs or not.
        //    Maybe a boolean flag which tells wether to draw final result to swapchain or to
        //    the RenderPipeline framebuffer which can then be read elsewhere?
        //resolveRenderPassAttachmentReferences();

        assert(m_extent.height > 0);
        assert(m_extent.width > 0);

        //Now I should have information about all the input attachments required in each stage.
        //Create memory backed images/depth resources and their respective image views next.
        bool success = createRenderTargetAttachments(device, depthFromPreviousPass);
        assert(success);

        success = createFramebuffers(device, depthFromPreviousPass);
        assert(success);

        return success;
    }

    bool isNewAttachment(const std::vector<VkAttachmentReference>& existingReferences, const VkAttachmentReference& newReference)
    {
        for(const auto& reference : existingReferences)
        {
            if(reference.attachment == newReference.attachment)
            {
                //Small debug print in case there is ever a mixup in layouts.
                if(reference.layout != newReference.layout)
                {
                    std::cerr << "Attachment reference has same attachment index but different layout!\n";
                }
                return false;
            }
        }
        return true;
    }

    void RenderPipeline::resolveRenderPassAttachmentReferences()
    {
        assert(!m_subpassDescriptions.empty());

        m_renderPassColorReferences.clear();
        m_renderPassDepthReferences.clear();

        for(auto& description : m_subpassDescriptions)
        {
            //TODO: Not sure yet what to do about these. Input attachments are the
            //attahcments that are passed from subpass to subpass and are
            //usually read from before writing to a (possibly) different output.
            for(size_t i = 0; i < description.inputAttachmentCount; i++)
            {

            }

            for(size_t i = 0; i < description.colorAttachmentCount; i++)
            {
                auto& reference = description.pColorAttachments[i];
                if(isNewAttachment(m_renderPassColorReferences, reference))
                {
                    m_renderPassColorReferences.push_back(reference);
                }
            }

            if(description.pDepthStencilAttachment)
            {
                auto& depthReference = description.pDepthStencilAttachment[0];
                if(isNewAttachment(m_renderPassDepthReferences, depthReference))
                {
                    m_renderPassDepthReferences.push_back(depthReference);
                }
            }
        }
    }

    bool RenderPipeline::createRenderTargetAttachments(const VkDevice& device, const VkImageView* depthFromPreviousPass)
    {
        const RavenVulkanManagerDelegates& delegates = DelegateManager::getInstance().getVulkanManagerDelegates();
        m_renderTarget.targetImageFormat = m_swapchainImageFormat;

        //A single image needs to be created for each attachment so no need to create swapchainlength amount of images.
        if (!m_renderPassColorReferences.empty())
        {
            bool success = delegates.createRavenImagesFunc.Invoke(m_swapchainImageFormat, m_extent, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT, 
                                                                  VK_IMAGE_ASPECT_COLOR_BIT, static_cast<uint32_t>(m_renderPassColorReferences.size()), m_renderTarget.targetImageAttachments);
            if (!success)
            {
                assert(success);
                return false;
            }
        }

        if (!m_renderPassDepthReferences.empty())
        {
            VkFormat depthFormat = VK_FORMAT_D32_SFLOAT;
            bool success = delegates.createRavenImagesFunc.Invoke(depthFormat, m_extent, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT,
                VK_IMAGE_ASPECT_DEPTH_BIT, 1, m_renderTarget.targetDepthAttachments);

            if (!success)
            {
                assert(success);
                return false;
            }
        }
        return true;
    }

    bool RenderPipeline::createFramebuffers(const VkDevice& device, const VkImageView* depthFromPreviousPass)
    {
        uint32_t attachmentCount = static_cast<uint32_t>(m_renderTarget.targetImageAttachments.size() + m_renderTarget.targetDepthAttachments.size());
        if(m_currentTargetType == RenderTargetType::SWAPCHAIN)
        {
            std::cerr << "Currently renderpipeline assumes that attachment N.0 is always swapchain image view.This should maybe be fixed.\n";
        }

        std::vector<VkImageView> attachments(attachmentCount);

        const auto& swapchain = DelegateManager::getInstance().getRendererDelegates().getSwapchainFunc.Invoke();

        VkFramebufferCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        createInfo.renderPass = m_renderPass;
        createInfo.attachmentCount = attachmentCount;
        createInfo.pAttachments = attachments.data();
        createInfo.width = m_extent.width;
        createInfo.height = m_extent.height;
        createInfo.layers = 1;
        m_renderTarget.targetFramebuffers.resize(m_swapchainLength);

        //Create the framebuffers
        for (size_t i = 0; i < m_swapchainLength; i++)
        {
            if (m_currentTargetType == RenderTargetType::SWAPCHAIN)
            {
                uint32_t currentIndex = 0;
                attachments[currentIndex] = swapchain.swapchainImageViews[i];
                currentIndex++;

                for (size_t attachmentIndex = currentIndex; attachmentIndex < m_renderTarget.targetImageAttachments.size(); attachmentIndex++)
                {
                    attachments[attachmentIndex] = m_renderTarget.targetImageAttachments[attachmentIndex].imageView;
                    currentIndex++;
                }
                if (depthFromPreviousPass)
                {
                    attachments[currentIndex] = *depthFromPreviousPass;
                    currentIndex++;
                }
                else if (!m_renderTarget.targetDepthAttachments.empty())
                {
                    attachments[currentIndex] = m_renderTarget.targetDepthAttachments[0].imageView;
                    currentIndex++;
                }
                VkResult result = vkCreateFramebuffer(device, &createInfo, nullptr, &m_renderTarget.targetFramebuffers[i]);
                if (result != VK_SUCCESS)
                {
                    m_renderTarget.targetFramebuffers.clear();
                    std::cerr << "Failed to create swapchain framebuffers!" << std::endl;
                    return false;
                }
            }
            else if (m_currentTargetType == RenderTargetType::OFFSCREEN)
            {
                uint32_t currentIndex = 0;
                for (size_t attachmentIndex = currentIndex; attachmentIndex < m_renderTarget.targetImageAttachments.size(); attachmentIndex++)
                {
                    attachments[currentIndex] = m_renderTarget.targetImageAttachments[attachmentIndex].imageView;
                    currentIndex++;
                }
                if (!m_renderTarget.targetDepthAttachments.empty())
                {
                    attachments[currentIndex] = m_renderTarget.targetDepthAttachments[0].imageView;
                    currentIndex++;
                }
                VkResult result = vkCreateFramebuffer(device, &createInfo, nullptr, &m_renderTarget.targetFramebuffers[i]);
                if (result != VK_SUCCESS)
                {
                    m_renderTarget.targetFramebuffers.clear();
                    std::cerr << "Failed to create swapchain framebuffers!" << std::endl;
                    return false;
                }
            }
        }
        return true;
    }

    bool RenderPipeline::setRenderTarget(RenderTarget& renderTarget, const RenderTargetType& targetType, const VkImageView* depthFromPreviousPass)
    {
        const RavenVulkanContext& context = DelegateManager::getInstance().getVulkanContextDelegates().getVulkanContextFunc.Invoke();
        //Wait for device to idle as we don't want to touch the resources if they are still in use.
        vkDeviceWaitIdle(context.device);

        if(m_currentTargetType != targetType)
        {
            m_currentTargetType = targetType;
            destroyAttachments();
        }
        return createRenderTarget(context.device, depthFromPreviousPass);
    }

    void RenderPipeline::destroyAttachments()
    {
        const RavenVulkanContext& context = DelegateManager::getInstance().getVulkanContextDelegates().getVulkanContextFunc.Invoke();
        const VulkanResourceManagerDelegates& delegates = DelegateManager::getInstance().getVulkanResourceManagerDelegates();

        for(size_t i = 0; i < m_renderTarget.targetFramebuffers.size(); i++)
        {
            vkDestroyFramebuffer(context.device, m_renderTarget.targetFramebuffers[i], nullptr);
        }
        m_renderTarget.targetFramebuffers.clear();
        m_renderTarget.targetFramebuffers.shrink_to_fit();

        for(auto& attachment : m_renderTarget.targetImageAttachments)
        {
            vkDestroyImageView(context.device, attachment.imageView, nullptr);
            delegates.destroyAllocatedImageFunc.Invoke(attachment);
        }
        m_renderTarget.targetImageAttachments.clear();
        m_renderTarget.targetImageAttachments.shrink_to_fit();

        for(size_t i = 0; i < m_renderTarget.targetDepthAttachments.size(); i++)
        {
            vkDestroyImageView(context.device, m_renderTarget.targetDepthAttachments[i].imageView, nullptr);
            delegates.destroyAllocatedImageFunc.Invoke(m_renderTarget.targetDepthAttachments[i]);
        }
        m_renderTarget.targetDepthAttachments.clear();
        m_renderTarget.targetDepthAttachments.shrink_to_fit();
    }
}