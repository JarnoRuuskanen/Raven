#ifndef RAVEN_SCENE_CREATOR_H_INCLUDED
#define RAVEN_SCENE_CREATOR_H_INCLUDED
#include <vector>
#include "AssetLoader/AssetGroup.h"
#include "EntityComponentSystem/EntityManager.h"

namespace Raven
{
    struct MeshPrimitive
    {
        Mesh* mesh;
        int materialIndex;
    };

    struct MeshGeometry
    {
        std::vector<MeshPrimitive> primitives;
    };
    //Describes entity relations for an asset group.
    struct AssetGraph
    {
        //This meshes-vector maps to gltf mesh index.
        //So that means group.node.mesh index.
        std::vector<MeshGeometry> meshes;
        std::vector<Material*> materials;
        std::vector<Animation*> animations;
        std::vector<Skeleton*> skeletons;
    };

    class SceneCreator
    {
    public:
        void createSceneGraphFromAssets(EntityManager* entityManager, const std::vector<AssetGroup>& assets);
    private:
        void createMeshComponents(EntityManager* entityManager, const RavenVulkanManagerDelegates& vulkanManagerDelegates, const AssetGroup& group, AssetGraph& graph);
        void createMaterialComponents(EntityManager* entityManager, const RavenVulkanManagerDelegates& vulkanManagerDelegates, const AssetGroup& group, AssetGraph& graph);
        void createAnimationComponents(EntityManager* entityManager, const AssetGroup& group, AssetGraph& graph);
        void createSkeletonComponents(EntityManager* entityManager, const AssetGroup& group, AssetGraph& graph);
        void createEntitiesFromAssetGroup(EntityManager* entityManager, const RavenVulkanManagerDelegates& vulkanManagerDelegates, const AssetGroup& group, AssetGraph& graph);
        void createTransformComponent(EntityManager* manager, const TransformData& data, Entity* entity);
        Entity* createMeshEntity(EntityManager* manager, const NodeData& node, const AssetGraph& graph);
    };
}

#endif