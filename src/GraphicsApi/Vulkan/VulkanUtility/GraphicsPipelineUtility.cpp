#include <GraphicsApi/Vulkan/VulkanUtility/DescriptorUtility.h>
#include "GraphicsApi/Vulkan/VulkanUtility/VulkanStructs.h"
#include "GraphicsApi/Vulkan/VulkanUtility/GraphicsPipelineUtility.h"
#include "Utility/ShaderUtility/ShaderProcessor.h"
#include "GraphicsApi/Vulkan/VulkanUtility/UtilityFunctions.h"
#include "GraphicsApi/Vulkan/VulkanUtility/StructInitializer.h"

namespace Raven
{
    namespace GraphicsPipelineUtility
    {
        VkResult createNonColorBlendingGraphicsPipeline(const VkDevice& device,
                                                        const VkExtent2D& swapchainExtent,
                                                        const VkRenderPass& renderPass,
                                                        const std::vector<VkDescriptorSetLayout>& descriptorSetLayouts,
                                                        const std::vector<ShaderLoadInformation>& shadersToLoad,
                                                        const std::vector<VkVertexInputBindingDescription>& vertexInputBindings,
                                                        const std::vector<VkVertexInputAttributeDescription>& vertexInputAttributes,
                                                        VkCullModeFlagBits cullMode, VkFrontFace frontFace, VkPolygonMode polygonMode,
                                                        VkPrimitiveTopology topology, GraphicsPipeline& graphicsPipeline)
        {
            std::vector<ShaderStageData> stageDatas;
            UtilityFunctions::fillShaderStageData(device, shadersToLoad, stageDatas);
            std::vector<VkPipelineShaderStageCreateInfo> stages = {};
            for (size_t i = 0; i < stageDatas.size(); i++)
            {
                stages.push_back(stageDatas[i].createInfo);
            }

            //Vertex input
            VkPipelineVertexInputStateCreateInfo vertexInputInfo = StructInitializer::getPipelineVertexInputStateCreateInfo(vertexInputBindings, vertexInputAttributes);

            //What kind of geometry to draw from vertices and if primitive restart should be enabled.
            VkPipelineInputAssemblyStateCreateInfo inputAssembly = StructInitializer::getPipelineInputAssemblyStateCreateInfo(topology, VK_FALSE);

            //Viewport
            VkViewport viewport = {};
            viewport.x = 0.0f;
            viewport.y = 0.0f;
            viewport.width = (float)swapchainExtent.width;
            viewport.height = (float)swapchainExtent.height;
            viewport.minDepth = 0.0f;
            viewport.maxDepth = 1.0f;
            std::vector<VkViewport> viewports = {viewport};

            //Scissor
            VkRect2D scissor = {};
            scissor.offset = {0,0};
            scissor.extent = swapchainExtent;
            std::vector<VkRect2D> scissors = {scissor};

            //Viewport state
            VkPipelineViewportStateCreateInfo viewportState = StructInitializer::getPipelineViewportStateCreateInfo(viewports, scissors);

            //Rasterizer
            VkPipelineRasterizationStateCreateInfo rasterizer =
                    StructInitializer::getPipelineRasterizationStateCreateInfo(VK_FALSE, VK_FALSE, polygonMode,
                                                                               1.0f, cullMode, frontFace,
                                                                               VK_FALSE, 0.0f, 0.0f, 0.0f);

            //Depth stencil testing.
            VkPipelineDepthStencilStateCreateInfo depthStencil =
                    StructInitializer::getPipelineDepthStencilStateCreateInfo(VK_TRUE, VK_TRUE, VK_COMPARE_OP_LESS, VK_FALSE, 0.0f, 1.0f,
                                                                              VK_FALSE, {}, {});

            //Multisampling.
            VkPipelineMultisampleStateCreateInfo multisampling =
                    StructInitializer::getPipelineMultisampleStateCreateInfo(VK_FALSE, VK_SAMPLE_COUNT_1_BIT, 1.0f, nullptr, VK_FALSE, VK_FALSE);

            //Color blending.
            VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
            colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
            colorBlendAttachment.blendEnable = VK_FALSE;
            std::vector<VkPipelineColorBlendAttachmentState> colorBlendAttachments = {colorBlendAttachment};

            VkPipelineColorBlendStateCreateInfo colorBlending =
                    StructInitializer::getPipelineColorBlendStateCreateInfo(VK_FALSE, VK_LOGIC_OP_COPY, colorBlendAttachments);

            //Dynamic state. These state datas will need to be specified at drawing time.
            std::vector<VkDynamicState> dynamicStates = {VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_LINE_WIDTH};
            VkPipelineDynamicStateCreateInfo dynamicState = StructInitializer::getPipelineDynamicStateCreateInfo(dynamicStates);

            //Pipeline layout.
            //Here we tell what kind of descriptors (uniforms, buffers etc.) and push constants go to the pipeline.
            VkPipelineLayoutCreateInfo pipelineLayoutInfo = StructInitializer::getPipelineLayoutCreateInfo(descriptorSetLayouts, {});

            VkResult result = vkCreatePipelineLayout(device, &pipelineLayoutInfo, nullptr, &graphicsPipeline.layout);
            CHECK_RESULT_FOR_ERRORS(result, "Failed to create a pipeline layout! \n");

            VkGraphicsPipelineCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
            createInfo.pNext = nullptr;
            createInfo.flags = 0;
            createInfo.stageCount = static_cast<uint32_t>(stages.size());
            createInfo.pStages = stages.data();
            createInfo.pVertexInputState = &vertexInputInfo;
            createInfo.pInputAssemblyState = &inputAssembly;
            createInfo.pViewportState = &viewportState;
            createInfo.pRasterizationState = &rasterizer;
            createInfo.pMultisampleState = &multisampling;
            createInfo.pDepthStencilState = &depthStencil;
            createInfo.pColorBlendState = &colorBlending;
            createInfo.pDynamicState = nullptr;
            createInfo.layout = graphicsPipeline.layout;
            createInfo.renderPass = renderPass;
            createInfo.subpass = 0; //Index
            createInfo.basePipelineHandle = VK_NULL_HANDLE;
            createInfo.basePipelineIndex = -1;

            result = vkCreateGraphicsPipelines(device, nullptr, 1, &createInfo, nullptr, &graphicsPipeline.pipeline);
            CHECK_RESULT_FOR_ERRORS(result, "Failed to create a graphics pipeline! \n");

            //Clean up the shader modules. They can be destroyed as their data has already been attached to the graphics pipeline.
            for (auto& stageData : stageDatas)
            {
                vkDestroyShaderModule(device, stageData.module, nullptr);
            }
            return VK_SUCCESS;
        }

        VkResult createColorBlendedGraphicsPipeline(const VkDevice& device, const VkExtent2D& swapchainExtent, const VkRenderPass& renderPass,
                                                    const std::vector<VkDescriptorSetLayout>& descriptorSetLayouts, const std::vector<ShaderLoadInformation>& shadersToLoad,
                                                    const std::vector<VkVertexInputBindingDescription>& vertexInputBindings, const std::vector<VkVertexInputAttributeDescription>& vertexInputAttributes,
                                                    VkCullModeFlagBits cullMode, VkFrontFace frontFace, VkPolygonMode polygonMode, VkPrimitiveTopology topology,
                                                    GraphicsPipeline& graphicsPipeline)
        {
            std::vector<ShaderStageData> stageDatas;
            UtilityFunctions::fillShaderStageData(device, shadersToLoad, stageDatas);
            std::vector<VkPipelineShaderStageCreateInfo> stages = {};
            for (size_t i = 0; i < stageDatas.size(); i++)
            {
                stages.push_back(stageDatas[i].createInfo);
            }

            //Vertex input
            VkPipelineVertexInputStateCreateInfo vertexInputInfo = StructInitializer::getPipelineVertexInputStateCreateInfo(vertexInputBindings, vertexInputAttributes);

            //What kind of geometry to draw from vertices and if primitive restart should be enabled.
            VkPipelineInputAssemblyStateCreateInfo inputAssembly = StructInitializer::getPipelineInputAssemblyStateCreateInfo(topology, VK_FALSE);

            //Viewport
            VkViewport viewport = {};
            viewport.x = 0.0f;
            viewport.y = 0.0f;
            viewport.width = (float)swapchainExtent.width;
            viewport.height = (float)swapchainExtent.height;
            viewport.minDepth = 0.0f;
            viewport.maxDepth = 1.0f;
            std::vector<VkViewport> viewports = {viewport};

            //Scissor
            VkRect2D scissor = {};
            scissor.offset = {0,0};
            scissor.extent = swapchainExtent;
            std::vector<VkRect2D> scissors = {scissor};

            //Viewport state
            VkPipelineViewportStateCreateInfo viewportState = StructInitializer::getPipelineViewportStateCreateInfo(viewports, scissors);

            //Rasterizer
            VkPipelineRasterizationStateCreateInfo rasterizer =
                    StructInitializer::getPipelineRasterizationStateCreateInfo(VK_FALSE, VK_FALSE, polygonMode,
                                                                               1.0f, cullMode, frontFace,
                                                                               VK_FALSE, 0.0f, 0.0f, 0.0f);

            //Depth stencil testing.
            VkPipelineDepthStencilStateCreateInfo depthStencil =
                    StructInitializer::getPipelineDepthStencilStateCreateInfo(VK_TRUE, VK_TRUE, VK_COMPARE_OP_LESS, VK_FALSE, 0.0f, 1.0f,
                                                                              VK_FALSE, {}, {});

            //Multisampling.
            VkPipelineMultisampleStateCreateInfo multisampling =
                    StructInitializer::getPipelineMultisampleStateCreateInfo(VK_FALSE, VK_SAMPLE_COUNT_1_BIT, 1.0f, nullptr, VK_FALSE, VK_FALSE);

            //Color blending.
            VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
            colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
            colorBlendAttachment.blendEnable = VK_TRUE;
            colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
            colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
            colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
            colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
            colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
            colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;
            std::vector<VkPipelineColorBlendAttachmentState> colorBlendAttachments = {colorBlendAttachment};

            VkPipelineColorBlendStateCreateInfo colorBlending =
                    StructInitializer::getPipelineColorBlendStateCreateInfo(VK_TRUE, VK_LOGIC_OP_AND, colorBlendAttachments);

            //Dynamic state. These state datas will need to be specified at drawing time.
            std::vector<VkDynamicState> dynamicStates = {VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_LINE_WIDTH};
            VkPipelineDynamicStateCreateInfo dynamicState = StructInitializer::getPipelineDynamicStateCreateInfo(dynamicStates);

            //Pipeline layout.
            //Here we tell what kind of descriptors (uniforms, buffers etc.) and push constants go to the pipeline.
            VkPipelineLayoutCreateInfo pipelineLayoutInfo = StructInitializer::getPipelineLayoutCreateInfo(descriptorSetLayouts, {});

            VkResult result = vkCreatePipelineLayout(device, &pipelineLayoutInfo, nullptr, &graphicsPipeline.layout);
            CHECK_RESULT_FOR_ERRORS(result, "Failed to create a pipeline layout! \n");

            VkGraphicsPipelineCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
            createInfo.pNext = nullptr;
            createInfo.flags = 0;
            createInfo.stageCount = static_cast<uint32_t>(stages.size());
            createInfo.pStages = stages.data();
            createInfo.pVertexInputState = &vertexInputInfo;
            createInfo.pInputAssemblyState = &inputAssembly;
            createInfo.pViewportState = &viewportState;
            createInfo.pRasterizationState = &rasterizer;
            createInfo.pMultisampleState = &multisampling;
            createInfo.pDepthStencilState = &depthStencil;
            createInfo.pColorBlendState = &colorBlending;
            createInfo.pDynamicState = nullptr;
            createInfo.layout = graphicsPipeline.layout;
            createInfo.renderPass = renderPass;
            createInfo.subpass = 0; //Index
            createInfo.basePipelineHandle = VK_NULL_HANDLE;
            createInfo.basePipelineIndex = -1;

            result = vkCreateGraphicsPipelines(device, nullptr, 1, &createInfo, nullptr, &graphicsPipeline.pipeline);
            CHECK_RESULT_FOR_ERRORS(result, "Failed to create a graphics pipeline! \n");

            //Clean up the shader modules. They can be destroyed as their data has already been attached to the graphics pipeline.
            for (auto& stageData : stageDatas)
            {
                vkDestroyShaderModule(device, stageData.module, nullptr);
            }
            return VK_SUCCESS;
        }

        void setPipelineDescriptorInfos(const std::vector<GraphicsPipelineDescriptorInfo>& infos, GraphicsPipeline& pipeline)
        {
            for(auto& info : infos)
            {
                pipeline.descriptorInfos.push_back(info);
            }
            pipeline.descriptorInfosSet = true;
        }

        bool createPipelineObjectFromShaders(const VkExtent2D &extent, const VkDevice& device, const std::vector<ShaderLoadInformation>& shaders,
                                             const std::vector<VkVertexInputBindingDescription>& bindings,
                                             const std::vector<VkVertexInputAttributeDescription>& attributes,
                                             const VkRenderPass& renderPass, bool enableColorBlending, VkCullModeFlagBits cullMode,
                                             VkFrontFace frontFace, VkPolygonMode polygonMode, VkPrimitiveTopology topology, GraphicsPipeline& pipelineObject)
        {
            //std::vector<ShaderStageData> stageDatas;
            //UtilityFunctions::fillShaderStageData(device, shaders, stageDatas);

            //DescriptorLayoutBindingsPerDescriptorSet descriptorLayoutBindings;
            //ShaderUtility::generateDescriptorLayoutBindingsForDescriptorSets(shaders, descriptorLayoutBindings);
            //for(auto& bindings : descriptorLayoutBindings)
            //{
            //    pipelineObject.descriptorSetLayouts.push_back({});
            //    VkResult result = DescriptorUtility::createDescriptorSetLayout(device, bindings.second, pipelineObject.descriptorSetLayouts.back());
            //    assert(result == VK_SUCCESS);
            //}

            //VkResult result;
            //if(enableColorBlending)
            //{
            //    result = GraphicsPipelineUtility::createColorBlendedGraphicsPipeline(device, extent, renderPass, pipelineObject.descriptorSetLayouts, shaders, bindings,
            //                                                                         attributes, cullMode, frontFace, polygonMode, topology, pipelineObject);
            //}
            //else
            //{
            //    result = GraphicsPipelineUtility::createNonColorBlendingGraphicsPipeline(device, extent, renderPass, pipelineObject.descriptorSetLayouts, shaders, bindings,
            //                                                                             attributes, cullMode, frontFace, polygonMode, topology, pipelineObject);
            //}
            //assert(result == VK_SUCCESS);

            ////Create descriptor pool.
            //std::vector<VkDescriptorPoolSize> poolSizes;
            //ShaderUtility::generatePoolSizesFromDescriptorData(descriptorLayoutBindings, poolSizes);
            //result = DescriptorUtility::createDescriptorPool(device, poolSizes, 100, true,  pipelineObject.descriptorPool);
            //assert(result == VK_SUCCESS);

            //return result == VK_SUCCESS;
            return false;
        }
    }
}