#include "GraphicsApi/Vulkan/Subpasses/Forward/LightPass.h"
#include "GraphicsApi/Vulkan/VulkanUtility/DescriptorUtility.h"
#include "GraphicsApi/Vulkan/VulkanUtility/StructInitializer.h"
#include "GraphicsApi/Vulkan/VulkanUtility/UtilityFunctions.h"
#include "Utility/DelegateManager.h"

namespace Raven
{
    void LightPass::initializeVertexInputState(VkPipelineVertexInputStateCreateInfo& vertexInputState)
    {
        m_vertexInputBindings = { Vertex::getBindingDescription() };
        m_vertexInputAttributes = {};

        auto attributes = Vertex::getAttributeDescriptions();
        for (auto& attribute : attributes)
        {
            m_vertexInputAttributes.push_back(attribute);
        }
        vertexInputState = StructInitializer::getPipelineVertexInputStateCreateInfo(m_vertexInputBindings, m_vertexInputAttributes);
    }

    void LightPass::initializeInputAssemblyState(VkPipelineInputAssemblyStateCreateInfo& inputAssembly)
    {
        inputAssembly = StructInitializer::getPipelineInputAssemblyStateCreateInfo(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST, VK_FALSE);
    }

    void LightPass::initializeViewportState(VkPipelineViewportStateCreateInfo& viewportState)
    {
        const WindowDelegates& delegates = DelegateManager::getInstance().getWindowDelegates();

        uint32_t windowWidth = delegates.getWindowWidthFunc.Invoke();
        uint32_t windowHeight = delegates.getWindowHeightFunc.Invoke();
        VkExtent2D extent = { windowWidth, windowHeight };

        //Viewport
        VkViewport viewport = {};
        viewport.x = 0.0f;
        viewport.y = 0.0f;
        viewport.width = static_cast<float>(extent.width);
        viewport.height = static_cast<float>(extent.height);
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;
        m_viewports = { viewport };

        //Scissor
        VkRect2D scissor = {};
        scissor.offset = { 0,0 };
        scissor.extent = extent;
        m_scissors = { scissor };

        //Viewport state
        viewportState = StructInitializer::getPipelineViewportStateCreateInfo(m_viewports, m_scissors);
    }

    void LightPass::initializeRasterizerState(VkPipelineRasterizationStateCreateInfo& rasterizer)
    {
        rasterizer =
            StructInitializer::getPipelineRasterizationStateCreateInfo(VK_FALSE, VK_FALSE, VK_POLYGON_MODE_FILL,
                1.0f, VK_CULL_MODE_BACK_BIT, VK_FRONT_FACE_COUNTER_CLOCKWISE,
                VK_FALSE, 0.0f, 0.0f, 0.0f);
    }

    void LightPass::initializeMultisampleState(VkPipelineMultisampleStateCreateInfo& multisampling)
    {
        multisampling = StructInitializer::getPipelineMultisampleStateCreateInfo(VK_FALSE, VK_SAMPLE_COUNT_1_BIT, 1.0f, nullptr, VK_FALSE, VK_FALSE);
    }

    void LightPass::initializeDepthStencilState(VkPipelineDepthStencilStateCreateInfo& depthStencil)
    {
        depthStencil = StructInitializer::getPipelineDepthStencilStateCreateInfo(VK_TRUE, VK_TRUE, VK_COMPARE_OP_LESS,
            VK_FALSE, 0.0f, 1.0f,
            VK_FALSE, {}, {});
    }

    void LightPass::initializeColorBlendState(VkPipelineColorBlendStateCreateInfo& colorBlend)
    {
        VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
        colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
        colorBlendAttachment.blendEnable = VK_FALSE;
        m_colorBlendAttachments = { colorBlendAttachment};

        colorBlend = StructInitializer::getPipelineColorBlendStateCreateInfo(VK_FALSE, VK_LOGIC_OP_COPY, m_colorBlendAttachments);
    }

    void LightPass::initializeDynamicState(VkPipelineDynamicStateCreateInfo& dynamicStateCreateInfo)
    {
        m_dynamicStates = { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_LINE_WIDTH };
        dynamicStateCreateInfo = StructInitializer::getPipelineDynamicStateCreateInfo(m_dynamicStates);
    }

    VkResult LightPass::initializePipelineLayout(const VkDevice& device)
    {
        std::vector<ShaderLoadInformation> shadersToLoad = { {"../shaders/ForwardRenderer/forwardLight.vert", VK_SHADER_STAGE_VERTEX_BIT},
                                                            {"../shaders/ForwardRenderer/forwardLight.frag", VK_SHADER_STAGE_FRAGMENT_BIT} };

        VkResult result = describePipeline(device, shadersToLoad);
        assert(result == VK_SUCCESS);
        return VK_SUCCESS;
    }

    void LightPass::initializeDescriptorSet(const VkDevice& device, const RavenBuffer& lightUniformBuffer)
    {
        std::vector<VkDescriptorPoolSize> poolSizes = {{VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1}};
        VkResult result = DescriptorUtility::createDescriptorPool(device, poolSizes, 1, true, m_descriptorPool);
        assert(result == VK_SUCCESS);

        //Light descriptor set is in index 1.
        std::vector<VkDescriptorSetLayout> lightLayout = {m_renderpassPipeline.descriptorSetLayouts[1]};

        result = DescriptorUtility::allocateDescriptorSet(device, m_descriptorPool, lightLayout, m_descriptorSet);
        assert(result == VK_SUCCESS);

        VkDescriptorBufferInfo lightDescriptor = {};
        lightDescriptor.buffer = lightUniformBuffer.buffer;
        lightDescriptor.offset = 0;
        lightDescriptor.range = sizeof(LightUniformBufferObject);

        //Image/buffer writes.
        VkWriteDescriptorSet lightWrite =
            StructInitializer::getWriteDescriptorSet(m_descriptorSet, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 0, &lightDescriptor);

        std::array<VkWriteDescriptorSet, 1> writes = { lightWrite };
        vkUpdateDescriptorSets(device, static_cast<uint32_t>(writes.size()), writes.data(), 0, nullptr);
    }

    void LightPass::draw(const RavenBuffer& vertexBuffer, const RavenBuffer& indexBuffer, const std::vector<VkDescriptorSet>& descriptorSets,
        const std::vector<PushConstantData*>& pushConstants, VkCommandBuffer& cmdBuffer)
    {
        std::vector<VkDescriptorSet> lightPassDescriptors = {descriptorSets};
        lightPassDescriptors.push_back(m_descriptorSet);

        vkCmdBindDescriptorSets(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, m_renderpassPipeline.layout, 0, static_cast<uint32_t>(lightPassDescriptors.size()), lightPassDescriptors.data(), 0, nullptr);
        vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, m_renderpassPipeline.pipeline);

        VkBuffer vertexBuffers[] = { vertexBuffer.buffer };
        VkDeviceSize offsets[] = { 0 };
        vkCmdBindVertexBuffers(cmdBuffer, 0, 1, vertexBuffers, offsets);
        vkCmdBindIndexBuffer(cmdBuffer, indexBuffer.buffer, 0, VK_INDEX_TYPE_UINT32);
        vkCmdDrawIndexed(cmdBuffer, indexBuffer.count, 1, 0, 0, 0);
    }
}
