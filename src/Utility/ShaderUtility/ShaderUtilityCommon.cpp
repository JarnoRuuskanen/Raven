#include "Utility/ShaderUtility/ShaderUtilityCommon.h"
#include "GraphicsApi/Vulkan/Common.h"

namespace Raven
{
    namespace ShaderUtility
    {
        bool descriptorsMatch(const VkDescriptorSetLayoutBinding& existingBinding, const VkDescriptorSetLayoutBinding& comparedBinding)
        {
            bool bindingMatch = existingBinding.binding == comparedBinding.binding;
            bool typeMatch = existingBinding.descriptorType == comparedBinding.descriptorType;
            return bindingMatch && typeMatch;
        }

        void getDescriptorSetLayoutBindingsFromShaderStageReflections(const std::vector<std::tuple<VkShaderStageFlagBits, ShaderUtility::ShaderReflection>>& reflections, DescriptorLayoutBindingsPerDescriptorSet& bindings)
        {
            for(auto& stageTuple : reflections)
            {
                VkShaderStageFlagBits stage = std::get<VkShaderStageFlagBits>(stageTuple);
                const std::vector<Descriptor>& stageDescriptors = std::get<ShaderUtility::ShaderReflection>(stageTuple).getShaderStageDescriptors();

                for(const auto& descriptor : stageDescriptors)
                {
                    VkDescriptorSetLayoutBinding newBinding;
                    newBinding.descriptorType = descriptor.descriptorType;
                    newBinding.binding = descriptor.binding;
                    newBinding.stageFlags = stage;
                    newBinding.descriptorCount = 1;
                    newBinding.pImmutableSamplers = nullptr;
                    //Binding is ready, now check if it already exists.
                    bool alreadyExists = false;
                    //Go through all descriptor sets
                    for(auto& setBindingsPair : bindings)
                    {
                        if (setBindingsPair.first == descriptor.set)
                        {
                            //Go through all descriptor set layout bindings and check if it already exists.
                            for (auto& existingBinding : setBindingsPair.second)
                            {
                                if (descriptorsMatch(existingBinding.layoutBinding, newBinding))
                                {
                                    alreadyExists = true;
                                    existingBinding.layoutBinding.stageFlags |= stage;
                                    existingBinding.layoutBinding.descriptorCount++;
                                    break;
                                }
                            }
                        }
                    }
                    if(!alreadyExists)
                    {
                        ShaderDescriptorBindingInfo layoutBinding;
                        layoutBinding.layoutBinding = newBinding;
                        //Here we prefer the secondary name. In spirv_cross it means the following: struct <primaryName>{}<secondaryName>;
                        layoutBinding.primaryName = descriptor.name;
                        layoutBinding.secondaryName = descriptor.secondaryName;
                        bindings[descriptor.set].push_back(layoutBinding);
                    }
                }
            }
        }

        void getPushConstantsFromShaderStageReflections(const std::vector<std::tuple<VkShaderStageFlagBits, ShaderUtility::ShaderReflection>>& reflections, std::vector<ShaderPushConstantInfo>& pushConstants)
        {
            for (auto& stageTuple : reflections)
            {
                VkShaderStageFlagBits stage = std::get<VkShaderStageFlagBits>(stageTuple);
                const std::vector<PushConstantDescriptor>& stagePushConstants = std::get<ShaderUtility::ShaderReflection>(stageTuple).getShaderStagePushConstantDescriptors();
                for (auto& pushConstant : stagePushConstants)
                {
                    VkPushConstantRange range;
                    range.offset = pushConstant.offset;
                    range.size = pushConstant.size;
                    range.stageFlags = stage;

                    ShaderPushConstantInfo info;
                    info.name = pushConstant.name;
                    info.fallbackName = pushConstant.fallbackName;
                    info.pushConstantRange = range;

                    pushConstants.push_back(info);
                }
            }
        }

        void generateDescriptorLayoutBindingsForDescriptorSets(const std::vector<ShaderLoadInformation>& shadersToLoad, DescriptorLayoutBindingsPerDescriptorSet& bindings)
        {
            std::vector<std::tuple<VkShaderStageFlagBits, ShaderUtility::ShaderReflection>> shaderReflections;
            for(const auto& shader : shadersToLoad)
            {
                std::vector<uint32_t> spirv;
                ShaderUtility::compileGLSLToSpirv(shader.filepath, spirv);
                shaderReflections.push_back(std::make_tuple(shader.shaderStage, ShaderUtility::ShaderReflection(spirv.data(), spirv.size())));
            }
            ShaderUtility::getDescriptorSetLayoutBindingsFromShaderStageReflections(shaderReflections, bindings);
            shaderReflections.clear();
        }

        void generatePipelineDescriptionFromShaders(const std::vector<ShaderStageData>& stages, DescriptorLayoutBindingsPerDescriptorSet& bindings, std::vector<ShaderPushConstantInfo>& pushConstantInfos)
        {
            assert(!stages.empty());
            std::vector<std::tuple<VkShaderStageFlagBits, ShaderUtility::ShaderReflection>> shaderReflections;
            for (const auto& stage : stages)
            {
                shaderReflections.push_back(std::make_tuple(stage.stage, ShaderUtility::ShaderReflection(stage.spirv.data(), stage.spirv.size())));
            }
            ShaderUtility::getDescriptorSetLayoutBindingsFromShaderStageReflections(shaderReflections, bindings);
            ShaderUtility::getPushConstantsFromShaderStageReflections(shaderReflections, pushConstantInfos);
            shaderReflections.clear();
        }

        void generatePoolSizesFromDescriptorData(const DescriptorLayoutBindingsPerDescriptorSet& bindingsPerSet, std::vector<VkDescriptorPoolSize>& poolSizes)
        {
            for(const auto& pair : bindingsPerSet)
            {
                for(const auto& descriptor : pair.second)
                {
                    VkDescriptorPoolSize size = {};
                    size.type = descriptor.layoutBinding.descriptorType;
                    size.descriptorCount = descriptor.layoutBinding.descriptorCount;
                    poolSizes.push_back(size);
                }
            }
        }

        VkResult createShaderModule(const VkDevice& device, const std::vector<char>& code, VkShaderModule& module)
        {
            VkShaderModuleCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
            createInfo.codeSize = static_cast<uint32_t>(code.size());
            createInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());

            VkResult result = vkCreateShaderModule(device, &createInfo, nullptr, &module);
            CHECK_RESULT_FOR_ERRORS(result, "Failed to create a shader module!");

            return result;
        }
    }
}