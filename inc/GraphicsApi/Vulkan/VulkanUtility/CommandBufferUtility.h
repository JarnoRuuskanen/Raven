#ifndef RAVEN_COMMAND_BUFFER_UTILITY_H_INCLUDED
#define RAVEN_COMMAND_BUFFER_UTILITY_H_INCLUDED

#include "GraphicsApi/Vulkan/Common.h"
#include <vector>

namespace Raven
{
    struct WaitSemaphoreInfo
    {
        VkSemaphore semaphore;
        VkPipelineStageFlags waitingStage;
    };
    namespace CommandBufferUtility
    {
        bool createCommandPool(const VkDevice& device, uint32_t queueFamilyIndex, bool resetableBuffers, VkCommandPool& handle);
        void destroyCommandPool(const VkDevice& device, VkCommandPool& pool);
        bool allocateCommandBuffers(uint32_t bufferCount, std::vector<VkCommandBuffer> &commandBuffers,
                                    const VkCommandPool& commandPool, const VkDevice& device);
        bool allocateSingleCommandBuffer(const VkDevice& device, const VkCommandPool& pool, bool primaryBuffer, VkCommandBuffer& buffer);
        VkCommandBuffer beginSingleTimeCommands(const VkCommandPool& commandPool, const VkDevice& device);
        void endSingleTimeCommands(const VkCommandBuffer& commandBuffer, const VkDevice& device,
                                   const VkQueue& queue, const VkCommandPool& commandPool);
        //Reference from VulkanCookbook 3.11.
        bool submitCommandBuffersToQueue(VkQueue queue, std::vector<WaitSemaphoreInfo> waitSemaphoreInfos,
                                         std::vector<VkCommandBuffer> commandBuffers, std::vector<VkSemaphore> signalSemaphores,
                                         VkFence fence);
        bool waitForQueueToFinish(const VkQueue& queue);
    }
}
#endif
