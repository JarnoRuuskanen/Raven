#include "RavenApplication.h"
#include "AssetLoader/AssetLoader.h"
#include <Utility/DelegateTypes.h>


namespace Raven
{
    RavenApplication::RavenApplication()
    {
    }

    RavenApplication::~RavenApplication()
    {
    }

    bool RavenApplication::start(const std::vector<AssetGroup>& assets, uint32_t windowWidth, uint32_t windowHeight)
    {
        glfwInit();

        if (!createWindow(windowWidth, windowHeight))
            return false;

        WindowDelegates delegates;
        //Binding delegates. Using these to avoid coupling between systems etc etc.
        delegates.getSurfaceFunc.Bind<ApplicationWindow, &ApplicationWindow::getSurface>(&m_appWindow);
        delegates.getNativeWindowFunc.Bind<ApplicationWindow, &ApplicationWindow::getWindow>(&m_appWindow);
        delegates.createWindowSurfaceFunc.Bind<ApplicationWindow, &ApplicationWindow::createWindowSurface>(&m_appWindow);
        delegates.getWindowWidthFunc.Bind<ApplicationWindow, &ApplicationWindow::getWidth>(&m_appWindow);
        delegates.getWindowHeightFunc.Bind<ApplicationWindow, &ApplicationWindow::getHeight>(&m_appWindow);

        DelegateManager& manager = DelegateManager::getInstance();
        manager.registerSystemDelegates(delegates);

        //What I should actually do here is:
        //Load game world materials into memory and create appropriate resources for rendering.
        m_entityManager.initialize(true);

        m_sceneCreator.createSceneGraphFromAssets(&m_entityManager, assets);

        /*TextureData heightMap;
        AssetLoader::loadTexture("../assets/world_assets/heightmap.jpg", heightMap);
        m_entityManager.generateTerrain(heightMap);*/

        //Initialize EntityManager creating all the entities that were in the scene graph.
        //for (const auto& assetGroup : assets)
        //{
        //    m_gameWorld.loadAssetGroupIntoWorld(assetGroup);
        //}
        ////m_gameWorld.createTestEntities();
        m_entityManager.createTestLights();

        //Initialize some basic input controls. This does not include player controls but rather
        //exiting the program, opening menus etc.
        initializeInputControls();

        //Start the game loop.
        mainLoop();

        return true;
    }

    void RavenApplication::mainLoop()
    {
        //Start timer.
        Timer timer;
        timer.deltaTime = 0.0f;
        double lastFrame = 0.0f;
        
        //FPS calculations:
        int frameCount = 0;
        float previousFPSTimepoint = 0;

        while(!glfwWindowShouldClose(m_window))
        {
            float currentFrame = glfwGetTime();
            timer.time = currentFrame;
            timer.deltaTime = currentFrame - lastFrame;
            lastFrame = currentFrame;

            frameCount++;
            if (currentFrame - previousFPSTimepoint >= 1.0)
            {
                std::cout << "FPS: " << frameCount << std::endl;
                frameCount = 0;
                previousFPSTimepoint = currentFrame;
            }

            //Update the game world and all sub-systems etc.
            m_entityManager.update(timer);

            glfwPollEvents();
        }
    }

    bool RavenApplication::createWindow(uint32_t width, uint32_t height)
    {
        assert(height > 0 && width > 0);
        m_windowWidth = width;
        m_windowHeight = height;

        WindowCreateInfo info = {};
        info.width = width;
        info.height = height;
        m_appWindow.initialize(info);

        m_window = m_appWindow.getWindow();
        assert(m_window != nullptr);

        return true;
    }

    bool RavenApplication::closeWindow(const KeyEvent& event)
    {
        if(event.state == KeyboardKeyState::RELEASE)
        {
            glfwSetWindowShouldClose(m_window, true);
        }
        return true;
    }

    void RavenApplication::initializeInputControls()
    {
        Input& inputHandler = Input::getInstance();
        inputHandler.initialize(m_window);

        KeyEvent exitEvent;
        exitEvent.ravenKey = (unsigned int)KeyboardKey::KEY_ESCAPE;
        exitEvent.state = KeyboardKeyState::RELEASE;
        exitEvent.glfwModifierKeys = 0;

        Delegate<bool(const KeyEvent&)> callback;
        callback.Bind<RavenApplication, &RavenApplication::closeWindow>(this);
        inputHandler.registerKeyEventCallback(exitEvent, callback);

        Delegate<bool(const MouseKeyEvent&)> mouseClickedCallback;
        mouseClickedCallback.Bind<RavenApplication, &RavenApplication::mouseKeyCallback>(this);
        inputHandler.registerMouseKeyCallback(mouseClickedCallback);

        glm::mat4 proj = glm::perspective(glm::radians(90.0f), 16.0f / 9.0f, 0.1f, 256.0f);
        proj[1][1] *= -1;
        Entity* playerEntity = m_entityManager.createPlayerEntity();
        auto camera = playerEntity->getComponent<Camera>();

        m_mousePicker = new RayCaster(RayCasterType::MOUSE_POINTER, camera, proj);
    }

    bool RavenApplication::mouseKeyCallback(const MouseKeyEvent& event)
    {
        bool pressed = event.state == MouseKeyState::PRESS;
        if(event.mouseKey == GLFW_MOUSE_BUTTON_LEFT)
        {
            if(pressed)
            {
                //TODO: OPTIMIZE
                double xPos, yPos;
                glfwGetCursorPos(m_window, &xPos, &yPos);
                float normalizedX = static_cast<float>((2 * xPos) / m_windowWidth - 1);
                float normalizedY = static_cast<float>((2 * yPos) / m_windowHeight - 1) * -1; //Flipping y to be positive towards up.

                const glm::vec3& ray = m_mousePicker->getRay(normalizedX, normalizedY);
                //EntityId id = m_gameWorld.selectClickedEntity(ray);
                return false;
            }
        }
        return true;
    }
}
