#include "GraphicsApi/Vulkan/VulkanUtility/BufferUtility.h"
#include <iostream>
#include <GraphicsApi/Vulkan/VulkanUtility/StructInitializer.h>
#include <GraphicsApi/Vulkan/VulkanUtility/UtilityFunctions.h>

namespace Raven
{
    namespace BufferUtility
    {
        VkBuffer createEmptyBuffer(const VkDevice &device, VkBufferUsageFlags bufferUsage, VkDeviceSize bufferSize)
        {
            VkBufferCreateInfo bufferInfo = StructInitializer::getBufferCreateInfo({}, bufferSize, bufferUsage, VK_SHARING_MODE_EXCLUSIVE);

            VkBuffer buffer = VK_NULL_HANDLE;
            VkResult result = vkCreateBuffer(device, &bufferInfo, nullptr, &buffer);
            if(result != VK_SUCCESS)
            {
                std::cerr << "Failed to create a buffer!" << std::endl;
                buffer = VK_NULL_HANDLE;
            }
            return buffer;
        }

        void allocateBufferMemory(const VkDevice& device, VkDeviceSize allocationSize, uint32_t memoryTypeIndex, VkDeviceMemory& bufferMemory)
        {
            VkMemoryAllocateInfo allocInfo = {};
            allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
            allocInfo.allocationSize = allocationSize;
            allocInfo.memoryTypeIndex = memoryTypeIndex;

            VkResult result = vkAllocateMemory(device, &allocInfo, nullptr, &bufferMemory);
            if(result != VK_SUCCESS)
            {
                std::cerr << "Failed to allocate vertex buffer memory!\n";
                bufferMemory = VK_NULL_HANDLE;
            }
        }

        bool createMemoryBackedBuffer(const VkDevice& device, const VkPhysicalDevice& physicalDevice, VkDeviceSize bufferSize,
                                      VkBufferUsageFlags bufferUsage, VkMemoryPropertyFlags properties, VkDeviceMemory &bufferMemory,
                                      VkBuffer &buffer)
        {
            buffer = createEmptyBuffer(device, bufferUsage, bufferSize);
            if(buffer == VK_NULL_HANDLE)
            {
                std::cerr << "Failed to create a buffer!\n";
                return false;
            }

            VkMemoryRequirements memReq;
            vkGetBufferMemoryRequirements(device, buffer, &memReq);

            int memTypeIndex = UtilityFunctions::findMemoryType(physicalDevice, memReq.memoryTypeBits, properties);
            if(memTypeIndex < 0)
            {
                std::cerr << "Couldn't find correct memory type for buffer" << std::endl;
                return false;
            }

            allocateBufferMemory(device, memReq.size, static_cast<uint32_t>(memTypeIndex), bufferMemory);
            if(bufferMemory == VK_NULL_HANDLE)
            {
                return false;
            }

            vkBindBufferMemory(device, buffer, bufferMemory, 0);

            return true;
        }
    }
}
