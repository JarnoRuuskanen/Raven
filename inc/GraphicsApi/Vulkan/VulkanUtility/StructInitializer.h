#ifndef RAVEN_STRUCT_INITIALIZER_H_INCLUDED
#define RAVEN_STRUCT_INITIALIZER_H_INCLUDED

#include "GraphicsApi/Vulkan/Common.h"
#include <vector>

namespace Raven
{
    namespace StructInitializer
    {
        VkPipelineShaderStageCreateInfo getShaderStageCreateInfo(VkShaderStageFlagBits shaderStage,
                                                                 const VkShaderModule &shaderModule);
        //Buffer
        VkBufferCreateInfo getBufferCreateInfo(const std::vector<uint32_t>& queueFamilyIndices, VkDeviceSize size,
                                               VkBufferUsageFlags usage, VkSharingMode sharingMode);
        //Image
        VkImageCreateInfo getImageCreateInfo(uint32_t width, uint32_t height, VkImageUsageFlags usage, VkFormat format);
        VkImageViewCreateInfo getImageViewCreateInfo(VkImage& image, VkFormat format, VkImageAspectFlags aspectFlags, VkImageViewType type);
        VkSamplerCreateInfo getSamplerCreateInfo();
        VkWriteDescriptorSet getWriteDescriptorSet(VkDescriptorSet dstSet, VkDescriptorType type, uint32_t binding,
                                                   VkDescriptorBufferInfo* info, uint32_t count = 1);
        VkWriteDescriptorSet getWriteDescriptorSet(VkDescriptorSet dstSet, VkDescriptorType type, uint32_t binding,
                                                   VkDescriptorImageInfo* info, uint32_t count = 1);

        //Pipeline
        VkPipelineVertexInputStateCreateInfo getPipelineVertexInputStateCreateInfo(const std::vector<VkVertexInputBindingDescription>& bindings,
                                                                                   const std::vector<VkVertexInputAttributeDescription>& attributes);
        VkPipelineInputAssemblyStateCreateInfo getPipelineInputAssemblyStateCreateInfo(VkPrimitiveTopology topology, VkBool32 primitiveRestartEnable);
        VkPipelineViewportStateCreateInfo getPipelineViewportStateCreateInfo(const std::vector<VkViewport>& viewports, const std::vector<VkRect2D>& scissors);
        VkPipelineRasterizationStateCreateInfo getPipelineRasterizationStateCreateInfo(VkBool32 depthClampEnable, VkBool32 rasterizerDiscardEnable, VkPolygonMode polygonMode,
                                                                                       float lineWidth, VkCullModeFlags cullMode, VkFrontFace frontFace, VkBool32 depthBiasEnable,
                                                                                       float depthBiasConstantFactor, float depthBiasClamp, float depthBiasSlopeFactor);
        VkPipelineDepthStencilStateCreateInfo getPipelineDepthStencilStateCreateInfo(VkBool32 depthTestEnable, VkBool32 depthWriteEnable,
                                                                                     VkCompareOp depthCompareOp, VkBool32 depthBoundsTestEnable,
                                                                                     float minDepthBounds, float maxDepthBounds,
                                                                                     VkBool32 stencilTestEnable, const VkStencilOpState& front,
                                                                                     const VkStencilOpState& back);
        VkPipelineMultisampleStateCreateInfo getPipelineMultisampleStateCreateInfo(VkBool32 sampleShadingEnable, VkSampleCountFlagBits rasterizationSamples,
                                                                                   float minSampleShading, const VkSampleMask* sampleMask,
                                                                                   VkBool32 alphaToCoverageEnable, VkBool32 alphaToOneEnable);
        VkPipelineColorBlendStateCreateInfo getPipelineColorBlendStateCreateInfo(VkBool32 logicOpEnable, VkLogicOp logicOp,
                                                                                 const std::vector<VkPipelineColorBlendAttachmentState>& attachments);
        VkPipelineDynamicStateCreateInfo getPipelineDynamicStateCreateInfo(const std::vector<VkDynamicState>& dynamicStates);
        VkPipelineLayoutCreateInfo getPipelineLayoutCreateInfo(const std::vector<VkDescriptorSetLayout>& descriptorSetLayouts,
                                                               const std::vector<VkPushConstantRange>& pushConstantRanges);
        VkVertexInputBindingDescription getVertexInputBindingDescription(uint32_t binding, uint32_t stride, VkVertexInputRate inputRate);
        VkVertexInputAttributeDescription getVertexInputAttributeDescription(uint32_t binding, uint32_t location,
                                                                             VkFormat format, uint32_t offset);
    }
}

#endif