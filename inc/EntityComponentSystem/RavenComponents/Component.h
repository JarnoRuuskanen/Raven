#ifndef RAVEN_COMPONENT_H_INCLUDED
#define RAVEN_COMPONENT_H_INCLUDED
#include "EntityComponentSystem/ECSTypes.h"
#include "AssetLoader/AssetGroup.h"
#include "Utility/StringHash.h"

namespace Raven
{
    //Base class for all components.
    class Component
    {
    public:
        static constexpr ComponentId ID = UINT32_MAX;

        Component() : m_entityId(INVALID_ENTITY_ID) {}
        virtual ~Component() = default; //Use the compiler default descturctor.
        virtual bool init(void* data) = 0;

        inline void setEntityId(EntityId id) { m_entityId = id; }
        EntityId getEntityId() const { return m_entityId; }
    private:
        EntityId m_entityId;
    };
}
#endif