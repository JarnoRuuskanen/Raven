#ifndef RAVEN_FUNCTION_DEDUCER_H_INCLUDED
#define RAVEN_FUNCTION_DEDUCER_H_INCLUDED
#include <functional>

//Reference https://stackoverflow.com/questions/9065081/how-do-i-get-the-argument-types-of-a-function-pointer-in-a-variadic-template-cla
namespace Raven
{
    template<typename T>
    struct functionTraits;

    template<typename R, typename ...Args>
    struct functionTraits<R(Args...)>
    {
        static const size_t argumentCount = sizeof...(Args);
        typedef R resultType;

        template <size_t i>
        struct arg
        {
            typedef typename std::tuple_element<i, std::tuple<Args...>>::type type;
        };
    };
}

#endif