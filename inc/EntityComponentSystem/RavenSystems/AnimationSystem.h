#ifndef RAVEN_ANIMATION_SYSTEM_H_INCLUDED
#define RAVEN_ANIMATION_SYSTEM_H_INCLUDED

#include "EntityComponentSystem/ECSTypes.h"
#include "EntityComponentSystem/RavenSystems/System.h"
#include "EntityComponentSystem/RavenComponents/Animation.h"
#include "EntityComponentSystem/RavenComponents/Renderable.h"
#include "EntityComponentSystem/RavenComponents/Transform.h"
#include "Utility/DelegateManager.h"

namespace Raven
{
    class AnimationSystem : public System<Animation, Renderable, Transform>
    {
        using BaseType = System<Animation, Renderable, Transform>;
	public:
		AnimationSystem(EntityManager* pGameWorld);
		// Inherited via System
		void update(const Timer& timer) override;
	private:
		void entityAddedToSystem(Entity* entity) override;
		void updateMeshJoints(const glm::mat4& skeletonMatrix, Renderable* renderable);

		//Graphics related stuff that should probably not be here.
		VkDescriptorPool m_descriptorPool;
		VkDescriptorSetLayout m_descriptorSetLayout;
		VulkanResourceManagerDelegates m_vulkanResourceManagerDelegates;
		VkDevice m_device;
    };
}
#endif