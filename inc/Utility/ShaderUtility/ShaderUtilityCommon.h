#ifndef RAVEN_SHADER_UTILITY_COMMON_H_INCLUDED
#define RAVEN_SHADER_UTILITY_COMMON_H_INCLUDED

#include <GraphicsApi/Vulkan/VulkanUtility/VulkanStructs.h>
#include "Utility/ShaderUtility/ShaderReflection.h"
#include "ShaderProcessor.h"

namespace Raven
{
    //This lists every descriptor layout binding for each descriptor set.
    using DescriptorLayoutBindingsPerDescriptorSet = std::map<uint32_t, std::vector<ShaderDescriptorBindingInfo>>;
    namespace ShaderUtility
    {
        void getDescriptorSetLayoutBindingsFromShaderStageReflections(const std::vector<std::tuple<VkShaderStageFlagBits, ShaderUtility::ShaderReflection>>& reflections, DescriptorLayoutBindingsPerDescriptorSet& bindings);
        void getPushConstantsFromShaderStageReflections(const std::vector<std::tuple<VkShaderStageFlagBits, ShaderUtility::ShaderReflection>>& reflections, std::vector<PushConstantDescriptor>& pushConstants);
        /**
         * @brief Utility function for creating the data used in VkDescriptorSetLayoutCreateInfo.
         * @param shadersToLoad
         * @param bindings
         */
        void generateDescriptorLayoutBindingsForDescriptorSets(const std::vector<ShaderLoadInformation>& shadersToLoad, DescriptorLayoutBindingsPerDescriptorSet& bindings);
        void generatePipelineDescriptionFromShaders(const std::vector<ShaderStageData>& stages, DescriptorLayoutBindingsPerDescriptorSet& bindings, std::vector<ShaderPushConstantInfo>& pushConstantInfos);
        void generatePoolSizesFromDescriptorData(const DescriptorLayoutBindingsPerDescriptorSet& bindingsPerSet, std::vector<VkDescriptorPoolSize>& poolSizes);
        VkResult createShaderModule(const VkDevice& device, const std::vector<char>& code, VkShaderModule& module);
        bool descriptorsMatch(const VkDescriptorSetLayoutBinding& existingBinding, const VkDescriptorSetLayoutBinding& comparedBinding);
    }
}

#endif