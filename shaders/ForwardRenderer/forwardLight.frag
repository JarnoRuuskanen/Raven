#version 450

layout(location = 0) out vec4 outColor;

layout(location = 0) in vec2 fragTexCoord;
layout(location = 1) in vec3 fragPosition;
layout(location = 2) in vec3 normal;

layout(set=0, binding = 1) uniform sampler2D albedo;

layout (constant_id = 0) const int NUM_LIGHTS = 1;

struct Light {
	vec4 position;
	vec3 color;
	float radius;
};

layout (set=1,binding = 0) uniform UBO 
{
	vec4 viewPos;
	Light lights[NUM_LIGHTS];
} lightUbo;

void main() 
{
    vec3 lightPos = lightUbo.lights[0].position.xyz;
    vec3 viewPos = lightUbo.viewPos.xyz;
    vec3 lightColor =  lightUbo.lights[0].color.xyz;
    vec3 objectColor = texture(albedo, fragTexCoord).xyz;

    // ambient
    float ambientStrength = 0.1;
    vec3 ambient = ambientStrength * lightColor;
  	
    // diffuse 
    vec3 norm = normalize(normal);
    vec3 lightDir = normalize(lightPos - fragPosition);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;
    
    // specular
    float specularStrength = 0.5;
    vec3 viewDir = normalize(viewPos - fragPosition);
    vec3 reflectDir = reflect(-lightDir, norm);  
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
    vec3 specular = specularStrength * spec * lightColor;  
        
    vec3 result = (ambient + diffuse + specular) * objectColor;
    outColor = vec4(result, 1.0);
}