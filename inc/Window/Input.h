#ifndef RAVEN_INPUT_H_INCLUDED
#define RAVEN_INPUT_H_INCLUDED

#include <Window/ApplicationWindow.h>
#include <Utility/Delegate.h>
#include <Utility/DelegateTypes.h>

#include <functional>
#include <algorithm>

namespace Raven
{
    enum class KeyboardKeyState
    {
        PRESS,
        HOLD,
        RELEASE,    //Key is also in "release" mode when it has not been pressed down.
        LAST
    };

    enum class MouseKeyState
    {
        PRESS,
        RELEASE,
        LAST
    };

    enum class KeyboardKey
    {
        NONE,
        KEY_A,
        KEY_W,
        KEY_S,
        KEY_D,
        KEY_SPACE,
        KEY_LEFT_CONTROL,
        KEY_LEFT_ALT,
        KEY_LEFT_SHIFT,
        KEY_TAB,
        KEY_ESCAPE,
        KEY_BACKSPACE,
        LAST
    };

    enum class MouseKey
    {
        KEY_LEFT,
        KEY_RIGHT,
        KEY_MIDDLE,
        KEY_SCROLL,
        LAST
    };

    struct KeyEvent
    {
        int ravenKey;
        int glfwModifierKeys;
        KeyboardKeyState state;

        bool operator==(const KeyEvent &other) const
        {
            return (ravenKey == other.ravenKey &&
                    glfwModifierKeys == other.glfwModifierKeys &&
                    state == other.state);
        }
    };

    struct KeyEventHasher
    {
        std::size_t operator()(const KeyEvent& keyEvent) const noexcept
        {
            //Individual hash values for each KeyEvent variable and
            //combine the results with XOR and bit shifting.
            return ((std::hash<int>()(keyEvent.ravenKey)
                     ^ (std::hash<int>()(keyEvent.glfwModifierKeys) << 1)) >> 1)
                    ^ (std::hash<unsigned int>()((unsigned int)keyEvent.state) << 1);
        }
    };

    struct MouseKeyEvent
    {
        int mouseKey;
        int glfwModifierKeys;
        MouseKeyState state;
    };

    using KeyEventMap = std::unordered_map<KeyEvent,std::vector<Delegate<bool(const KeyEvent&)>>,KeyEventHasher>;
    class Input
    {
    public:
        static Input& getInstance()
        {
            static Input instance;
            return instance;
        }
        void initialize(GLFWwindow* window);

        //Register a specific key event callback so that whenever it happens, a callback
        //function is called. KeyEvent is sent back as a parameter as it might be of valuable use
        //in case multiple triggers launch the same callback.
        void registerKeyEventCallback(KeyEvent keyEvent, Delegate<bool(const KeyEvent&)> callback) { m_keyboardKeyEventCallbacks[keyEvent].push_back(callback); }
        //General keyboard callbacks that are called whenever any key is clicked.
        inline void registerCharacterCallback(Delegate<bool(unsigned int)> callback) { m_characterCallbacks.push_back(callback); }
        inline void registerKeyboardKeyCallback(Delegate<bool(int, int, int, int)> callback) { m_keyboardKeyCallbacks.push_back(callback); }

        //Register general listener so that whenever something happens, the callback
        //function is called with the information about what happened. If registered
        //this way, the callback function should handle sorting which key/event occured.
        inline void registerMouseKeyCallback(Delegate<bool(const MouseKeyEvent&)> callback) { m_mouseKeyCallbacks.push_back(callback); }
        inline void registerMouseMovementCallback(Delegate<bool(float, float)> callback) { m_mouseMovementCallbacks.push_back(callback); }

        bool* getKeyboardKeyStates() { return m_keyboardKeyStates; }
        bool* getMouseKeyStates() { return m_mouseKeyStates; }
    private:
        Input(){};

        void handleMouseKeyCallbacks(int button, int action, int mods);
        void handleMouseMovementCallbacks(float xpos, float ypos);
        void handleKeyboardKeyCallbacks(int key, int scancode, int action, int mods);
        void handleCharacterCallbacks(unsigned int glfwCodePoint);
        unsigned int getMatchingKey(int glfwKey);

        bool m_initialized = false;
        GLFWwindow* m_window = nullptr;

        std::vector<Delegate<bool(const MouseKeyEvent&)>> m_mouseKeyCallbacks;
        std::vector<Delegate<bool(float, float)>> m_mouseMovementCallbacks;
        //For a very specific event.
        KeyEventMap m_keyboardKeyEventCallbacks;
        //For any character.
        std::vector<Delegate<bool(unsigned int)>> m_characterCallbacks;
        //For any keyboard key.
        std::vector<Delegate<bool(int, int, int, int)>> m_keyboardKeyCallbacks;

        //Saving all of the states of every key in an array which size is
        bool m_keyboardKeyStates[(unsigned)KeyboardKey::LAST] = {false};
        bool m_mouseKeyStates[(unsigned)MouseKey::LAST] = {false};
        
    public:
        Input(Input const&) = delete;
        void operator=(Input const&) = delete;
    };
}
#endif