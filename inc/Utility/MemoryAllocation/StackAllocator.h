#ifndef RAVEN_STACK_ALLOCATOR_H_INCLUDED
#define RAVEN_STACK_ALLOCATOR_H_INCLUDED

#include "Allocator.h"

//Reference https://github.com/mtrebi/memory-allocators/blob/master/includes/StackAllocator.h
namespace Raven
{
    class StackAllocator : public Allocator
    {
    public:
        StackAllocator(const std::size_t totalSize);
        virtual ~StackAllocator();
        virtual void* allocate(const std::size_t size, const std::size_t alignment = 0) override;
        virtual void free(void* ptr);
        virtual void init() override;
        virtual void reset();
    protected:
        void* m_startPtr = nullptr;
        std::size_t m_offset;
    private:
        StackAllocator(StackAllocator& stackAllocator);
        struct AllocationHeader
        {
            char padding;
        };
    };
}

#endif