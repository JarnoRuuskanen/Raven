#ifndef RAVEN_ENTITY_COMPONENT_SYSTEM_TYPES_H_INCLUDED
#define RAVEN_ENTITY_COMPONENT_SYSTEM_TYPES_H_INCLUDED
#include <limits>
#include <cstdint>
#include <cstddef>

namespace Raven
{
    using EntityId = size_t;
    using ComponentId = size_t;
    constexpr std::size_t INVALID_POOL_INDEX = std::numeric_limits<std::size_t>::max();
    constexpr EntityId INVALID_ENTITY_ID = -1;
    struct ComponentAutoCounterId {};
}

#endif