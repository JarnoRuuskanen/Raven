#include "Utility/MemoryAllocation/PoolAllocator.h"
#include <cassert>
#include <stdlib.h>
#include <algorithm>

namespace Raven
{
    PoolAllocator::PoolAllocator(const std::size_t totalSize, const std::size_t chunkSize)
    : Allocator(totalSize)
    {
        assert(chunkSize >= 8 && "Minimum required chunk size is 8.");
        assert(totalSize % chunkSize == 0 && "Allocation total size must be aligned with chunk size.");
        m_chunkSize = chunkSize;
    }
    PoolAllocator::~PoolAllocator()
    {
        free(m_startPtr);
    }

    void PoolAllocator::init()
    {
        m_startPtr = malloc(m_totalSize);
        reset();
    }

    void* PoolAllocator::allocate(const std::size_t size, std::size_t alignment)
    {
        assert(size == m_chunkSize && "Allocation size doesn't match chunk size.");
        Node* freePosition = m_freeList.pop();
        assert(freePosition != nullptr && "The pool is full. Allocate more space!");
        m_sizeUsed += m_chunkSize;
        m_peakSize = std::max(m_peakSize, m_sizeUsed);
        return static_cast<void*>(freePosition);
    }

    void PoolAllocator::free(void *ptr)
    {
        m_sizeUsed -= m_chunkSize;
        m_freeList.push(static_cast<Node*>(ptr));
    }

    void PoolAllocator::reset()
    {
        m_sizeUsed = 0;
        m_peakSize = 0;
        assert(m_chunkSize != 0);

        const int chunkCount = m_totalSize / m_chunkSize;
        for(size_t i = 0; i < chunkCount; i++)
        {
            //Using c-type casts as cpp cast doesn't allow void* -> size_t.
            std::size_t address = (std::size_t)m_startPtr + i*m_chunkSize;
            m_freeList.push((Node*)address);
        }

    }
}