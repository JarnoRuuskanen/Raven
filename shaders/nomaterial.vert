#version 450

layout(location = 0) in vec4 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in vec2 inTexCoord;

layout(location = 0) out vec3 outColor;

layout(binding = 0) uniform UniformBufferObject
{
    mat4 model;
    mat4 view;
    mat4 proj;
}ubo;

layout(push_constant) uniform colorBlock
{
    vec3 color;
}pushConstant;

void main()
{
    gl_Position = ubo.proj * ubo.view * ubo.model * inPosition;
    outColor = pushConstant.color;
}
