#include "GraphicsApi/Vulkan/VulkanUtility/CommandBufferUtility.h"
#include <iostream>
namespace Raven
{
    namespace CommandBufferUtility
    {
        bool createCommandPool(const VkDevice& device, uint32_t queueFamilyIndex, bool resetableBuffers, VkCommandPool& handle)
        {
            VkCommandPoolCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
            createInfo.pNext = nullptr;
            createInfo.flags = resetableBuffers ? VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT : 0;
            createInfo.queueFamilyIndex = queueFamilyIndex;

            VkResult result = vkCreateCommandPool(device, &createInfo, nullptr, &handle);
            if(result != VK_SUCCESS)
            {
                std::cerr << "Failed to create a command pool! \n";
                return false;
            }
            return true;
        }

        void destroyCommandPool(const VkDevice& device, VkCommandPool& pool)
        {
            vkDestroyCommandPool(device, pool, nullptr);
            pool = VK_NULL_HANDLE;
        }

        bool allocateCommandBuffers(uint32_t bufferCount, std::vector<VkCommandBuffer> &commandBuffers,
                                    const VkCommandPool& commandPool, const VkDevice& device)
        {
            commandBuffers.clear();
            commandBuffers.resize(bufferCount);
            VkCommandBufferAllocateInfo allocInfo = {};
            allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
            allocInfo.pNext = nullptr;
            allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
            allocInfo.commandPool = commandPool;
            allocInfo.commandBufferCount = static_cast<uint32_t>(commandBuffers.size());

            VkResult result = vkAllocateCommandBuffers(device, &allocInfo, commandBuffers.data());

            if(result != VK_SUCCESS)
            {
                commandBuffers.clear();
                std::cerr << "Failed to allocate command buffers!" << std::endl;
                return false;
            }
            return true;
        }

        bool allocateSingleCommandBuffer(const VkDevice& device, const VkCommandPool& pool, bool primaryBuffer, VkCommandBuffer& buffer)
        {
            VkCommandBufferAllocateInfo allocInfo = {};
            allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
            allocInfo.pNext = nullptr;
            allocInfo.level = primaryBuffer ? VK_COMMAND_BUFFER_LEVEL_PRIMARY : VK_COMMAND_BUFFER_LEVEL_SECONDARY;
            allocInfo.commandPool = pool;
            allocInfo.commandBufferCount = 1;

            VkResult result = vkAllocateCommandBuffers(device, &allocInfo, &buffer);

            if(result != VK_SUCCESS)
            {
                std::cerr << "Failed to allocate a command buffer!" << std::endl;
                return false;
            }
            return true;
        }

        VkCommandBuffer beginSingleTimeCommands(const VkCommandPool& commandPool, const VkDevice& device)
        {
            VkCommandBufferAllocateInfo allocInfo = {};
            allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
            allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
            allocInfo.commandPool = commandPool;
            allocInfo.commandBufferCount = 1;

            VkCommandBuffer commandBuffer;
            vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer);

            VkCommandBufferBeginInfo beginInfo = {};
            beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
            beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

            vkBeginCommandBuffer(commandBuffer, &beginInfo);
            return commandBuffer;
        }

        void endSingleTimeCommands(const VkCommandBuffer& commandBuffer, const VkDevice& device,
                                   const VkQueue& queue, const VkCommandPool& commandPool)
        {
            vkEndCommandBuffer(commandBuffer);

            VkSubmitInfo submitInfo = {};
            submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
            submitInfo.commandBufferCount = 1;
            submitInfo.pCommandBuffers = &commandBuffer;

            vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);
            vkQueueWaitIdle(queue);

            vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);
        }
        bool submitCommandBuffersToQueue(VkQueue queue, std::vector<WaitSemaphoreInfo> waitSemaphoreInfos,
                                         std::vector<VkCommandBuffer> commandBuffers, std::vector<VkSemaphore> signalSemaphores,
                                         VkFence fence)
        {
            std::vector<VkSemaphore> waitSemaphoreHandles;
            std::vector<VkPipelineStageFlags> waitSemaphoreStages;
            for(auto& waitSemaphoreInfo : waitSemaphoreInfos)
            {
                waitSemaphoreHandles.emplace_back(waitSemaphoreInfo.semaphore);
                waitSemaphoreStages.emplace_back(waitSemaphoreInfo.waitingStage);
            }

            VkSubmitInfo submitInfo = {};
            submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
            submitInfo.pNext = nullptr;
            submitInfo.waitSemaphoreCount = static_cast<uint32_t>(waitSemaphoreHandles.size());
            submitInfo.pWaitSemaphores = waitSemaphoreHandles.empty() ? 0 : waitSemaphoreHandles.data();
            submitInfo.signalSemaphoreCount = static_cast<uint32_t>(signalSemaphores.size());
            submitInfo.pSignalSemaphores = signalSemaphores.empty() ? 0 : signalSemaphores.data();
            submitInfo.pWaitDstStageMask = waitSemaphoreStages.empty() ? nullptr : waitSemaphoreStages.data();
            submitInfo.commandBufferCount = static_cast<uint32_t>(commandBuffers.size());
            submitInfo.pCommandBuffers = commandBuffers.empty() ? nullptr : commandBuffers.data();

            VkResult result = vkQueueSubmit(queue, 1, &submitInfo, fence);
            if(result == VK_ERROR_DEVICE_LOST)
            {
                std::exit(1);
            }

            bool success = result == VK_SUCCESS;
            assert(success);
            return success;
        }

        bool waitForQueueToFinish(VkQueue queue)
        {
            VkResult result = vkQueueWaitIdle(queue);
            assert(result == VK_SUCCESS);
            return result;
        }
    }
}
