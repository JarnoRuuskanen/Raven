#ifndef RAVEN_STACK_LINKED_LIST_H_INCLUDED
#define RAVEN_STACK_LINKED_LIST_H_INCLUDED

//A template class for keeping list of free memory.
template <class Type>
class StackLinkedList
{
public:
    struct Node
    {
        Type data;
        Node* next;
    };

    Node* head;

    StackLinkedList() = default;
    StackLinkedList(StackLinkedList &stackLinkedList) = delete;
    void push(Node* newNode);
    Node* pop();
};

template <class Type>
void StackLinkedList<Type>::push(Node* newNode)
{
    newNode->next = head;
    head = newNode;
}

template <class Type>
typename StackLinkedList<Type>::Node* StackLinkedList<Type>::pop()
{
    Node* top = head;
    head = head->next;
    return top;
}

#endif
