#ifndef RAVEN_ENTITY_H_INCLUDED
#define RAVEN_ENTITY_H_INCLUDED

#include "EntityComponentSystem/ECSTypes.h"

#include <cstddef>
#include <unordered_map>
#include <set>

namespace Raven
{
    //Entity will be holding pointers to all its components for ease of use.
    class Component;
    class Entity
    {
        friend class EntityManager;

        using EntityComponents = std::unordered_map<ComponentId, Component*>;
    public:

        inline EntityId getId() const { return m_id; }
        inline EntityId getParent() const { return m_parentId; }
        inline const std::set<EntityId>& getChildren() const { return m_children; }
        template<typename ComponentType>
        void addComponent(ComponentType* pComponent)
        {
            //No duplicates allowed.
            ComponentId id = ComponentType::ID;
            if (m_components.find(id) != m_components.end())
                return;

            m_components.emplace(id, pComponent);
            //Add a tag to this component.
            auto component = (ComponentType*)m_components.at(id);
            component->setEntityId(m_id);
        }
        const EntityComponents& getComponents() const { return m_components; }

        //!Get component pointer of a given type if the entity has it.
        template <class ComponentType>
        ComponentType* getComponent() const
        {
            ComponentId id = ComponentType::ID;
            auto component = m_components.find(id);
            if (component != m_components.end())
            {
                return (ComponentType*)component->second;
            }
            return nullptr;
        }
        //Explicit to mark that it has to be this type.
        explicit Entity(EntityId id) : m_id(id), m_parentId(0), m_children({}) {}
    private:
        //Entities are not allowed to be copied but they can be moved.
        Entity(const Entity&) = delete;
        Entity(Entity&&) = default;
        Entity& operator=(const Entity&) = delete;
        Entity& operator=(Entity&&) = default;
        //~Entity() = default;

        EntityId m_id;
        EntityComponents m_components;
        EntityId m_parentId;
        std::set<EntityId> m_children;
    };

    //Aliasing for shorter code.
    using Entities = std::unordered_map<EntityId, Entity>;
}

#endif