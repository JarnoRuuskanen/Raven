#ifndef RAVEN_UTILITY_STRING_HASH_H_INCLUDED
#define RAVEN_UTILITY_STRING_HASH_H_INCLUDED

#include <cstdint>
#include <cstring>

namespace Raven
{
    namespace RavenUtility
    {
        //String hashing, this implementation is from Game Engine Gems 3 lesson 14.
        namespace CompileTime
        {
            template <size_t Index>
            struct Hash
            {
                template <size_t N>
                constexpr static inline uint32_t generate(const char (&str)[N])
                {
                    return static_cast<uint32_t>(static_cast<uint64_t>(Hash<Index - 1u>::generate(str) ^ uint32_t(str[Index - 1u])) * 16777619ull);
                }
            };

            template<>
            struct Hash<0u>
            {
                template <size_t N>
                constexpr static inline uint32_t generate(const char (&str)[N])
                {
                    return 2166136261u;
                }
            };
        }

        namespace RunTime
        {
            inline uint32_t fnv1aHash(uint32_t hash, const char* str, size_t length)
            {
                for(size_t i = 0; i < length; ++i)
                {
                    const uint32_t value = static_cast<uint32_t>(*str++);
                    hash ^= value;
                    hash *= 16777619u;
                }

                return hash;
            }

            inline uint32_t fnv1aHash(const char* str, size_t length)
            {
                return fnv1aHash(2166136261u, str, length);
            }

            inline uint32_t fnv1aHash(const char* str)
            {
                return fnv1aHash(str, std::strlen(str));
            }
        }

        namespace Detail
        {
            template <typename T>
            struct HashHelper{};

            template <>
            struct HashHelper<const char*>
            {
                static inline uint32_t generate(const char* str)
                {
                    return RunTime::fnv1aHash(str);
                }
            };

            template <size_t N>
            struct HashHelper<char [N]>
            {
                constexpr static inline uint32_t generate(const char (&str)[N])
                {
                    return CompileTime::Hash<N - 1u>::generate(str);
                }
            };
        }

    template <typename Type>
    constexpr static inline uint32_t generateHash(const Type& str)
    {
        return Detail::HashHelper<Type>::generate(str);
    }

    class StringHash
    {
    public:
        template <typename T>
        StringHash(const T& str) : m_hash(generateHash(str))
        {
        }

        inline uint32_t getHash(void) const
        {
            return m_hash;
        }
    private:
        uint32_t m_hash;
    };

    }
}
#endif