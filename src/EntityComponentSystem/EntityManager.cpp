#include "EntityComponentSystem/EntityManager.h"
#include "Utility/MemoryAllocation/PoolAllocator.h"
#include "Utility/MemoryAllocation/StackAllocator.h"
#include <AssetLoader/AssetLoader.h>
#include <GraphicsApi/Vulkan/VulkanUtility/GraphicsPipelineUtility.h>
#include <GraphicsApi/Vulkan/VulkanUtility/StructInitializer.h>
#include <Utility/DelegateManager.h>

#include "EntityComponentSystem/RavenComponents/Light.h"
#include "EntityComponentSystem/RavenComponents/Renderable.h"

#include "GraphicsApi/Vulkan/VulkanUtility/UtilityFunctions.h"

namespace Raven
{
    EntityManager::EntityManager() : m_entityCount(0), m_invalidEntity(-1)
    {
    }

    EntityManager::~EntityManager()
    {

    }

    void EntityManager::initialize(bool loadUI)
    {
        m_vulkanManager.initialize();

        EntityManagerDelegates entityManagerDelegates;
        entityManagerDelegates.getAllEntitiesFunc.Bind<EntityManager, &EntityManager::getAllEntities>(this);
        entityManagerDelegates.getEntityDelegateFunc.Bind<EntityManager, &EntityManager::getEntity>(this);
        entityManagerDelegates.getMeshComponentHashesFunc.Bind<EntityManager, &EntityManager::getMeshComponentNames>(this);
        entityManagerDelegates.addNewEntityFunc.Bind<EntityManager, &EntityManager::createEntity>(this);
        entityManagerDelegates.getAllComponentsFunc.Bind<EntityManager, &EntityManager::getAllComponents>(this);
        entityManagerDelegates.addComponentToEntityFunc.Bind<EntityManager, &EntityManager::addComponentToEntity>(this);
        /*entityManagerDelegates.changeEntityMeshFunc.Bind < EntityManager, &EntityManager::changeEntityMesh>(this);*/

        DelegateManager& manager = DelegateManager::getInstance();
        manager.registerEntityManagerDelegates(entityManagerDelegates);

        allocateComponentMemoryContainers();
        createSystems();
        createRootEntity();
        createErrorAssets();

        if(loadUI)
        {
            m_renderSystem->initializeUI();
        }
    }

    void EntityManager::createRootEntity()
    {
        m_root = createEntity();

        TagData tagData = {};
        tagData.name = "Root";
        Tag* tag = createComponent<Tag>();
        tag->init((void*)&tagData);
        m_root->addComponent<Tag>(tag);

        //Adding new entities is pretty cumbersome.
        TransformData transformData;
        //Going to move the terrain to be around the starting point.
        transformData.translation = glm::vec3(0.0, -1.0, 0.0);
        transformData.rotation = glm::quat(glm::vec3(glm::radians(0.0f), glm::radians(0.0f), glm::radians(0.0f)));
        transformData.scale = glm::vec3(1.0f);
        transformData.localTransform = glm::translate(glm::mat4{ 1.0f }, transformData.translation);
        transformData.worldTransform = glm::translate(glm::mat4{ 1.0f }, transformData.translation);
        Transform* transform = createComponent<Transform>();
        transform->init((void*)&transformData);

        //Place at origin.
        transform->m_worldTransform = glm::translate(glm::mat4{ 1.0f }, glm::vec3{ 0.0f });
        m_root->addComponent<Transform>(transform);

        notifySystems(m_root);

        std::cout << "Root created.\n";
    }

    void EntityManager::generateTerrain(const TextureData& heightMap)
    {
        std::vector<Vertex> vertices;
        std::vector<uint32_t> indices;
        //TerrainUtility::generatePlaneFromHeightMap(heightMap, 60.0f, terrainMesh);
        uint32_t planeWidth = 64; //heightMap.width;
        TerrainUtility::generatePlane(planeWidth, glm::vec3(0.5,0.5,0.5), vertices, indices);

        //Let the terrain be the root entity.
        Entity* terrain = createEntity();

        TagData tagData = {};
        tagData.name = "Terrain";
        addComponentToEntity(Tag::ID, terrain->getId(), (void*)&tagData);

        //Adding new entities is pretty cumbersome.
        TransformData transformData;
        //Going to move the terrain to be around the starting point.
        transformData.translation = glm::vec3(-static_cast<float>(planeWidth) / 2, -1.0f, -static_cast<float>(planeWidth) / 2);
        transformData.rotation = glm::quat(glm::vec3(glm::radians(0.0f), glm::radians(0.0f), glm::radians(0.0f)));
        transformData.scale = glm::vec3(1.0f);
        transformData.localTransform = glm::translate(glm::mat4{ 1.0f }, transformData.translation);
        transformData.worldTransform = glm::translate(glm::mat4{ 1.0f }, transformData.translation);
        Transform* transform = createComponent<Transform>();
        transform->init((void*)&transformData);

        //Place at origin.
        transform->m_worldTransform = glm::translate(glm::mat4{ 1.0f }, glm::vec3{0.0f});
        terrain->addComponent<Transform>(transform);

        //This whole mesh thing being allocated this way might not be a good idea since there are void pointers inside
        //Vma objects so the size is not known beforehand.
        Mesh* mesh = createComponent<Mesh>();
        m_vulkanManager.createRavenVertexBuffer((void*)vertices.data(),sizeof(Vertex), vertices.size(), mesh->m_vertexBuffer);
        m_vulkanManager.createRavenIndexBuffer((void*)indices.data(),sizeof(uint32_t), indices.size(), mesh->m_indexBuffer);

        uint32_t serialization = RavenUtility::generateHash("Terrain");
        mesh->m_serialization = serialization;
        //m_meshSerializations["Terrain"] = serialization;

        m_meshComponentPointers[serialization] = mesh;

        Renderable* renderable = createAndInitializeRenderable(mesh, nullptr);

        terrain->addComponent<Renderable>(renderable);

        notifySystems(terrain);

        PushConstantData terrainColor;
        m_terrainColor = glm::vec3(1.0);
        terrainColor.data = &m_terrainColor;
        terrainColor.dataOffset = 0;
        terrainColor.dataSize = sizeof(glm::vec3);
        m_entityPushConstantDatas.insert(std::make_pair(terrain->getId(), terrainColor));

        std::vector<PushConstantData*> terrainPushConstantPointers = {&m_entityPushConstantDatas.at(terrain->getId())};

        m_renderSystem->registerRenderableToRenderPipeline(renderable, terrainPushConstantPointers, "forward");
    }

    void EntityManager::shutdown()
    {
        m_vulkanManager.shutdown();
    }

    void EntityManager::createTestLights()
    {
        Light* light = createComponent<Light>();
        light->init(nullptr);
        light->lightColor = glm::vec3(1.0f, 1.0f, 1.0f);
        light->lightIntensity = 120;
        light->hasChanged = true;
        light->lightType = LIGHT_TYPE::SUN;

        Entity* lightEntity = createEntity();
        EntityId lightEntityId = lightEntity->getId();
        lightEntity->addComponent(light);

        Tag* lightTag = createComponent<Tag>();
        TagData lightTagData = {};
        lightTagData.name = "light";
        lightTag->init((void*)&lightTagData);
        lightEntity->addComponent<Tag>(lightTag);

        /*setParentForEntity(lightEntityId, 2);*/

        TransformData transformData = {};
        transformData.scale = glm::vec3(0.1f);
        transformData.translation = glm::vec3(1.0f, 12.0f, 1.0f);
        transformData.rotation = glm::quat(glm::vec3(glm::radians(0.0), glm::radians(0.0f), glm::radians(0.0f)));
        transformData.localTransform = glm::translate(glm::mat4{ 1.0f }, transformData.translation);
        addComponentToEntity(Transform::ID, lightEntityId, (void*)&transformData);

        //ComponentId meshId = Mesh::ID;
        //Mesh* lightMesh = static_cast<Mesh*>(m_worldComponents[meshId].back()); //Sphere
        //Renderable* lightRenderable = createRenderable(lightMesh, nullptr);
        //lightEntity->addComponent(lightRenderable);

        //PushConstantData lightData;
        //lightData.data = &light->lightColor;
        //lightData.dataOffset = 0;
        //lightData.dataSize = sizeof(light->lightColor);
        //m_entityPushConstantDatas.insert(std::make_pair(lightEntityId, lightData));

        /*std::vector<PushConstantData*> pushConstantDatas = {&m_entityPushConstantDatas.at(lightEntityId)};
        m_renderSystem->registerRenderableToRenderPipeline(lightRenderable, pushConstantDatas, "mesh");*/

        notifySystems(lightEntity);

        //Second light.
        //Light* secondLight = createComponent<Light>();
        //secondLight->init(nullptr);
        //secondLight->lightColor = glm::vec3(1.0f, 1.0f, 1.0f);
        //secondLight->lightIntensity = 3;
        //secondLight->hasChanged = true;
        //secondLight->lightType = LIGHT_TYPE::SUN;

        //Entity* secondLightEntity = createEntity();
        //EntityId secondLightId = secondLightEntity->getId();
        //secondLightEntity->addComponent(secondLight);

        //TransformData secondTransformData = {};
        //secondTransformData.scale = glm::vec3(0.1f);
        //secondTransformData.translation = glm::vec3(-0.3f, 1.0f, 1.0f);
        //secondTransformData.rotation = glm::quat(glm::vec3(glm::radians(0.0f), glm::radians(0.0f), glm::radians(0.0f)));
        //secondTransformData.localTransform = glm::translate(glm::mat4{ 1.0f }, secondTransformData.translation);
        //addComponentToEntity(Transform::ID, secondLightId, (void*)&secondTransformData);

        //notifySystems(secondLightEntity);

        m_renderSystem->setupLights(m_lightSystem->getLightBuffer());
    }

    void EntityManager::createErrorAssets()
    {
        AssetLoader::loadTexture("../assets/world_assets/white.png", m_lightTexture);
        VkFormat format = VK_FORMAT_R8G8B8A8_UNORM;
        bool success = m_vulkanManager.createAlbedoOnlyMaterial(format, m_lightTexture, m_lightMaterial);
        assert(success);

        m_missingMaterialData = {};
        success = AssetLoader::loadTexture("../assets/world_assets/error_material/albedoMissing.png", m_missingAlbedo);
        assert(success);
        if (success)
        {
            m_missingMaterial.m_hasBaseColor = true;
            m_missingMaterialData.baseColorTexture = &m_missingAlbedo;

        }
        success = AssetLoader::loadTexture("../assets/world_assets/error_material/normalMissing.png", m_missingNormal);
        assert(success);
        if (success)
        {
            m_missingMaterial.m_hasNormal = true;
            m_missingMaterialData.normalTexture = &m_missingNormal;
        }

        success = m_vulkanManager.createRavenMaterial(m_missingMaterialData, m_missingMaterial);
        assert(success);
    }

    void EntityManager::createSystems()
    {
        m_renderSystem = new RenderSystem(this);
        m_playerControlSystem = new PlayerControlSystem(this);
        m_transformSystem = new TransformSystem(this);
        m_lightSystem = new LightSystem(this);
        m_animationSystem = new AnimationSystem(this);

        m_systems[0] = m_renderSystem;
        m_systems[1] = m_playerControlSystem;
        m_systems[2] = m_transformSystem;
        m_systems[3] = m_lightSystem;
        m_systems[4] = m_animationSystem;
    }

    void EntityManager::notifySystems(Entity* entity)
    {
        for (auto& system : m_systems)
        {
            system->onEntityCreated(entity);
        }
    }

    //TODO:Possibly delete.
    Entity* EntityManager::createPlayerEntity()
    {
        Entity* entity = createEntity();

        m_playerTransformComponent = createComponent<Transform>();
        m_playerTransformComponent->m_position = glm::vec3(0.0f,5.0f,5.0f);
        m_playerTransformComponent->m_rotation = glm::vec3(0.0f);
        m_playerTransformComponent->m_scale = glm::vec3(1.0f);
        m_playerTransformComponent->m_localTransform = glm::translate(m_playerTransformComponent->m_localTransform, m_playerTransformComponent->m_position);

        entity->addComponent<Transform>(m_playerTransformComponent);

        m_playerCameraComponent = createComponent<Camera>();
        m_playerCameraComponent->m_viewMatrix = glm::lookAt(glm::vec3{5}, glm::vec3{0}, glm::vec3(0.0f, 1.0f, 0.0f));
        m_playerCameraComponent->m_cameraUp = glm::vec3(0.0f,1.0f,0.0f);
        m_playerCameraComponent->m_cameraFront = glm::vec3(1.0f, 0.0f,0.0f);
        entity->addComponent<Camera>(m_playerCameraComponent);

        Tag* tag = createComponent<Tag>();
        TagData tagData;
        tagData.name = "Player";
        tag->init(&tagData);
        entity->addComponent<Tag>(tag);

        notifySystems(entity);

        m_renderSystem->setActivePlayer(entity);
        m_lightSystem->setViewTransform(m_playerTransformComponent);

        return entity;
    }

    void EntityManager::allocateComponentMemoryContainers()
    {
        size_t generalCount = 1000;

        size_t transformComponentCount = 1000;
        size_t transformComponentSize = sizeof(Transform);
        ComponentId transformId = Transform::ID;
        m_poolAllocators[transformId] = new PoolAllocator(transformComponentCount * transformComponentSize, transformComponentSize);
        m_poolAllocators[transformId]->init();
        m_worldComponents[transformId] = {};

        size_t renderableComponentCount = 1000;
        ComponentId renderableId = Renderable::ID;
        m_poolAllocators[renderableId] = new PoolAllocator(renderableComponentCount * sizeof(Renderable), sizeof(Renderable));
        m_poolAllocators[renderableId]->init();
        m_worldComponents[renderableId] = {};

        size_t cameraComponentSize = sizeof(Camera);
        ComponentId cameraId = Camera::ID;
        m_poolAllocators[cameraId] = new PoolAllocator(generalCount * cameraComponentSize, cameraComponentSize);
        m_poolAllocators[cameraId]->init();
        m_worldComponents[cameraId] = {};

        ComponentId meshId = Mesh::ID;
        size_t maxMeshCount = 1000;
        size_t meshComponentSize = sizeof(Mesh);
        m_poolAllocators[meshId] = new PoolAllocator(maxMeshCount * meshComponentSize, meshComponentSize);
        m_poolAllocators[meshId]->init();
        m_worldComponents[meshId] = {};

        ComponentId materialId = Material::ID;
        size_t maxMaterialCount = 1000;
        size_t materialSize = sizeof(Material);
        m_poolAllocators[materialId] = new PoolAllocator(maxMaterialCount * materialSize, materialSize);
        m_poolAllocators[materialId]->init();
        m_worldComponents[materialId] = {};

        ComponentId lightId = Light::ID;
        size_t lightSize = sizeof(Light);
        m_poolAllocators[lightId] = new PoolAllocator(generalCount * lightSize, lightSize);
        m_poolAllocators[lightId]->init();
        m_worldComponents[lightId] = {};

        ComponentId tagId = Tag::ID;
        size_t tagSize = sizeof(Tag);
        m_poolAllocators[tagId] = new PoolAllocator(generalCount * tagSize, tagSize);
        m_poolAllocators[tagId]->init();
        m_worldComponents[tagId] = {};

        ComponentId animationId = Animation::ID;
        size_t animationSize = sizeof(Animation);
        m_poolAllocators[animationId] = new PoolAllocator(100 * animationSize, animationSize);
        m_poolAllocators[animationId]->init();
        m_worldComponents[animationId] = {};

        ComponentId skeletonId = Skeleton::ID;
        size_t skeletonSize = sizeof(Skeleton);
        m_poolAllocators[skeletonId] = new PoolAllocator(100 * skeletonSize, skeletonSize);
        m_poolAllocators[skeletonId]->init();
        m_worldComponents[skeletonId] = {};
    }

    Entity* EntityManager::createEntity()
    {
        const EntityId id = m_entityCount++;
        m_entities.emplace(std::piecewise_construct, std::forward_as_tuple(id), std::forward_as_tuple(id));

        //Set root as default parent for every entity
        if (id != 0)
        {
            setParentForEntity(id, m_root->getId());
        }
        return &m_entities.at(id);
    }

    void EntityManager::destroyEntity(EntityId id)
    {
    }

    const Entity& EntityManager::getEntity(const EntityId id) const
    {
        if(m_entities.find(id) != m_entities.end())
        {
            return m_entities.at(id);
        }
        return m_invalidEntity;
    }

    void EntityManager::update(const Timer& timer)
    {
        m_transformSystem->update(timer);
        m_animationSystem->update(timer);
        m_lightSystem->update(timer);
        m_renderSystem->update(timer);
        m_playerControlSystem->update(timer);
    }

    //Sphere collision for early collision implementation. Implement
    //more robust version once this one works.
    //Real-Time Rendering page 957 for reference.
    //bool objectClicked(const glm::vec3 &ray, const glm::vec3& rayOriginPos, const glm::vec3& objectPos, float radius)
    //{
    //    //Calculate vector from ray origin to object position.
    //    const glm::vec3 fromRayOriginToObject = objectPos - rayOriginPos;
    //    //Calculate squared length of this vector.
    //    float rayToObjectSquaredLength = glm::length(fromRayOriginToObject) * glm::length(fromRayOriginToObject);
    //    //If the length above is less than the squared length of the object radius, then the ray cast originated 
    //    //from inside the sphere and can be seen as hitting the object.
    //    float squaredRadiusLength = radius * radius;
    //    if (rayToObjectSquaredLength < squaredRadiusLength)
    //    {
    //        return true;
    //    }

    //    //Next we calculate projection of fromRayOriginToObject on the ray direction.
    //    auto s = glm::dot(fromRayOriginToObject, ray);
    //    glm::vec3 projectionPoint = s * ray;
    //    //First real rejection test.
    //    if (s < 0)
    //    {
    //        return false;
    //    }

    //    float objectToProjectionSquared = rayToObjectSquaredLength - (s * s);
    //    if (objectToProjectionSquared < squaredRadiusLength)
    //    {
    //        return true;
    //    }
    //    
    //    return false;
    //}

    //TODO: CPU SIDE PICKING IS EXTREMELY INEFFICIENT. DO IT ON GPU SIDE INSTEAD USING THE GRAPHICS PIPELINE!
    //READ MORE IN REAL-TIME RENDERING PAGE 942.
    //const EntityId EntityManager::selectClickedEntity(const glm::vec3 &ray) const
    //{
    //    bool entitySelected = false;
    //    EntityId id = INVALID_ENTITY_ID;
    //    //TODO:Add quadtrees and world chunks so most of the entities 
    //    //can be rejected as early as possible.
    //    for(const auto& iter : m_entities)
    //    {
    //        //Skip terrain entity.
    //        if(iter.first != 0)
    //        {
    //            const auto& entity = iter.second;
    //            Transform* transform = entity.getComponent<Transform>();
    //            if(transform)
    //            {
    //                EntityId candidate = transform->getEntityId();
    //                const Entity& entity = m_entities.at(candidate);
    //                Renderable* renderable = entity.getComponent<Renderable>();
    //                if(renderable)
    //                {
    //                    if (renderable->m_isVisible)
    //                    {
    //                        glm::vec3 pos = transform->m_position;
    //                        float radius = renderable->m_mesh->m_sphereRadius;
    //                        //Find the closest point to the center of the sphere on
    //                        //this ray cast. If it is closer than the radius,
    //                        //we have a hit.
    //                        bool intersects = objectClicked(ray, m_playerTransformComponent->m_position, pos, radius);
    //                        if (intersects)
    //                        {
    //                            std::cout << "Object clicked.\n";
    //                            entitySelected = true;
    //                            id = candidate;
    //                            break;
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //    }
    //    m_renderSystem->updateSelectedObjectInfo(id);

    //    return id;
    //}

    void EntityManager::addComponentToEntity(ComponentId componentId, EntityId entityId, void* data)
    {
        //TODO: optimize
        Entity& entity = m_entities.at(entityId);
        assert(entity.getId() != INVALID_ENTITY_ID);
        switch(componentId)
        {
            case Renderable::ID :
            {
                Renderable* renderable = createComponent<Renderable>();
                renderable->init(data);
                entity.addComponent(renderable);
                break;
            }
            case Mesh::ID :
            {
                Mesh* mesh = createComponent<Mesh>();
                mesh->init(data);
                entity.addComponent(mesh);
                break;
            }
            case Tag::ID :
            {
                Tag* tag = createComponent<Tag>();
                tag->init(data);
                entity.addComponent(tag);
                break;
            }
            case Material::ID :
            {
                Material* material = createComponent<Material>();
                material->init(data);
                entity.addComponent(material);
                break;
            }
            case Transform::ID :
            {
                Transform* transform = createComponent<Transform>();
                transform->init(data);
                entity.addComponent(transform);
                break;
            }
            case Camera::ID :
            {
                Camera* camera = createComponent<Camera>();
                camera->init(data);
                entity.addComponent(camera);
                break;
            }
            case Light::ID :
            {
                Light* light = createComponent<Light>();
                light->init(data);
                entity.addComponent(light);
                break;
            }
            default:
            {
                assert(!"No such component type!");
            }
        }
    }

    //void EntityManager::changeEntityMesh(EntityId id, uint32_t meshHash)
    //{
    //    auto& it = m_entities.find(id);
    //    if (it != m_entities.end())
    //    {
    //        Renderable* renderable = it->second.getComponent<Renderable>();
    //        if (renderable)
    //        {
    //            //Go through all available meshes, check their names and if match is found, change mesh.
    //            auto id = Mesh::ID;
    //            auto& availableMeshes = m_worldComponents[id];
    //            for (auto& component : availableMeshes)
    //            {
    //                Mesh* mesh = static_cast<Mesh*>(component);
    //                if (mesh->m_serialization == meshHash)
    //                {
    //                    renderable->m_mesh = mesh;
    //                    break;
    //                }
    //            }
    //        }
    //    }
    //}

    Renderable* EntityManager::createAndInitializeRenderable(Raven::Mesh* mesh, Raven::Material* material)
    {
        Renderable* renderable = createComponent<Renderable>();
        renderable->m_mesh = mesh;
        renderable->m_material = material ? material : &m_missingMaterial;
        renderable->m_isVisible = true;

        VkBufferCreateInfo uboCreateInfo = StructInitializer::getBufferCreateInfo({}, sizeof(UniformBufferObject), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);

        DelegateManager& manager = DelegateManager::getInstance();
        manager.getVulkanResourceManagerDelegates().createAllocatedBufferFunc.Invoke(uboCreateInfo, VMA_MEMORY_USAGE_CPU_TO_GPU, renderable->m_uniformBuffer);

        return renderable;
    }

    Skeleton* EntityManager::createAndInitializeSkeleton()
    {
        Skeleton* skeleton = createComponent<Skeleton>();
        VkBufferCreateInfo uboCreateInfo = StructInitializer::getBufferCreateInfo({}, sizeof(SkeletonUniform), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);

        DelegateManager& manager = DelegateManager::getInstance();
        manager.getVulkanResourceManagerDelegates().createAllocatedBufferFunc.Invoke(uboCreateInfo, VMA_MEMORY_USAGE_CPU_TO_GPU, skeleton->m_uniformBuffer);
        return skeleton;
    }

    const Entity& EntityManager::getEntity(EntityId id)
    {
        const auto& it = m_entities.find(id);
        if (it != m_entities.end())
        {
            return it->second;
        }
        return m_invalidEntity;
    }

    void EntityManager::setParentForEntity(EntityId entityId, EntityId parentEntityId)
    {
        assert(entityId != parentEntityId);
        Entity& entity = m_entities.at(entityId);
        //TODO: Make sure that there is no cyclic relation (parent being child to the current entityId etc).
        entity.m_parentId = parentEntityId;
        //Parent's children-list should also be updated.
        Entity& parentEntity = m_entities.at(parentEntityId);
        addChildForEntity(parentEntity, entityId);
    }

    void EntityManager::setChildrenForEntity(EntityId entityId, const std::vector<EntityId> &childrenIds)
    {
        Entity& entity = m_entities.at(entityId);
        for(const auto& id : childrenIds)
        {
            addChildForEntity(entity, id);
        }
    }

    void EntityManager::addChildForEntity(Entity& entity, EntityId childId)
    {
        bool isDifferent = entity.getId() != childId;
        if(isDifferent)
        {
            entity.m_children.insert(childId);
            //Child should also have its parent-id set.
            Entity& childEntity = m_entities.at(childId);
            if(childEntity.m_parentId != entity.getId())
            {
                setParentForEntity(childId, entity.getId());
            }
        }
    }
}