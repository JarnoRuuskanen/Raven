#ifndef RAVEN_RENDER_TARGET_H_INCLUDED
#define RAVEN_RENDER_TARGET_H_INCLUDED

#include <vector>
#include "RavenImage.h"
#include "GraphicsApi/Vulkan/Common.h"

namespace Raven
{
    enum class RenderTargetType
    {
        OFFSCREEN,
        SWAPCHAIN
    };

    struct RenderTarget
    {
        std::vector<RavenImage> targetImageAttachments;
        std::vector<RavenImage> targetDepthAttachments;
        VkFormat targetImageFormat;
        VkExtent2D targetExtent;
        std::vector<VkFramebuffer> targetFramebuffers;
        RenderTargetType targetType;
    };
}

#endif