#ifndef PERLIN_NOISE_H_INCLUDED
#define PERLIN_NOISE_H_INLCUDED

#include <vector>
#include <cassert>

namespace Raven
{
    namespace PerlinNoiseGenerator
    {
        void generate1DSeed(size_t outputSize, std::vector<float>& seed1D)
        {
            assert(seed1D.empty());
            seed1D.clear();
            seed1D.shrink_to_fit();

            for (size_t i = 0; i < outputSize; i++)
            {
                float noise = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
                seed1D.push_back(noise);
            }
        }

        //These two functions are basically identical but having them for the sake of being clear.
        void generate2DSeed(size_t width, size_t height, std::vector<float>& seed2D)
        {
            assert(seed2D.empty());
            seed2D.clear();
            seed2D.resize(width*height);

            for (size_t i = 0; i < width * height; i++)
            {
                float noise = (float)rand() / (float)RAND_MAX;
                seed2D[i] = noise;
            }
        }

        void generate1DPerlinNoise(int count, std::vector<float> seed, size_t octaves, float scaleBias, std::vector<float>& output)
        {
            assert(output.empty());

            for (size_t x = 0; x < count; x++)
            {
                float noise = 0.0f;
                float scale = 1.0f;
                //Make sure that output is between 0 and 1.
                float scaleAcc = 0.0f;

                for (size_t octave = 0; octave < octaves; octave++)
                {
                    //Dividing by 2 depending on which octave we are operating on.
                    int pitch = count >> octave;
                    int sample1 = (x / pitch) * pitch;
                    int sample2 = (sample1 + pitch) % count;

                    float blend = static_cast<float>(x - sample1) / static_cast<float>(pitch);
                    //Linear interpolation
                    float finalSample = (1.0f - blend) * seed[sample1] + blend * seed[sample2];
                    noise += finalSample * scale;
                    scaleAcc += scale;

                    scale = scale / scaleBias;
                }

                float finalNoiseValue = noise / scaleAcc;
                output.push_back(finalNoiseValue);
            }
        }

        void generate2DPerlinNoise(size_t width, size_t height, std::vector<float> seed, size_t octaves, float scaleBias, std::vector<float>& output)
        {
            assert(output.empty());
            output.resize(width * height);

            for (size_t x = 0; x < width; x++)
            {
                for (size_t y = 0; y < height; y++)
                {
                    float noise = 0.0f;
                    float scale = 1.0f;
                    //Make sure that output is between 0 and 1.
                    float scaleAcc = 0.0f;

                    for (size_t octave = 0; octave < octaves; octave++)
                    {
                        //Dividing by 2 depending on which octave we are operating on.
                        int pitch = width >> octave;
                        int sampleX = (x / pitch) * pitch;
                        int sampleY = (y / pitch) * pitch;

                        int sampleX2 = (sampleX + pitch) % width;
                        int sampleY2 = (sampleY + pitch) % width;


                        float blendX = static_cast<float>(x - sampleX) / static_cast<float>(pitch);
                        float blendY = static_cast<float>(y - sampleY) / static_cast<float>(pitch);

                        //Linear interpolation
                        float finalSampleX = (1.0f - blendX) * seed[sampleY * width + sampleX] + blendX * seed[sampleY * width + sampleX2];
                        float finalSampleY = (1.0f - blendX) * seed[sampleY2 * width + sampleX] + blendX * seed[sampleY2 * width + sampleX2];

                        noise += (blendY * (finalSampleY - finalSampleX) + finalSampleX) * scale;
                        scaleAcc += scale;

                        scale = scale / scaleBias;
                    }

                    float finalNoiseValue = noise / scaleAcc;
                    output[y * width + x] = (finalNoiseValue);
                }
            }
        }  
    };
}

#endif