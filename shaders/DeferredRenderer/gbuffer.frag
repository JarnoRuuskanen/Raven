//Reference with some changes: 
//https://github.com/SaschaWillems/Vulkan/blob/master/data/shaders/subpasses/gbuffer.frag

#version 450

layout (location = 0) in vec3 inNormal;
layout (location = 1) in vec3 inColor;
layout (location = 2) in vec3 inWorldPos;
layout (location = 3) in vec2 inTexCoord;

layout(binding = 1) uniform sampler2D albedo;

layout (location = 0) out vec4 outColor;
layout (location = 1) out vec4 outPosition;
layout (location = 2) out vec4 outNormal;
layout (location = 3) out vec4 outAlbedo;


layout (constant_id = 0) const float NEAR_PLANE = 0.1f;
layout (constant_id = 1) const float FAR_PLANE = 256.0f;

float linearDepth(float depth)
{
	float z = depth * 2.0f - 1.0f; 
	return (2.0f * NEAR_PLANE * FAR_PLANE) / (FAR_PLANE + NEAR_PLANE - z * (FAR_PLANE - NEAR_PLANE));	
}

void main()
{
    outPosition = vec4(inWorldPos, 1.0);

    outNormal = vec4(inNormal, 0.0);

    outPosition.a = linearDepth(gl_FragCoord.z);

    outColor = vec4(0.0);

    outAlbedo = texture(albedo, inTexCoord);
}