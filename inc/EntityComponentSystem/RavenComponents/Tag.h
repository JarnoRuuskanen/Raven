#ifndef RAVEN_TAG_COMPONENT_H_INCLUDED
#define RAVEN_TAG_COMPONENT_H_INCLUDED

#include "Utility/StringHash.h"
#include "Component.h"

namespace Raven
{
    static constexpr ComponentId ID = RavenUtility::generateHash("Tag");
    class Tag : public Component
    {
    public:
        Tag():m_name("No tag label"){}
        virtual bool init(void* data) override final
        {
            TagData* tagData = static_cast<TagData*>(data);
            strcpy(m_name, tagData->name.c_str());
            return true;
        }
        char m_name [64];
    };
}
#endif
