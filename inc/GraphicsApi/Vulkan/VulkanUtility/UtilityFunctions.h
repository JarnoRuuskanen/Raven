#ifndef RAVEN_UTILITIES_H_INCLUDED
#define RAVEN_UTILITIES_H_INCLUDED
#include "GraphicsApi/Vulkan/Common.h"
#include "GraphicsApi/Vulkan/VulkanUtility/VulkanStructs.h"
#include <vector>

namespace Raven
{
    namespace UtilityFunctions
    {
        VkShaderModule createShaderModule(const VkDevice& device, const std::vector<uint32_t>& shaderCode);
        void fillShaderStageData(const VkDevice& device, const std::vector<ShaderLoadInformation>& infos, std::vector<ShaderStageData>& shaderStages);

        VkSemaphore createSemaphore(const VkDevice& device);
        VkSemaphore createTimelineSemaphoreKHR(const VkDevice& device);
        VkFence createSignaledFence(const VkDevice &device);

        int findMemoryType(VkPhysicalDevice physicalDevice, uint32_t typeFilter, VkMemoryPropertyFlags properties);

        [[deprecated]]
        bool createDefaultRenderPass(const VkDevice& device, VkFormat colorAttachmentFormat, VkRenderPass& targetRenderPass);
        VkPresentModeKHR selectBestAvailablePresentMode(const std::vector<VkPresentModeKHR>& presentModes);
        VkFormat getFormatBasedOnTextureChannels(uint32_t channels);
        std::size_t hashVertexVertices(const std::vector<Vertex>& vertices);
    }
}

#endif