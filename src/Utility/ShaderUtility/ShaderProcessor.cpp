#include "Utility/ShaderUtility/ShaderProcessor.h"

namespace Raven
{
    namespace ShaderUtility
    {
        //These default limits are listed in khonos official git:
        //https://github.com/KhronosGroup/glslang/blob/master/StandAlone/ResourceLimits.cpp

        const TBuiltInResource defaultBuiltInResource = {
            /* .MaxLights = */ 32,
            /* .MaxClipPlanes = */ 6,
            /* .MaxTextureUnits = */ 32,
            /* .MaxTextureCoords = */ 32,
            /* .MaxVertexAttribs = */ 64,
            /* .MaxVertexUniformComponents = */ 4096,
            /* .MaxVaryingFloats = */ 64,
            /* .MaxVertexTextureImageUnits = */ 32,
            /* .MaxCombinedTextureImageUnits = */ 80,
            /* .MaxTextureImageUnits = */ 32,
            /* .MaxFragmentUniformComponents = */ 4096,
            /* .MaxDrawBuffers = */ 32,
            /* .MaxVertexUniformVectors = */ 128,
            /* .MaxVaryingVectors = */ 8,
            /* .MaxFragmentUniformVectors = */ 16,
            /* .MaxVertexOutputVectors = */ 16,
            /* .MaxFragmentInputVectors = */ 15,
            /* .MinProgramTexelOffset = */ -8,
            /* .MaxProgramTexelOffset = */ 7,
            /* .MaxClipDistances = */ 8,
            /* .MaxComputeWorkGroupCountX = */ 65535,
            /* .MaxComputeWorkGroupCountY = */ 65535,
            /* .MaxComputeWorkGroupCountZ = */ 65535,
            /* .MaxComputeWorkGroupSizeX = */ 1024,
            /* .MaxComputeWorkGroupSizeY = */ 1024,
            /* .MaxComputeWorkGroupSizeZ = */ 64,
            /* .MaxComputeUniformComponents = */ 1024,
            /* .MaxComputeTextureImageUnits = */ 16,
            /* .MaxComputeImageUniforms = */ 8,
            /* .MaxComputeAtomicCounters = */ 8,
            /* .MaxComputeAtomicCounterBuffers = */ 1,
            /* .MaxVaryingComponents = */ 60,
            /* .MaxVertexOutputComponents = */ 64,
            /* .MaxGeometryInputComponents = */ 64,
            /* .MaxGeometryOutputComponents = */ 128,
            /* .MaxFragmentInputComponents = */ 128,
            /* .MaxImageUnits = */ 8,
            /* .MaxCombinedImageUnitsAndFragmentOutputs = */ 8,
            /* .MaxCombinedShaderOutputResources = */ 8,
            /* .MaxImageSamples = */ 0,
            /* .MaxVertexImageUniforms = */ 0,
            /* .MaxTessControlImageUniforms = */ 0,
            /* .MaxTessEvaluationImageUniforms = */ 0,
            /* .MaxGeometryImageUniforms = */ 0,
            /* .MaxFragmentImageUniforms = */ 8,
            /* .MaxCombinedImageUniforms = */ 8,
            /* .MaxGeometryTextureImageUnits = */ 16,
            /* .MaxGeometryOutputVertices = */ 256,
            /* .MaxGeometryTotalOutputComponents = */ 1024,
            /* .MaxGeometryUniformComponents = */ 1024,
            /* .MaxGeometryVaryingComponents = */ 64,
            /* .MaxTessControlInputComponents = */ 128,
            /* .MaxTessControlOutputComponents = */ 128,
            /* .MaxTessControlTextureImageUnits = */ 16,
            /* .MaxTessControlUniformComponents = */ 1024,
            /* .MaxTessControlTotalOutputComponents = */ 4096,
            /* .MaxTessEvaluationInputComponents = */ 128,
            /* .MaxTessEvaluationOutputComponents = */ 128,
            /* .MaxTessEvaluationTextureImageUnits = */ 16,
            /* .MaxTessEvaluationUniformComponents = */ 1024,
            /* .MaxTessPatchComponents = */ 120,
            /* .MaxPatchVertices = */ 32,
            /* .MaxTessGenLevel = */ 64,
            /* .MaxViewports = */ 16,
            /* .MaxVertexAtomicCounters = */ 0,
            /* .MaxTessControlAtomicCounters = */ 0,
            /* .MaxTessEvaluationAtomicCounters = */ 0,
            /* .MaxGeometryAtomicCounters = */ 0,
            /* .MaxFragmentAtomicCounters = */ 8,
            /* .MaxCombinedAtomicCounters = */ 8,
            /* .MaxAtomicCounterBindings = */ 1,
            /* .MaxVertexAtomicCounterBuffers = */ 0,
            /* .MaxTessControlAtomicCounterBuffers = */ 0,
            /* .MaxTessEvaluationAtomicCounterBuffers = */ 0,
            /* .MaxGeometryAtomicCounterBuffers = */ 0,
            /* .MaxFragmentAtomicCounterBuffers = */ 1,
            /* .MaxCombinedAtomicCounterBuffers = */ 1,
            /* .MaxAtomicCounterBufferSize = */ 16384,
            /* .MaxTransformFeedbackBuffers = */ 4,
            /* .MaxTransformFeedbackInterleavedComponents = */ 64,
            /* .MaxCullDistances = */ 8,
            /* .MaxCombinedClipAndCullDistances = */ 8,
            /* .MaxSamples = */ 4,
            /* .maxMeshOutputVerticesNV = */ 256,
            /* .maxMeshOutputPrimitivesNV = */ 512,
            /* .maxMeshWorkGroupSizeX_NV = */ 32,
            /* .maxMeshWorkGroupSizeY_NV = */ 1,
            /* .maxMeshWorkGroupSizeZ_NV = */ 1,
            /* .maxTaskWorkGroupSizeX_NV = */ 32,
            /* .maxTaskWorkGroupSizeY_NV = */ 1,
            /* .maxTaskWorkGroupSizeZ_NV = */ 1,
            /* .maxMeshViewCountNV = */ 4,
            /* .maxDualSourceDrawBuffersEXT = */ 1,

            /* .limits = */ {
                /* .nonInductiveForLoops = */ 1,
                /* .whileLoops = */ 1,
                /* .doWhileLoops = */ 1,
                /* .generalUniformIndexing = */ 1,
                /* .generalAttributeMatrixVectorIndexing = */ 1,
                /* .generalVaryingIndexing = */ 1,
                /* .generalSamplerIndexing = */ 1,
                /* .generalVariableIndexing = */ 1,
                /* .generalConstantMatrixVectorIndexing = */ 1,
            } };

        std::string getFilePath(const std::string& path)
        {
            size_t index = path.find_last_of("/\\");
            return path.substr(0, index);
        }

        std::string getShaderStageSuffix(const std::string& name)
        {
            const size_t pos = name.rfind('.');
            return (pos == std::string::npos) ? "" : name.substr(pos + 1);
        }

        EShLanguage getShaderStage(const std::string& stage)
        {
            if (stage == "vert")
            {
                return EShLangVertex;
            }
            if (stage == "tesc")
            {
                return EShLangTessControl;
            }
            if (stage == "tese")
            {
                return EShLangTessEvaluation;
            }
            if (stage == "geom")
            {
                return EShLangGeometry;
            }
            if (stage == "frag")
            {
                return EShLangFragment;
            }
            if (stage == "comp")
            {
                return EShLangCompute;
            }

            assert(0 && "No such shader stage!");
            return EShLangCount;
        }

        std::string getFileContents(const std::string& filepath)
        {
            std::ifstream file(filepath);
            if (!file.is_open())
            {
                assert(0 && "Failed to open shader file!");
                return "";
            }

            std::string glslString((std::istreambuf_iterator<char>(file)),
                std::istreambuf_iterator<char>());
            return glslString;
        }

        void compileGLSLToSpirv(const std::string& filename, std::vector<uint32_t>& spirv)
        {
            glslang::InitializeProcess();

            std::string suffix = getShaderStageSuffix(filename);
            EShLanguage shaderType = getShaderStage(suffix);

            glslang::TShader shader(shaderType);

            std::string glslCode = getFileContents(filename);
            const char* code = glslCode.c_str();

            shader.setStrings(&code, 1);

            int clientInputSemanticsVersion = 110;
            glslang::EShTargetClientVersion vulkanClientVersion = glslang::EShTargetVulkan_1_1;
            glslang::EShTargetLanguageVersion targetVersion = glslang::EShTargetSpv_1_0;

            shader.setEnvInput(glslang::EShSourceGlsl, shaderType, glslang::EShClientVulkan, clientInputSemanticsVersion);
            shader.setEnvClient(glslang::EShClientVulkan, vulkanClientVersion);
            shader.setEnvTarget(glslang::EShTargetSpv, targetVersion);

            EShMessages messages = (EShMessages)(EShMsgSpvRules | EShMsgVulkanRules);

            //Preprocessing:

            //Includer is used to located all shaders. 
            //Each shader directory needs to be given to the includer in order for the 
            //keyword #include in shaders to work correctly.
            DirStackFileIncluder defaultIncluder;
            std::string path = getFilePath(filename);
            defaultIncluder.pushExternalLocalDirectory(path);

            const int glslVersion = 450;
            std::string preprocessedGLSL;
            if (!shader.preprocess(&defaultBuiltInResource, glslVersion, ENoProfile, false, false, messages, &preprocessedGLSL, defaultIncluder))
            {
                std::cout << "GLSL Preprocessing failed for: " << filename << std::endl;
                std::cout << shader.getInfoLog() << std::endl;
                std::cout << shader.getInfoDebugLog() << std::endl;
                return;
            }

            //Update shader with the preprocessed shader code.
            const char* preprocessedCString = preprocessedGLSL.c_str();
            shader.setStrings(&preprocessedCString, 1);

            std::cout << preprocessedGLSL << std::endl;

            //Compile shader code to spirv.
            if (!shader.parse(&defaultBuiltInResource, glslVersion, false, messages))
            {
                std::cout << "GLSL parsing failed for: " << filename << std::endl;
                std::cout << shader.getInfoLog() << std::endl;
                std::cout << shader.getInfoDebugLog() << std::endl;
                return;
            }

            glslang::TProgram shaderProgram;
            shaderProgram.addShader(&shader);

            if (!shaderProgram.link(messages))
            {
                std::cout << "GLSL linking failed for: " << filename << std::endl;
                std::cout << shader.getInfoLog() << std::endl;
                std::cout << shader.getInfoDebugLog() << std::endl;
                return;
            }

            //GLSL to spirv:
            spv::SpvBuildLogger logger;
            glslang::SpvOptions spvOptions;
            glslang::GlslangToSpv(*shaderProgram.getIntermediate(shaderType), spirv, &logger, &spvOptions);

            glslang::FinalizeProcess();
        }
    }
}