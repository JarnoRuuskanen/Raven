#include "GraphicsApi/Vulkan/Subpasses/Deferred/CompositionPass.h"
#include "GraphicsApi/Vulkan/VulkanUtility/UtilityFunctions.h"
#include "GraphicsApi/Vulkan/VulkanUtility/StructInitializer.h"
#include "GraphicsApi/Vulkan/VulkanUtility/DescriptorUtility.h"
#include "Utility/DelegateManager.h"

namespace Raven
{
    void CompositionPass::initializeVertexInputState(VkPipelineVertexInputStateCreateInfo& vertexInputState)
    {
        m_vertexInputBindings = { Vertex::getBindingDescription() };
        m_vertexInputAttributes = {};

        auto attributes = Vertex::getAttributeDescriptions();
        for (auto& attribute : attributes)
        {
            m_vertexInputAttributes.push_back(attribute);
        }
        vertexInputState = StructInitializer::getPipelineVertexInputStateCreateInfo(m_vertexInputBindings, m_vertexInputAttributes);
    }

    void CompositionPass::initializeInputAssemblyState(VkPipelineInputAssemblyStateCreateInfo& inputAssembly)
    {
        inputAssembly = StructInitializer::getPipelineInputAssemblyStateCreateInfo(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST, VK_FALSE);
    }

    void CompositionPass::initializeViewportState(VkPipelineViewportStateCreateInfo& viewportState)
    {
        const WindowDelegates& delegates = DelegateManager::getInstance().getWindowDelegates();

        uint32_t windowWidth = delegates.getWindowWidthFunc.Invoke();
        uint32_t windowHeight = delegates.getWindowHeightFunc.Invoke();
        VkExtent2D extent = { windowWidth, windowHeight };

        //Viewport
        VkViewport viewport = {};
        viewport.x = 0.0f;
        viewport.y = 0.0f;
        viewport.width = static_cast<float>(extent.width);
        viewport.height = static_cast<float>(extent.height);
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;
        m_viewports = { viewport };

        //Scissor
        VkRect2D scissor = {};
        scissor.offset = { 0,0 };
        scissor.extent = extent;
        m_scissors = { scissor };

        //Viewport state
        viewportState = StructInitializer::getPipelineViewportStateCreateInfo(m_viewports, m_scissors);
    }

    void CompositionPass::initializeRasterizerState(VkPipelineRasterizationStateCreateInfo& rasterizer)
    {
        rasterizer =
            StructInitializer::getPipelineRasterizationStateCreateInfo(VK_FALSE, VK_FALSE, VK_POLYGON_MODE_FILL,
                1.0f, VK_CULL_MODE_NONE, VK_FRONT_FACE_CLOCKWISE,
                VK_FALSE, 0.0f, 0.0f, 0.0f);
    }

    void CompositionPass::initializeMultisampleState(VkPipelineMultisampleStateCreateInfo& multisampling)
    {
        multisampling = StructInitializer::getPipelineMultisampleStateCreateInfo(VK_FALSE, VK_SAMPLE_COUNT_1_BIT, 1.0f, nullptr, VK_FALSE, VK_FALSE);
    }

    void CompositionPass::initializeDepthStencilState(VkPipelineDepthStencilStateCreateInfo& depthStencil)
    {
        depthStencil = StructInitializer::getPipelineDepthStencilStateCreateInfo(VK_TRUE, VK_TRUE, VK_COMPARE_OP_LESS,
            VK_FALSE, 0.0f, 1.0f,
            VK_FALSE, {}, {});
    }

    void CompositionPass::initializeColorBlendState(VkPipelineColorBlendStateCreateInfo& colorBlend)
    {
        VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
        colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
        colorBlendAttachment.blendEnable = VK_FALSE;
        m_colorBlendAttachments = { colorBlendAttachment };

        colorBlend = StructInitializer::getPipelineColorBlendStateCreateInfo(VK_FALSE, VK_LOGIC_OP_COPY, m_colorBlendAttachments);
    }

    void CompositionPass::initializeDynamicState(VkPipelineDynamicStateCreateInfo& dynamicStateCreateInfo)
    {
        m_dynamicStates = { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_LINE_WIDTH };
        dynamicStateCreateInfo = StructInitializer::getPipelineDynamicStateCreateInfo(m_dynamicStates);
    }

    VkResult CompositionPass::initializePipelineLayout(const VkDevice& device)
    {
        std::vector<ShaderLoadInformation> shadersToLoad = { {"../shaders/DeferredRenderer/composition.vert", VK_SHADER_STAGE_VERTEX_BIT},
                                                            {"../shaders/DeferredRenderer/composition.frag", VK_SHADER_STAGE_FRAGMENT_BIT} };

        VkResult result = describePipeline(device, shadersToLoad);
        assert(result == VK_SUCCESS);
        return VK_SUCCESS;
        ////First deferred pass is going to have only a single uniform buffer which will be 
        ////holding model, view and projection matrices.
        //VkDescriptorSetLayoutBinding positionInputAttachment = {};
        //positionInputAttachment.binding = 0;
        //positionInputAttachment.descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
        //positionInputAttachment.descriptorCount = 1;
        //positionInputAttachment.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
        //positionInputAttachment.pImmutableSamplers = nullptr;

        //VkDescriptorSetLayoutBinding normalInputAttachment = {};
        //normalInputAttachment.binding = 1;
        //normalInputAttachment.descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
        //normalInputAttachment.descriptorCount = 1;
        //normalInputAttachment.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
        //normalInputAttachment.pImmutableSamplers = nullptr;

        //VkDescriptorSetLayoutBinding albedoInputAttachment = {};
        //albedoInputAttachment.binding = 2;
        //albedoInputAttachment.descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
        //albedoInputAttachment.descriptorCount = 1;
        //albedoInputAttachment.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
        //albedoInputAttachment.pImmutableSamplers = nullptr;

        //VkDescriptorSetLayoutBinding lightUniformBuffer = {};
        //lightUniformBuffer.binding = 3;
        //lightUniformBuffer.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        //lightUniformBuffer.descriptorCount = 1;
        //lightUniformBuffer.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
        //lightUniformBuffer.pImmutableSamplers = nullptr;

        //m_descriptorSetBindingInfos = { {positionInputAttachment, "samplerPosition"}, {normalInputAttachment, "samplerNormal"}, {albedoInputAttachment, "samplerAlbedo"}, { lightUniformBuffer, "lightUbo" } };

        //std::vector<VkDescriptorSetLayoutBinding> bindings = { positionInputAttachment, normalInputAttachment, albedoInputAttachment, lightUniformBuffer };
        //VkDescriptorSetLayout layout;
        //VkResult result = DescriptorUtility::createDescriptorSetLayout(device, bindings, layout);
        //CHECK_RESULT_FOR_ERRORS(result, "Failed to create a descriptor set layout! \n");

        //m_renderpassPipeline.descriptorSetLayouts = { layout };
        ////Here we tell what kind of descriptors (uniforms, buffers etc.) and push constants go to the pipeline.
        //VkPipelineLayoutCreateInfo pipelineLayoutInfo = StructInitializer::getPipelineLayoutCreateInfo(m_renderpassPipeline.descriptorSetLayouts, {});

        //result = vkCreatePipelineLayout(device, &pipelineLayoutInfo, nullptr, &m_renderpassPipeline.layout);
        //CHECK_RESULT_FOR_ERRORS(result, "Failed to create a pipeline layout! \n");

        //return VK_SUCCESS;
    }

    void CompositionPass::draw(const RavenBuffer& vertexBuffer, const RavenBuffer& indexBuffer, const std::vector<VkDescriptorSet>& descriptorSets, 
                               const std::vector<PushConstantData*>& pushConstants, VkCommandBuffer& cmdBuffer)
    {
        std::array<VkDescriptorSet, 1> classDescriptorSet = { m_descriptorSet };
        vkCmdBindDescriptorSets(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, m_renderpassPipeline.layout, 0, static_cast<uint32_t>(classDescriptorSet.size()), classDescriptorSet.data(), 0, nullptr);
        vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, m_renderpassPipeline.pipeline);
        vkCmdDraw(cmdBuffer, 3, 1, 0, 0);
    }

    void CompositionPass::initializeDescriptorSet(const VkDevice& device, const std::vector<RavenImage>& imageAttachments, const RavenBuffer& lightUniformBuffer)
    {
        //Image attachments includes (in this order): color, position, normal, albedo. We want to use position and normal as input attachments.
        assert(imageAttachments.size() >= 3);

        std::vector<VkDescriptorPoolSize> poolSizes = { {VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 3}, {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1} };
        VkResult result = DescriptorUtility::createDescriptorPool(device, poolSizes, 1, true, m_descriptorPool);
        assert(result == VK_SUCCESS);

        result = DescriptorUtility::allocateDescriptorSet(device, m_descriptorPool, m_renderpassPipeline.descriptorSetLayouts, m_descriptorSet);
        assert(result == VK_SUCCESS);

        VkDescriptorImageInfo positionDescriptor = {};
        positionDescriptor.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        positionDescriptor.imageView = imageAttachments[1].imageView;
        positionDescriptor.sampler = VK_NULL_HANDLE;

        VkDescriptorImageInfo normalDescriptor = {};
        normalDescriptor.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        normalDescriptor.imageView = imageAttachments[2].imageView;
        normalDescriptor.sampler = VK_NULL_HANDLE;

        VkDescriptorImageInfo albedoDescriptor = {};
        albedoDescriptor.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        albedoDescriptor.imageView = imageAttachments[3].imageView;
        albedoDescriptor.sampler = VK_NULL_HANDLE;

        VkDescriptorBufferInfo lightDescriptor = {};
        lightDescriptor.buffer = lightUniformBuffer.buffer;
        lightDescriptor.offset = 0;
        lightDescriptor.range = sizeof(LightUniformBufferObject);

        //Image/buffer writes.
        VkWriteDescriptorSet positionWrite =
            StructInitializer::getWriteDescriptorSet(m_descriptorSet, VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 0, &positionDescriptor);
        VkWriteDescriptorSet normalWrite =
            StructInitializer::getWriteDescriptorSet(m_descriptorSet, VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 1, &normalDescriptor);
        VkWriteDescriptorSet albedoWrite =
                StructInitializer::getWriteDescriptorSet(m_descriptorSet, VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 2, &albedoDescriptor);
        VkWriteDescriptorSet lightWrite =
            StructInitializer::getWriteDescriptorSet(m_descriptorSet, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 3, &lightDescriptor);

        std::array<VkWriteDescriptorSet, 4> writes = {positionWrite, normalWrite, albedoWrite, lightWrite};
        vkUpdateDescriptorSets(device, static_cast<uint32_t>(writes.size()), writes.data(), 0, nullptr);
    }
}
