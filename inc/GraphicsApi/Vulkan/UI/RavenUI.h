#ifndef RAVEN_UI_H_INCLUDED
#define RAVEN_UI_H_INCLUDED

#include <Window/Input.h>
#include <queue>
#include "GraphicsApi/Vulkan/Common.h"
#include "GraphicsApi/Vulkan/Subpasses/UIPass.h"
#include "imgui.h"

namespace Raven
{
    //Note: not sure yet about the current location of this class.
    class RavenUI
    {
    public:
        void update(float deltaTime);
        bool draw(VkCommandBuffer cmdBuffer, uint32_t imageIndex);
        void initialize();
        void updateSelectedObject(const EntityId& id);

        void setTimelineWaitAndSignalValue(uint64_t waitForTimelineValue, uint64_t signalTimelineValue);
        const VkTimelineSemaphoreSubmitInfo& getTimelineSubmitInfo() const { return m_submitInfo; }
        const VkCommandBuffer& getCommandBufferForFrame(uint32_t frameIndex) const { return m_commandBuffers[frameIndex].buffers[0]; }
        const VkCommandPool& getCommandPoolForFrame(uint32_t frameIndex) const { return m_commandBuffers[frameIndex].pool; }
    private:
        bool createFramebuffers();
        bool createDepthResources();
        void updateImGuiIO(float deltaTime);
        void newFrame();
        void updateBuffers();
        void updateSceneData();
        void generateComponentTypeStrings();
        void updateEntityWindow();
        void updateEntityListUI();
        void updateEntityAttributesUI();
        void updateObjectTransformUI();
        void updateObjectLocalTransformUI(Transform* transform);
        void updateObjectWorldTransformUI(Transform* transform);
        void updateObjectLightUI();
        void showComponentInformation(const std::string& component);
        void updateObjectRenderableUI();
        void updateObjectTagUI();
        void updateSelectedObjectRenderable();
        void updateCurrentSelectedPipeline(Renderable* renderable);
        void updateCurrentSelectedMesh(Renderable* renderable);
        void addNewComponentToEntity(ComponentId id);

        bool createRenderpass();
        void saveWindowExtent();
        void saveSystemDelegates();
        void initImGui();
        void bindDelegates();
        void createCommandPoolsAndBuffers(const VkDevice& device, uint32_t frameCount);

        bool mouseKeyCallback(const MouseKeyEvent& event);
        bool keyboardCharacterCallback(unsigned int glfwCodePoint);
        bool keyboardKeyCallback(int key, int scancode, int action, int mods);
        Delegate<bool(const MouseKeyEvent& event)> m_mouseKeyCallbackDelegate;
        Delegate<bool(unsigned int)> m_characterCallbackDelegate;
        Delegate<bool(int key, int scancode, int action, int mods)> m_keyboardCallbackDelegate;

        VkRenderPass m_uiRenderPass;
        UIPass m_uiSubpass;
        VkFormat m_colorAttachmentFormat;
        size_t m_windowWidth;
        size_t m_windowHeight;

        WindowDelegates m_systemDelegates;
        VulkanResourceManagerDelegates m_resourceManagerDelegates;
        RendererDelegates m_rendererDelegates;
        EntityManagerDelegates m_entityManagerDelegates;

        std::vector<VkDescriptorSet> m_descriptorSets;

        std::map<std::string, Component*> m_selectedObjectComponents;
        std::vector<std::string> m_selectedObjectComponentNames;
        EntityId m_selectedEntityId;

        bool m_objectSelected = false;
        bool m_objectSelectionChanged = false;
        int m_currentEntityListSelection;
        int m_currentPipelineSelection;
        int m_currentMeshSelection;
        int m_currentComponentSelection;
        uint32_t m_updateCounter;
        bool m_firstVertexBufferInitialized, m_firstIndexBufferInitialized;

        std::vector<std::string> m_availableMeshes;
        std::unordered_map<std::string, uint32_t> m_meshHashes;
        std::queue<uint32_t> m_meshToSet;

        //Main scene window variables.
        std::vector<std::string> m_entities;
        std::vector<std::string> m_availableComponentTypes;

        RavenBuffer m_vertexBuffer;
        RavenBuffer m_indexBuffer;

        GLFWwindow* m_window;
        VkExtent2D m_swapchainExtent;
        std::vector<VkFramebuffer> m_targetFramebuffers;

        //Depth testing objects.
        VkImage m_depthImage;
        VkDeviceMemory m_depthImageMemory;
        VkImageView m_depthImageView;

        VkTimelineSemaphoreSubmitInfo m_submitInfo;
        uint64_t m_waitForTimelineValue;
        uint64_t m_signalTimelineValue;

        std::vector<PoolAndBuffers> m_commandBuffers;
        std::vector<VkAttachmentDescription> m_attachments;
    };
}

#endif