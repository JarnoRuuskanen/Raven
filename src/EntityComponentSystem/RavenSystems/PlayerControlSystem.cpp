#include <EntityComponentSystem/EntityManager.h>

namespace Raven
{
    float pitch = 0.0f, yaw = -90.0f;
    bool firstMouse = true;
    float fov = 45.0f;
    float lastX, lastY;

    //For calling class function from glfw function.
    using RotateCameraDelegate = Delegate<bool(float, float)>;
    RotateCameraDelegate rotateCameraDelegate;

    PlayerControlSystem::PlayerControlSystem(EntityManager* pGameWorld) : BaseInputType(pGameWorld)
    {
        Input& input = Input::getInstance();
        rotateCameraDelegate.Bind<PlayerControlSystem, &PlayerControlSystem::handleMouseMovementCallback>(this);
        input.registerMouseMovementCallback(rotateCameraDelegate);
    }

    void PlayerControlSystem::update(const Timer& timer)
    {
        static Input& input = Input::getInstance();
        const auto keyStates = input.getKeyboardKeyStates();

        float cameraSpeed = 2.5 * timer.deltaTime;
        if(keyStates[(unsigned int)KeyboardKey::KEY_LEFT_SHIFT])
            cameraSpeed = cameraSpeed * 30;

        for (auto& compTuple : m_components)
        {
            Transform* transform = std::get<Transform*>(compTuple);
            Camera* camera = std::get<Camera*>(compTuple);

            if (keyStates[(unsigned int)KeyboardKey::KEY_W])
            {
                moveForwards(transform, camera, cameraSpeed);
            }
            if (keyStates[(unsigned int)KeyboardKey::KEY_S])
                moveBackwards(transform, camera, cameraSpeed);
            if (keyStates[(unsigned int)KeyboardKey::KEY_A])
                moveLeft(transform, camera, cameraSpeed);
            if (keyStates[(unsigned int)KeyboardKey::KEY_D])
                moveRight(transform, camera, cameraSpeed);
            if(keyStates[(unsigned int)KeyboardKey::KEY_SPACE])
                moveUp(transform, camera, cameraSpeed);
            if(keyStates[(unsigned int)KeyboardKey::KEY_LEFT_CONTROL])
                moveDown(transform, camera, cameraSpeed);
        }
    }

    void PlayerControlSystem::moveForwards(Transform* transform, Camera* camera, float speed)
    {
        /* Replace m_info.cameraInfo in each function to create a "true" fps camera
         * glm::vec3(m_info.cameraFront.x, m_info.position.y, m_info.cameraFront.z);*/
        transform->m_position += speed * camera->m_cameraFront;
    }

    void PlayerControlSystem::moveBackwards(Transform* transform, Camera* camera, float speed)
    {
        transform->m_position -= speed * camera->m_cameraFront;
    }

    void PlayerControlSystem::moveLeft(Transform* transform, Camera* camera, float speed)
    {
        transform->m_position -= glm::normalize(glm::cross(camera->m_cameraFront, camera->m_cameraUp)) * speed;
    }

    void PlayerControlSystem::moveRight(Transform* transform, Camera* camera, float speed)
    {
        transform->m_position += glm::normalize(glm::cross(camera->m_cameraFront, camera->m_cameraUp)) * speed;
    }

    void PlayerControlSystem::moveUp(Transform* transform, Camera* camera, float speed)
    {
        transform->m_position += speed * camera->m_cameraUp;
    }

    void PlayerControlSystem::moveDown(Transform* transform, Camera* camera, float speed)
    {
        transform->m_position -= speed * camera->m_cameraUp;
    }

    bool PlayerControlSystem::handleMouseMovementCallback(float xpos, float ypos)
    {
        if (firstMouse)
        {
            lastX = xpos;
            lastY = ypos;
            firstMouse = false;
        }

        if(!m_pGameWorld->renderingUI())
        {
            float xOffset = xpos - lastX;
            //Y axis is reversed because we want to range from bottom to top.
            float yOffset = lastY - ypos;
            lastX = xpos;
            lastY = ypos;

            float sensitivity = 0.02f;
            xOffset *= sensitivity;
            yOffset *= sensitivity;

            yaw += xOffset;
            pitch += yOffset;

            if (pitch > 89.0f)
                pitch = 89.0f;
            if (pitch < -89.0f)
                pitch = -89.0f;

            glm::vec3 front;
            front.x = cos(glm::radians(pitch)) * cos(glm::radians(yaw));
            front.y = sin(glm::radians(pitch));
            front.z = cos(glm::radians(pitch)) * sin(glm::radians(yaw));

            //Rotate camera.
            for (auto& compTuple : m_components)
            {
                Camera* camera = std::get<Camera*>(compTuple);
                camera->m_cameraFront = front;
            }
            //If not rendering UI, mouse movement should continue.
            return true;
        }
        return false;
    }

    void PlayerControlSystem::entityAddedToSystem(Entity* entity)
    {

    }
}