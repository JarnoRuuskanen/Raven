#ifndef RAVEN_COMPILE_TIME_AUTO_ID_H_INCLUDED
#define RAVEN_COMPILE_TIME_AUTO_ID_H_INCLUDED

#include <cstddef>

namespace Raven
{
    //-----------------------------------------------------------------------------------------------------------
    //NOTE: I did not write this code. It was originally posted to Stackoverflow by username Potatoswatter
    //Reference: https://stackoverflow.com/a/6174263
    //-----------------------------------------------------------------------------------------------------------

    #define COUNTER_READ_CRUMB( TAG, RANK, ACC ) counter_crumb( TAG(), constant_index< RANK >(), constant_index< ACC >() )
    #define COUNTER_READ( TAG ) COUNTER_READ_CRUMB( TAG, 1, COUNTER_READ_CRUMB( TAG, 2, COUNTER_READ_CRUMB( TAG, 4, COUNTER_READ_CRUMB( TAG, 8, \
            COUNTER_READ_CRUMB( TAG, 16, COUNTER_READ_CRUMB( TAG, 32, COUNTER_READ_CRUMB( TAG, 64, COUNTER_READ_CRUMB( TAG, 128, 0 ) ) ) ) ) ) ) )

    #define COUNTER_INC( TAG ) \
    constexpr \
    constant_index< COUNTER_READ( TAG ) + 1 > \
    counter_crumb( TAG, constant_index< ( COUNTER_READ( TAG ) + 1 ) & ~ COUNTER_READ( TAG ) >, \
                                                    constant_index< ( COUNTER_READ( TAG ) + 1 ) & COUNTER_READ( TAG ) > ) { return {}; }    

    template< std::size_t n >
    struct constant_index : std::integral_constant< std::size_t, n > {};

    template< typename id, std::size_t rank, std::size_t acc >
    constexpr constant_index< acc > counter_crumb(id, constant_index< rank >, constant_index< acc >) { return {}; } // found by ADL via constant_index
}

#endif