#include "GraphicsApi/Vulkan/ResourceManager/VulkanResourceManager.h"

#define VMA_IMPLEMENTATION
#include "GraphicsApi/Vulkan/ResourceManager/vk_mem_alloc.h"

namespace Raven
{
    VulkanResourceManager::VulkanResourceManager()
    {

    }

    VulkanResourceManager::~VulkanResourceManager()
    {

    }

    bool VulkanResourceManager::initialize(const VkPhysicalDevice& physicalDevice, const VkDevice& device)
    {
        VmaAllocatorCreateInfo allocatorInfo = {};
        allocatorInfo.physicalDevice = physicalDevice;
        allocatorInfo.device = device;

        VkResult result = vmaCreateAllocator(&allocatorInfo, &m_allocator);
        return result == VK_SUCCESS;
    }

    void VulkanResourceManager::shutdown()
    {
        vmaDestroyAllocator(m_allocator);
    }

    bool VulkanResourceManager::createAllocatedBuffer(const VkBufferCreateInfo& createInfo, VmaMemoryUsage bufferUsage, RavenBuffer& buffer)
    {
        VmaAllocationCreateInfo allocInfo = {};
        allocInfo.usage = bufferUsage;

        VkResult success = vmaCreateBuffer(m_allocator, &createInfo, &allocInfo,&buffer.buffer, &buffer.memoryAllocation, nullptr);
        return success == VK_SUCCESS;
    }

    void VulkanResourceManager::destroyBuffer(RavenBuffer& buffer)
    {
        vmaDestroyBuffer(m_allocator, buffer.buffer, buffer.memoryAllocation);
    }

    bool VulkanResourceManager::createAllocatedImage(const VkImageCreateInfo& createInfo, VmaMemoryUsage imageUsage, RavenImage& image)
    {
        VmaAllocationCreateInfo allocInfo = {};
        allocInfo.usage = imageUsage;

        VkResult success = vmaCreateImage(m_allocator, &createInfo, &allocInfo, &image.image, &image.memoryAllocation, nullptr);
        return success == VK_SUCCESS;
    }

    void VulkanResourceManager::destroyImage(RavenImage& image)
    {
        vmaDestroyImage(m_allocator, image.image, image.memoryAllocation);
    }

    VkResult VulkanResourceManager::mapMemory(const VmaAllocation& allocation, void** data)
    {
        VkResult result = vmaMapMemory(m_allocator, allocation, data);
        CHECK_RESULT_FOR_ERRORS(result, "Failed to map memory!");
        return result;
    }

    void VulkanResourceManager::unmapMemory(const VmaAllocation& allocation)
    {
        vmaUnmapMemory(m_allocator, allocation);
    }

    void VulkanResourceManager::flushMemory(const VmaAllocation &allocation)
    {
        vmaFlushAllocation(m_allocator, allocation, 0, VK_WHOLE_SIZE);
    }
}