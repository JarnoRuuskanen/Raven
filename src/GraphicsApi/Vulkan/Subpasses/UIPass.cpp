#include <GraphicsApi/Vulkan/VulkanUtility/UtilityFunctions.h>
#include <GraphicsApi/Vulkan/VulkanUtility/StructInitializer.h>
#include <Utility/FileIO.h>
#include <imgui.h>
#include <GraphicsApi/Vulkan/VulkanUtility/DescriptorUtility.h>
#include <EntityComponentSystem/RavenComponents/ComponentUtility/ComponentInitializationStructs.h>
#include <GraphicsApi/Vulkan/VulkanUtility/ImageUtility.h>
#include <Utility/DelegateManager.h>
#include "GraphicsApi/Vulkan/Subpasses/UIPass.h"

namespace Raven
{
    void UIPass::createSubpassDescription(const std::vector<VkAttachmentReference>& inputReferences, const std::vector<VkAttachmentReference>& colorReferences,
                                          const std::vector<VkAttachmentReference>& depthReferences, const std::vector<VkAttachmentDescription>& attachments, 
                                          const std::vector<uint32_t>& preservedAttachmentIndices)
    {
        BaseSubpass::createSubpassDescription(inputReferences, colorReferences, depthReferences, attachments, preservedAttachmentIndices);

        //Creates the font sampler which is required in pipeline layout creation.
        const RavenVulkanContext& context = DelegateManager::getInstance().getVulkanContextDelegates().getVulkanContextFunc.Invoke();
        initFontTexture(context.device);
        VkResult result = createDescriptorSets(context.device);
        assert(result == VK_SUCCESS);
    }

    void UIPass::initFontTexture(const VkDevice &device)
    {
        ImGuiIO& io = ImGui::GetIO();

        //First create the image which will hold all the fonts and buffer the data to the gpu.
        io.Fonts->GetTexDataAsRGBA32(&m_textureData.pixels, &m_textureData.width, &m_textureData.height);
        m_textureData.name = "UiFonts";
        m_textureData.texChannels = 4;
        m_textureData.imageSize = m_textureData.width * m_textureData.height * 4 * sizeof(char);

        //Data has been loaded to the cpu, now create the image and push data to the gpu.
        VkFormat imageFormat = VK_FORMAT_R8G8B8A8_UNORM;
        VkImageCreateInfo createInfo = StructInitializer::getImageCreateInfo(m_textureData.width, m_textureData.height,
                                                                             VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT, imageFormat);

        const auto& vulkanManagerDelegates = DelegateManager::getInstance().getVulkanManagerDelegates();
        //This function will create a new image and use a staging buffer to upload data into the device memory.
        bool success = vulkanManagerDelegates.createTextureImageFunc.Invoke(m_textureData, createInfo, m_fontImage);
        assert(success);

        VkImageViewCreateInfo viewCreateInfo = StructInitializer::getImageViewCreateInfo(m_fontImage.image, imageFormat, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_VIEW_TYPE_2D);
        success = ImageUtility::createImageView(device, viewCreateInfo, m_fontImage.imageView);
        assert(success);

        //Create the font texture sampler.
        VkSamplerCreateInfo samplerInfo = {};
        samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
        samplerInfo.magFilter = VK_FILTER_LINEAR;
        samplerInfo.minFilter = VK_FILTER_LINEAR;
        samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
        samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        samplerInfo.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;

        VkResult result = vkCreateSampler(device, &samplerInfo, nullptr, &m_fontImage.sampler);
        assert(result == VK_SUCCESS);
    }

    VkResult UIPass::createDescriptorSets(const VkDevice &device)
    {
        //If no fonts are loaded, ImGui will use default fonts.
        std::vector<VkDescriptorPoolSize> poolSizes = { { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1} };

        VkResult result = DescriptorUtility::createDescriptorPool(device, poolSizes, 1000, true, m_descriptorPool);
        CHECK_RESULT_FOR_ERRORS(result, "Failed to create a descriptor pool for imgui!");

        VkSampler sampler[1] = {m_fontImage.sampler};
        VkDescriptorSetLayoutBinding imageSamplerBinding = {};
        imageSamplerBinding.binding = 0;
        imageSamplerBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        imageSamplerBinding.descriptorCount = 1;
        imageSamplerBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
        imageSamplerBinding.pImmutableSamplers = sampler;

        m_descriptorSetBindingInfos = { { imageSamplerBinding, "fontSampler" } };

        std::vector<VkDescriptorSetLayoutBinding> bindings = {imageSamplerBinding};

        result = DescriptorUtility::createDescriptorSetLayout(device, bindings, m_descriptorSetLayout);
        CHECK_RESULT_FOR_ERRORS(result, "Failed to create a descriptor set layout for imgui!");

        m_renderpassPipeline.descriptorSetLayouts.push_back(m_descriptorSetLayout);
        result = DescriptorUtility::allocateDescriptorSets(device, m_descriptorPool, m_renderpassPipeline.descriptorSetLayouts, 1, m_descriptorSets);
        CHECK_RESULT_FOR_ERRORS(result, "Failed to allocate descriptor sets for imgui!");

        //Update descriptor set to use the font texture.
        VkDescriptorImageInfo fontDescriptor = {};
        fontDescriptor.sampler = m_fontImage.sampler;
        fontDescriptor.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        fontDescriptor.imageView = m_fontImage.imageView;

        VkWriteDescriptorSet imageWrite =
                StructInitializer::getWriteDescriptorSet(m_descriptorSets[0], VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 0, &fontDescriptor);

        std::vector<VkWriteDescriptorSet> writes = {imageWrite};

        vkUpdateDescriptorSets(device, static_cast<uint32_t>(writes.size()), writes.data(), 0, nullptr);
        return VK_SUCCESS;
    }

    void UIPass::initializeInputAssemblyState(VkPipelineInputAssemblyStateCreateInfo& inputAssembly)
    {
        inputAssembly = StructInitializer::getPipelineInputAssemblyStateCreateInfo(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST, VK_FALSE);
    }

    void UIPass::initializeVertexInputState(VkPipelineVertexInputStateCreateInfo& vertexInputState)
    {
        m_vertexInputBindings = {StructInitializer::getVertexInputBindingDescription(0, sizeof(ImDrawVert), VK_VERTEX_INPUT_RATE_VERTEX)};

        m_vertexInputAttributes =
        {
                StructInitializer::getVertexInputAttributeDescription(0, 0, VK_FORMAT_R32G32_SFLOAT, offsetof(ImDrawVert, pos)),
                StructInitializer::getVertexInputAttributeDescription(0,1,VK_FORMAT_R32G32_SFLOAT, offsetof(ImDrawVert, uv)),
                StructInitializer::getVertexInputAttributeDescription(0, 2, VK_FORMAT_R8G8B8A8_UNORM, offsetof(ImDrawVert, col))
        };

        vertexInputState = StructInitializer::getPipelineVertexInputStateCreateInfo(m_vertexInputBindings, m_vertexInputAttributes);
    }

    void UIPass::initializeViewportState(VkPipelineViewportStateCreateInfo& viewportState)
    {
        viewportState = {};
        viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        viewportState.scissorCount = 1;
        viewportState.viewportCount = 1;
        viewportState.flags = 0;
    }

    void UIPass::initializeRasterizerState(VkPipelineRasterizationStateCreateInfo& rasterizer)
    {
        rasterizer =
                StructInitializer::getPipelineRasterizationStateCreateInfo(VK_FALSE, VK_FALSE, VK_POLYGON_MODE_FILL,
                                                                           1.0f, VK_CULL_MODE_NONE, VK_FRONT_FACE_COUNTER_CLOCKWISE,
                                                                           VK_FALSE, 0.0f, 0.0f, 0.0f);
    }

    void UIPass::initializeMultisampleState(VkPipelineMultisampleStateCreateInfo& multisampling)
    {
        multisampling = {};
        multisampling.flags = 0;
        multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    }

    void UIPass::initializeDepthStencilState(VkPipelineDepthStencilStateCreateInfo& depthStencil)
    {
        depthStencil =
                StructInitializer::getPipelineDepthStencilStateCreateInfo(VK_FALSE, VK_FALSE,
                                                          VK_COMPARE_OP_LESS_OR_EQUAL,VK_FALSE,
                                                          0.0f, 1.0f, VK_FALSE, {}, {});
    }

    void UIPass::initializeColorBlendState(VkPipelineColorBlendStateCreateInfo& colorBlend)
    {
        VkPipelineColorBlendAttachmentState blendAttachmentState = {};
        blendAttachmentState.blendEnable = VK_TRUE;
        blendAttachmentState.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
        blendAttachmentState.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
        blendAttachmentState.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
        blendAttachmentState.colorBlendOp = VK_BLEND_OP_ADD;
        blendAttachmentState.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
        blendAttachmentState.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
        blendAttachmentState.alphaBlendOp = VK_BLEND_OP_ADD;

        m_colorBlendAttachments = {blendAttachmentState};

        //Don't apply the logic operation between the fragment's color values and the existing value in the framebuffer.
        //https://www.khronos.org/registry/vulkan/specs/1.1-extensions/man/html/VkPipelineColorBlendStateCreateInfo.html
        //https://www.khronos.org/registry/vulkan/specs/1.1-extensions/html/vkspec.html#framebuffer-logicop
        colorBlend = StructInitializer::getPipelineColorBlendStateCreateInfo(VK_FALSE, VK_LOGIC_OP_CLEAR, m_colorBlendAttachments);
    }

    void UIPass::initializeDynamicState(VkPipelineDynamicStateCreateInfo& dynamicStateCreateInfo)
    {
        m_dynamicStates= {VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR};
        dynamicStateCreateInfo = StructInitializer::getPipelineDynamicStateCreateInfo(m_dynamicStates);
    }

    VkResult UIPass::initializePipelineLayout(const VkDevice& device)
    {
        std::vector<ShaderLoadInformation> shadersToLoad = { {"../shaders/ui/imgui.vert", VK_SHADER_STAGE_VERTEX_BIT},
                                                             {"../shaders/ui/imgui.frag", VK_SHADER_STAGE_FRAGMENT_BIT} };

        VkResult result = describePipeline(device, shadersToLoad);
        assert(result == VK_SUCCESS);
        return VK_SUCCESS;
    }

    void UIPass::draw(const RavenBuffer &vertexBuffer, const RavenBuffer &indexBuffer, const std::vector<VkDescriptorSet> &descriptorSets, 
                      const std::vector<PushConstantData*>& pushConstants, VkCommandBuffer &cmdBuffer)
    {
        //Draws current ImGui frame into a command buffer.
        ImGuiIO& io = ImGui::GetIO();

        vkCmdBindDescriptorSets(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, m_renderpassPipeline.layout, 0,
                                static_cast<uint32_t>(m_descriptorSets.size()), m_descriptorSets.data(), 0, nullptr);
        vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, m_renderpassPipeline.pipeline);

        VkViewport viewport = {};
        viewport.width = io.DisplaySize.x;
        viewport.height = io.DisplaySize.y;
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;

        vkCmdSetViewport(cmdBuffer, 0,1, &viewport);

        //Scale and translate are sent to the shaders through push constants.
        m_pushConstBlock.scale = glm::vec2(2.0f / io.DisplaySize.x, 2.0f / io.DisplaySize.y);
        m_pushConstBlock.translate = glm::vec2(-1.0f);
        vkCmdPushConstants(cmdBuffer, m_renderpassPipeline.layout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(UIPushConstantBlock), &m_pushConstBlock);

        //ImGui rendering commands.
        ImDrawData* imDrawData = ImGui::GetDrawData();
        int32_t vertexOffset = 0;
        int32_t indexOffset = 0;

        if(imDrawData->CmdListsCount > 0)
        {
            std::array<VkDeviceSize, 1> offsets = {0};
            vkCmdBindVertexBuffers(cmdBuffer, 0, 1, &vertexBuffer.buffer, offsets.data());
            vkCmdBindIndexBuffer(cmdBuffer, indexBuffer.buffer, 0, VK_INDEX_TYPE_UINT16);

            for(int32_t i = 0; i < imDrawData->CmdListsCount; i++)
            {
                const ImDrawList* cmdList = imDrawData->CmdLists[i];
                for(int32_t j = 0; j < cmdList->CmdBuffer.Size; j++)
                {
                    const ImDrawCmd* cmd = &cmdList->CmdBuffer[j];
                    VkRect2D scissorRect;
                    scissorRect.offset.x = std::max((int32_t)(cmd->ClipRect.x), 0);
                    scissorRect.offset.y = std::max((int32_t)(cmd->ClipRect.y), 0);
                    scissorRect.extent.width = (uint32_t)(cmd->ClipRect.z - cmd->ClipRect.x);
                    scissorRect.extent.height = (uint32_t)(cmd->ClipRect.w - cmd->ClipRect.y);
                    vkCmdSetScissor(cmdBuffer, 0, 1, &scissorRect);
                    vkCmdDrawIndexed(cmdBuffer, cmd->ElemCount, 1, indexOffset, vertexOffset, 0);
                    indexOffset += cmd->ElemCount;
                }
                vertexOffset += cmdList->VtxBuffer.Size;
            }
        }
    }
}