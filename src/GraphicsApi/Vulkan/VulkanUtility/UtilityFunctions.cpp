#include "GraphicsApi/Vulkan/VulkanUtility/UtilityFunctions.h"
#include "Utility/ShaderUtility/ShaderProcessor.h"
#include "GraphicsApi/Vulkan/VulkanUtility/StructInitializer.h"

#include <cstdlib>
#include <iostream>
#include <cstring>
#include <cassert>

namespace Raven
{
    namespace UtilityFunctions
    {
        VkShaderModule createShaderModule(const VkDevice& device, const std::vector<uint32_t>& shaderCode)
        {
            VkShaderModuleCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
            createInfo.pNext = nullptr;
            createInfo.flags = 0;
            createInfo.codeSize = shaderCode.size() * sizeof(uint32_t);
            createInfo.pCode = shaderCode.data();

            VkShaderModule module;
            VkResult result = vkCreateShaderModule(device, &createInfo, nullptr, &module);

            assert(result == VK_SUCCESS);
            return module;
        }

        VkSemaphore createSemaphore(const VkDevice& device)
        {
            VkSemaphoreCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
            createInfo.pNext = nullptr;
            createInfo.flags = 0;

            VkSemaphore semaphore;
            VkResult result = vkCreateSemaphore(device, &createInfo, nullptr, &semaphore);
            if(result != VK_SUCCESS)
            {
                std::cerr << "Failed to create a semaphore! \n";
                semaphore = VK_NULL_HANDLE;
            }
            return semaphore;
        }

        VkSemaphore createTimelineSemaphoreKHR(const VkDevice& device)
        {
            VkSemaphoreTypeCreateInfo timelineCreateInfo;
            timelineCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO;
            timelineCreateInfo.pNext = nullptr;
            timelineCreateInfo.semaphoreType = VK_SEMAPHORE_TYPE_TIMELINE_KHR;
            timelineCreateInfo.initialValue = 0;

            VkSemaphoreCreateInfo createInfo;
            createInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
            createInfo.pNext = &timelineCreateInfo;
            createInfo.flags = 0;

            VkSemaphore timelineSemaphore;
            VkResult result = vkCreateSemaphore(device, &createInfo, nullptr, &timelineSemaphore);
            if(result != VK_SUCCESS)
            {
                std::cerr << "Failed to create a timeline semaphore! \n";
                timelineSemaphore = VK_NULL_HANDLE;
            }
            return timelineSemaphore;
        }

        VkFence createSignaledFence(const VkDevice &device)
        {
            VkFenceCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
            createInfo.pNext = nullptr;
            createInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

            VkFence fence;
            VkResult result = vkCreateFence(device, &createInfo, nullptr, &fence);

            if(result != VK_SUCCESS)
            {
                std::cerr << "Failed to create a fence!" << std::endl;
                fence = VK_NULL_HANDLE;
            }
            return fence;
        }

        int findMemoryType(VkPhysicalDevice physicalDevice, uint32_t typeFilter, VkMemoryPropertyFlags properties)
        {
            VkPhysicalDeviceMemoryProperties memProperties;
            vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);

            for(uint32_t i = 0; i < memProperties.memoryTypeCount; i++)
            {
                if((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties)
                {
                    return i;
                }
            }
            std::cerr << "DID NOT FIND CORRECT MEMORY TYPE!" << std::endl;
            return -1;
        }
        
        /**
        *@brief Render pass is used to describe what kind of attachments will be accessed during rendering.
        */
        bool createDefaultRenderPass(const VkDevice& device, VkFormat colorAttachmentFormat, VkRenderPass& targetRenderPass)
        {
            VkAttachmentDescription colorAttachment = {};
            colorAttachment.format = colorAttachmentFormat;
            colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
            colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
            colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
            colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
            colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
            colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED; //What layout the image will have before presentation.
            colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR; // What layout to switch when the render pass finishes.

            VkAttachmentReference colorAttachmentRef = {};
            colorAttachmentRef.attachment = 0;
            colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

            //Depth testing
            VkAttachmentDescription depthAttachment = {};
            depthAttachment.format = VK_FORMAT_D32_SFLOAT; //TODO: optimize.
            depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
            depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
            depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
            depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
            depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
            depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
            depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

            VkAttachmentReference depthAttachmentRef = {};
            depthAttachmentRef.attachment = 1;
            depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

            VkSubpassDescription subpass = {};
            subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
            subpass.colorAttachmentCount = 1;
            subpass.pColorAttachments = &colorAttachmentRef;
            subpass.pInputAttachments = nullptr;
            subpass.pResolveAttachments = nullptr;
            subpass.pDepthStencilAttachment = &depthAttachmentRef;
            subpass.pPreserveAttachments = nullptr;

            //Describe a dependency.
            // This makes the render pass wait until VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT stage before executing.
            VkSubpassDependency dependency = {};
            dependency.srcSubpass = VK_SUBPASS_EXTERNAL; // implicit subpass before or after the render pass depending if it's specified in srcSubpass. of dstSubpass
            dependency.dstSubpass = 0; // our subpass.
            //dstSubpass must always be higher than srcSubpass to prevent cycles in the dependency graph.
            dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
            dependency.srcAccessMask = 0;
            dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
            dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

            std::array<VkAttachmentDescription, 2> attachments = { colorAttachment, depthAttachment };

            VkRenderPassCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
            createInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
            createInfo.pAttachments = attachments.data();
            createInfo.subpassCount = 1;
            createInfo.pSubpasses = &subpass;
            createInfo.dependencyCount = 1;
            createInfo.pDependencies = &dependency;

            VkResult result = vkCreateRenderPass(device, &createInfo, nullptr, &targetRenderPass);
            assert(result == VK_SUCCESS);
            return result == VK_SUCCESS;
        }

        void fillShaderStageData(const VkDevice& device, const std::vector<ShaderLoadInformation>& infos, std::vector<ShaderStageData>& shaderStages)
        {
            assert(shaderStages.empty());

            shaderStages.clear();
            shaderStages.shrink_to_fit();

            //TODO: Check that there is only one of each stage.
            for (auto& info : infos)
            {
                shaderStages.push_back({});
                ShaderStageData& data = shaderStages.back();

                //TODO:Add shader stag data creation directly from spirv if no changes in glsl shader has occured.
                ShaderUtility::compileGLSLToSpirv(info.filepath, data.spirv);
                data.module = createShaderModule(device, data.spirv);
                data.stage = info.shaderStage;
                data.createInfo = StructInitializer::getShaderStageCreateInfo(info.shaderStage, data.module);
            }
        }

        VkPresentModeKHR selectBestAvailablePresentMode(const std::vector<VkPresentModeKHR>& presentModes)
        {
            VkPresentModeKHR bestAvailable = VK_PRESENT_MODE_FIFO_KHR;
            for (const auto& presentMode : presentModes)
            {
                if (presentMode == VK_PRESENT_MODE_MAILBOX_KHR)
                {
                    bestAvailable = presentMode;
                    break;
                }
                else if (presentMode == VK_PRESENT_MODE_IMMEDIATE_KHR)
                {
                    bestAvailable = presentMode;
                }
            }
            return bestAvailable;
        }

        VkFormat getFormatBasedOnTextureChannels(uint32_t channels)
        {
            switch(channels)
            {
                case 1:
                    return VK_FORMAT_R8_UNORM;
                case 2:
                    return VK_FORMAT_R8G8_UNORM;
                case 3:
                    return VK_FORMAT_R8G8B8_UNORM;
                case 4:
                    return VK_FORMAT_R8G8B8A8_UNORM;
                default:
                    return VK_FORMAT_UNDEFINED;
            }
        }

        std::size_t hashVertexVertices(const std::vector<Vertex>& vertices)
        {
            std::size_t seed = vertices.size(); //This is potentially dumb.
            for (auto& vertex : vertices)
            {
                seed ^= VertexHash{}(vertex);
            }
            return seed;
        }
    }
}