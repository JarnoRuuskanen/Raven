#include "Utility/ShaderUtility/ShaderReflection.h"

namespace Raven
{
    //https://www.khronos.org/assets/uploads/developers/library/2016-vulkan-devday-uk/4-Using-spir-v-with-spirv-cross.pdf
    namespace ShaderUtility
    {
        ShaderReflection::ShaderReflection(const uint32_t* spirv, size_t codeLength)
        {
            std::vector<uint32_t> code(spirv, spirv + codeLength);
            spirv_cross::CompilerGLSL glsl(std::move(code));
            spirv_cross::ShaderResources resources = glsl.get_shader_resources();

            findStageIOData(glsl, resources.stage_inputs, m_inputs);
            findStageIOData(glsl, resources.stage_outputs, m_outputs);

            findCombinedImageSamplers(glsl, resources.sampled_images, m_combinedImageSamplers);
            findUniformBuffers(glsl, resources.uniform_buffers, m_uniformBuffers);
            findPushConstants(glsl, resources.push_constant_buffers, m_pushConstantDescriptor);
            findInputAttachments(glsl, resources.subpass_inputs, m_subpassInputAttachments);

            generateShaderStageDescriptors();
        }

        ShaderReflection::~ShaderReflection()
        {

        }

        void ShaderReflection::findStageIOData(const spirv_cross::CompilerGLSL& glsl, 
                                               const spirv_cross::SmallVector<spirv_cross::Resource>& resources,
                                               std::vector<StageIOReflection>& target)
        {
            for (const auto& resource : resources)
            {
                spirv_cross::SPIRType type = glsl.get_type(resource.type_id);

                StageIOReflection input = {};
                input.location = glsl.get_decoration(resource.id, spv::DecorationLocation);
                input.name = resource.name;
                input.columns = type.columns;
                input.vecsize = type.vecsize;
                input.width = type.width;

                target.push_back(input);
            }
        }

        void ShaderReflection::findCombinedImageSamplers(const spirv_cross::CompilerGLSL& glsl,
                                                         const spirv_cross::SmallVector<spirv_cross::Resource>& resources, 
                                                         std::vector<CombinedImageSamplerReflection>& target)
        {
            for (const auto& resource : resources)
            {
                CombinedImageSamplerReflection sampler2D;
                sampler2D.binding = glsl.get_decoration(resource.id, spv::DecorationBinding);
                sampler2D.set = glsl.get_decoration(resource.id, spv::DecorationDescriptorSet);
                sampler2D.name = resource.name;

                target.push_back(sampler2D);
            }
        }

        void ShaderReflection::findUniformBuffers(const spirv_cross::CompilerGLSL& glsl,
                                                  const spirv_cross::SmallVector<spirv_cross::Resource>& resources,
                                                  std::vector<BufferReflection>& target)
        {
            for (const auto& resource : resources)
            {
                BufferReflection reflection = {};
                reflection.binding = glsl.get_decoration(resource.id, spv::DecorationBinding);
                reflection.set = glsl.get_decoration(resource.id, spv::DecorationDescriptorSet);
                reflection.name = resource.name;
                reflection.fallbackName = glsl.get_block_fallback_name(resource.id);

                auto& type = glsl.get_type(resource.base_type_id);
                uint32_t memberCount = type.member_types.size();

                for (uint32_t i = 0; i < memberCount; i++)
                {
                    auto& memberType = glsl.get_type(type.member_types[i]);

                    BufferMemberReflection member = {};
                    member.size = glsl.get_declared_struct_member_size(type, i);
                    member.vecsize = memberType.vecsize;
                    member.arrayComponentCount = !memberType.array.empty() ? memberType.array[0] : 1;
                    member.arrayByteStride = !memberType.array.empty() ? glsl.type_struct_member_array_stride(type, i) : 0;
                    member.columns = memberType.columns;
                    member.matrixByteStride = memberType.columns > 1 ? glsl.type_struct_member_matrix_stride(type, i) : 0;
                    member.name = glsl.get_member_name(type.self, i);

                    reflection.members.push_back(member);
                }

                target.push_back(reflection);
            }
        }

        void ShaderReflection::findPushConstants(const spirv_cross::CompilerGLSL& glsl, const spirv_cross::SmallVector<spirv_cross::Resource>& resources, std::vector<PushConstantDescriptor>& target)
        {
            uint32_t offset = 0;
            for (const auto& resource : resources)
            {
                std::string name = resource.name;
                std::string fallbackName = glsl.get_block_fallback_name(resource.id);
                const spirv_cross::SPIRType& type = glsl.get_type(resource.base_type_id);
                size_t size = glsl.get_declared_struct_size(type);

                PushConstantDescriptor description;
                description.size = size;
                description.offset = offset;
                description.name = name;
                description.fallbackName = fallbackName;

                spirv_cross::SmallVector<spirv_cross::BufferRange> ranges = glsl.get_active_buffer_ranges(resource.id);
                for (auto& rng : ranges)
                {
                    PushConstantMemberRange range;
                    range.index = rng.index;   // Struct member index
                    range.offset = rng.offset; // Offset into struct
                    range.range = rng.range;   // Size of struct member
                    description.pushConstantMembers.push_back(range);
                }
                offset += size;

                target.push_back(description);
            }
        }

        void ShaderReflection::findInputAttachments(const spirv_cross::CompilerGLSL& glsl, const spirv_cross::SmallVector<spirv_cross::Resource>& resources, std::vector<InputAttachmentDescriptor>& target)
        {
            for (const auto& resource : resources)
            {
                InputAttachmentDescriptor descriptor;
                uint32_t attachmentIndex = glsl.get_decoration(resource.id, spv::DecorationInputAttachmentIndex);
                descriptor.binding = glsl.get_decoration(resource.id, spv::DecorationBinding);
                descriptor.set = glsl.get_decoration(resource.id, spv::DecorationDescriptorSet);
                descriptor.name = resource.name;
                descriptor.fallbackName = glsl.get_block_fallback_name(resource.id);

                target.push_back(descriptor);
            }
        }

        void ShaderReflection::generateVertexBindingDescriptors()
        {

        }

        void ShaderReflection::generateVertexAttributeDescriptors()
        {
            VertexAttributeDescriptor attribute;
        }

        void ShaderReflection::generateShaderStageDescriptors()
        {
            for(const auto& sampler : m_combinedImageSamplers)
            {
                Descriptor descriptor;
                descriptor.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
                descriptor.binding = sampler.binding;
                descriptor.set = sampler.set;
                descriptor.name = sampler.name;
                descriptor.secondaryName = "";

                m_stageDescriptors.push_back(descriptor);
            }

            for(const auto& uniformBuffer : m_uniformBuffers)
            {
                Descriptor descriptor;
                descriptor.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
                descriptor.binding = uniformBuffer.binding;
                descriptor.set = uniformBuffer.set;
                descriptor.name = uniformBuffer.name;
                descriptor.secondaryName = uniformBuffer.fallbackName;

                m_stageDescriptors.push_back(descriptor);
            }

            for (const auto& inputAttachment : m_subpassInputAttachments)
            {
                Descriptor descriptor;
                descriptor.descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
                descriptor.binding = inputAttachment.binding;
                descriptor.set = inputAttachment.set;
                descriptor.name = inputAttachment.name;
                descriptor.secondaryName = inputAttachment.fallbackName;

                m_stageDescriptors.push_back(descriptor);
            }
        }
    }
}