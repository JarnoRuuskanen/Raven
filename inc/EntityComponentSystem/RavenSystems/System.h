#ifndef RAVEN_SYSTEM_H_INCLUDED
#define RAVEN_SYSTEM_H_INCLUDED
#include "EntityComponentSystem/RavenSystems/BaseSystem.h"
#include "EntityComponentSystem/RavenEntities/Entity.h"
#include "EntityComponentSystem/ECSTypes.h"

#include <vector>
#include <tuple>
#include <type_traits>
#include <assert.h>
#include <iostream>
namespace Raven
{
    //DISCLAIMER:
    //Most of this is not my own idea but is instead from:
    //https://www.youtube.com/watch?v=39e1qpsutBU&list=PLUUXnYtS5hcVFwd4Z794vA-HsoF2OIWlR&index=6

    using EntityIdToIndexMap = std::unordered_map<EntityId, size_t, std::hash<EntityId>, std::equal_to<EntityId>>;

    //A variadic template. Variadic templates are templates that take a variable number of arguments.
    //https://en.wikipedia.org/wiki/Variadic_template
    template<class... Comps>
    class System : public BaseSystem
    {
    protected:
        //Each system is going to hold a list of N types of components that it cares about.
        //Using a tuple as it best suits this use case.
        //https://en.cppreference.com/w/cpp/utility/tuple
        using ComponentsTuple = std::tuple <std::add_pointer_t<Comps> ... >; //a tuple of component pointers.
        std::vector<ComponentsTuple> m_components;
    public:
        System(EntityManager* pGameWorld) : BaseSystem(pGameWorld){}

        virtual void onEntityCreated(Entity* entity) override final;
        virtual void onEntityDestroyed(EntityId id) override final;
    private:
        virtual void entityAddedToSystem(Entity* entity) = 0;
        EntityIdToIndexMap m_entityIdToIndexMap;
        //Recursively looks through component types
        template<size_t INDEX, class ComponentType, class... ComponentArgs>
        bool processEntityComponent(ComponentId id, Component* pComponent, ComponentsTuple& tupleToFill);
        //Termination specialization of the recursion. Happens when we no longer have a ComponentType that is passed in.
        template<size_t INDEX>
        bool processEntityComponent(ComponentId id, Component* pComponent, ComponentsTuple& tupleToFill);
    };

    template<class ... Comps>
    inline void System<Comps...>::onEntityCreated(Entity* entity)
    {
        ComponentsTuple compTuple;
        std::size_t matchingComps = 0;
        for (auto& pair : entity->getComponents())
        {
            if (processEntityComponent<0, Comps...>(pair.first, pair.second, compTuple))
            {
                ++matchingComps;
                if (matchingComps == sizeof...(Comps))
                {
                    //This means that we have an entity that has all the components that we care about.
                    //Emplace back uses perfect forwarding so it doesn't create additional copies like, for instance, vector push_back() does..
                    //Check that the entity doesn't already exist and if not, add it.
                    if(m_entityIdToIndexMap.find(entity->getId()) == m_entityIdToIndexMap.end())
                    {
                        m_components.emplace_back(std::move(compTuple));
                        m_entityIdToIndexMap.emplace(entity->getId(), m_components.size() - 1);
                        entityAddedToSystem(entity);
                    }
                    else
                    {
                        std::cout << "Entity was already part of the system! Not adding\n";
                    }
                    break;
                }
            }
        }
    }

    template<class ... Comps>
    inline void System<Comps...>::onEntityDestroyed(EntityId id)
    {
        const auto findIt = m_entityIdToIndexMap.find(id);
        if (findIt != m_entityIdToIndexMap.end())
        {
            //Moving the components tuple for this given entity to the back of the vector and removing it with pop_back().
            m_components[findIt->second] = std::move(m_components.back());
            m_components.pop_back();

            //Next we need to update the index to id map. Figure out which entity the moved component belongs to.
            //All the components in the tuple belong to the same entity, so grab the first one.
            const auto* pMovedComp = std::get<0>(m_components[findIt->second]);

            //Find the entry for the moved id and update it with the new index
            auto movedTupleIt = m_entityIdToIndexMap.find(pMovedComp->getEntityId());
            assert(movedTupleIt != m_entityIdToIndexMap.end());
            movedTupleIt->second = findIt->second;
        }
    }

    template<class ...Comps>
    template<size_t INDEX, class ComponentType, class ...ComponentArgs>
    inline bool System<Comps...>::processEntityComponent(ComponentId id, Component* pComponent, ComponentsTuple& tupleToFill)
    {
        if (ComponentType::ID == id)
        {
            //If match, get the correct spot from the tuple to fill.
            //Cast to correct type just to be safe.
            std::get<INDEX>(tupleToFill) = static_cast<ComponentType*>(pComponent);
            return true;
        }
        else
        {
            return processEntityComponent<INDEX + 1, ComponentArgs ...>(id, pComponent, tupleToFill);
        }
    }

    template<class ... Comps>
    template<size_t INDEX>
    inline bool System<Comps...>::processEntityComponent(ComponentId id, Component* pComponent, ComponentsTuple& tupleToFill)
    {
        return false;
    }
}

#endif