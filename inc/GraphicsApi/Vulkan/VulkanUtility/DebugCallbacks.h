#ifndef RAVEN_DEBUG_CALLBACKS_H_INCLUDED
#define RAVEN_DEBUG_CALLBACKS_H_INCLUDED

#include "GraphicsApi/Vulkan/Common.h"

#include <iostream>

namespace Raven
{
    class DebugCallbacks
    {
    public:
        DebugCallbacks();
        ~DebugCallbacks();
        bool initialize(VkInstance& instance);
    private:
        static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
                VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
                VkDebugUtilsMessageTypeFlagsEXT messageType,
                const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
                void* pUserData) {

            std::cerr << "validation layer: " << pCallbackData->pMessage << std::endl;

            return VK_FALSE;
        }

        VkResult createDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger);
        void destroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator);

        VkDebugUtilsMessengerEXT m_debugMessenger;
        VkInstance* m_instance = nullptr;
    };
}

#endif