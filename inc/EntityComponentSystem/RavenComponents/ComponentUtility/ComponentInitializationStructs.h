#ifndef RAVEN_COMPONENT_INITIALIZATION_DATA_H_INCLUDED
#define RAVEN_COMPONENT_INITIALIZATION_DATA_H_INCLUDED

#include "GraphicsApi/Vulkan/VulkanUtility/VulkanStructs.h"

namespace Raven
{
    //Let's say we have a building which is one huge object.
    //In that huge building there is a vase. That vase would be 
    //defined with this primitive.
    struct Primitive
    {
        std::vector<Vertex> vertices;
        std::vector<uint32_t> indices;
        uint32_t material; //-1 if mesh has no designated material.
    };

    struct MeshData
    {
        std::string name;
        std::vector<Primitive> primitives;
        glm::vec3 minVal;
        glm::vec3 maxVal;
    };

    struct TextureData
    {
        int width;
        int height;
        int texChannels;
        std::string name;
        size_t imageSize;
        unsigned char* pixels{nullptr};
    };

    //MaterialData describes how an object should be rendered and how it acts with light.
    //You cannot apply a texture to an object without a material.
    //This part was mainly taken from Sascha Willems example here under "MaterialData" though I made some minor changes:
    //https://github.com/SaschaWillems/Vulkan-glTF-PBR/blob/master/base/VulkanglTFModel.hpp
    struct MaterialData
    {
        std::string materialName = "";
        enum class AlphaMode{ALPHAMODE_OPAQUE, ALPHAMODE_MASK, ALPHAMODE_BLEND};
        AlphaMode alphaMode = AlphaMode::ALPHAMODE_OPAQUE;
        float alphaCutoff = 1.0f;
        //Factors
        float metallicFactor = 1.0f;
        float roughtnessFactor = 1.0f;
        glm::vec4 baseColorFactor = glm::vec4(1.0f);
        glm::vec4 emissiveFactor = glm::vec4(1.0f);
        //These textures correspond to the gltf material names.
        TextureData* baseColorTexture = nullptr;
        TextureData* metallicRoughnessTexture = nullptr;
        TextureData* normalTexture = nullptr;
        TextureData* occlusionTexture = nullptr;
        TextureData* emissiveTexture = nullptr;

        struct TexCoordSets
        {
            uint8_t baseColor = 0;
            uint8_t metallicRoughness = 0;
            uint8_t specularGlossiness = 0;
            uint8_t normal = 0;
            uint8_t occlusion = 0;
            uint8_t emissive = 0;
        } texCoordSets;

        struct Extension {
            TextureData* specularGlossinessTexture;
            TextureData* diffuseTexture;
            glm::vec4 diffuseFactor = glm::vec4(1.0f);
            glm::vec3 specularFactor = glm::vec3(0.0f);
        } extension;
        struct PbrWorkflows {
            bool metallicRoughness = true;
            bool specularGlossiness = false;
        } pbrWorkflows;
    };

    struct TransformData
    {
        glm::mat4 localTransform{1.0f};
        glm::mat4 worldTransform{1.0f};
        glm::vec3 translation{0.0f};
        glm::vec3 scale{1.0f};
        glm::quat rotation{0.0f,0.0f,0.0f,1.0f};
    };

    struct CameraData
    {
        //Placeholder
        uint32_t cameraIndex;
    };

    struct SkeletonData
    {
        std::string name;
        int rootSkeleton;
        std::vector<int> jointNodes;
        std::vector<Joint> joints;
        std::vector<glm::mat4> inverseBindMatrices;
    };

    struct NodeData
    {
        TransformData transformData;
        std::string name;
        int mesh;      //-1 if not doesn't exist for this node.
        int camera;    //-1 if not doesn't exist for this node.
        //In GLTF skeleton is referred to as skin.
        int skeleton;  //-1 if not doesn't exist for this node.
        std::vector<int> animations; //This corresponds to NodeAnimation vector.
        std::vector<int> children;
    };

    //This animation part is partly from Sascha Willem's open source project:
    //Reference: https://github.com/SaschaWillems/Vulkan-glTF-PBR/blob/master/base/VulkanglTFModel.hpp
    struct AnimationChannel
    {
        enum PathType {TRANSLATION, ROTATION, SCALE};
        PathType path;
        //Not yet sure if this makes sense.
        uint32_t targetNodeIndex;
        uint32_t samplerIndex;
    };

    struct AnimationSampler
    {
        enum InterpolationType {LINEAR, STEP, CUBICSPLINE};
        InterpolationType interpolationType;
        //Inputs are the time points (in seconds) when the animation happens.
        std::vector<float> inputs;
        //Outputs are the the values at given time points.
        std::vector<glm::vec4> outputsVec4;
    };

    struct NodeAnimation
    {
        std::string name;
        std::vector<AnimationSampler> samplers;
        std::vector<AnimationChannel> channels;
        float start = std::numeric_limits<float>::max();
        float end = std::numeric_limits<float>::min();
    };

    struct TagData
    {
        std::string name;
    };
}

#endif