#ifndef RAVEN_ALLOCATOR_H_INCLUDED
#define RAVEN_ALLOCATOR_H_INCLUDED

#include <cstddef>

//References:
//https://github.com/mtrebi/memory-allocators/tree/master/src
//https://www.gamedev.net/articles/programming/general-and-gameplay-programming/c-custom-memory-allocation-r3010
namespace Raven
{
    class Allocator
    {
    public:
        Allocator(const std::size_t totalSize);
        virtual ~Allocator();
        virtual void* allocate(const std::size_t size, const std::size_t alignment = 0) = 0;
        virtual void free(void* ptr) = 0;
        virtual void init() = 0;
    protected:
        std::size_t m_totalSize;
        std::size_t m_sizeUsed;
        std::size_t m_peakSize;
    };
}

#endif
