#ifndef RAVEN_LIGHT_PASS_H_INCLUDED
#define RAVEN_LIGHT_PASS_H_INCLUDED
#include "GraphicsApi/Vulkan/Subpasses/BaseSubpass.h"

namespace Raven
{
    class LightPass : public BaseSubpass
    {
    public:
        LightPass() : BaseSubpass("forwardLightPass") {}
        void draw(const RavenBuffer& vertexBuffer, const RavenBuffer& indexBuffer, const std::vector<VkDescriptorSet>& descriptorSets, 
                  const std::vector<PushConstantData*>& pushConstants, VkCommandBuffer& cmdBuffer) override;
        void initializeDescriptorSet(const VkDevice& device, const RavenBuffer& lightUniformBuffer);
    private:
        // Inherited via BaseSubpass
        void initializeVertexInputState(VkPipelineVertexInputStateCreateInfo& vertexInputState) override;
        void initializeInputAssemblyState(VkPipelineInputAssemblyStateCreateInfo& inputAssembly) override;
        void initializeViewportState(VkPipelineViewportStateCreateInfo& viewportState) override;
        void initializeRasterizerState(VkPipelineRasterizationStateCreateInfo& rasterizer) override;
        void initializeMultisampleState(VkPipelineMultisampleStateCreateInfo& multisampling) override;
        void initializeDepthStencilState(VkPipelineDepthStencilStateCreateInfo& depthStencil) override;
        void initializeColorBlendState(VkPipelineColorBlendStateCreateInfo& colorBlend) override;
        void initializeDynamicState(VkPipelineDynamicStateCreateInfo& dynamicStateCreateInfo) override;
        VkResult initializePipelineLayout(const VkDevice& device) override;

        VkDescriptorPool m_descriptorPool;
        VkDescriptorSet m_descriptorSet;
    };
}

#endif