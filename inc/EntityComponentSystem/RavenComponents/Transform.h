#ifndef RAVEN_TRANSFORM_COMPONENT_H_INCLUDED
#define RAVEN_TRANSFORM_COMPONENT_H_INCLUDED

#include "EntityComponentSystem/RavenComponents/Component.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace Raven
{
    class Transform : public Component
    {
    public:
        static constexpr ComponentId ID = RavenUtility::generateHash("Transform");
        Transform() : m_position(glm::vec3{0}), m_rotation(glm::vec3{ 0 }), m_scale(glm::vec3{ 1.0f }), 
            m_localTransform(glm::mat4{1.0f}), m_worldTransform(glm::mat4{ 1.0f }), dirty(false){}
        virtual bool init(void* data) override final
        {
            TransformData* transformData = (TransformData*)data;
            m_localTransform = transformData->localTransform;
            m_worldTransform = transformData->worldTransform;
            m_rotation = transformData->rotation;
            m_position = transformData->translation;
            m_scale = transformData->scale;
            dirty = true;
            return true; 
        }

        glm::mat4 m_localTransform;
        glm::mat4 m_worldTransform;
        glm::vec3 m_position;
        glm::vec3 m_scale;
        glm::quat m_rotation;
        bool dirty;
    };
}
#endif