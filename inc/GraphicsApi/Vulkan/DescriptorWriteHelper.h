#ifndef RAVEN_DESCRIPTOR_WRITE_HELPER_H_INCLUDED
#define RAVEN_DESCRIPTOR_WRITE_HELPER_H_INCLUDED

#include "EntityComponentSystem/RavenComponents/Renderable.h"
#include "GraphicsApi/Vulkan/VulkanUtility/VulkanStructs.h"

#include <vector>

namespace Raven
{
	void getRenderableShaderData(Renderable* renderable, std::vector<ShaderDescriptorMatchInfo>& matchInfo)
	{
		matchInfo.clear();
		if (renderable)
		{
			ShaderDescriptorMatchInfo info = {};
			info.name = "ubo";
			info.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			matchInfo.push_back(info);

			if (renderable->m_material)
			{
				if (renderable->m_material->m_hasBaseColor)
				{
					info.name = "albedo";
					info.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
					matchInfo.push_back(info);
				}
				if (renderable->m_material->m_hasNormal)
				{
					info.name = "normal";
					info.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
					matchInfo.push_back(info);
				}
				if (renderable->m_material->m_hasMetallicRoughness)
				{
					info.name = "roughness";
					info.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
					matchInfo.push_back(info);
				}
				if (renderable->m_material->m_hasOcclusion)
				{
					info.name = "occlusion";
					info.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
					matchInfo.push_back(info);
				}
				if (renderable->m_material->m_hasEmissive)
				{
					info.name = "emissive";
					info.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
					matchInfo.push_back(info);
				}
			}
		}
	}

	std::string getMaterialDefinitionName(MaterialDefinition definition)
	{
		switch (definition)
		{
		case MaterialDefinition::ALBEDO:
		{
			return "albedo";
		}
		case MaterialDefinition::NORMAL:
		{
			return "normal";
		}
		case MaterialDefinition::ROUGHNESS:
		{
			return "roughness";
		}
		case MaterialDefinition::OCCLUSION:
		{
			return "occlusion";
		}
		case MaterialDefinition::EMISSIVE:
		{
			return "emissive";
		}
		default:
			return "";
		}
	}

	uint32_t getImageIndexBasedOnName(const std::string& name)
	{
		uint32_t result = -1;
		if (name == "albedo")
		{
			return 0;
		}
		if (name == "normal")
		{
			return 1;
		}
		if (name == "roughness")
		{
			return 2;
		}
		if (name == "occlusion")
		{
			return 3;
		}
		if (name == "emissive")
		{
			return 4;
		}

		assert(result != -1);
		return result;
	}
}

#endif