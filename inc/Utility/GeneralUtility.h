#ifndef RAVEN_GENERAL_UTILITY_H_INCLUDED
#define RAVEN_GENERAL_UTILITY_H_INCLUDED

#include <future>

namespace Raven
{
    //General truly async task. Check Effective Modern C++ page 250.
    template<typename F, typename... Ts>
    inline auto
    launchAsyncTask(F&& f, Ts&& ... params)
    {
        return std::async(std::launch::async,
                          std::forward<F>(f),
                          std::forward<Ts>(params)...);
    }

    //Reference: https://stackoverflow.com/a/38140932

    inline void hash_combine(std::size_t& seed) { }

    template <typename T, typename... Rest>
    inline void hash_combine(std::size_t& seed, const T& v, Rest... rest) {
        std::hash<T> hasher;
        seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
        hash_combine(seed, rest...);
    }
}

#endif