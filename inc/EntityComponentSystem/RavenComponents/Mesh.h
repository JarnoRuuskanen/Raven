#ifndef RAVEN_MESH_COMPONENT_H_INCLUDED
#define RAVEN_MESH_COMPONENT_H_INCLUDED

#include "Component.h"
#include "GraphicsApi/Vulkan/VulkanObjects/RavenBuffer.h"
#include "EntityComponentSystem/RavenComponents/Skeleton.h"

namespace Raven
{
    class Mesh : public Component
    {
    public:
        static constexpr ComponentId ID = RavenUtility::generateHash("Mesh");
        Mesh():m_serialization(0), skeleton(nullptr){}
        virtual bool init(void* data) override final
        {
            skeleton = nullptr;
            m_sphereRadius = 0.0f;
            return true;
        }
        RavenBuffer m_vertexBuffer;
        RavenBuffer m_indexBuffer;
        //No two meshes with same hash can exist.
        size_t m_serialization;
        float m_sphereRadius = 0.0f;
        Skeleton* skeleton;
    };
}

#endif