#ifndef RAVEN_VULKAN_RESOURCE_MANAGER_H_INCLUDED
#define RAVEN_VULKAN_RESOURCE_MANAGER_H_INCLUDED
#include "GraphicsApi/Vulkan/Common.h"

//Using Vulkan Memory Allocator provided by AMD. Note that this head includes vulkan.h and on Windows windows.h. Add all required defines before this include.
#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#endif
#include "vk_mem_alloc.h"

#include <vector>
#include <GraphicsApi/Vulkan/VulkanObjects/GraphicsPipeline.h>
#include <GraphicsApi/Vulkan/VulkanUtility/VulkanStructs.h>
#include <GraphicsApi/Vulkan/VulkanObjects/RavenBuffer.h>
#include <GraphicsApi/Vulkan/VulkanObjects/RavenImage.h>

namespace Raven
{
    //This class should be the one that creates, manages and deletes all graphics api related resources
    class VulkanResourceManager
    {
        public:
            VulkanResourceManager();
            ~VulkanResourceManager();
            bool initialize(const VkPhysicalDevice& physicalDevice, const VkDevice& device);
            void shutdown();

            bool createAllocatedBuffer(const VkBufferCreateInfo& createInfo, VmaMemoryUsage bufferUsage, RavenBuffer& buffer);
            void destroyBuffer(RavenBuffer& buffer);

            bool createAllocatedImage(const VkImageCreateInfo& createInfo, VmaMemoryUsage imageUsage, RavenImage& image);
            void destroyImage(RavenImage& image);

            //Only usable for host visible memory.
            VkResult mapMemory(const VmaAllocation& allocation, void** data);
            void unmapMemory(const VmaAllocation& allocation);
            void flushMemory(const VmaAllocation& allocation);
        private:
            //Memory is allocated by the m_allocator. Remember to clean in shutdown.
            VmaAllocator m_allocator;
    };
}
#endif