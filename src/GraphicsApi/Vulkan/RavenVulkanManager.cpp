#include <Utility/DelegateManager.h>
#include "GraphicsApi/Vulkan/RavenVulkanManager.h"
#include "GraphicsApi/Vulkan/VulkanUtility/UtilityFunctions.h"
#include "GraphicsApi/Vulkan/VulkanUtility/VulkanInitialization.h"
#include "GraphicsApi/Vulkan/VulkanUtility/StructInitializer.h"
#include "GraphicsApi/Vulkan/VulkanUtility/CommandBufferUtility.h"
#include "GraphicsApi/Vulkan/VulkanUtility/ImageUtility.h"

namespace Raven
{
    RavenVulkanManager::RavenVulkanManager() {};

    RavenVulkanManager::~RavenVulkanManager() {};

    bool RavenVulkanManager::initialize()
    {
        bool contextInitialized = initializeVulkanContext();
        assert(contextInitialized);

        m_context.funcPointers.pfnGetDeviceProcAddr = (PFN_vkGetDeviceProcAddr)
            glfwGetInstanceProcAddress(m_context.instance, "vkGetDeviceProcAddr");

        m_context.funcPointers.pfnWaitSemaphoresKHR = (PFN_vkWaitSemaphoresKHR)m_context.funcPointers.pfnGetDeviceProcAddr(m_context.device, "vkWaitSemaphoresKHR");
        assert(m_context.funcPointers.pfnWaitSemaphoresKHR);

        m_context.funcPointers.pfnSignalSemaphoreKHR = (PFN_vkSignalSemaphoreKHR)m_context.funcPointers.pfnGetDeviceProcAddr(m_context.device, "vkSignalSemaphoreKHR");
        assert(m_context.funcPointers.pfnSignalSemaphoreKHR);

        m_context.funcPointers.pfnGetSemaphoreCounterValueKHR = (PFN_vkGetSemaphoreCounterValueKHR)m_context.funcPointers.pfnGetDeviceProcAddr(m_context.device, "vkGetSemaphoreCounterValueKHR");
        assert(m_context.funcPointers.pfnGetSemaphoreCounterValueKHR);

#ifdef RAVEN_DEBUG
        m_context.funcPointers.pfnDebugMarkerSetObjectNameEXT = (PFN_vkDebugMarkerSetObjectNameEXT)glfwGetInstanceProcAddress(m_context.instance, "vkDebugMarkerSetObjectNameEXT");
        assert(m_context.funcPointers.pfnDebugMarkerSetObjectNameEXT);
        m_context.funcPointers.pfnCreateDebugUtilsMessengexEXT = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr(m_context.instance, "vkCreateDebugUtilsMessengerEXT");

        if(m_context.funcPointers.pfnCreateDebugUtilsMessengexEXT)
        {
            createDebugMessenger();
        }
#endif

        VulkanContextDelegates contextDelegates;
        contextDelegates.getVulkanContextFunc.Bind<RavenVulkanManager, &RavenVulkanManager::getVulkanContext>(this);

        DelegateManager& manager = DelegateManager::getInstance();
        manager.registerVulkanContextDelegates(contextDelegates);

        //Create window surface
        SurfaceCreateInfo createInfo = {};
        createInfo.physicalDevice = m_context.physicalDevice;
        createInfo.instance = m_context.instance;
        createInfo.queueFamilyIndex = m_context.primaryQueueFamilyIndex;
        bool surfaceCreated = manager.getWindowDelegates().createWindowSurfaceFunc.Invoke(createInfo);

        bool resourceManagerInitialized = m_resourceManager.initialize(m_context.physicalDevice, m_context.device);

        createDelegates();

        return contextInitialized && surfaceCreated && resourceManagerInitialized;
    }

    void RavenVulkanManager::shutdown()
    {
        m_resourceManager.shutdown();
        //TODO: Add context destruction.
    }

    bool RavenVulkanManager::createRavenVertexBuffer(void* data, size_t elementSize, size_t elementCount, RavenBuffer& buffer)
    {
        assert(data);
        VkDeviceSize dataSize = static_cast<VkDeviceSize>(elementSize * elementCount);
        buffer.count = elementCount;
        return createDeviceLocalBuffer(data, dataSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, buffer);
    }

    bool RavenVulkanManager::createRavenIndexBuffer(void* data, size_t elementSize, size_t elementCount, RavenBuffer& buffer)
    {
        assert(data);
        VkDeviceSize dataSize = static_cast<VkDeviceSize>(elementSize * elementCount);
        buffer.count = elementCount;
        return createDeviceLocalBuffer(data, dataSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, buffer);
    }

    bool RavenVulkanManager::addNewTextureToMaterial(Material& material, const TextureData* texture, MaterialDefinition definition)
    {
        uint32_t currentImageIndex = static_cast<uint32_t>(definition);
        //Currently materials can only hold a fixed (5) amount of textures.
        assert(currentImageIndex < material.m_images.size());

        VkFormat format = UtilityFunctions::getFormatBasedOnTextureChannels(static_cast<uint32_t>(texture->texChannels));
        VkImageCreateInfo createInfo = StructInitializer::getImageCreateInfo(texture->width, texture->height, VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT, format);
        bool success = createTextureImage(*texture, createInfo, material.m_images.at(currentImageIndex));
        assert(success);
        VkImageViewCreateInfo viewCreateInfo = StructInitializer::getImageViewCreateInfo(material.m_images.at(currentImageIndex).image, format, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_VIEW_TYPE_2D);
        success = ImageUtility::createImageView(m_context.device, viewCreateInfo,material.m_images.at(currentImageIndex).imageView);
        assert(success);
        
        //Materials should always be placed in the same slot. This should guarantee that.
        material.m_images.at(currentImageIndex).sampler = ImageUtility::createDefaultLinearSampler(m_context.device);
        return success;
    }

    bool RavenVulkanManager::createRavenMaterial(const MaterialData& data, Material& material)
    {
        if(data.baseColorTexture)
        {
            if(!addNewTextureToMaterial(material, data.baseColorTexture, MaterialDefinition::ALBEDO))
                return false;
            material.m_hasBaseColor = true;
        }
        if(data.normalTexture)
        {
            if(!addNewTextureToMaterial(material, data.normalTexture, MaterialDefinition::NORMAL))
                return false;
            material.m_hasNormal = true;
        }
        if (data.metallicRoughnessTexture)
        {
            if (!addNewTextureToMaterial(material, data.metallicRoughnessTexture, MaterialDefinition::ROUGHNESS))
                return false;
            material.m_hasMetallicRoughness = true;
        }
        if(data.occlusionTexture)
        {
            if(!addNewTextureToMaterial(material, data.occlusionTexture, MaterialDefinition::OCCLUSION))
                return false;
            material.m_hasOcclusion = true;
        }
        if(data.emissiveTexture)
        {
            if(!addNewTextureToMaterial(material, data.emissiveTexture, MaterialDefinition::EMISSIVE))
                return false;
            material.m_hasEmissive = true;
        }
        return true;
    }

    bool RavenVulkanManager::createAlbedoOnlyMaterial(VkFormat format, const TextureData& textureData, Material& material)
    {
        if (textureData.pixels)
        {
            uint32_t currentImageIndex = static_cast<uint32_t>(MaterialDefinition::ALBEDO);
            //Currently materials can only hold a fixed (5) amount of textures.
            assert(currentImageIndex < material.m_images.size());

            VkImageCreateInfo createInfo = StructInitializer::getImageCreateInfo(textureData.width, textureData.height, VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT, format);
            bool success = createTextureImage(textureData, createInfo, material.m_images.at(currentImageIndex));
            assert(success);
            VkImageViewCreateInfo viewCreateInfo = StructInitializer::getImageViewCreateInfo(material.m_images.at(currentImageIndex).image, format, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_VIEW_TYPE_2D);
            success = ImageUtility::createImageView(m_context.device, viewCreateInfo, material.m_images.at(currentImageIndex).imageView);
            assert(success);

            //Materials should always be placed in the same slot. This should guarantee that.
            material.m_images.at(currentImageIndex).sampler = ImageUtility::createDefaultLinearSampler(m_context.device);

            material.m_hasBaseColor = true;
            return success;
        }
        return false;
    }

    bool RavenVulkanManager::initializeVulkanContext()
    {
        bool instanceCreated = initializeVulkanInstance();
        if(!instanceCreated)
        {
            std::cerr << "Failed to create vulkan instance! \n";
        }

        bool physicalDeviceInitialized = initializePhysicalDevice();
        if(!physicalDeviceInitialized)
        {
            std::cerr << "Failed to initialize physical device!\n";
        }

        bool logicalDeviceCreated = initializeLogicalDevice();
        if(!logicalDeviceCreated)
        {
            std::cerr << "Failed to initialize logical device!\n";
        }
        return instanceCreated && physicalDeviceInitialized && logicalDeviceCreated;
    }

    void RavenVulkanManager::createDelegates()
    {
        VulkanResourceManagerDelegates delegates;
        delegates.createAllocatedBufferFunc.Bind<VulkanResourceManager, &VulkanResourceManager::createAllocatedBuffer>(&m_resourceManager);
        delegates.destroyAllocatedBufferFunc.Bind<VulkanResourceManager, &VulkanResourceManager::destroyBuffer>(&m_resourceManager);
        delegates.createAllocatedImageFunc.Bind<VulkanResourceManager, &VulkanResourceManager::createAllocatedImage>(&m_resourceManager);
        delegates.destroyAllocatedImageFunc.Bind<VulkanResourceManager, &VulkanResourceManager::destroyImage>(&m_resourceManager);
        delegates.mapAllocatedMemoryFunc.Bind<VulkanResourceManager, &VulkanResourceManager::mapMemory>(&m_resourceManager);
        delegates.unmapAllocatedMemoryFunc.Bind<VulkanResourceManager, &VulkanResourceManager::unmapMemory>(&m_resourceManager);
        delegates.flushAllocatedMemoryFunc.Bind<VulkanResourceManager, &VulkanResourceManager::flushMemory>(&m_resourceManager);

        RavenVulkanManagerDelegates vulkanManagerDelegates;
        vulkanManagerDelegates.createVertexBufferFunc.Bind<RavenVulkanManager, &RavenVulkanManager::createRavenVertexBuffer>(this);
        vulkanManagerDelegates.createIndexBufferFunc.Bind<RavenVulkanManager, &RavenVulkanManager::createRavenIndexBuffer>(this);
        //m_delegates.createCubemapFunc.Bind<RavenVulkanManager, &RavenVulkanManager::createCubemap>(this);
        vulkanManagerDelegates.createTextureImageFunc.Bind<RavenVulkanManager, &RavenVulkanManager::createTextureImage>(this);
        vulkanManagerDelegates.createRavenImagesFunc.Bind<RavenVulkanManager, &RavenVulkanManager::createRavenImages>(this);
        vulkanManagerDelegates.createRavenMaterialFunc.Bind<RavenVulkanManager, &RavenVulkanManager::createRavenMaterial>(this);

        DelegateManager& manager = DelegateManager::getInstance();
        manager.registerVulkanResourceManagerDelegates(delegates);
        manager.registerVulkanManagerDelegates(vulkanManagerDelegates);
    }

    bool RavenVulkanManager::createDeviceLocalBuffer(void* dataToCopy, VkDeviceSize bufferSize, VkBufferUsageFlags bufferUsage, RavenBuffer& buffer)
    {
        //First create a staging buffer.
        RavenBuffer stagingBuffer;
        VkBufferCreateInfo bufferInfo = StructInitializer::getBufferCreateInfo({}, bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_SHARING_MODE_EXCLUSIVE);
        if(!m_resourceManager.createAllocatedBuffer(bufferInfo, VMA_MEMORY_USAGE_CPU_ONLY, stagingBuffer))
        {
            return false;
        }

        //Map and copy the data to the staging buffer.
        void* data;
        m_resourceManager.mapMemory(stagingBuffer.memoryAllocation, &data);
        memcpy(data, dataToCopy, (size_t)bufferSize);
        m_resourceManager.unmapMemory(stagingBuffer.memoryAllocation);

        //Create the actual device local buffer.
        VkBufferCreateInfo deviceBufferInfo = StructInitializer::getBufferCreateInfo({}, bufferSize, bufferUsage, VK_SHARING_MODE_EXCLUSIVE);
        bool success = m_resourceManager.createAllocatedBuffer(deviceBufferInfo, VMA_MEMORY_USAGE_GPU_ONLY, buffer);
        assert (success);

        //Copy data from the staging buffer to the device local buffer.
        success = copyBuffer(stagingBuffer.buffer, buffer.buffer, bufferSize);
        assert(success);

        //Clean up.
        m_resourceManager.destroyBuffer(stagingBuffer);

        return success;
    }

    bool RavenVulkanManager::copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size)
    {
        VkCommandPool pool;
        CommandBufferUtility::createCommandPool(m_context.device, 0, false, pool);

        VkCommandBuffer commandBuffer = CommandBufferUtility::beginSingleTimeCommands(pool, m_context.device);

        VkBufferCopy copyRegion = {};
        copyRegion.size = size;
        vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);

        CommandBufferUtility::endSingleTimeCommands(commandBuffer, m_context.device, m_context.queues[0], pool);

        CommandBufferUtility::destroyCommandPool(m_context.device, pool);

        return pool == VK_NULL_HANDLE;
    }

    bool RavenVulkanManager::createTextureImage(const TextureData& textureData, const VkImageCreateInfo& createInfo, RavenImage& image)
    {
        RavenBuffer stagingBuffer;
        VkBufferCreateInfo bufferInfo = StructInitializer::getBufferCreateInfo({}, textureData.imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_SHARING_MODE_EXCLUSIVE);
        if(!m_resourceManager.createAllocatedBuffer(bufferInfo, VMA_MEMORY_USAGE_CPU_ONLY, stagingBuffer))
        {
            return false;
        }

        void* data;
        m_resourceManager.mapMemory(stagingBuffer.memoryAllocation, &data);
        memcpy(data, textureData.pixels, static_cast<size_t>(textureData.imageSize));
        m_resourceManager.unmapMemory(stagingBuffer.memoryAllocation);

        //AssetLoader::releaseTextureData(m_textureData);

        bool success = m_resourceManager.createAllocatedImage(createInfo, VMA_MEMORY_USAGE_GPU_ONLY, image);
        assert(success);

        VkImageSubresourceRange beginRange = ImageUtility::getDefaultImageSubresourceRange(createInfo.format, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
        if (!ImageUtility::transitionImageLayout(m_context.device, m_context.queues[0],
                VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, beginRange, image.image))
        {
            return false;
        }

        copyBufferToImage(stagingBuffer.buffer, image.image, static_cast<uint32_t>(textureData.width), static_cast<uint32_t>(textureData.height));

        VkImageSubresourceRange endRange = ImageUtility::getDefaultImageSubresourceRange(createInfo.format, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
        if (!ImageUtility::transitionImageLayout(m_context.device,m_context.queues[0],
                VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,endRange, image.image))
        {
            return false;
        }

        image.height = textureData.height;
        image.width = textureData.width;
        image.mipLevels = 1;

        m_resourceManager.destroyBuffer(stagingBuffer);

        return true;
    }

    bool RavenVulkanManager::createRavenImages(VkFormat format, VkExtent2D extent, VkImageUsageFlags imageUsages, 
        VkImageAspectFlagBits imageAspectFlags, uint32_t allocationCount, std::vector<RavenImage>& images)
    {
        images.clear();
        images.resize(allocationCount);
        for (size_t frameIndex = 0; frameIndex < allocationCount; frameIndex++)
        {
            VkImageCreateInfo createInfo = StructInitializer::getImageCreateInfo(extent.width, extent.height, imageUsages, format);
            if (!m_resourceManager.createAllocatedImage(createInfo, VMA_MEMORY_USAGE_GPU_ONLY, images[frameIndex]))
            {
                return false;
            }

            VkImageViewCreateInfo viewInfo = StructInitializer::getImageViewCreateInfo(images[frameIndex].image, createInfo.format, imageAspectFlags, VK_IMAGE_VIEW_TYPE_2D);
            if (!ImageUtility::createImageView(m_context.device, viewInfo, images[frameIndex].imageView))
            {
                return false;
            }
        }
        return true;
    }

    void RavenVulkanManager::copyBufferToImage(VkBuffer& srcBuffer, VkImage& dstImage, uint32_t width, uint32_t height)
    {
        VkCommandPool pool;
        CommandBufferUtility::createCommandPool(m_context.device, 0, false, pool);

        VkCommandBuffer commandBuffer = CommandBufferUtility::beginSingleTimeCommands(pool, m_context.device);

        VkBufferImageCopy region = {};
        region.bufferOffset = 0;
        region.bufferRowLength = 0;
        region.bufferImageHeight = 0;
        region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        region.imageSubresource.mipLevel = 0;
        region.imageSubresource.baseArrayLayer = 0;
        region.imageSubresource.layerCount = 1;
        region.imageOffset = { 0,0,0 };
        region.imageExtent = { width, height, 1 };

        vkCmdCopyBufferToImage(commandBuffer, srcBuffer, dstImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

        CommandBufferUtility::endSingleTimeCommands(commandBuffer, m_context.device, m_context.queues[0], pool);

        CommandBufferUtility::destroyCommandPool(m_context.device, pool);
    }

    void RavenVulkanManager::copyBufferToCubemap(VkBuffer& srcBuffer, VkImage& dstImage, std::vector<VkBufferImageCopy>& copyRegions)
    {
        VkCommandPool pool;
        CommandBufferUtility::createCommandPool(m_context.device, 0, false, pool);

        VkCommandBuffer commandBuffer = CommandBufferUtility::beginSingleTimeCommands(pool, m_context.device);

        vkCmdCopyBufferToImage(commandBuffer, srcBuffer, dstImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, static_cast<uint32_t>(copyRegions.size()), copyRegions.data());

        CommandBufferUtility::endSingleTimeCommands(commandBuffer, m_context.device, m_context.queues[0], pool);

        CommandBufferUtility::destroyCommandPool(m_context.device, pool);
    }

    bool RavenVulkanManager::initializeVulkanInstance()
    {
#ifdef RAVEN_DEBUG
        m_enableValidationLayers = true;
#else
        m_enableValidationLayers = false;
#endif
        uint32_t count;
        const char** glfwExtensions = glfwGetRequiredInstanceExtensions(&count);

        std::vector<const char*> extensions(glfwExtensions, glfwExtensions + count);
        std::vector<const char*> enabledLayers;

        if (m_enableValidationLayers)
        {
            extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
            enabledLayers.push_back("VK_LAYER_KHRONOS_validation");
            //enabledLayers.push_back("VK_LAYER_LUNARG_api_dump");
        }

        bool success = VulkanInitialization::createVulkanInstance(extensions, enabledLayers, m_context.instance);

        return success;
    }

    bool RavenVulkanManager::initializePhysicalDevice()
    {
        //Select physical device
        std::vector<VkPhysicalDevice> physicalDevices;
        VulkanInitialization::getPhysicalDevices(m_context.instance, physicalDevices);

        assert(physicalDevices.size() > 0);

        for (size_t i = 0; i < physicalDevices.size(); i++)
        {
            if (VulkanInitialization::arePhysicalDeviceExtensionsSupported(physicalDevices[i], desiredDeviceExtensions))
            {
                m_context.physicalDevice = physicalDevices[i];
            }
        }

        //Query timeline semaphore feature support.
        m_context.timelineSemaphoreFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_FEATURES;
        m_context.timelineSemaphoreFeatures.pNext = nullptr;

        VkPhysicalDeviceFeatures features{};
        m_context.extraDeviceFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
        m_context.extraDeviceFeatures.pNext = &m_context.timelineSemaphoreFeatures;
        m_context.extraDeviceFeatures.features = features;

        vkGetPhysicalDeviceFeatures2(m_context.physicalDevice, &m_context.extraDeviceFeatures);

        return m_context.physicalDevice != VK_NULL_HANDLE;
    }

    bool RavenVulkanManager::initializeLogicalDevice()
    {
        assert(m_context.physicalDevice != VK_NULL_HANDLE);

        VulkanQueueInfo queueInfo;
        if(!VulkanInitialization::getPhysicalDevicePrimaryQueueFamily(m_context.physicalDevice, queueInfo))
            return false;

        bool deviceCreated = VulkanInitialization::createLogicalDevice(m_context.physicalDevice, desiredDeviceExtensions, queueInfo, m_context.device);
        assert(deviceCreated);

        initializeDeviceQueues(queueInfo);
        m_context.primaryQueueFamilyIndex = queueInfo.queueFamilyIndex;

        return deviceCreated;
    }

    bool RavenVulkanManager::initializeDeviceQueues(const VulkanQueueInfo& queueInfo)
    {
        //Get the handle of each device queue.
        for(size_t i = 0 ; i < queueInfo.priorities.size(); i++)
        {
            VkQueue queue;
            m_context.queues.push_back(queue);
            vkGetDeviceQueue(m_context.device, queueInfo.queueFamilyIndex, i, &m_context.queues.back());
        }
        return true;
    }

    bool RavenVulkanManager::createDebugMessenger()
    {
        VkDebugUtilsMessengerCreateInfoEXT debugMessengerCreateInfo = {};
        debugMessengerCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
        debugMessengerCreateInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
        debugMessengerCreateInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
        debugMessengerCreateInfo.pfnUserCallback = debugCallback;
        debugMessengerCreateInfo.pUserData = nullptr;
        VkResult result = m_context.funcPointers.pfnCreateDebugUtilsMessengexEXT(m_context.instance, &debugMessengerCreateInfo, nullptr, &m_context.debugMessenger);
        assert(result == VK_SUCCESS);
        return result == VK_SUCCESS;
    }
}
