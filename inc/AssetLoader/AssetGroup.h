#ifndef RAVEN_GRAPHICS_OBJECT_H_INCLUDED
#define RAVEN_GRAPHICS_OBJECT_H_INCLUDED

#include "GraphicsApi/Vulkan/Common.h"
#include "GraphicsApi/Vulkan/VulkanUtility/VulkanStructs.h"
#include <vector>
#include "EntityComponentSystem/RavenComponents/ComponentUtility/ComponentInitializationStructs.h"

namespace Raven
{
    struct AssetGroup
    {
        //These arrays contain all the "raw" data.
        std::vector<MeshData> meshes;
        std::vector<CameraData> cameras;
        std::vector<SkeletonData> skeletons;
        std::vector<MaterialData> materials;
        std::vector<TextureData> textures;

        //This array should hold the information about 
        //mesh, camera and skeleton relations etc.
        //Scene graph essentially.
        std::vector<NodeData> nodes;
        std::vector<NodeAnimation> animations;
    };
}

#endif