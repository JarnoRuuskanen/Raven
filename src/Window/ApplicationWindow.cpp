#include "Window/ApplicationWindow.h"

#include <iostream>
#include <assert.h>

namespace Raven
{
    ApplicationWindow::ApplicationWindow()
    {
    }

    ApplicationWindow::~ApplicationWindow()
    {
        glfwDestroyWindow(m_window);
        glfwTerminate();
    }

    void ApplicationWindow::initialize(const WindowCreateInfo &info)
    {
        bool success = createWindow(info);
        assert(success);
    }

    bool ApplicationWindow::createWindow(const WindowCreateInfo& info)
    {
        assert (info.height > 0 && info.width > 0);
        m_height = info.height;
        m_width = info.width;

        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
        m_window = glfwCreateWindow(m_width, m_height, "Raven", nullptr, nullptr);
        glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

        return m_window != nullptr;
    }

    bool ApplicationWindow::createWindowSurface(const SurfaceCreateInfo& info)
    {
        if (!glfwGetPhysicalDevicePresentationSupport(info.instance, info.physicalDevice, info.queueFamilyIndex))
        {
            std::cerr << "Primary queue family index does not support presentation! \n";
            return false;
        }

        //glfwSetFramebufferSizeCallback(m_window, info.framebufferResizeCallback);
        VkResult result = glfwCreateWindowSurface(info.instance, m_window, nullptr, &m_windowSurface);
        if (result != VK_SUCCESS)
        {
            return false;
        }
        return true;
    }

    GLFWwindow* ApplicationWindow::getWindow()
    {
        return m_window;
    }

    VkSurfaceKHR& ApplicationWindow::getSurface()
    {
        return m_windowSurface;
    }
}