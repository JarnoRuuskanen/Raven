#version 450

layout(location = 0) in vec4 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in vec2 inTexCoord;

layout(binding = 0) uniform UniformBufferObject
{
    mat4 model;
    mat4 view;
    mat4 proj;
}ubo;

layout(location = 0) out vec3 vertTexCoord;

void main()
{
    vertTexCoord = inPosition;
    vertTexCoord *= -1.0f;
    gl_Position = ubo.proj * ubo.model * vec4(inPosition, 1.0);
}