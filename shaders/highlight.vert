#version 450

layout(location = 0) in vec4 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in vec2 inTexCoord;

layout(binding = 0) uniform UniformBufferObject
{
    mat4 model;
    mat4 view;
    mat4 proj;
}ubo;

void main() {
    vec4 pos = vec4((inPosition.xyz + (inNormal * 0.02)),1.0);
    vec4 position = ubo.proj * ubo.view * ubo.model * pos;
    gl_Position = position;
}
