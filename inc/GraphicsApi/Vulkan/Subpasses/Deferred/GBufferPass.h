#ifndef RAVEN_DEFERRED_PASS_H_INCLUDED
#define RAVEN_DEFERRED_PASS_H_INCLUDED

#include "GraphicsApi/Vulkan/Subpasses/BaseSubpass.h"

namespace Raven
{
    class GBufferPass : public BaseSubpass
    {
    public:
        GBufferPass() : BaseSubpass("deferredPass") {}
        void draw(const RavenBuffer& vertexBuffer, const RavenBuffer& indexBuffer, const std::vector<VkDescriptorSet>& descriptorSets, 
                  const std::vector<PushConstantData*>& pushConstants, VkCommandBuffer& cmdBuffer) override;
    private:
        void initializeVertexInputState(VkPipelineVertexInputStateCreateInfo& vertexInputState) override;
        void initializeInputAssemblyState(VkPipelineInputAssemblyStateCreateInfo& inputAssembly) override;
        void initializeViewportState(VkPipelineViewportStateCreateInfo& viewportState) override;
        void initializeRasterizerState(VkPipelineRasterizationStateCreateInfo& rasterizer) override;
        void initializeMultisampleState(VkPipelineMultisampleStateCreateInfo& multisampling) override;
        void initializeDepthStencilState(VkPipelineDepthStencilStateCreateInfo& depthStencil) override;
        void initializeColorBlendState(VkPipelineColorBlendStateCreateInfo& colorBlend) override;
        void initializeDynamicState(VkPipelineDynamicStateCreateInfo& dynamicStateCreateInfo) override;
        VkResult initializePipelineLayout(const VkDevice& device) override;
    };
}
#endif