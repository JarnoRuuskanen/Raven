#ifndef RAVEN_COMPONENT_UTILITY_H_INCLUDED
#define RAVEN_COMPONENT_UTILITY_H_INCLUDED

#include "EntityComponentSystem/ECSTypes.h"

#include <cassert>
#include <string>

namespace Raven
{
    namespace ComponentUtility
    {
        std::string getComponentName(ComponentId id);
        ComponentId getComponentIdFromString(const std::string& componentString);
    }
}
#endif