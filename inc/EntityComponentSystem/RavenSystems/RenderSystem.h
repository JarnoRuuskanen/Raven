#ifndef RAVEN_RENDER_SYSTEM_H_INCLUDED
#define RAVEN_RENDER_SYSTEM_H_INCLUDED

#include <GraphicsApi/Vulkan/VulkanObjects/Swapchain.h>
#include <Utility/DelegateTypes.h>
#include "GraphicsApi/Vulkan/VulkanObjects/GraphicsPipeline.h"
#include "EntityComponentSystem/RavenSystems/System.h"
#include "EntityComponentSystem/RavenComponents/Renderable.h"
#include "EntityComponentSystem/RavenComponents/Transform.h"
#include <EntityComponentSystem/RavenComponents/Camera.h>
#include <EntityComponentSystem/RavenComponents/Material.h>
#include <GraphicsApi/Vulkan/Renderers/RenderPipeline.h>
#include <GraphicsApi/Vulkan/UI/RavenUI.h>
#include "GraphicsApi/Vulkan/VulkanWorker/RavenVulkanWorker.h"
#include "GraphicsApi/Vulkan/Subpasses/Deferred/GBufferPass.h"
#include "GraphicsApi/Vulkan/Subpasses/Deferred/CompositionPass.h"
#include "GraphicsApi/Vulkan/Subpasses/Forward/LightPass.h"

namespace Raven
{
    class RenderSystem : public System<Renderable, Transform>
    {
        using BaseType = System<Renderable, Transform>;
    public:
        RenderSystem(EntityManager* pGameWorld);
        void update(const Timer& timer) override;
        /**
         * @brief Sets the active player. View matrix and transform of the active player entity will be used to view the scene.
         * @param entity
         */
        void setActivePlayer(Entity* entity);
        void initializeUI();
        inline bool renderingUI(){ return m_renderUI; }
        void updateSelectedObjectInfo(const EntityId& entityId);
        void changeHighlightedEntity(EntityId id);
        void highlightEntity(const Entity& entity);
        size_t getSwapchainImageCount() { return m_swapchain.swapchainImages.size(); }
        void registerRenderableToRenderPipeline(Renderable* renderable, const std::vector<PushConstantData*>& pushConstants, const std::string& pipelineName);
        const Swapchain& getSwapchain(){ return m_swapchain; }
        void setupLights(const RavenBuffer& lightBuffer);
    private:
        void entityAddedToSystem(Entity* entity) override;
        uint64_t prepareTimelineWaitAndSignalInfos(uint32_t imageIndex);
        void waitForPreviousFrameToFinish();
        void render(uint32_t currentFrame);
        void present(uint32_t imageIndex);
        bool createSynchronizationObjects();
        bool initializeSwapchain();
        bool reCreateSwapchain();
        bool cleanupSwapchain();

        void initializeUniformBuffers();
        void updateUniformBuffers(double deltaTime);
        void createMenuControls();
        bool toggleMenu(const KeyEvent& event);
        void createSubpasses();
        void createRenderPipelines();
        void createDeferredRenderPipeline();
        void createForwardRenderPipeline();
        void createTerrainPipeline();
        void createUnlitPipeline();
        void createHighlightPipeline();
        void createMeshPipeline();
        uint64_t recordMainRenderingCommands(uint32_t imageIndex, uint64_t timelineCurrentValue);
        uint64_t recordPostProcessingCommands(uint32_t imageIndex, uint64_t timelineCurrentValue);
        uint64_t recordUICommands(uint32_t imageIndex, uint64_t timelineCurrentValue);

        //Synchronization
        std::vector<VkSemaphore> m_imageAvailableSemaphores;
        VkSemaphore m_timelineSemaphore;
        uint64_t m_currentSignalToWait;

        std::vector<VkSemaphore> m_allRenderingDoneSemaphores;
        std::vector<VkFence> m_inFlightFences;
        uint32_t m_currentFrame = 0;

        //Swapchain.
        Swapchain m_swapchain;

        //Device queue family indices
        uint32_t m_presentationQueueIndex;
        VkQueue m_commandBufferQueue;
        VkQueue m_presentationQueue;

        VkDevice m_device;

        //ACTIVE PLAYER COMPONENT POINTERS:
        Transform* m_playerTransformComponent = nullptr;
        Camera* m_playerCameraComponent = nullptr;

        //RenderSystem should have a default camera which can then be replaced if necessary.
        UniformBufferObject m_ubo = {};

        RavenUI m_ui;
        bool m_renderUI = false;

        EntityId m_highlightEntityId;

        //Delegates to be exposed outside.
        VulkanResourceManagerDelegates m_vulkanResourceManagerDelegates;

        WindowDelegates m_appWindowDelegates;

        RavenVulkanWorker m_vulkanWorker;

        //These will hold all the rendering commands.
        std::vector<CommandBufferRecordingThreadParameters> m_mainRenderingParams;
        std::vector<CommandBufferRecordingThreadParameters> m_postProcessingRenderingParams;
        CommandBufferRecordingThreadParameters m_uiRecordingFunction;

        //Prepared every frame.
        std::vector<CommandBufferRecordingThreadParameters> m_currentFrameRecordingParams;

        //New renderer functions, objects and variables.

        //All available render pipelines.
        //These are executed before postprocessing pipelines.
        std::vector<RenderPipeline*> m_mainRenderingPipelines;
        //These are executed just before UI rendering.
        std::vector<RenderPipeline*> m_postProcessingPipelines;  
        //A list of all existing subpasses.
        std::unordered_map<std::string, BaseSubpass*> m_availableSubpasses;

        VulkanExtensionFunctionPointers m_extensionFunctions;

        //Deferred lighting pipelines:
        GBufferPass m_gBufferPass;
        CompositionPass m_compositionPass;

        //Forward lighting pipelines:
        LightPass m_forwardLightPass;

        //All the rendering pipelines;
        RenderPipeline m_terrainPipeline;
        RenderPipeline m_highlightPipeline;
        RenderPipeline m_deferredPipeline;
        RenderPipeline m_forwardPipeline;
        RenderPipeline m_wireframePipeline;
        RenderPipeline m_unlitPipeline;
    };
}

#endif