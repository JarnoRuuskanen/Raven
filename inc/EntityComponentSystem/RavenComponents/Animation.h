#ifndef RAVEN_ANIMATION_COMPONENT_H_INCLUDED
#define RAVEN_ANIMATION_COMPONENT_H_INCLUDED
#include "EntityComponentSystem/RavenComponents/Component.h"
#include "EntityComponentSystem/RavenComponents/ComponentUtility/ComponentInitializationStructs.h"

#include "GraphicsApi/Vulkan/VulkanObjects/RavenBuffer.h"

namespace Raven
{
    class Animation : public Component
    {
    public:
        static constexpr ComponentId ID = RavenUtility::generateHash("Animation");
        Animation() : name("Animation Component") {};
        virtual bool init(void* data) override final
        {
            NodeAnimation* animation = static_cast<NodeAnimation*>(data);
            strcpy(name, animation->name.c_str());
            //I don't really want to store pointers but instead store the 
            //data here so that it can then be released elsewhere. This
            //needs to be fixed.
            samplers = &animation->samplers;
            channels = &animation->channels;
            start = animation->start;
            end = animation->end;
            currentTimePoint = 0;
            paused = false;
            return true;
        }
        char name [64];
        std::vector<AnimationSampler>* samplers;
        std::vector<AnimationChannel>* channels;
        float start = std::numeric_limits<float>::max();
        float end = std::numeric_limits<float>::min();
        float currentTimePoint;
        bool paused;

        //Used to pass uniform data about joints.
        VkDescriptorSet m_descriptorSet;
    };
}

#endif