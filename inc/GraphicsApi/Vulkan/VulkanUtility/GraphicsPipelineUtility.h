#ifndef RAVEN_GRAPHICS_PIPELINE_UTILITY_H_INCLUDED
#define RAVEN_GRAPHICS_PIPELINE_UTILITY_H_INCLUDED

#include "GraphicsApi/Vulkan/Common.h"
#include "GraphicsApi/Vulkan/VulkanObjects/GraphicsPipeline.h"
#include "GraphicsApi/Vulkan/VulkanUtility/VulkanStructs.h"

namespace Raven
{
    namespace GraphicsPipelineUtility
    {
        VkResult createNonColorBlendingGraphicsPipeline(const VkDevice& device, const VkExtent2D& swapchainExtent, const VkRenderPass& renderPass,
                                                        const std::vector<VkDescriptorSetLayout>& descriptorSetLayouts, const std::vector<ShaderLoadInformation>& shadersToLoad,
                                                        const std::vector<VkVertexInputBindingDescription>& vertexInputBindings, const std::vector<VkVertexInputAttributeDescription>& vertexInputAttributes,
                                                        VkCullModeFlagBits cullMode, VkFrontFace frontFace, VkPolygonMode polygonMode, VkPrimitiveTopology topology,
                                                        GraphicsPipeline& graphicsPipeline);

        VkResult createColorBlendedGraphicsPipeline(const VkDevice& device, const VkExtent2D& swapchainExtent, const VkRenderPass& renderPass,
                                                    const std::vector<VkDescriptorSetLayout>& descriptorSetLayouts, const std::vector<ShaderLoadInformation>& shadersToLoad,
                                                    const std::vector<VkVertexInputBindingDescription>& vertexInputBindings, const std::vector<VkVertexInputAttributeDescription>& vertexInputAttributes,
                                                    VkCullModeFlagBits cullMode, VkFrontFace frontFace, VkPolygonMode polygonMode, VkPrimitiveTopology topology,
                                                    GraphicsPipeline& graphicsPipeline);

        //This function is meant to save the information what sort of descriptor data this pipeline will accept.
        //This data can then be used when mapping material outputs into descriptor sets.
        void setPipelineDescriptorInfos(const std::vector<GraphicsPipelineDescriptorInfo>& descriptorInfo, GraphicsPipeline& pipeline);
        //This function can be used to edit shaders live during program execution.
        bool createPipelineObjectFromShaders(const VkExtent2D &extent, const VkDevice& device, const std::vector<ShaderLoadInformation>& shaders,
                                             const std::vector<VkVertexInputBindingDescription>& bindings,
                                             const std::vector<VkVertexInputAttributeDescription>& attributes,
                                             const VkRenderPass& renderPass, bool enableColorBlending, VkCullModeFlagBits cullMode,
                                             VkFrontFace frontFace, VkPolygonMode polygonMode, VkPrimitiveTopology topology, GraphicsPipeline& pipelineObject);
    }
}

#endif