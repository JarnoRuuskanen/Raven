#ifndef RAVEN_RAY_CASTER_H_INCLUDED
#define RAVEN_RAY_CASTER_H_INCLUDED

#include <glm/glm.hpp>

namespace Raven
{
#define RAY_LENGTH 600
    enum class RayCasterType
    {
        MOUSE_POINTER,
        CROSSHAIR
    };

    class RayCaster
    {
    public:
        //Some reference http://antongerdelan.net/opengl/raycasting.html
        RayCaster(RayCasterType type, Camera* camera, const glm::mat4& projMatrix) : m_type(type), m_camera(camera), m_projectionMatrix(projMatrix), m_currentRay(glm::vec3{0.0f}) {}
        const glm::vec3& getRay(float normalizedXPos, float normalizedYPos)
        {
            update(normalizedXPos, normalizedYPos);
            return m_currentRay;
        }
        RayCasterType getRayType() { return m_type; }
    private:
        void update(float normalizedXPos, float normalizedYPos)
        {
            calculateRay(normalizedXPos, normalizedYPos);
        };
        void calculateRay(float normalizedXPos, float normalizedYPos)
        {
            //To get to clip space, we want the vector to point towards the scene so Z-coordinate should be inverted.
            glm::vec4 clipCoord = glm::vec4(normalizedXPos, normalizedYPos, -1.0f, 1.0f);
            //Convert from clip space to camera coordinates by taking inverse of projection matrix.
            glm::vec4 cameraCoord = glm::inverse(m_projectionMatrix) * clipCoord;
            cameraCoord = glm::vec4(cameraCoord.x, cameraCoord.y, -1.0f, 0.0f);
            //Lastly get the world coordinates for the ray.
            glm::vec3 rayInWorldSpace = (glm::inverse(m_camera->m_viewMatrix) * cameraCoord);
            //Normalize the vector since it's only a direction and assign it to current ray.
            m_currentRay = glm::normalize(rayInWorldSpace);
        }

        glm::vec3 m_currentRay;
        glm::mat4 m_projectionMatrix;
        Camera* m_camera;
        RayCasterType m_type;
    };
}
#endif