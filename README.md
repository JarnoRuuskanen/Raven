# Raven

My game/rendering engine project refactored and renamed.

ERRORS CAUSED BY EXTERNAL LIBRARIES:   
   
GLI:   
- For the gli library, use master branch version of gli. The newest release version of gli is not compatible with newest glm -version.   
More information here: https://github.com/g-truc/gli/issues/151
   
GLM:   
If you are on linux and have problems compiling glm, it is probably because your gcc version is not listed in glm's platform.h.    
