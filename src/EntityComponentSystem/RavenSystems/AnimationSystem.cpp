#include "EntityComponentSystem/RavenSystems/AnimationSystem.h"
#include "GraphicsApi/Vulkan/VulkanUtility/DescriptorUtility.h"
#include "GraphicsApi/Vulkan/VulkanUtility/StructInitializer.h"

#include <algorithm>
#include <glm/gtx/spline.hpp>
namespace Raven
{
	AnimationSystem::AnimationSystem(EntityManager* pGameWorld) : BaseType(pGameWorld)
	{
		//TODO: Remove all Vulkan-related stuff away from systems as it shouldn't be here. Add a descriptor manager of some sort.
		m_vulkanResourceManagerDelegates = DelegateManager::getInstance().getVulkanResourceManagerDelegates();

		const auto& context = DelegateManager::getInstance().getVulkanContextDelegates().getVulkanContextFunc.Invoke();
		m_device = context.device;
		VkDescriptorPoolSize poolSize = {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1};
		std::vector<VkDescriptorPoolSize> poolSizes = {poolSize};
		DescriptorUtility::createDescriptorPool(context.device, poolSizes, 1000, true, m_descriptorPool);

		VkDescriptorSetLayoutBinding binding;
		binding.binding = 0;
		binding.descriptorCount = 1;
		binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		binding.pImmutableSamplers = nullptr;
		binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		std::vector<VkDescriptorSetLayoutBinding> bindings = {binding};
		VkResult result = DescriptorUtility::createDescriptorSetLayout(context.device, bindings, m_descriptorSetLayout);
		assert(result == VK_SUCCESS);
	}

	void AnimationSystem::update(const Timer& timer)
	{
		//https://www.youtube.com/watch?v=f3Cr8Yx3GGA
		//Interpolate between current and next keyframes and find 
		//what the current pose for any given transform should be.

		//Set joint transforms.

		for (auto& compTuple : m_components)
		{
			Renderable* renderable = std::get<Renderable*>(compTuple);
			//This should be the mesh/skin transform.
			Transform* transform = std::get<Transform*>(compTuple);
			Animation* animation = std::get<Animation*>(compTuple);

			//Use current time point to calculate transform.
			if (!animation->paused)
			{
				animation->currentTimePoint += timer.deltaTime;
				if (animation->currentTimePoint > animation->end)
				{
					animation->currentTimePoint = animation->start;
				}
			}

			//Reference https://github.com/SaschaWillems/Vulkan-glTF-PBR/blob/master/base/VulkanglTFModel.hpp#L1358
			for (auto& channel : *animation->channels)
			{
				AnimationSampler& sampler = animation->samplers->at(channel.samplerIndex);
				if (sampler.inputs.size() > sampler.outputsVec4.size())
				{
					continue;
				}

				for (size_t i = 0; i < sampler.inputs.size() - 1; i++)
				{
					if ((animation->currentTimePoint >= sampler.inputs[i]) && (animation->currentTimePoint <= sampler.inputs[i + 1]))
					{
						float u = std::max(0.0f, animation->currentTimePoint - sampler.inputs[i]) / (sampler.inputs[i + 1] - sampler.inputs[i]);
						if (u <= 1.0f)
						{
							switch (channel.path)
							{
								case AnimationChannel::PathType::ROTATION:
								{
									glm::quat q1;
									q1.x = sampler.outputsVec4[i].x;
									q1.y = sampler.outputsVec4[i].y;
									q1.z = sampler.outputsVec4[i].z;
									q1.w = sampler.outputsVec4[i].w;
									glm::quat q2;
									q2.x = sampler.outputsVec4[i + 1].x;
									q2.y = sampler.outputsVec4[i + 1].y;
									q2.z = sampler.outputsVec4[i + 1].z;
									q2.w = sampler.outputsVec4[i + 1].w;
									
									if (sampler.interpolationType == AnimationSampler::InterpolationType::LINEAR)
									{
										transform->m_rotation = glm::normalize(glm::slerp(q1, q2, u));
									}
									else if (sampler.interpolationType == AnimationSampler::InterpolationType::STEP)
									{
										transform->m_rotation = q2;
									}
									else if (sampler.interpolationType == AnimationSampler::InterpolationType::CUBICSPLINE)
									{
										int test = 0;
									}
									break;
								}

								case AnimationChannel::PathType::TRANSLATION :
								{
									if (sampler.interpolationType == AnimationSampler::InterpolationType::LINEAR)
									{
										glm::vec4 trans = glm::mix(sampler.outputsVec4[i], sampler.outputsVec4[i + 1], u);
										transform->m_position = glm::vec3(trans);
									}
									else if (sampler.interpolationType == AnimationSampler::InterpolationType::STEP)
									{
										transform->m_position = glm::vec3(sampler.outputsVec4[i + 1]);
									}
									else if (sampler.interpolationType == AnimationSampler::InterpolationType::CUBICSPLINE)
									{
										int test = 0;
									}
									break;
								}

								case AnimationChannel::PathType::SCALE : 
								{
									if (sampler.interpolationType == AnimationSampler::InterpolationType::LINEAR)
									{
										glm::vec4 trans = glm::mix(sampler.outputsVec4[i], sampler.outputsVec4[i + 1], u);
										transform->m_scale = glm::vec3(trans);
									}
									else if (sampler.interpolationType == AnimationSampler::InterpolationType::STEP)
									{
										transform->m_scale = glm::vec3(sampler.outputsVec4[i + 1]);
									}
									else if (sampler.interpolationType == AnimationSampler::InterpolationType::CUBICSPLINE)
									{
										int test = 0;
									}
									break;
								}
							}
							transform->dirty = true;
						}
					}
				}
			}
			glm::mat4 skeletonMatrix = transform->m_localTransform;
			updateMeshJoints(skeletonMatrix, renderable);
		}
	}

	void AnimationSystem::updateMeshJoints(const glm::mat4& skeletonMatrix, Renderable* renderable)
	{
		Mesh* mesh = renderable->m_mesh;
		if (mesh)
		{
			Skeleton* skeleton = mesh->skeleton;
			if (skeleton != nullptr)
			{
				skeleton->m_skeletonUniform.matrix = skeletonMatrix;
				//Next we will update all the joints.
				glm::mat4 inverseTransform = glm::inverse(skeletonMatrix);
				//Joint count array is a fixed size array with max length of MAX_JOINT_COUNT.
				uint32_t jointCount = std::min(skeleton->m_jointCount, MAX_JOINT_COUNT);
				for (uint32_t i = 0; i < jointCount; i++)
				{
					Joint& joint = skeleton->m_joints[i];
					glm::mat4 jointMatrix = joint.jointLocalTransform * skeleton->m_inverseBindMatrices[i];
					skeleton->m_skeletonUniform.jointMatrices[i] = jointMatrix;
				}
				skeleton->m_jointCount = jointCount;
				void* data;
				m_vulkanResourceManagerDelegates.mapAllocatedMemoryFunc.Invoke(skeleton->m_uniformBuffer.memoryAllocation, &data);
				std::memcpy(data, &skeleton->m_skeletonUniform, sizeof(skeleton->m_skeletonUniform));
				m_vulkanResourceManagerDelegates.unmapAllocatedMemoryFunc.Invoke(skeleton->m_uniformBuffer.memoryAllocation);
			}
		}
	}

	void AnimationSystem::entityAddedToSystem(Entity* entity)
	{
		Renderable* renderable = entity->getComponent<Renderable>();
		if (renderable)
		{
			Mesh* mesh = renderable->m_mesh;
			if (mesh)
			{
				Skeleton* skeleton = mesh->skeleton;
				if (skeleton)
				{
					VkBufferCreateInfo uboCreateInfo = StructInitializer::getBufferCreateInfo({}, sizeof(SkeletonUniform), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);
					DelegateManager& manager = DelegateManager::getInstance();
					bool success = manager.getVulkanResourceManagerDelegates().createAllocatedBufferFunc.Invoke(uboCreateInfo, VMA_MEMORY_USAGE_CPU_TO_GPU, skeleton->m_uniformBuffer);
					assert(success);
					if (success)
					{
						//Allocate descriptor set from descriptor pool and write into it.
						std::vector<VkDescriptorSetLayout> layouts = { m_descriptorSetLayout };
						VkResult result = DescriptorUtility::allocateDescriptorSet(m_device, m_descriptorPool, layouts, skeleton->m_descriptorSet);
						assert(result == VK_SUCCESS);

						VkDescriptorBufferInfo skeletonDescriptor = {};
						skeletonDescriptor.buffer = skeleton->m_uniformBuffer.buffer;
						skeletonDescriptor.offset = 0;
						skeletonDescriptor.range = sizeof(SkeletonUniform);

						VkWriteDescriptorSet skeletonWrite =
							StructInitializer::getWriteDescriptorSet(skeleton->m_descriptorSet, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 0, &skeletonDescriptor);

						std::array<VkWriteDescriptorSet, 1> writes = { skeletonWrite };
						vkUpdateDescriptorSets(m_device, static_cast<uint32_t>(writes.size()), writes.data(), 0, nullptr);
					}
					else
					{
						std::cerr << "Did not manage to allocate a uniform buffer for animation component, remove component from entity!";
					}
				}
			}
		}
	}
}