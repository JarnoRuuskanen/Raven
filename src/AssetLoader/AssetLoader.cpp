// Define these only in *one* .cc file.
#define TINYGLTF_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#define TINYGLTF_NOEXCEPTION // optional. disable exception handling.
#include "AssetLoader/AssetLoader.h" //This includes tiny_gltf.h
#include "GraphicsApi/Vulkan/Common.h"
#include "AssetLoader/AssetGroup.h"

#include <iostream>
#include <glm/ext.hpp>

namespace Raven
{
    /*using namespace tinygltf;*/
    namespace AssetLoader
    {
        //Forward declarations:
        bool loadModel(const std::string& path, tinygltf::Model& model);

        void generateSceneGraph(tinygltf::Model& model, AssetGroup& group);

        void loadMeshData(tinygltf::Model& model, AssetGroup& group);

        //Load textures before materials as the latter will be using textures.
        void loadTextures(tinygltf::Model& model, AssetGroup& group);
        void loadMaterials(tinygltf::Model& model, AssetGroup& group);
        void loadAnimations(const tinygltf::Model& model, AssetGroup& group);
        void loadSkeletons(const tinygltf::Model& model, AssetGroup& group);

        bool fillAssetGroup(const std::string& path, AssetGroup& group)
        {
            //TODO: Separate gltf from AssetLoader to it's own sub-loader in order to support other file formats.
            tinygltf::Model model;
            bool modelLoaded = AssetLoader::loadModel(path, model);

            assert(modelLoaded);

            std::cout << "Model copyright: " << model.asset.copyright << std::endl;
            std::cout << "Model generator: " << model.asset.generator << std::endl;
            std::cout << "Model version: " << model.asset.version << std::endl;

            generateSceneGraph(model, group);
            loadMeshData(model, group);
            loadAnimations(model, group);
            loadSkeletons(model, group);

            //Load textures before materials as the latter will be using textures.
            if(!model.textures.empty())
            {
                loadTextures(model, group);
                loadMaterials(model, group);
            }

            return modelLoaded;
        }

        bool loadModel(const std::string &path, tinygltf::Model &model)
        {
            tinygltf::TinyGLTF loader;
            std::string err;
            std::string warn;

            bool ret = false;
            bool isBinary = path.find(".glb") != std::string::npos;
            if (isBinary)
            {
                ret = loader.LoadBinaryFromFile(&model, &err, &warn, path);
            }
            else
            {
                ret = loader.LoadASCIIFromFile(&model, &err, &warn, path);
            }

            if (!warn.empty()) {
                printf("Warn: %s\n", warn.c_str());
            }

            if (!err.empty()) {
                printf("Err: %s\n", err.c_str());
            }

            if (!ret) {
                printf("Failed to parse glTF\n");
            }

            return ret;
        }

        std::string findFileName(const std::string& path)
        {
            std::string name = "No name";
            if (!path.empty())
            {
                std::size_t found = path.find_last_of("/\\");
                if (found < path.length())
                {
                    name = path.substr(found + 1);
                }
            }
            return name;
        }

        bool loadTexture(const std::string& filepath, TextureData& textureData)
        {
            textureData.pixels = stbi_load(filepath.c_str(), &textureData.width, &textureData.height, &textureData.texChannels, STBI_default);
            textureData.imageSize = textureData.width * textureData.height * 4;

            if(!textureData.pixels)
            {
                assert(!"No such texture could be loaded!");
                return false;
            }

            textureData.name = findFileName(filepath);
            return true;
        }

        void releaseTextureData(TextureData& data)
        {
            stbi_image_free(data.pixels);
        }

        //Scene sits above all. Under it are nodes. The buffer data will not be stored here.
        void generateSceneGraph(tinygltf::Model& model, AssetGroup& group)
        {
            for (auto& node : model.nodes)
            {
                NodeData data = {};

                if (!node.rotation.empty())
                {
                    auto& rotation = node.rotation;
                    data.transformData.rotation = glm::quat(rotation[0], rotation[1], rotation[2],rotation[3]);
                }
                else
                {
                    data.transformData.rotation = glm::quat(glm::vec3(glm::radians(0.0f), glm::radians(0.0f), glm::radians(0.0f)));
                }

                if (!node.scale.empty())
                {
                    auto& scale = node.scale;
                    data.transformData.scale = glm::vec3(scale[0], scale[1], scale[2]);
                }
                else
                {
                    data.transformData.scale = glm::vec3(1.0f);
                }

                if (!node.translation.empty())
                {
                    auto& translation = node.translation;
                    data.transformData.translation = glm::vec3(translation[0], translation[1], translation[2]);
                }
                else
                {
                    data.transformData.translation = glm::vec3(0.0f);
                }
                if (!node.matrix.empty())
                {
                    auto& localTransform = node.matrix;
                    data.transformData.localTransform = glm::mat4(glm::vec4(localTransform[0], localTransform[4], localTransform[8], localTransform[12]),
                        glm::vec4(localTransform[1], localTransform[5], localTransform[9], localTransform[13]),
                        glm::vec4(localTransform[2], localTransform[6], localTransform[10], localTransform[14]),
                        glm::vec4(localTransform[3], localTransform[7], localTransform[11], localTransform[15]));
                }
                else
                {
                    glm::mat4 basisMatrix{ 1.0f };
                    glm::mat4 rotation = glm::toMat4(data.transformData.rotation);
                    glm::mat4 translation = glm::translate(basisMatrix, data.transformData.translation);
                    glm::mat4 scale = glm::scale(basisMatrix, data.transformData.scale);
                    //Apply scale, then rotation, then position.
                    data.transformData.localTransform = translation * rotation * scale;
                }

                data.camera = node.camera;
                data.mesh = node.mesh;
                //In gltf skeleton is referred to as skin.
                data.skeleton = node.skin;
                data.children = node.children;
                data.name = node.name;
                data.animations = {};

                group.nodes.push_back(data); 
            }
        }

        void loadMeshData(tinygltf::Model& model, AssetGroup& group)
        {
            //This part is following Sascha Willems tutorial on github.
            //https://github.com/SaschaWillems/Vulkan-glTF-PBR/blob/master/base/VulkanglTFModel.hpp
            //https://computergraphics.stackexchange.com/questions/7519/how-mesh-geometry-data-vertex-coordinates-stored-in-gltf

            for (auto& mesh : model.meshes)
            {
                MeshData meshData = {};
                meshData.name = mesh.name;
                //Each mesh primitive has a rendering mode with one of the following values:
                //POINTS, LINES or TRIANGLES
                for (auto& primitive : mesh.primitives)
                {
                    Primitive prim;
                    prim.material = primitive.material;
                    uint32_t vertexCount = 0;
                    uint32_t indexCount = 0;
                    uint32_t vertexStart = static_cast<uint32_t>(prim.vertices.size());
                    uint32_t indexStart = static_cast<uint32_t>(prim.indices.size());
                    
                    bool hasIndices = primitive.indices > -1;

                    //Vertices
                    {
                        //In which assessors is the position and indice data located?
                        auto pos = primitive.attributes["POSITION"];
                        //First get the vertex buffer view and from that the buffer data.
                        const tinygltf::Accessor& posAccessor = model.accessors[pos];
                        const tinygltf::BufferView& posView = model.bufferViews[posAccessor.bufferView];

                        //The actual data that we want.
                        const float* bufferPos = nullptr;
                        const float* bufferTexCoordSet0 = nullptr;
                        const float* bufferTexCoordSet1 = nullptr;
                        const float* bufferNormals = nullptr;
                        const uint16_t* bufferJoints = nullptr;
                        const float* bufferWeights = nullptr;

                        int jointByteStride;
                        int weightByteStride;

                        bufferPos = reinterpret_cast<const float*>(&(model.buffers[posView.buffer].data[posAccessor.byteOffset + posView.byteOffset]));
                        meshData.minVal = glm::vec3(posAccessor.minValues[0], posAccessor.minValues[1], posAccessor.minValues[2]);
                        meshData.maxVal = glm::vec3(posAccessor.maxValues[0], posAccessor.maxValues[1], posAccessor.maxValues[2]);
                        vertexCount = static_cast<uint32_t>(posAccessor.count);

                        //Valid attributes (without extensions) are: POSITION, NORMAL, TANGENT, TEXCOORD_0, TEXCOORD_1, COLOR_0, JOINTS_0, and WEIGHTS_0
                        if (primitive.attributes.find("TEXCOORD_0") != primitive.attributes.end())
                        {
                            const tinygltf::Accessor& uvAccessor = model.accessors[primitive.attributes.find("TEXCOORD_0")->second];
                            const tinygltf::BufferView& uvView = model.bufferViews[uvAccessor.bufferView];
                            bufferTexCoordSet0 = reinterpret_cast<const float*>(&(model.buffers[uvView.buffer].data[uvAccessor.byteOffset + uvView.byteOffset]));
                        }

                        if (primitive.attributes.find("TEXCOORD_1") != primitive.attributes.end())
                        {
                            const tinygltf::Accessor& uvAccessor = model.accessors[primitive.attributes.find("TEXCOORD_1")->second];
                            const tinygltf::BufferView& uvView = model.bufferViews[uvAccessor.bufferView];
                            bufferTexCoordSet1 = reinterpret_cast<const float*>(&(model.buffers[uvView.buffer].data[uvAccessor.byteOffset + uvView.byteOffset]));
                        }

                        if(primitive.attributes.find("NORMAL") != primitive.attributes.end())
                        {
                            const tinygltf::Accessor &normalAccessor = model.accessors[primitive.attributes.find("NORMAL")->second];
                            const tinygltf::BufferView &normalView = model.bufferViews[normalAccessor.bufferView];
                            bufferNormals = reinterpret_cast<const float*>(&(model.buffers)[normalView.buffer].data[normalAccessor.byteOffset + normalView.byteOffset]);
                        }

                        if(primitive.attributes.find("TANGENT") != primitive.attributes.end())
                        {
                            int tangents = 0;
                        }

                        if (primitive.attributes.find("JOINTS_0") != primitive.attributes.end())
                        {
                            const tinygltf::Accessor& jointAccessor = model.accessors[primitive.attributes.find("JOINTS_0")->second];
                            const tinygltf::BufferView& jointView = model.bufferViews[jointAccessor.bufferView];
                            bufferJoints = reinterpret_cast<const uint16_t*>(&(model.buffers[jointView.buffer].data[jointAccessor.byteOffset + jointView.byteOffset]));
                            jointByteStride = (jointAccessor.ByteStride(jointView) / sizeof(bufferJoints[0]));
                        }

                        if (primitive.attributes.find("WEIGHTS_0") != primitive.attributes.end())
                        {
                            const tinygltf::Accessor& weightAccessor = model.accessors[primitive.attributes.find("WEIGHTS_0")->second];
                            const tinygltf::BufferView& weightView = model.bufferViews[weightAccessor.bufferView];
                            bufferWeights = reinterpret_cast<const float*>(&(model.buffers[weightView.buffer].data[weightAccessor.byteOffset + weightView.byteOffset]));
                            weightByteStride = (weightAccessor.ByteStride(weightView) / sizeof(float));
                        }

                        bool hasSkeleton = (bufferJoints && bufferWeights);

                        //Create the vertices
                        for (size_t i = 0; i < posAccessor.count; i++)
                        {
                            Vertex vert = {};
                            vert.pos = glm::vec4(glm::make_vec3(&bufferPos[i * 3]), 1.0f);
                            vert.normal = bufferNormals ? glm::make_vec3(&bufferNormals[i * 3]) : glm::vec3(0.0f);
                            vert.texCoord1 = bufferTexCoordSet0 ? glm::make_vec2(&bufferTexCoordSet0[i * 2]) : glm::vec3{0.0f};
                            vert.texCoord2 = bufferTexCoordSet1 ? glm::make_vec2(&bufferTexCoordSet1[i * 2]) : glm::vec3{0.0f};
                            vert.color = glm::vec3(1);
                            vert.joint0 = hasSkeleton ? glm::vec4(glm::make_vec4(&bufferJoints[i * jointByteStride])) : glm::vec4{0.0f};
                            vert.weight0 = hasSkeleton ? glm::make_vec4(&bufferWeights[i * weightByteStride]) : glm::vec4{ 0.0f };
                            
                            if (glm::length(vert.weight0) == 0.0f)
                            {
                                vert.weight0 = glm::vec4{1.0f,0.0f,0.0f,0.0f};
                            }
                            
                            prim.vertices.push_back(vert);
                        }
                        std::cout << prim.vertices.size() << std::endl;
                    }

                    if (hasIndices)
                    {
                        const tinygltf::Accessor& accessor = model.accessors[primitive.indices];
                        const tinygltf::BufferView& bufferView = model.bufferViews[accessor.bufferView];
                        const tinygltf::Buffer& buffer = model.buffers[bufferView.buffer];

                        indexCount = static_cast<uint32_t>(accessor.count);
                        const void* dataPtr = &(buffer.data[accessor.byteOffset + bufferView.byteOffset]);

                        switch (accessor.componentType)
                        {
                        case TINYGLTF_PARAMETER_TYPE_UNSIGNED_INT:
                        {
                            const uint32_t* buf = static_cast<const uint32_t*>(dataPtr);
                            for (size_t i = 0; i < accessor.count; i++)
                            {
                                prim.indices.push_back(buf[i] + vertexStart);
                            }
                            break;
                        }

                        case TINYGLTF_PARAMETER_TYPE_UNSIGNED_SHORT:
                        {
                            const uint16_t* buf = static_cast<const uint16_t*>(dataPtr);
                            for (size_t i = 0; i < accessor.count; i++)
                            {
                                prim.indices.push_back(buf[i] + vertexStart);
                            }
                            break;
                        }

                        case TINYGLTF_PARAMETER_TYPE_UNSIGNED_BYTE:
                        {
                            const uint8_t* buf = static_cast<const uint8_t*>(dataPtr);
                            for (size_t i = 0; i < accessor.count; i++)
                            {
                                prim.indices.push_back(buf[i] + vertexStart);
                            }
                            break;
                        }

                        default:
                        {
                            std::cerr << "Index component type " << accessor.componentType << "  is not supported!" << std::endl;
                            return;
                        }
                        }
                    }
                    meshData.primitives.push_back(prim);
                }
                group.meshes.push_back(meshData);
            }
        }

        //Convenience function to avoid duplicate code. Also I'm lazy.
        void gatherTextureData(tinygltf::Material& mat, std::vector<TextureData>& textures, TextureData*& texture, uint8_t& textureTexCoord, const std::string materialName)
        {
            if(mat.values.find(materialName) != mat.values.end())
            {
                texture = &textures[mat.values[materialName].TextureIndex()];
                textureTexCoord = mat.values[materialName].TextureTexCoord();
            }
            else
            {
                texture = nullptr;
            }
        }

        void gatherAdditionalTextureData(tinygltf::Material& mat, std::vector<TextureData>& textures, TextureData*& texture, uint8_t& textureTexCoord, const std::string materialName)
        {
            if(mat.additionalValues.find(materialName) != mat.additionalValues.end())
            {
                texture = &textures[mat.additionalValues[materialName].TextureIndex()];
                textureTexCoord = mat.additionalValues[materialName].TextureTexCoord();
            }
            else
            {
                texture = nullptr;
            }
        }

        void
        loadMaterials(tinygltf::Model& model, AssetGroup& group)
        {
            //Textures must have been loaded before querying for materials.
            assert(!group.textures.empty());
            for(tinygltf::Material& mat : model.materials)
            {
                MaterialData material;
                material.materialName = mat.name;
                //First gather all the texture data used by this material.
                gatherTextureData(mat, group.textures, material.baseColorTexture, material.texCoordSets.baseColor, "baseColorTexture");
                gatherTextureData(mat, group.textures, material.metallicRoughnessTexture, material.texCoordSets.metallicRoughness, "metallicRoughnessTexture");
                gatherAdditionalTextureData(mat, group.textures, material.normalTexture, material.texCoordSets.normal, "normalTexture");
                gatherAdditionalTextureData(mat, group.textures, material.emissiveTexture, material.texCoordSets.emissive, "emissiveTexture");
                gatherAdditionalTextureData(mat, group.textures, material.occlusionTexture, material.texCoordSets.occlusion, "occlusionTexture");

                //Gather factor data.
                if(mat.values.find("baseColorFactor") != mat.values.end())
                {
                    material.baseColorFactor = glm::make_vec4(mat.values["baseColorFactor"].ColorFactor().data());
                }
                if(mat.values.find("roughnessFactor") != mat.values.end())
                {
                    material.roughtnessFactor = static_cast<float>(mat.values["roughnessFactor"].Factor());
                }
                if(mat.values.find("metallicFactor") != mat.values.end())
                {
                    material.metallicFactor = static_cast<float>(mat.values["metallicFactor"].Factor());
                }
                if (mat.additionalValues.find("emissiveFactor") != mat.additionalValues.end())
                {
                    material.emissiveFactor = glm::vec4(glm::make_vec3(mat.additionalValues["emissiveFactor"].ColorFactor().data()), 1.0);
                }
                if(mat.additionalValues.find("alphaMode") != mat.additionalValues.end())
                {
                    tinygltf::Parameter param = mat.additionalValues["alphaMode"];
                    if(param.string_value == "BLEND")
                    {
                        material.alphaMode = MaterialData::AlphaMode::ALPHAMODE_BLEND;
                    }
                    if(param.string_value == "MASK")
                    {
                        material.alphaCutoff = 0.5f;
                        material.alphaMode = MaterialData::AlphaMode::ALPHAMODE_MASK;
                    }
                }
                if(mat.additionalValues.find("alphaCutoff") != mat.additionalValues.end())
                {
                    material.alphaCutoff = static_cast<float>(mat.additionalValues["alphaCutoff"].Factor());
                }
                const auto& ext = mat.extensions.find("KHR_materials_pbrSpecularGlossiness");
                if (ext != mat.extensions.end())
                {
                    std::cout << "KHR_materials_pbrSpecularGlossiness available but not imported!\n";
                    if (ext->second.Has("specularGlossinessTexture"))
                    {
                        auto index = ext->second.Get("specularGlossinessTexture").Get("index");
                        material.extension.specularGlossinessTexture = &group.textures[index.Get<int>()];
                        auto texCoordSet = ext->second.Get("specularGlossinessTexture").Get("texCoord");
                        material.texCoordSets.specularGlossiness = texCoordSet.Get<int>();
                        material.pbrWorkflows.specularGlossiness = true;
                    }
                    if (ext->second.Has("diffuseTexture"))
                    {
                        auto index = ext->second.Get("diffuseTexture").Get("index");
                        material.extension.diffuseTexture = &group.textures[index.Get<int>()];
                    }
                    if (ext->second.Has("diffuseFactor"))
                    {
                        auto factor = ext->second.Get("diffuseFactor");
                        for (uint32_t i = 0; i < factor.ArrayLen(); i++)
                        {
                            auto val = factor.Get(i);
                            material.extension.diffuseFactor[i] = val.IsNumber() ? (float)val.Get<double>() : (float)val.Get<int>();
                        }
                    }
                    if (ext->second.Has("specularFactor"))
                    {
                        auto factor = ext->second.Get("specularFactor");
                        for (uint32_t i = 0; i < factor.ArrayLen(); i++)
                        {
                            auto val = factor.Get(i);
                            material.extension.specularFactor[i] = val.IsNumber() ? (float)val.Get<double>() : (float)val.Get<int>();
                        }
                    }
                }

                group.materials.push_back(material);
            }
        }

        void loadTextures(tinygltf::Model& model, AssetGroup& group)
        {
            for (const tinygltf::Texture& texture : model.textures)
            {
                tinygltf::Image image = model.images[texture.source];

                TextureData data = {};
                data.height = image.height;
                data.width = image.width;
                data.imageSize = data.width * data.height * image.component;
                data.texChannels = image.component;
                data.name = image.name;
                //TODO:Fix memory leak here.
                data.pixels = new unsigned char[data.imageSize];
                std::copy(image.image.begin(), image.image.end(), data.pixels);
                group.textures.push_back(data);

                if (texture.sampler == -1)
                {
                    //No sampler, create a default one possibly?
                    //
                }
                else
                {
                    //Assing texture sampler.
                }
            }

            for (tinygltf::Sampler sampler : model.samplers)
            {

            }
        }

        void loadAnimations(const tinygltf::Model& model, AssetGroup& group)
        {
            for (auto& anim : model.animations)
            {
                NodeAnimation animation;
                animation.name = anim.name;
                if (anim.name.empty())
                {
                    animation.name = std::to_string(group.animations.size());
                }

                for (auto& samp : anim.samplers)
                {
                    AnimationSampler sampler;
                    if (samp.interpolation == "LINEAR")
                    {
                        sampler.interpolationType = AnimationSampler::InterpolationType::LINEAR;
                    }
                    else if (samp.interpolation == "STEP")
                    {
                        sampler.interpolationType = AnimationSampler::InterpolationType::STEP;
                    }
                    else if (samp.interpolation == "CUBICSPLINE")
                    {
                        sampler.interpolationType = AnimationSampler::InterpolationType::CUBICSPLINE;
                    }


                    //Animation sampler input values.
                    {
                        const tinygltf::Accessor& accessor = model.accessors[samp.input];
                        const tinygltf::BufferView& bufferView = model.bufferViews[accessor.bufferView];
                        const tinygltf::Buffer& buffer = model.buffers[bufferView.buffer];
                        assert(accessor.componentType == TINYGLTF_COMPONENT_TYPE_FLOAT);

                        const void* data = &buffer.data[accessor.byteOffset + bufferView.byteOffset];
                        const float* buf = static_cast<const float*>(data);
                        for (size_t index = 0; index < accessor.count; index++)
                        {
                            sampler.inputs.push_back(buf[index]);
                        }

                        for (const float& input : sampler.inputs)
                        {
                            if (input < animation.start)
                            {
                                animation.start = input;
                            }
                            if (input > animation.end)
                            {
                                animation.end = input;
                            }
                        }
                    }

                    //Sampler transform, rotation and scale outputs.
                    {
                        const tinygltf::Accessor& accessor = model.accessors[samp.output];
                        const tinygltf::BufferView& bufferView = model.bufferViews[accessor.bufferView];
                        const tinygltf::Buffer& buffer = model.buffers[bufferView.buffer];
                        assert(accessor.componentType == TINYGLTF_COMPONENT_TYPE_FLOAT);

                        const void* data = &buffer.data[accessor.byteOffset + bufferView.byteOffset];
                        
                        switch (accessor.type)
                        {
                            case TINYGLTF_TYPE_VEC3:
                            {
                                const glm::vec3* buf = static_cast<const glm::vec3*>(data);
                                for (size_t index = 0; index < accessor.count; index++)
                                {
                                    sampler.outputsVec4.push_back(glm::vec4(buf[index], 0.0f));
                                }
                                break;
                            }
                            case TINYGLTF_TYPE_VEC4:
                            {
                                const glm::vec4* buf = static_cast<const glm::vec4*>(data);
                                for (size_t index = 0; index < accessor.count; index++)
                                {
                                    sampler.outputsVec4.push_back(buf[index]);
                                }
                                break;
                            }
                            default:
                            {
                                std::cerr << "Unknown accessor type!\n";
                                break;
                            }
                        }
                    }
                    animation.samplers.push_back(sampler);
                }

                //Channel data.
                for (auto& chan : anim.channels)
                {
                    AnimationChannel channel;

                    if (chan.target_path == "rotation")
                    {
                        channel.path = AnimationChannel::PathType::ROTATION;
                    }
                    if (chan.target_path == "translation")
                    {
                        channel.path = AnimationChannel::PathType::TRANSLATION;
                    }
                    if (chan.target_path == "scale") 
                    {
                        channel.path = AnimationChannel::PathType::SCALE;
                    }
                    if (chan.target_path == "weights")
                    {
                        std::cerr << "Skipping weight targets.\n";
                    }
                    channel.samplerIndex = chan.sampler;
                    channel.targetNodeIndex = chan.target_node;

                    animation.channels.push_back(channel);
                }
                group.animations.push_back(animation);
            }

            //Update scene graph nodes to point to correct animations.
            int animationIndex = 0;
            for (auto& animation : group.animations)
            {
                for (auto& channel : animation.channels)
                {
                    if (channel.targetNodeIndex < group.nodes.size())
                    {
                        auto& node = group.nodes[channel.targetNodeIndex];
                        node.animations.push_back(animationIndex);
                        int test = 0;
                    }
                }
                animationIndex++;
            }
        }

        void loadSkeletons(const tinygltf::Model& model, AssetGroup& group)
        {
            for (const tinygltf::Skin& skin : model.skins)
            {
                SkeletonData newSkeleton = {};
                newSkeleton.name = skin.name;
                if (skin.skeleton > -1)
                {
                    newSkeleton.rootSkeleton = skin.skeleton;
                }

                for(int jointNodeIndex : skin.joints)
                {
                    newSkeleton.jointNodes.push_back(jointNodeIndex);
                    Joint joint;
                    joint.jointNodeIndex = jointNodeIndex;
                    std::string jointName = group.nodes[jointNodeIndex].name;
                    strcpy(joint.jointName, jointName.c_str());
                    joint.jointLocalTransform = group.nodes[jointNodeIndex].transformData.localTransform;
                    newSkeleton.joints.push_back(joint);
                }

                if (skin.inverseBindMatrices > -1)
                {
                    const tinygltf::Accessor& accessor = model.accessors[skin.inverseBindMatrices];
                    const tinygltf::BufferView& bufferView = model.bufferViews[accessor.bufferView];
                    const tinygltf::Buffer& buffer = model.buffers[bufferView.buffer];
                    newSkeleton.inverseBindMatrices.resize(accessor.count);
                    memcpy(newSkeleton.inverseBindMatrices.data(), &buffer.data[accessor.byteOffset + bufferView.byteOffset], accessor.count * sizeof(glm::mat4));
                }
                group.skeletons.push_back(newSkeleton);
            }
        }
    }
}