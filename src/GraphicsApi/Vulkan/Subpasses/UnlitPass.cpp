#include <GraphicsApi/Vulkan/VulkanUtility/UtilityFunctions.h>
#include <GraphicsApi/Vulkan/VulkanUtility/StructInitializer.h>
#include <Utility/DelegateManager.h>
#include <GraphicsApi/Vulkan/VulkanUtility/DescriptorUtility.h>
#include "GraphicsApi/Vulkan/Subpasses/UnlitPass.h"

namespace Raven
{
    void UnlitPass::initializeVertexInputState(VkPipelineVertexInputStateCreateInfo& vertexInputState)
    {
        m_vertexInputBindings = {Vertex::getBindingDescription()};
        m_vertexInputAttributes = {};

        auto attributes = Vertex::getAttributeDescriptions();
        for(auto& attribute : attributes)
        {
            m_vertexInputAttributes.push_back(attribute);
        }
        vertexInputState = StructInitializer::getPipelineVertexInputStateCreateInfo(m_vertexInputBindings, m_vertexInputAttributes);
    }

    void UnlitPass::initializeInputAssemblyState(VkPipelineInputAssemblyStateCreateInfo &inputAssembly)
    {
        inputAssembly = StructInitializer::getPipelineInputAssemblyStateCreateInfo(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST, VK_FALSE);
    }

    void UnlitPass::initializeViewportState(VkPipelineViewportStateCreateInfo &viewportState)
    {
        const WindowDelegates& delegates = DelegateManager::getInstance().getWindowDelegates();

        uint32_t windowWidth = delegates.getWindowWidthFunc.Invoke();
        uint32_t windowHeight = delegates.getWindowHeightFunc.Invoke();
        VkExtent2D extent = {windowWidth, windowHeight};

        //Viewport
        VkViewport viewport = {};
        viewport.x = 0.0f;
        viewport.y = 0.0f;
        viewport.width = static_cast<float>(extent.width);
        viewport.height = static_cast<float>(extent.height);
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;
        m_viewports = {viewport};

        //Scissor
        VkRect2D scissor = {};
        scissor.offset = {0,0};
        scissor.extent = extent;
        m_scissors = {scissor};

        //Viewport state
        viewportState = StructInitializer::getPipelineViewportStateCreateInfo(m_viewports, m_scissors);
    }

    void UnlitPass::initializeRasterizerState(VkPipelineRasterizationStateCreateInfo &rasterizer)
    {
        rasterizer = StructInitializer::getPipelineRasterizationStateCreateInfo(VK_FALSE, VK_FALSE, VK_POLYGON_MODE_FILL,
                                                                      1.0f, VK_CULL_MODE_BACK_BIT, VK_FRONT_FACE_COUNTER_CLOCKWISE,
                                                                                VK_FALSE, 0.0f, 0.0f, 0.0f);
    }

    void UnlitPass::initializeMultisampleState(VkPipelineMultisampleStateCreateInfo &multisampling)
    {
        multisampling = StructInitializer::getPipelineMultisampleStateCreateInfo(VK_FALSE, VK_SAMPLE_COUNT_1_BIT, 1.0f, nullptr, VK_FALSE, VK_FALSE);
    }

    void UnlitPass::initializeDepthStencilState(VkPipelineDepthStencilStateCreateInfo &depthStencil)
    {
        depthStencil = StructInitializer::getPipelineDepthStencilStateCreateInfo(VK_TRUE, VK_TRUE, VK_COMPARE_OP_LESS,
                                                                                 VK_FALSE, 0.0f, 1.0f,
                                                                                 VK_FALSE, {}, {});
    }

    void UnlitPass::initializeColorBlendState(VkPipelineColorBlendStateCreateInfo &colorBlend)
    {
        VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
        colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
        colorBlendAttachment.blendEnable = VK_FALSE;
        m_colorBlendAttachments = {colorBlendAttachment};

        colorBlend = StructInitializer::getPipelineColorBlendStateCreateInfo(VK_FALSE, VK_LOGIC_OP_COPY, m_colorBlendAttachments);
    }

    void UnlitPass::initializeDynamicState(VkPipelineDynamicStateCreateInfo &dynamicStateCreateInfo)
    {
        m_dynamicStates = {VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_LINE_WIDTH};
        dynamicStateCreateInfo = StructInitializer::getPipelineDynamicStateCreateInfo(m_dynamicStates);
    }

    VkResult UnlitPass::initializePipelineLayout(const VkDevice &device)
    {
        std::vector<ShaderLoadInformation> shadersToLoad = { {"../shaders/basic.vert", VK_SHADER_STAGE_VERTEX_BIT},
                                                             {"../shaders/basic.frag", VK_SHADER_STAGE_FRAGMENT_BIT} };

        describePipeline(device, shadersToLoad);

        return VK_SUCCESS;
    }

    void UnlitPass::draw(const RavenBuffer &vertexBuffer, const RavenBuffer &indexBuffer, const std::vector<VkDescriptorSet>& descriptorSets, 
                         const std::vector<PushConstantData*>& pushConstants, VkCommandBuffer& cmdBuffer)
    {
        vkCmdBindDescriptorSets(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, m_renderpassPipeline.layout, 0, static_cast<uint32_t>(descriptorSets.size()), descriptorSets.data(), 0, nullptr);
        vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, m_renderpassPipeline.pipeline);

        VkBuffer vertexBuffers[] = {vertexBuffer.buffer};
        VkDeviceSize offsets[] = {0};
        vkCmdBindVertexBuffers(cmdBuffer, 0, 1, vertexBuffers, offsets);
        vkCmdBindIndexBuffer(cmdBuffer, indexBuffer.buffer, 0, VK_INDEX_TYPE_UINT32);
        vkCmdDrawIndexed(cmdBuffer, indexBuffer.count, 1, 0, 0, 0);
    }
}