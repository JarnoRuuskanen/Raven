#include <Utility/FileIO.h>
#include "Utility/VertexUtility/TerrainUtility.h"

namespace Raven
{
    namespace TerrainUtility
    {
        float getHeightFromColor(const float minMaxHeight, uint32_t pixel)
        {
            auto height = static_cast<float>(pixel);
            //To make the value fall between -minMaxHeight and minMaxHeight, do the following.
            height += MAX_PIXEL_COLOR / 2.0f;
            height /= MAX_PIXEL_COLOR / 2.0f; //Value should be between -1 and 1
            height *= minMaxHeight; //Times max height will give the value between min/max height.
            return height;
        }

        uint32_t pixelToBinary(uint32_t channels, unsigned char r, unsigned char g, unsigned char b, unsigned char a)
        {
            //Reference: https://stackoverflow.com/questions/55815235/c-get-binary-value-of-a-buffer
            //Need to turn rgb into binary in order to get the correct height.
            //00000000 00000000 00000000
            uint32_t inBinary = 0;
            if(channels >= 4)
            {
                inBinary = (r << 24) | (g << 16) | (b << 8) | a;
            }
            else
            {
                inBinary = (r << 16) | (g << 8) | (b);
            }
            return inBinary;
        }

        void getIndices(size_t sideSize, std::vector<Vertex>& vertices, std::vector<uint32_t>& indices)
        {
            //Build the index data:
            //This implementation requires pipeline to use vertex strips.
            //It also uses triangle degeneration (see the link).
            uint32_t numStripsRequired = sideSize - 1;
            uint32_t numDegensRequired = 2 * (numStripsRequired - 1);
            uint32_t verticesPerStrip = 2 * sideSize;

            indices.resize(verticesPerStrip * numStripsRequired + numDegensRequired);
            uint32_t offset = 0;
            for(uint32_t z = 0; z < sideSize - 1; z++)
            {
                if(z > 0)
                {
                    //Degenerate begins, repeating first vertex.
                    indices[offset++] = z * sideSize;
                }

                for(uint32_t x = 0; x < sideSize; x++)
                {
                    //One part of the strip.
                    indices[offset++] = (z * sideSize) + x;
                    indices[offset++] = ((z + 1)*sideSize) + x;
                }
                if(z < sideSize - 2)
                {
                    //Degenerate ends, repeating last vertex.
                    indices[offset++] = ((z + 1) * sideSize)+(sideSize - 1);
                }
            }
        }

        //Just a helper function.
        bool valueIsInBounds(size_t minVal, size_t maxVal, size_t value)
        {
            return value >= minVal && value <= maxVal;
        }

        void getVertices(const TextureData& textureData, float maxHeight, glm::vec3 color, std::vector<Vertex>& vertices)
        {
            //First get all the pixel colors of the 2D grid.
            uint32_t channelCount = textureData.texChannels;
            for (uint32_t z = 0; z < textureData.height; z++) {
                for (uint32_t x = 0; x < textureData.width; x++) {
                    uint32_t index = x + textureData.width * z;
                    unsigned char *pixel = textureData.pixels + (index) * textureData.texChannels;
                    unsigned char r = pixel[0];
                    unsigned char g = pixel[1];
                    unsigned char b = pixel[2];
                    unsigned char a = channelCount >= 4 ? pixel[3] : 0.0f;
                    uint32_t pixelInBinary = pixelToBinary(channelCount, r,g,b,a);

                    Vertex vert;
                    float y = getHeightFromColor(maxHeight, pixelInBinary);
                    vert.pos = glm::vec4(x, y, z, 1.0f);
                    vert.normal = glm::vec3(0, 1, 0);
                    vert.color = color;
                    vert.texCoord1 = glm::vec2(0);
                    vertices.push_back(vert);
                }
            }
        }

        void getVertices(size_t widthHeight, const std::vector<float> noiseData, float scalingFactor, glm::vec3 color, std::vector<Vertex>& vertices)
        {
            //First get all the pixel colors of the 2D grid.
            uint32_t channelCount = 1;
            for (uint32_t z = 0; z < widthHeight; z++) 
            {
                for (uint32_t x = 0; x < widthHeight; x++) 
                {
                    uint32_t index = x + widthHeight * z;
                    float pixel = noiseData[index];
                    float y = pixel * scalingFactor;

                    Vertex vert;
                    vert.pos = glm::vec4(x, y, z, 1.0f);
                    vert.normal = glm::vec3(0, 1, 0);
                    vert.color = color;
                    vert.texCoord1 = glm::vec2(0);
                    vertices.push_back(vert);
                }
            }
        }

        //Must be called after getVertices. Do not expose outside.
        void getNormals(size_t width, size_t height, size_t maxVal, std::vector<Vertex> &vertices)
        {
            assert(!vertices.empty());

            int leftIndex = 0;
            int rightIndex = 0;
            int downIndex = 0;
            int upIndex = 0;
            //Calculate all the normals now that the vertices with heights have been calculated.
            for (uint32_t xOffset = 0; xOffset < width; xOffset++)
            {
                for (uint32_t zOffset = 0; zOffset < height; zOffset++)
                {
                    leftIndex = (xOffset - 1) * zOffset;
                    rightIndex = (xOffset + 1) * zOffset;
                    downIndex = xOffset * (zOffset - 1);
                    upIndex = xOffset * (zOffset + 1);

                    //Get the height of all the neighboring vertices and calculate the normal
                    float leftHeight = valueIsInBounds(0, maxVal, leftIndex) ? vertices[leftIndex].pos.y : 0;
                    float rightHeight = valueIsInBounds(0, maxVal, rightIndex) ? vertices[rightIndex].pos.y : 0;
                    float downHeight = valueIsInBounds(0, maxVal, downIndex) ? vertices[downIndex].pos.y : 0;
                    float upHeight = valueIsInBounds(0, maxVal, upIndex) ? vertices[upIndex].pos.y : 0;

                    //Set the normal.
                    glm::vec3 normalVec = glm::vec3((leftHeight - rightHeight), 2.0f, (downHeight - upHeight));
                    auto normalized = glm::normalize(normalVec);
                    vertices[xOffset*zOffset].normal = normalized;
                }
            }
        }

        void generatePlaneFromHeightMap(const TextureData& textureData, float maxHeight, glm::vec3 color, std::vector<Vertex>& vertices, std::vector<uint32_t>& indices) {
            //Clear any previous vertices.
            vertices.clear();
            vertices.shrink_to_fit();

            getVertices(textureData, maxHeight, color, vertices);
            getIndices(textureData.height, vertices, indices);
            //Must be called after getVertices.
            size_t maxVal = textureData.width * textureData.height - 1;
            getNormals(textureData.width, textureData.height, maxVal, vertices);
        }

        //Reference: www.learnopengles.com/android-lesson-eight-an-introduction-to-index-buffer-objects-ibos/
        void generatePlane(uint32_t sideSize, glm::vec3 color, std::vector<Vertex>& vertices, std::vector<uint32_t>& indices)
        {
            if(sideSize <= 0)
            {
                assert(!"Side size too small!");
                return;
            }

            vertices.clear();
            vertices.shrink_to_fit();

            //Build the data:
            for(size_t z = 0; z < sideSize; z++)
            {
                for(size_t x = 0; x < sideSize; x++)
                {
                    //Building heightmap from top down so that the triangles are in counter-clockwise order.
                    Vertex vert;
                    vert.pos = glm::vec4{x, 0.0f, z, 1.0f};
                    vert.normal = glm::vec3{0.0f,1.0f,0.0f};
                    vert.color = color;
                    vert.texCoord1 = glm::vec2(0.0);
                    vertices.push_back(vert);
                }
            }

            //Build the index data:
            //This implementation requires pipeline to use vertex strips.
            //It also uses triangle degeneration (see the link).
            uint32_t numStripsRequired = sideSize - 1;
            uint32_t numDegensRequired = 2 * (numStripsRequired - 1);
            uint32_t verticesPerStrip = 2 * sideSize;

            indices.resize(verticesPerStrip * numStripsRequired + numDegensRequired);
            uint32_t offset = 0;
            for(uint32_t z = 0; z < sideSize - 1; z++)
            {
                if(z > 0)
                {
                    //Degenerate begins, repeating first vertex.
                    indices[offset++] = z * sideSize;
                }

                for(uint32_t x = 0; x < sideSize; x++)
                {
                    //One part of the strip.
                    indices[offset++] = (z * sideSize) + x;
                    indices[offset++] = ((z + 1)*sideSize) + x;
                }
                if(z < sideSize - 2)
                {
                    //Degenerate ends, repeating last vertex.
                    indices[offset++] = ((z + 1) * sideSize)+(sideSize - 1);
                }
            }
        }

        void generatePlaneFromNoise(size_t widthHeight, float maxHeight, const std::vector<float>& noiseData, std::vector<Vertex>& vertices, std::vector<uint32_t>& indices)
        {
            //Clear any previous vertices.
            vertices.clear();
            vertices.shrink_to_fit();

            getVertices(widthHeight, noiseData, maxHeight, glm::vec3(1.0f, 0,0) , vertices);
            getIndices(widthHeight, vertices, indices);
            //Must be called after getVertices.
            size_t maxVal = widthHeight * widthHeight - 1;
            getNormals(widthHeight, widthHeight, maxVal, vertices);
        }
    }
}