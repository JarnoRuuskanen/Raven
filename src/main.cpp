#include "AssetLoader/AssetGroup.h"
#include "RavenApplication.h"
#include "AssetLoader/AssetLoader.h"
using namespace Raven;

int main()
{
    /*AssetGroup group;
    AssetLoader::fillAssetGroup("../assets/models/DamagedHelmet.glb", group);
    AssetGroup cube;
    AssetLoader::fillAssetGroup("../assets/models/cube.glb",cube);*/
    AssetGroup asset;
    AssetLoader::fillAssetGroup("../assets/models/cube.glb", asset);
    //AssetGroup lightBall;
    //AssetLoader::fillAssetGroup("../assets/models/lightBall.glb", lightBall);

    std::vector<AssetGroup> assets = { asset };
    uint32_t windowWidth = 1920;
    uint32_t windowHeight = 1080;

    RavenApplication raven;

    if (!raven.start(assets, windowWidth, windowHeight))
    {
        std::cerr << "Failed to start Raven" << std::endl;
    }
    return 0;
}