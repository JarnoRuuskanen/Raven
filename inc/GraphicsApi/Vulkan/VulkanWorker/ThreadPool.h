#ifndef THREAD_POOL_H_INCLUDED
#define THREAD_POOL_H_INCLUDED

#include <cstddef>
#ifdef unix
#include <pthread.h>
#endif

#include <mutex>
#include <condition_variable>
#include <deque>
#include <future>
#include <vector>
#include <iostream>

//References:
//https://riptutorial.com/cplusplus/example/15806/create-a-simple-thread-pool
//https://en.cppreference.com/w/
class ThreadPool
{
public:
    ThreadPool(size_t threadCount = std::thread::hardware_concurrency() - 1) : m_threadCount(threadCount)
    {
        start();
    }
    ~ThreadPool()
    {
        shutdown();
    }

    template<typename T, typename Result=std::result_of_t<T&()>>
    std::future<Result> queueTask(T&& func)
    {
        std::packaged_task<Result()> job(std::forward<T>(func));

        auto result = job.get_future();
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            m_taskQueue.emplace_back(std::move(job));
        }
        //Start the m_taskQueue by notifying a thread.
        m_condition.notify_one();
        return result;
    }

    void abort()
    {
        cancelPending();
        shutdown();
    }

    void cancelPending()
    {
        //Takes a hold of the lock and removes all un started tasks.
        std::unique_lock<std::mutex>(m_mutex);
        m_taskQueue.clear();
    }

    inline size_t getThreadCount() const { return m_threadCount; }

private:
    size_t m_threadCount;

    void start()
    {
        for(std::size_t i = 0; i < m_threadCount; i++)
        {
            //Launch thread count of async workers that will poll for tasks.
            m_workerPool.push_back(std::async(std::launch::async, [this]{ startWorker();}));
        }
    }

    void shutdown()
    {
        {
            std::unique_lock<std::mutex>(m_mutex);
            for(auto&& worker : m_workerPool)
            {
                //Sends an invalid future to the thread which will
                //force the thread to return;
                m_taskQueue.push_back({});
            }
        }
        m_condition.notify_all();
        m_workerPool.clear();
    }

    void startWorker()
    {
        while(true)
        {
            std::packaged_task<void()> func;
            {
                std::unique_lock<std::mutex> lock(m_mutex);
                if(m_taskQueue.empty())
                {
                    //Waits until the task queue is no longer empty.
                    m_condition.wait(lock, [&]{return !m_taskQueue.empty();});
                }
                func = std::move(m_taskQueue.front());
                m_taskQueue.pop_front();
            }
            //Check if abort has been called.
            if(!func.valid())
            {
                return;
            }
            func();
        }
    }

    std::mutex m_mutex;
    std::condition_variable m_condition;
    //Container for all the tasks that needs doing.
    //https://en.cppreference.com/w/cpp/thread/packaged_task
    std::deque<std::packaged_task<void()>> m_taskQueue;
    //Threads that will pop the task queue and return result futures.
    std::vector<std::future<void>> m_workerPool;
};

#endif