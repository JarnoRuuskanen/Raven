#ifndef RAVEN_IMAGE_H_INCLUDED
#define RAVEN_IMAGE_H_INCLUDED

#include "GraphicsApi/Vulkan/Common.h"
#include "GraphicsApi/Vulkan/ResourceManager/vk_mem_alloc.h"

namespace Raven
{
    struct RavenImage
    {
        VkImage image;
        VkImageView imageView;
        VkSampler sampler;
        VmaAllocation memoryAllocation;
        size_t width;
        size_t height;
        size_t mipLevels;
    };
}
#endif