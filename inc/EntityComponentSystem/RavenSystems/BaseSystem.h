#ifndef RAVEN_BASE_SYSTEM_H_INCLUDED
#define RAVEN_BASE_SYSTEM_H_INCLUDED
#include "EntityComponentSystem/ECSTypes.h"
#include "Utility/Timer.h"

namespace Raven
{
    class EntityManager;
    class Entity;

    //Baseclass for all systems.
    class BaseSystem
    {
    protected:
        EntityManager* m_pGameWorld;
    public:
        explicit BaseSystem(EntityManager* world) : m_pGameWorld(world){}
        virtual ~BaseSystem() = default; //Because it is a polymorphic baseclass.

        //Each system needs to know when an entity is created.
        virtual void onEntityCreated(Entity* entity) = 0;
        virtual void onEntityDestroyed(EntityId id) = 0;
        virtual void update(const Timer& timer) = 0;
    };
}
#endif