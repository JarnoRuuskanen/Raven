#ifndef RAVEN_POOL_ALLOCATOR_H_INCLUDED
#define RAVEN_POOL_ALLOCATOR_H_INCLUDED
#include "Allocator.h"
#include "StackLinkedList.h"

namespace Raven
{
    class PoolAllocator : public Allocator
    {
    public:
        PoolAllocator(const std::size_t totalSize, const std::size_t chunkSize);
        virtual ~PoolAllocator();
        virtual void* allocate(const std::size_t size, const std::size_t alignment = 0) override;
        virtual void free(void* ptr) override;
        virtual void init() override;
        virtual void reset();
    private:
        PoolAllocator(PoolAllocator& poolAllocator);

        struct FreeHeader{};
        using Node = StackLinkedList<FreeHeader>::Node;
        StackLinkedList<FreeHeader> m_freeList;
        void* m_startPtr = nullptr;
        std::size_t m_chunkSize;

    };
}

#endif
