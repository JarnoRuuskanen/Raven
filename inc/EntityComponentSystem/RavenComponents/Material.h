#ifndef RAVEN_MATERIAL_H_INCLUDED
#define RAVEN_MATERIAL_H_INCLUDED

#include <Utility/StringHash.h>
#include <GraphicsApi/Vulkan/ResourceManager/VulkanResourceManager.h>
#include "Component.h"

namespace Raven
{
    enum class MaterialDefinition
    {
        ALBEDO = 0,
        NORMAL = 1,
        ROUGHNESS = 2,
        OCCLUSION = 3, 
        EMISSIVE = 4
    };

    class Material : public Component
    {
    public:
        static constexpr ComponentId ID = RavenUtility::generateHash("Material");
        Material() :m_images(),m_hasBaseColor(false), m_hasMetallicRoughness(false), m_hasNormal(false), m_hasOcclusion(false), m_hasEmissive(false), m_serialization(0) {}
        virtual bool init(void* data) override final
        {
            m_hasBaseColor = false;
            m_hasMetallicRoughness = false;
            m_hasNormal = false;
            m_hasOcclusion = false;
            m_hasEmissive = false;
            return true;
        }
        std::array<RavenImage,5> m_images;
        bool m_hasBaseColor;
        bool m_hasMetallicRoughness;
        bool m_hasNormal;
        bool m_hasOcclusion;
        bool m_hasEmissive;
        uint32_t m_serialization;
    };
}
#endif