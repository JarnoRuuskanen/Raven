#ifndef RAVEN_INITIALIZER_H_INCLUDED
#define RAVEN_INITIALIZER_H_INCLUDED

#include "GraphicsApi/Vulkan/Common.h"
#include "GraphicsApi/Vulkan/VulkanUtility/UtilityFunctions.h"
#include "GraphicsApi/Vulkan/VulkanUtility/VulkanStructs.h"
#include "GraphicsApi/Vulkan/VulkanObjects/GraphicsPipeline.h"
#include "Window/ApplicationWindow.h"
#include "AssetLoader/AssetGroup.h"
#include "GraphicsApi/Vulkan/ResourceManager/VulkanResourceManager.h"
#include "EntityComponentSystem/EntityManager.h"
#include "GraphicsApi/Vulkan/RavenVulkanManager.h"
#include "Utility/DelegateManager.h"
#include "EntityComponentSystem/SceneCreator.h"

#include <chrono>
#include <iostream>
#include "EntityComponentSystem/RayCaster.h"

namespace Raven
{
    class RavenVulkanManager;
    class RavenApplication
    {
    public:
        RavenApplication();
        ~RavenApplication();
        bool start(const std::vector<AssetGroup>& assets, uint32_t windowWidth, uint32_t windowHeight);
    private:
        bool createWindow(uint32_t width, uint32_t height);
        bool closeWindow(const KeyEvent& event);
        void initializeInputControls();
        bool mouseKeyCallback(const MouseKeyEvent& event);
        void mainLoop();

        ApplicationWindow m_appWindow;
        GLFWwindow* m_window = nullptr;
        EntityManager m_entityManager;
        SceneCreator m_sceneCreator;

        RayCaster* m_mousePicker;
        int m_windowWidth, m_windowHeight;
    };
}

#endif