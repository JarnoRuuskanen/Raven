#ifndef RAVEN_SHADER_PROCESSOR_H_INCLUDED
#define RAVEN_SHADER_PROCESSOR_H_INCLUDED

//Reference: https://forestsharp.com/glslang-cpp/

#include <glslang/Public/ShaderLang.h>
#include <SPIRV/GlslangToSpv.h>
#include <StandAlone/DirStackFileIncluder.h>

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

namespace Raven
{
    namespace ShaderUtility
    {
        void compileGLSLToSpirv(const std::string& filename, std::vector<uint32_t>& spirv);
    }
}

#endif