#ifndef RAVEN_FILE_IO_H_INCLUDED
#define RAVEN_FILE_IO_H_INCLUDED

#include <string>
#include <vector>

namespace Raven
{
    namespace FileIO
    {
        std::vector<char> readBinaryFile(std::string filename);
    }
}

#endif