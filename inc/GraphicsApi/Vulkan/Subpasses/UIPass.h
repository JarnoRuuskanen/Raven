#include "BaseSubpass.h"

namespace Raven
{
    class UIPass : public BaseSubpass
    {
    public:
        UIPass() : BaseSubpass("uiPass") {}

        void draw(const RavenBuffer& vertexBuffer, const RavenBuffer& indexBuffer, const std::vector<VkDescriptorSet>& descriptorSets, 
                  const std::vector<PushConstantData*>& pushConstants, VkCommandBuffer& cmdBuffer) override;
        void createSubpassDescription(const std::vector<VkAttachmentReference>& inputReferences, const std::vector<VkAttachmentReference>& colorReferences,
                                      const std::vector<VkAttachmentReference>& depthReferences, const std::vector<VkAttachmentDescription>& attachments,
                                      const std::vector<uint32_t>& preservedAttachmentIndices) override;
    private:
        void initializeInputAssemblyState(VkPipelineInputAssemblyStateCreateInfo& inputAssembly) override;
        void initializeVertexInputState(VkPipelineVertexInputStateCreateInfo& vertexInputState) override;
        void initializeViewportState(VkPipelineViewportStateCreateInfo& viewportState) override;
        void initializeRasterizerState(VkPipelineRasterizationStateCreateInfo& rasterizer) override;
        void initializeMultisampleState(VkPipelineMultisampleStateCreateInfo& multisampling) override;
        void initializeDepthStencilState(VkPipelineDepthStencilStateCreateInfo& depthStencil) override;
        void initializeColorBlendState(VkPipelineColorBlendStateCreateInfo& colorBlend) override;
        void initializeDynamicState(VkPipelineDynamicStateCreateInfo& dynamicStateCreateInfo) override;
        VkResult initializePipelineLayout(const VkDevice& device) override;

        void initFontTexture(const VkDevice& device);
        VkResult createDescriptorSets(const VkDevice& device);
        TextureData m_textureData;

        //UI will be using its own descriptor sets
        VkDescriptorSetLayout m_descriptorSetLayout;
        std::vector<VkDescriptorSet> m_descriptorSets;
        VkDescriptorPool m_descriptorPool;

        RavenImage m_fontImage;

        struct UIPushConstantBlock
        {
            glm::vec2 scale;
            glm::vec2 translate;
        };
        UIPushConstantBlock m_pushConstBlock;
    };
}