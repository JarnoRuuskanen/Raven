#include "GraphicsApi/Vulkan/VulkanUtility/DescriptorUtility.h"
#include <iostream>

namespace Raven
{
    namespace DescriptorUtility
    {
        VkResult createDescriptorSetLayout(const VkDevice& device, const std::vector<VkDescriptorSetLayoutBinding>& bindings, VkDescriptorSetLayout& layout)
        {
            VkDescriptorSetLayoutCreateInfo layoutInfo = {};
            layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
            layoutInfo.bindingCount = static_cast<uint32_t>(bindings.size());
            layoutInfo.pBindings = bindings.data();

            VkResult result = vkCreateDescriptorSetLayout(device, &layoutInfo, nullptr, &layout);
            CHECK_RESULT_FOR_ERRORS(result, "Failed to create a descriptor set layout!");
            return VK_SUCCESS;
        }

        VkResult createDescriptorPool(const VkDevice& device, const std::vector<VkDescriptorPoolSize>& poolSizes,
                                      uint32_t maxSets, bool releasable, VkDescriptorPool& pool)
        {
            VkDescriptorPoolCreateInfo poolInfo = {};
            poolInfo.flags = releasable ? VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT : 0;
            poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
            poolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
            poolInfo.pPoolSizes = poolSizes.data();
            poolInfo.maxSets = maxSets;

            VkResult result = vkCreateDescriptorPool(device, &poolInfo, nullptr, &pool);
            CHECK_RESULT_FOR_ERRORS(result, "Failed to create a descriptor pool!");
            return VK_SUCCESS;
        }

        VkResult allocateDescriptorSets(const VkDevice& device, const VkDescriptorPool& pool,
                                    const std::vector<VkDescriptorSetLayout>& layouts,
                                    uint32_t allocationCount, std::vector<VkDescriptorSet>& descriptorSets)
        {
            VkDescriptorSetAllocateInfo allocInfo = {};
            allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
            allocInfo.descriptorPool = pool;
            allocInfo.descriptorSetCount = allocationCount;
            allocInfo.pSetLayouts = layouts.data();
            descriptorSets.resize(allocationCount);

            VkResult result = vkAllocateDescriptorSets(device, &allocInfo, descriptorSets.data());
            CHECK_RESULT_FOR_ERRORS(result, "Failed to allocate descriptor sets!");
            return VK_SUCCESS;
        }

        VkResult allocateDescriptorSet(const VkDevice& device, const VkDescriptorPool& pool,
                                       const std::vector<VkDescriptorSetLayout>& layouts, VkDescriptorSet& descriptorSet)
        {
            VkDescriptorSetAllocateInfo allocInfo = {};
            allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
            allocInfo.descriptorPool = pool;
            allocInfo.descriptorSetCount = 1;
            allocInfo.pSetLayouts = layouts.data();

            VkResult result = vkAllocateDescriptorSets(device, &allocInfo, &descriptorSet);
            CHECK_RESULT_FOR_ERRORS(result, "Failed to allocate a descriptor set!");
            return VK_SUCCESS;
        }
    }
}