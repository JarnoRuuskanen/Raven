#ifndef RAVEN_RENDERABLE_COMPONENT_H_INCLUDED
#define RAVEN_RENDERABLE_COMPONENT_H_INCLUDED

#include "EntityComponentSystem/RavenComponents/Component.h"
#include "EntityComponentSystem/ECSTypes.h"
#include "Utility/StringHash.h"
#include "EntityComponentSystem/RavenComponents/Mesh.h"
#include "EntityComponentSystem/RavenComponents/Material.h"

namespace Raven
{
    class Renderable : public Component
    {
    public:
        static constexpr ComponentId ID = RavenUtility::generateHash("Renderable");
        Renderable() : m_isVisible(true), m_material(nullptr), m_mesh(nullptr){}
        virtual bool init(void* data) override final
        {
            return true;
        }
        Mesh* m_mesh;
        Material* m_material;
        RavenBuffer m_uniformBuffer;
        bool m_isVisible;
    };
}
#endif