#include <iostream>
#include <GraphicsApi/Vulkan/VulkanUtility/UtilityFunctions.h>
#include "GraphicsApi/Vulkan/VulkanUtility/ImageUtility.h"
#include "GraphicsApi/Vulkan/VulkanUtility/CommandBufferUtility.h"

namespace Raven
{
    namespace ImageUtility
    {
        VkSampler createDefaultLinearSampler(const VkDevice& device)
        {
            VkSamplerCreateInfo createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
            createInfo.magFilter = VK_FILTER_LINEAR;
            createInfo.minFilter = VK_FILTER_LINEAR;
            createInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
            createInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
            createInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
            createInfo.anisotropyEnable = VK_TRUE; //Enable anisotropic filtering?
            createInfo.maxAnisotropy = 16; //Max amount, difference beyond this is negligible.
            createInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
            createInfo.unnormalizedCoordinates = VK_FALSE;
            createInfo.compareEnable = VK_FALSE; //Used for percentage-closer filtering.
            createInfo.compareOp = VK_COMPARE_OP_ALWAYS;
            //Mipmapping values.
            createInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
            createInfo.mipLodBias = 0.0f;
            createInfo.minLod = 0.0f;
            createInfo.maxLod = 0.0f;

            //NOTE: Sampler is not bound to any one texture but is instead an interface to extract colors from a texture.
            //It can be applied to any image you want.
            VkSampler sampler;
            VkResult result = vkCreateSampler(device, &createInfo, nullptr, &sampler);

            assert(result == VK_SUCCESS);
            return sampler;
        }

        bool hasStencilComponent(VkFormat format)
        {
            return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
        }

        VkImageSubresourceRange getDefaultImageSubresourceRange(VkFormat format, VkImageLayout newLayout)
        {
            VkImageSubresourceRange range;
            if (newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
            {
                range.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;

                if (hasStencilComponent(format))
                {
                    range.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
                }
            }
            else
            {
                range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            }

            range.baseMipLevel = 0;
            range.levelCount = 1;
            range.baseArrayLayer = 0;
            range.layerCount = 1;

            return range;
        }

        bool createMemoryBackedImage(const VkDevice& device, const VkPhysicalDevice& physicalDevice, const VkImageCreateInfo& createInfo,
                                     VkMemoryPropertyFlags properties, VkDeviceMemory& imageMemory, VkImage& image)
        {
            if(vkCreateImage(device, &createInfo, nullptr, &image) != VK_SUCCESS)
            {
                std::cerr << "Failed to create an image!\n";
                return false;
            }

            VkMemoryRequirements memReq;
            vkGetImageMemoryRequirements(device, image, &memReq);

            VkMemoryAllocateInfo allocInfo = {};
            allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
            allocInfo.allocationSize = memReq.size;
            allocInfo.memoryTypeIndex = static_cast<uint32_t>(UtilityFunctions::findMemoryType(physicalDevice, memReq.memoryTypeBits, properties));
            assert(allocInfo.memoryTypeIndex != (uint32_t)(-1));//This flips -1 to max value of uint32_t which would be an error case.
            if(vkAllocateMemory(device, &allocInfo, nullptr, &imageMemory) != VK_SUCCESS)
            {
                std::cerr << "Failed to allocate memory for an image!\n";
                return false;
            }

            vkBindImageMemory(device, image, imageMemory, 0);

            return true;
        }

        bool createImageView(const VkDevice& device, VkImageViewCreateInfo& createInfo, VkImageView& imageView)
        {
            VkResult res = vkCreateImageView(device, &createInfo, nullptr, &imageView);
            if(res != VK_SUCCESS)
            {
                std::cerr << "Failed to create an image view!\n";
                return false;
            }
            return true;
        }

        bool transitionImageLayout(const VkDevice& device, const VkQueue& queue, VkImageLayout oldLayout,
                                   VkImageLayout newLayout, VkImageSubresourceRange& subresourceRange,VkImage& image)
        {
            VkCommandPool pool;
            CommandBufferUtility::createCommandPool(device, 0, false, pool);

            VkCommandBuffer commandBuffer = CommandBufferUtility::beginSingleTimeCommands(pool, device);

            VkImageMemoryBarrier barrier = {};
            barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
            barrier.oldLayout = oldLayout;
            barrier.newLayout = newLayout;
            barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            barrier.image = image;
            barrier.subresourceRange = subresourceRange;

            VkPipelineStageFlags sourceStage;
            VkPipelineStageFlags destinationStage;

            if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
            {
                barrier.srcAccessMask = 0;
                barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

                sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
                destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
            }
            else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
            {
                barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
                barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

                sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
                destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
            }
            else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
            {
                barrier.srcAccessMask = 0;
                barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

                sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
                destinationStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
            }
            else
            {
                std::cerr << "Layout transition not supported" << std::endl;
                return false;
            }

            vkCmdPipelineBarrier(commandBuffer, sourceStage, destinationStage, 0, 0, nullptr, 0, nullptr, 1, &barrier);

            CommandBufferUtility::endSingleTimeCommands(commandBuffer, device, queue, pool);

            CommandBufferUtility::destroyCommandPool(device, pool);

            return pool == VK_NULL_HANDLE;
        }
    }
}
