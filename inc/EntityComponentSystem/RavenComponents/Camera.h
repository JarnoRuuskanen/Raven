#ifndef RAVEN_CAMERA_COMPONENT_H_INCLUDED
#define RAVEN_CAMERA_COMPONENT_H_INCLUDED

#include <Utility/StringHash.h>
#include "Component.h"

namespace Raven
{
    class Camera : public Component
    {
    public:
        static constexpr ComponentId ID = RavenUtility::generateHash("Camera");
        Camera() : m_viewMatrix(glm::mat4(1.0f)), m_cameraUp(glm::vec3(0.0f, 0.0f, 1.0f)), m_cameraFront(glm::vec3{1.0f}){}
        virtual bool init(void* data) override final
        {
            return true;
        }
        glm::mat4 m_viewMatrix;
        glm::vec3 m_cameraUp;
        glm::vec3 m_cameraFront;
    };
}

#endif