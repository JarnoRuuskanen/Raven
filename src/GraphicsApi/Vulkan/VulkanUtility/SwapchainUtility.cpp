#include <GraphicsApi/Vulkan/VulkanUtility/VulkanInitialization.h>
#include <GraphicsApi/Vulkan/VulkanUtility/UtilityFunctions.h>
#include "GraphicsApi/Vulkan/VulkanUtility/SwapchainUtility.h"

namespace Raven
{
    namespace SwapchainUtility
    {
        //A few forward declarations.
        VkExtent2D chooseSwapChainExtent(GLFWwindow* appWindow, const VkSurfaceCapabilitiesKHR& capabilities);
        VkSurfaceFormatKHR chooseSwapChainSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats);

        bool getDefaultSwapchain(const VkPhysicalDevice& physicalDevice, const VkDevice& device,
                                 const VkSurfaceKHR& surface, GLFWwindow* appWindow, Swapchain& swapchain)
        {
            SwapchainSupportInfo swapchainInfo = VulkanInitialization::getSwapchainSupportInfo(physicalDevice, surface);
            VkPresentModeKHR selectedPresentMode = UtilityFunctions::selectBestAvailablePresentMode(swapchainInfo.presentModes);
            VkExtent2D swapchainExtent = chooseSwapChainExtent(appWindow, swapchainInfo.capabilities);
            VkSurfaceFormatKHR bestAvailableFormat = chooseSwapChainSurfaceFormat(swapchainInfo.formats);

            uint32_t imageCount = swapchainInfo.capabilities.minImageCount + 1;
            if (swapchainInfo.capabilities.maxImageCount > 0 && imageCount > swapchainInfo.capabilities.maxImageCount)
            {
                imageCount = swapchainInfo.capabilities.maxImageCount;
            }

            std::vector<VkQueueFamilyProperties> queueFamilies;
            VulkanInitialization::getPhysicalDeviceQueuesWithProperties(physicalDevice, queueFamilies);

            bool success = SwapchainUtility::createSwapchain(device, swapchainInfo, surface, selectedPresentMode,
                                                             swapchainExtent, bestAvailableFormat, imageCount, swapchain.swapchain);

            assert(success);

            success = SwapchainUtility::getSwapchainImages(device, swapchain.swapchain, swapchain.swapchainImages);
            assert(success);

            swapchain.swapchainImageFormat = bestAvailableFormat.format;
            swapchain.swapChainExtent = swapchainExtent;

            success = SwapchainUtility::createSwapchainImageViews(device, swapchain.swapchainImageFormat, swapchain.swapchainImages, swapchain.swapchainImageViews);
            assert(success);

            return success;
        }

        bool createSwapchain(const VkDevice& device, const SwapchainSupportInfo& swapchainInfo, const VkSurfaceKHR& surface,
                             VkPresentModeKHR selectedPresentMode, VkExtent2D swapchainExtent, VkSurfaceFormatKHR surfaceFormat,
                             uint32_t imageCount, VkSwapchainKHR& swapchain)
        {
            VkSwapchainCreateInfoKHR createInfo = {};
            createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
            createInfo.flags = 0;
            createInfo.pNext = nullptr;
            createInfo.surface = surface;
            createInfo.presentMode = selectedPresentMode;
            createInfo.imageExtent = swapchainExtent;
            createInfo.minImageCount = imageCount;
            createInfo.imageArrayLayers = 1;
            createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
            createInfo.clipped = VK_TRUE;
            createInfo.imageFormat = surfaceFormat.format;
            createInfo.imageColorSpace = surfaceFormat.colorSpace;
            createInfo.oldSwapchain = VK_NULL_HANDLE;
            createInfo.preTransform = swapchainInfo.capabilities.currentTransform; //Want to add pre transform operation to an image?
            createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
            //A single queue family will have the ownership.
            createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;

            VkResult result = vkCreateSwapchainKHR(device, &createInfo, nullptr, &swapchain);
            assert(result == VK_SUCCESS);
            return result == VK_SUCCESS;
        }

        bool getSwapchainImages(const VkDevice& device, const VkSwapchainKHR& swapchain, std::vector<VkImage>& images)
        {
            //Get the swapchain images.
            uint32_t swapchainImageCount;
            vkGetSwapchainImagesKHR(device, swapchain, &swapchainImageCount, nullptr);
            images.resize(swapchainImageCount);
            VkResult result = vkGetSwapchainImagesKHR(device, swapchain, &swapchainImageCount, images.data());
            assert(result == VK_SUCCESS);
            return result == VK_SUCCESS;
        }

        bool createSwapchainImageViews(const VkDevice& device, VkFormat imageFormat, std::vector<VkImage>& images, std::vector<VkImageView>& imageViews)
        {
            imageViews.resize(images.size());
            for (size_t i = 0; i < imageViews.size(); i++)
            {
                VkImageViewCreateInfo info =
                        StructInitializer::getImageViewCreateInfo(images[i], imageFormat,
                                                                  VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_VIEW_TYPE_2D);
                if (!ImageUtility::createImageView(device, info, imageViews[i]))
                {
                    imageViews.clear();
                    return false;
                }
            }
            return true;
        }

        VkExtent2D chooseSwapChainExtent(GLFWwindow* appWindow, const VkSurfaceCapabilitiesKHR& capabilities)
        {
            assert(appWindow != nullptr);

            if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max())
            {
                return capabilities.currentExtent;
            }
            else
            {
                int width, height;
                glfwGetFramebufferSize(appWindow, &width, &height);

                VkExtent2D extent = { static_cast<uint32_t>(width),static_cast<uint32_t>(height) };
                extent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, extent.width));
                extent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, extent.height));
                return extent;
            }
        }

        VkSurfaceFormatKHR chooseSwapChainSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats)
        {
            VkSurfaceFormatKHR chosenFormat = availableFormats[0];
            //This means that Vulkan doesn't really care what the surface format is in this case.
            if (availableFormats.size() == 1 && availableFormats[0].format == VK_FORMAT_UNDEFINED)
            {
                return { VK_FORMAT_B8G8R8A8_UNORM, VK_COLORSPACE_SRGB_NONLINEAR_KHR };
            }

            for (const auto& availableFormat : availableFormats)
            {
                if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
                {
                    chosenFormat = availableFormat;
                    break;
                }
            }
            return chosenFormat;
        }
    }
}