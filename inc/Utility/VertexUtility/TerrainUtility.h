#ifndef RAVEN_TERRAIN_UTILITY_H_INCLUDED
#define RAVEN_TERRAIN_UTILITY_H_INCLUDED

#include "AssetLoader/AssetGroup.h"
#include "GraphicsApi/Vulkan/VulkanUtility/VulkanStructs.h"
#include <vector>
#include <unordered_map>
#include <functional>

//Reference: OpenGL 3D Game Tutorial 21: Terrain Height Maps
//https://www.youtube.com/watch?v=O9v6olrHPwI
#define MAX_PIXEL_COLOR 16777216 // = 256 * 256 * 256

namespace Raven
{
    namespace TerrainUtility
    {
        void generatePlaneFromHeightMap(const TextureData& textureData, float maxHeight, glm::vec3 color, std::vector<Vertex>& vertices, std::vector<uint32_t>& indices);
        void generatePlane(uint32_t sideSize, glm::vec3 color, std::vector<Vertex>& vertices, std::vector<uint32_t>& indices);
        void generatePlaneFromNoise(size_t widthHeight, float maxHeight, const std::vector<float>& noiseData, std::vector<Vertex>& vertices, std::vector<uint32_t>& indices);
    };
}
#endif