#ifndef RAVEN_BUFFER_UTILITY_H_INCLUDED
#define RAVEN_BUFFER_UTILITY_H_INCLUDED
#include "GraphicsApi/Vulkan/Common.h"

namespace Raven
{
    namespace BufferUtility
    {
        //PREFER VULKAN MEMORY ALLOCATOR AND RAVENVULKANMANAGER OVER THIS FUNCTION!
        VkBuffer createEmptyBuffer(const VkDevice &device, VkBufferUsageFlags bufferUsage, VkDeviceSize bufferSize);
        //PREFER VULKAN MEMORY ALLOCATOR AND RAVENVULKANMANAGER OVER THIS FUNCTION!
        void allocateBufferMemory(const VkDevice& device, VkDeviceSize allocationSize, uint32_t memoryTypeIndex, VkDeviceMemory& bufferMemory);
        //PREFER VULKAN MEMORY ALLOCATOR AND RAVENVULKANMANAGER OVER THIS FUNCTION!
        bool createMemoryBackedBuffer(const VkDevice& device, const VkPhysicalDevice& physicalDevice, VkDeviceSize bufferSize,
                                      VkBufferUsageFlags bufferUsage, VkMemoryPropertyFlags properties, VkDeviceMemory &bufferMemory,
                                      VkBuffer &buffer);
    }
}
#endif
