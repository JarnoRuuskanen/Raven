#ifndef RAVEN_GRAPHICS_PIPELINE_H_INCLUDED
#define RAVEN_GRAPHICS_PIPELINE_H_INCLUDED

#include <Utility/ShaderUtility/ShaderUtilityCommon.h>
#include "GraphicsApi/Vulkan/Common.h"
namespace Raven
{
    struct GraphicsPipeline
    {
        VkPipeline pipeline;
        VkPipelineLayout layout;
        VkPipelineCache cache;

        //Use this information to allocate descriptor sets.
        //NOTE: Descriptor pool should use VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT as descriptor sets
        //will be released from this pool whenever a new pipeline is set for a renderable.
        VkDescriptorPool descriptorPool;
        std::vector<VkDescriptorSetLayout> descriptorSetLayouts;
        std::vector<VkPushConstantRange> pushConstantRanges;

        //Use this information to update renderable descriptor sets.
        std::vector<GraphicsPipelineDescriptorInfo> descriptorInfos;
        bool descriptorInfosSet = false;
        uint32_t serialization;
    };
}

#endif