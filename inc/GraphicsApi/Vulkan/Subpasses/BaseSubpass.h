#ifndef RAVEN_RENDERPASS_H_INCLUDED
#define RAVEN_RENDERPASS_H_INCLUDED

#include "GraphicsApi/Vulkan/Common.h"
#include <vector>
#include <GraphicsApi/Vulkan/ResourceManager/VulkanResourceManager.h>
#include "GraphicsApi/Vulkan/VulkanObjects/GraphicsPipeline.h"

namespace Raven
{
    //What kind 
    class BaseSubpass
    {
    public:
        BaseSubpass(std::string passName) : m_subpassDescriptionCreated(false), m_subpassDependencySet(false), 
            m_orderNumberInRenderPass(-1), m_subpassDependency({}), m_subpassDescription({}), m_passName(passName) {}
        bool descriptorCountMatches(size_t descriptorCountToBeSubmitted) { return descriptorCountToBeSubmitted == m_descriptorSetBindingInfos.size(); }
        const std::vector<ShaderDescriptorBindingInfo>& getShaderDescriptorBindingInfos() const { return m_descriptorSetBindingInfos; }
        const std::vector<ShaderPushConstantInfo>& getShaderPushConstantInfos() const { return m_shaderPushConstantInfos; }
        virtual void createSubpassDescription(const std::vector<VkAttachmentReference>& inputReferences, const std::vector<VkAttachmentReference>& colorReferences,
                                      const std::vector<VkAttachmentReference>& depthReferences, const std::vector<VkAttachmentDescription>& attachments, 
                                      const std::vector<uint32_t>& preservedAttachmentIndices);
        virtual bool initPipeline(const VkDevice& device, const VkRenderPass& parentRenderPass, uint32_t subpassIndex);
        virtual void draw(const RavenBuffer& vertexBuffer, const RavenBuffer& indexBuffer,
                          const std::vector<VkDescriptorSet>& descriptorSets , const std::vector<PushConstantData*>& pushConstants, VkCommandBuffer& cmdBuffer) = 0;
        void setSubpassDependency(uint32_t src, uint32_t dst, VkPipelineStageFlags srcStageMask,
                                  VkPipelineStageFlags dstStageMask, VkAccessFlags srcAccessMask, 
                                  VkAccessFlags dstAccessMask, VkDependencyFlags flags = 0);
        inline const VkSubpassDependency& getSubpassDependency() const { return m_subpassDependency; }
        inline const std::vector<VkAttachmentDescription> getAttachmentDescriptions() { return m_attachmentDescriptions; }
        inline const VkSubpassDescription& getSubpassDescription() const { return m_subpassDescription; }
        inline const std::vector<VkDescriptorSetLayout>& getDescriptorSetLayouts() const { return m_renderpassPipeline.descriptorSetLayouts; }
        inline const std::string& getSubpassName() const { return m_passName; }
        void destroyPipeline(const VkDevice& device);
        VkResult describePipeline(const VkDevice& device, const std::vector<ShaderLoadInformation>& shadersToLoad);
    protected:
        //Data used to describe what kind of attachments the renderpass will be using, subpass dependency etc.
        std::vector<VkAttachmentReference> m_inputAttachmentReferences;
        std::vector<VkAttachmentReference> m_colorAttachmentReferences;
        std::vector<VkAttachmentReference> m_depthAttachmentReferences;
        //Indices of the attachments that pass through this subpass but 
        //are not affected by it and must be preserved.
        std::vector<uint32_t> m_preservedAttachmentsIndices;

        std::vector<VkAttachmentDescription> m_attachmentDescriptions;
        std::vector<ShaderDescriptorBindingInfo> m_descriptorSetBindingInfos;
        std::vector<ShaderPushConstantInfo> m_shaderPushConstantInfos;
        VkSubpassDependency m_subpassDependency;
        VkSubpassDescription m_subpassDescription;
        GraphicsPipeline m_renderpassPipeline;
        std::vector<VkPipelineShaderStageCreateInfo> m_shaderStages;
        std::vector<ShaderStageData> m_shaderStageDatas;

        //For pipeline initialization.
        std::vector<VkViewport> m_viewports;
        std::vector<VkRect2D> m_scissors;
        std::vector<VkPipelineColorBlendAttachmentState> m_colorBlendAttachments;
        std::vector<VkDynamicState> m_dynamicStates;
        std::vector<VkVertexInputBindingDescription> m_vertexInputBindings;
        std::vector<VkVertexInputAttributeDescription> m_vertexInputAttributes;

        size_t m_orderNumberInRenderPass;
        std::string m_passName;
        bool m_subpassDescriptionCreated;
    private:
        virtual void initializeVertexInputState(VkPipelineVertexInputStateCreateInfo& vertexInputState) = 0;
        virtual void initializeInputAssemblyState(VkPipelineInputAssemblyStateCreateInfo& inputAssembly) = 0;
        virtual void initializeViewportState(VkPipelineViewportStateCreateInfo& viewportState) = 0;
        virtual void initializeRasterizerState(VkPipelineRasterizationStateCreateInfo& rasterizer) = 0;
        virtual void initializeMultisampleState(VkPipelineMultisampleStateCreateInfo& multisampling) = 0;
        virtual void initializeDepthStencilState(VkPipelineDepthStencilStateCreateInfo& depthStencil) = 0;
        virtual void initializeColorBlendState(VkPipelineColorBlendStateCreateInfo& colorBlend) = 0;
        virtual void initializeDynamicState(VkPipelineDynamicStateCreateInfo& dynamicStateCreateInfo) = 0;
        virtual VkResult initializePipelineLayout(const VkDevice& device) = 0;
        bool m_subpassDependencySet;
    };
}

#endif