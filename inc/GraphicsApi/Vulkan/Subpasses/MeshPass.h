#ifndef RAVEN_MESH_PASS_H_INCLUDED
#define RAVEN_MESH_PASS_H_INCLUDED

#include "BaseSubpass.h"

namespace Raven
{
	class MeshPass : public BaseSubpass
	{
	public:
		// Inherited via BaseSubpass
		MeshPass() : BaseSubpass("meshPass") {}
		void draw(const RavenBuffer& vertexBuffer, const RavenBuffer& indexBuffer, const std::vector<VkDescriptorSet>& descriptorSets, 
				  const std::vector<PushConstantData*>& pushConstants, VkCommandBuffer& cmdBuffer) override;
	private:
		void initializeVertexInputState(VkPipelineVertexInputStateCreateInfo& vertexInputState) override;
		void initializeInputAssemblyState(VkPipelineInputAssemblyStateCreateInfo& inputAssembly) override;
		void initializeViewportState(VkPipelineViewportStateCreateInfo& viewportState) override;
		void initializeRasterizerState(VkPipelineRasterizationStateCreateInfo& rasterizer) override;
		void initializeMultisampleState(VkPipelineMultisampleStateCreateInfo& multisampling) override;
		void initializeDepthStencilState(VkPipelineDepthStencilStateCreateInfo& depthStencil) override;
		void initializeColorBlendState(VkPipelineColorBlendStateCreateInfo& colorBlend) override;
		void initializeDynamicState(VkPipelineDynamicStateCreateInfo& dynamicStateCreateInfo) override;
		VkResult initializePipelineLayout(const VkDevice& device) override;
	};
}

#endif