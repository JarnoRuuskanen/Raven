#ifndef RAVEN_COMPOSITION_PASS_H_INCLUDED
#define RAVEN_COMPOSITION_PASS_H_INCLUDED

#include "GraphicsApi/Vulkan/Subpasses/BaseSubpass.h"

namespace Raven
{
	class CompositionPass : public BaseSubpass
	{
	public:
		CompositionPass() : BaseSubpass("compositionPass") {}
		// Inherited via BaseSubpass
		void draw(const RavenBuffer& vertexBuffer, const RavenBuffer& indexBuffer, const std::vector<VkDescriptorSet>& descriptorSets, 
			      const std::vector<PushConstantData*>& pushConstants, VkCommandBuffer& cmdBuffer) override;
		void initializeDescriptorSet(const VkDevice& device, const std::vector<RavenImage>& imageAttachments, const RavenBuffer& lightUniformBuffer);
	private:
		void initializeVertexInputState(VkPipelineVertexInputStateCreateInfo& vertexInputState) override;
		void initializeInputAssemblyState(VkPipelineInputAssemblyStateCreateInfo& inputAssembly) override;
		void initializeViewportState(VkPipelineViewportStateCreateInfo& viewportState) override;
		void initializeRasterizerState(VkPipelineRasterizationStateCreateInfo& rasterizer) override;
		void initializeMultisampleState(VkPipelineMultisampleStateCreateInfo& multisampling) override;
		void initializeDepthStencilState(VkPipelineDepthStencilStateCreateInfo& depthStencil) override;
		void initializeColorBlendState(VkPipelineColorBlendStateCreateInfo& colorBlend) override;
		void initializeDynamicState(VkPipelineDynamicStateCreateInfo& dynamicStateCreateInfo) override;
		VkResult initializePipelineLayout(const VkDevice& device) override;

		VkDescriptorPool m_descriptorPool;
		VkDescriptorSet m_descriptorSet;
	};
}
#endif