#include <EntityComponentSystem/EntityManager.h>
#include <GraphicsApi/Vulkan/VulkanUtility/UtilityFunctions.h>
#include <GraphicsApi/Vulkan/VulkanUtility/VulkanInitialization.h>
#include <GraphicsApi/Vulkan/VulkanUtility/CommandBufferUtility.h>
#include "GraphicsApi/Vulkan/VulkanUtility/SwapchainUtility.h"
#include "GraphicsApi/Vulkan/VulkanUtility/GraphicsPipelineUtility.h"
#include <GraphicsApi/Vulkan/VulkanUtility/DescriptorUtility.h>
#include <Utility/DelegateManager.h>
#include <GraphicsApi/Vulkan/Subpasses/UnlitPass.h>
#include <GraphicsApi/Vulkan/Subpasses/HighlightPass.h>
#include "GraphicsApi/Vulkan/Subpasses/TerrainPass.h"
#include "GraphicsApi/Vulkan/Subpasses/MeshPass.h"
#include "Utility/GeneralUtility.h"

//How many frames can be processed concurrently.
const int MAX_FRAMES_IN_FLIGHT = 2;

using namespace std::placeholders;

namespace Raven
{
    RenderSystem::RenderSystem(EntityManager* pGameWorld) : BaseType(pGameWorld)
    {
        DelegateManager& manager = DelegateManager::getInstance();
        m_appWindowDelegates = manager.getWindowDelegates();
        const RavenVulkanContext& context = manager.getVulkanContextDelegates().getVulkanContextFunc.Invoke();
        m_device = context.device;
        m_presentationQueue = context.queues[0];
        m_commandBufferQueue = context.queues[1];
        m_extensionFunctions = context.funcPointers;

        VulkanQueueInfo queueInfo;
        VulkanInitialization::getPhysicalDevicePrimaryQueueFamily(context.physicalDevice, queueInfo);

        bool success = createSynchronizationObjects();
        assert(success);

        success = initializeSwapchain();
        assert(success);

        initializeUniformBuffers();

        RendererDelegates delegates;
        delegates.highlightSelectedEntityFunc.Bind<RenderSystem, &RenderSystem::changeHighlightedEntity>(this);
        delegates.getSwapchainImageCountFunc.Bind<RenderSystem, &RenderSystem::getSwapchainImageCount>(this);
        delegates.getSwapchainFunc.Bind<RenderSystem, &RenderSystem::getSwapchain>(this);
        manager.registerRendererDelegates(delegates);

        m_vulkanResourceManagerDelegates = manager.getVulkanResourceManagerDelegates();

        m_vulkanWorker.initialize();

        createSubpasses();
        //Testing general async task manager. Future will block until completion before being destroyed so no need to wait.
        auto fut = launchAsyncTask(&RenderSystem::createRenderPipelines, this);

        m_uiRecordingFunction.recordingFunction.Bind<RavenUI, &RavenUI::draw>(&m_ui);
    }

    void RenderSystem::update(const Timer& timer)
    {
        //Wait for the previous frame to be finished.
        waitForPreviousFrameToFinish();

        //Get next available image and start rendering into it.
        uint32_t imageIndex;
        VkResult result = vkAcquireNextImageKHR(m_device, m_swapchain.swapchain, std::numeric_limits<uint64_t>::max(), m_imageAvailableSemaphores[m_currentFrame], VK_NULL_HANDLE, &imageIndex);
        if(result == VK_ERROR_OUT_OF_DATE_KHR)
        {
            assert(!"Failed to acquire next swapchainImage! Swapchain recreation not implemented yet!");
            std::exit(1);
        }

        updateUniformBuffers(timer.deltaTime);

        //Update UI.
        if(m_renderUI)
        {
            m_ui.update(timer.deltaTime);
        }

        //Submit all the rendering work that has been recorded to correct queues and wait for result.
        render(imageIndex);

        //Present the results.
        present(imageIndex);
    }

    void RenderSystem::render(uint32_t imageIndex)
    {
        //First get the waitValue. Waitvalue is the last timeline semaphore signal that will be signaled in this batch.
        m_currentSignalToWait = prepareTimelineWaitAndSignalInfos(imageIndex);
        if(m_renderUI)
        {
            int test = 0;
        }

        //TODO: DO NOT RECORD DRAWING COMMAND BUFFERS IF THERE WERE NO CHANGES.

        ////Multithreaded command buffer recording:
        m_vulkanWorker.recordAndSubmitCommands(m_currentFrameRecordingParams, m_timelineSemaphore,
                                               m_imageAvailableSemaphores[m_currentFrame],
                                               m_allRenderingDoneSemaphores[m_currentFrame],
                                               imageIndex);
        //TODO: REMOVE!
        vkDeviceWaitIdle(m_device);
    }

    void RenderSystem::present(uint32_t imageIndex)
    {
        VkSemaphore renderFinishedSemaphore [] = {m_allRenderingDoneSemaphores[m_currentFrame]};
        VkPresentInfoKHR presentInfo = {};
        presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        presentInfo.waitSemaphoreCount = 1;
        presentInfo.pWaitSemaphores = renderFinishedSemaphore;

        VkSwapchainKHR swapChains[] = { m_swapchain.swapchain};
        presentInfo.swapchainCount = 1;
        presentInfo.pSwapchains = swapChains;
        presentInfo.pImageIndices = &imageIndex;
        presentInfo.pResults = nullptr;

        VkResult result = vkQueuePresentKHR(m_presentationQueue, &presentInfo);
        if(result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR)
        {
            m_currentFrame = (m_currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
            reCreateSwapchain();
            std::cerr << "PRESENTATION FAILED!!\n";
            std::exit(1);
        }

        m_currentFrame = (m_currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
    }

    void RenderSystem::entityAddedToSystem(Entity* entity)
    {
        //Here you can add renderable to go through correct pipeline so that
        //it gets shown on screen.
        Renderable* renderable = entity->getComponent<Renderable>();
        assert(renderable);
        if (renderable)
        {
            std::string targetPipeline = "forward";
#ifdef RAVEN_DEFERRED_LIGHTING
            targetPipeline = "deferred";
#endif
            //TODO:Remove push constant dependency, it shouldn't be here.
            //TODO: Add logic for setting renderable to correct pipeline.
            registerRenderableToRenderPipeline(renderable, {}, targetPipeline);
        }
    }

    uint64_t RenderSystem::prepareTimelineWaitAndSignalInfos(uint32_t imageIndex)
    {
        uint64_t timelineCurrentValue;
        m_extensionFunctions.pfnGetSemaphoreCounterValueKHR(m_device, m_timelineSemaphore, &timelineCurrentValue);

        m_currentFrameRecordingParams.clear();
        m_currentFrameRecordingParams.shrink_to_fit();

        //Idea here is that each stage will wait for the previous one to complete.
        
        uint64_t afterMainRendering = recordMainRenderingCommands(imageIndex, timelineCurrentValue);

        uint64_t afterPostProcessing = recordPostProcessingCommands(imageIndex, afterMainRendering);

        //Lastly add UI rendering commands.
        uint64_t afterUIRendering = recordUICommands(imageIndex, afterPostProcessing);

        return afterUIRendering;
    }

    uint64_t RenderSystem::recordMainRenderingCommands(uint32_t imageIndex, uint64_t timelineCurrentValue)
    {
        size_t pipelineIndex = 0;
        //First add normal rendering commands.
        assert(m_mainRenderingParams.size() <= m_mainRenderingPipelines.size());
        for (auto& recorder : m_mainRenderingParams)
        {
            recorder.commandBuffer = m_mainRenderingPipelines[pipelineIndex]->getCommandBufferForFrame(imageIndex);
            m_mainRenderingPipelines[pipelineIndex]->setTimelineWaitAndSignalValue(timelineCurrentValue, timelineCurrentValue + 1);
            recorder.timelineInfo = m_mainRenderingPipelines[pipelineIndex]->getTimelineSubmitInfo();

            pipelineIndex++;
            timelineCurrentValue++;

            m_currentFrameRecordingParams.push_back(recorder);
        }
        return timelineCurrentValue;
    }

    uint64_t RenderSystem::recordPostProcessingCommands(uint32_t imageIndex, uint64_t timelineCurrentValue)
    {
        uint32_t pipelineIndex = 0;
        //Add post processing commands.
        assert(m_postProcessingRenderingParams.size() <= m_postProcessingPipelines.size());
        for (auto& recorder : m_postProcessingRenderingParams)
        {
            recorder.commandBuffer = m_postProcessingPipelines[pipelineIndex]->getCommandBufferForFrame(imageIndex);
            m_postProcessingPipelines[pipelineIndex]->setTimelineWaitAndSignalValue(timelineCurrentValue, timelineCurrentValue + 1);
            recorder.timelineInfo = m_postProcessingPipelines[pipelineIndex]->getTimelineSubmitInfo();

            pipelineIndex++;
            timelineCurrentValue++;

            m_currentFrameRecordingParams.push_back(recorder);
        }
        return timelineCurrentValue;
    }

    uint64_t RenderSystem::recordUICommands(uint32_t imageIndex, uint64_t timelineCurrentValue)
    {
        if (m_renderUI)
        {
            m_uiRecordingFunction.commandBuffer = m_ui.getCommandBufferForFrame(imageIndex);
            m_ui.setTimelineWaitAndSignalValue(timelineCurrentValue, timelineCurrentValue + 1);
            m_uiRecordingFunction.timelineInfo = m_ui.getTimelineSubmitInfo();
            timelineCurrentValue++;

            m_currentFrameRecordingParams.push_back(m_uiRecordingFunction);
        }
        return timelineCurrentValue;
    }

    void RenderSystem::waitForPreviousFrameToFinish()
    {
        //Wait for the signal from timeline semaphore.
        VkSemaphoreWaitInfo waitInfo;
        waitInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_WAIT_INFO;
        waitInfo.pNext = nullptr;
        waitInfo.flags = 0;
        waitInfo.semaphoreCount = 1;
        waitInfo.pSemaphores = &m_timelineSemaphore;
        waitInfo.pValues = &m_currentSignalToWait;

        VkResult result = m_extensionFunctions.pfnWaitSemaphoresKHR(m_device, &waitInfo, UINT64_MAX);
        if (result != VK_SUCCESS)
        {
            if (result == VK_TIMEOUT)
            {
                std::cout << "Semaphore wait timed out, exiting.\n";
            }
            else
            {
                std::cout << "Semaphore wait was not VK_TIMEOUT, but wait still failed. Exiting. \n";
            }
            std::exit(0);
        }
    }

    void RenderSystem::setActivePlayer(Entity* entity)
    {
        m_playerTransformComponent = entity->getComponent<Transform>();
        m_playerCameraComponent = entity->getComponent<Camera>();

        assert(m_playerTransformComponent != nullptr);
        assert(m_playerCameraComponent != nullptr);
    }


    bool RenderSystem::createSynchronizationObjects()
    {
        m_imageAvailableSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
        m_allRenderingDoneSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
        m_inFlightFences.resize(MAX_FRAMES_IN_FLIGHT);

        m_timelineSemaphore = UtilityFunctions::createTimelineSemaphoreKHR(m_device);
        m_currentSignalToWait = 0;

        for(size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
        {
            m_imageAvailableSemaphores[i] = UtilityFunctions::createSemaphore(m_device);
            m_allRenderingDoneSemaphores[i] = UtilityFunctions::createSemaphore(m_device);
            m_inFlightFences[i] = UtilityFunctions::createSignaledFence(m_device);
            if(m_imageAvailableSemaphores[i] == VK_NULL_HANDLE ||
               m_timelineSemaphore == VK_NULL_HANDLE ||
               m_inFlightFences[i] == VK_NULL_HANDLE)
            {
                return false;
            }
        }
        return true;
    }

    bool RenderSystem::initializeSwapchain()
    {
        DelegateManager& manager = DelegateManager::getInstance();
        const auto& context = manager.getVulkanContextDelegates().getVulkanContextFunc.Invoke();;
        const VkPhysicalDevice& physicalDevice = context.physicalDevice;

        VkSurfaceKHR& windowSurface =  m_appWindowDelegates.getSurfaceFunc.Invoke();
        assert(windowSurface != VK_NULL_HANDLE);

        GLFWwindow* appWindow = m_appWindowDelegates.getNativeWindowFunc.Invoke();
        assert(appWindow);

        std::vector<VkQueueFamilyProperties> queueFamilies;
        VulkanInitialization::getPhysicalDeviceQueuesWithProperties(physicalDevice, queueFamilies);

        m_presentationQueueIndex = VulkanInitialization::getPresentationQueueFamilyIndex(physicalDevice, windowSurface, queueFamilies);

        bool success = SwapchainUtility::getDefaultSwapchain(physicalDevice, m_device, windowSurface, appWindow, m_swapchain);
        assert(success);

        return success;
    }

    bool RenderSystem::reCreateSwapchain()
    {
        //Destroy swapchain, all render targets, pipelines etc and recreate them all.
        vkDeviceWaitIdle(m_device);
        
        cleanupSwapchain();
        for (auto& pipeline : m_mainRenderingPipelines)
        {
            pipeline->destroyPipeline(m_device);
        }

        return false;
    }

    bool RenderSystem::cleanupSwapchain()
    {
        vkDestroySwapchainKHR(m_device, m_swapchain.swapchain, nullptr);

        for (auto& imageView : m_swapchain.swapchainImageViews)
        {
            vkDestroyImageView(m_device, imageView, nullptr);
        }

        return true;
    }

    void RenderSystem::initializeUniformBuffers()
    {
        m_ubo.model = glm::mat4(0.0f); //glm::rotate(glm::mat4{1.0f}, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 0.0f));
        m_ubo.proj = glm::perspective(glm::radians(45.0f), 16.0f / 9.0f, 0.1f, 256.0f);
        m_ubo.view = glm::lookAt(glm::vec3{0.0f,0.0f, -5.0f}, glm::vec3{0.0f}, glm::vec3{0.0f, 1.0f, 0.0f}); //m_activeCamera->getViewMatrix();
        m_ubo.proj[1][1] *= -1; //Flip y-axis since in OpenGL y-axis is inverted compared to Vulkan. https://vulkan-tutorial.com/Uniform_buffers/Descriptor_layout_and_buffer
    }

    void RenderSystem::updateUniformBuffers(double deltaTime)
    {
        //First data for the model.
        auto& position = m_playerTransformComponent->m_position;
        auto& cameraFront = m_playerCameraComponent->m_cameraFront;
        auto& cameraUp = m_playerCameraComponent->m_cameraUp;
        m_playerCameraComponent->m_viewMatrix = glm::lookAt(position, position + cameraFront, cameraUp);
        m_ubo.view = m_playerCameraComponent->m_viewMatrix;

        for (auto& compTuple : m_components)
        {
            Transform* transform = std::get<Transform*>(compTuple);
            Renderable* renderable = std::get<Renderable*>(compTuple);
            m_ubo.model = transform->m_worldTransform;

            void* data;
            m_vulkanResourceManagerDelegates.mapAllocatedMemoryFunc.Invoke(renderable->m_uniformBuffer.memoryAllocation, &data);
            std::memcpy(data, &m_ubo, sizeof(m_ubo));
            m_vulkanResourceManagerDelegates.unmapAllocatedMemoryFunc.Invoke(renderable->m_uniformBuffer.memoryAllocation);
        }
    }

    void RenderSystem::initializeUI()
    {
        m_ui.initialize();
        createMenuControls();

        m_renderUI = false;
    }

    void RenderSystem::createMenuControls()
    {
        Input& inputHandler = Input::getInstance();

        KeyEvent event;
        event.glfwModifierKeys = 0;
        event.state = KeyboardKeyState::PRESS;
        event.ravenKey = (unsigned int)KeyboardKey::KEY_TAB;

        Delegate<bool(const KeyEvent&)> callback;
        callback.Bind<RenderSystem, &RenderSystem::toggleMenu>(this);

        inputHandler.registerKeyEventCallback(event, callback);
    }

    bool RenderSystem::toggleMenu(const KeyEvent& event)
    {
        if(event.state == KeyboardKeyState::PRESS)
        {
            m_renderUI = !m_renderUI;
            GLFWwindow* appWindow = m_appWindowDelegates.getNativeWindowFunc.Invoke();
            if(!m_renderUI)
            {
                glfwSetInputMode(appWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
            }
            else
            {
                glfwSetInputMode(appWindow, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            }
        }
        return true;
    }

    void RenderSystem::updateSelectedObjectInfo(const EntityId& entityId)
    {
        m_ui.updateSelectedObject(entityId);
    }

    void RenderSystem::changeHighlightedEntity(EntityId id)
    {
        if (id != INVALID_ENTITY_ID)
        {
            m_highlightEntityId = id;
            const Entity& entity = m_pGameWorld->getEntity(id);

            //This is a bit of a hack. Removes highlight from other objects so
            //that only one object can be highlighted at once. This should be
            //thought through once I have time.
            for (auto& pipeline : m_mainRenderingPipelines)
            {
                if (pipeline->getName() == "highlight")
                {
                    pipeline->clearAllRenderables(m_device);
                }
            }
            highlightEntity(entity);
        }
    }

    void RenderSystem::highlightEntity(const Entity& entity)
    {
        if (entity.getId() != INVALID_ENTITY_ID)
        {
            Renderable* renderable = entity.getComponent<Renderable>();
            if (renderable)
            {
                registerRenderableToRenderPipeline(renderable, {}, "highlight");
            }
            for (auto& childId : entity.getChildren())
            {
                const Entity& child = m_pGameWorld->getEntity(childId);
                //Recursively adds highlight to all children that have a renderable.
                highlightEntity(child);
            }
        }
    }

    void RenderSystem::createSubpasses()
    {
        m_availableSubpasses.insert({"unlitPass", new UnlitPass});
        m_availableSubpasses.insert({"terrainPass", new TerrainPass});
        m_availableSubpasses.insert({"highlightPass", new HighlightPass});
        m_availableSubpasses.insert({"meshPass", new MeshPass });
    }

    void RenderSystem::createRenderPipelines()
    {
#ifdef RAVEN_DEFERRED_LIGHTING
        createDeferredRenderPipeline();
#else
        createForwardRenderPipeline();
#endif
        createTerrainPipeline();

        createHighlightPipeline();

        createMeshPipeline();

        for(RenderPipeline* pipeline : m_mainRenderingPipelines)
        {
            CommandBufferRecordingThreadParameters recorder;
            recorder.recordingFunction.Bind<RenderPipeline, &RenderPipeline::draw>(pipeline);
            m_mainRenderingParams.push_back(recorder);
        }
    }

    void RenderSystem::registerRenderableToRenderPipeline(Renderable* renderable, const std::vector<PushConstantData*>& pushConstants, const std::string& pipelineName)
    {
        for(RenderPipeline* pipeline : m_mainRenderingPipelines)
        {
            if (pipeline->getName() == pipelineName)
            {
                pipeline->registerDataForDrawing(m_device, pushConstants, renderable);
                break;
            }
        }
    }

    void RenderSystem::createDeferredRenderPipeline()
    {
        const Swapchain& swapchain = DelegateManager::getInstance().getRendererDelegates().getSwapchainFunc.Invoke();

        //Reference https://github.com/SaschaWillems/Vulkan/blob/master/examples/subpasses/subpasses.cpp

        std::vector<VkAttachmentDescription> attachments(5);

        // Color attachment
        attachments[0].format = swapchain.swapchainImageFormat;
        attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
        attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        attachments[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

        // Deferred attachments
        // Position
        attachments[1].format = swapchain.swapchainImageFormat;;
        attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
        attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        attachments[1].finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
        // Normals
        attachments[2].format = swapchain.swapchainImageFormat;;
        attachments[2].samples = VK_SAMPLE_COUNT_1_BIT;
        attachments[2].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachments[2].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[2].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachments[2].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[2].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        attachments[2].finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
        // Albedo
        attachments[3].format = swapchain.swapchainImageFormat;;
        attachments[3].samples = VK_SAMPLE_COUNT_1_BIT;
        attachments[3].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachments[3].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[3].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachments[3].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[3].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        attachments[3].finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
        // Depth attachment
        attachments[4].format = VK_FORMAT_D32_SFLOAT;
        attachments[4].samples = VK_SAMPLE_COUNT_1_BIT;
        attachments[4].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachments[4].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        attachments[4].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachments[4].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[4].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        attachments[4].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        //Initialize the first subpass.
        std::vector<VkAttachmentReference> firstPassColorReferences(4);
        firstPassColorReferences[0] = { 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };
        firstPassColorReferences[1] = { 1, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };
        firstPassColorReferences[2] = { 2, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };
        firstPassColorReferences[3] = { 3, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };

        std::vector<VkAttachmentReference> depthReferences(1);
        depthReferences[0] = {4, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL };
        m_gBufferPass.createSubpassDescription({}, firstPassColorReferences, depthReferences, attachments, {});

        //Second subpass.
        std::vector<VkAttachmentReference> secondPassInputReferences(3);
        secondPassInputReferences[0] = { 1, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL };
        secondPassInputReferences[1] = { 2, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL };
        secondPassInputReferences[2] = { 3, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL };

        std::vector<VkAttachmentReference> secondPassColorReferences(1);
        secondPassColorReferences[0] = { 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };

        std::vector<VkAttachmentDescription> secondPassColorAttachments = {attachments[0]};
        m_compositionPass.createSubpassDescription(secondPassInputReferences, secondPassColorReferences, {}, secondPassColorAttachments, { 4 });

        ////Last subpass, forward transparency.
        //std::vector<VkAttachmentReference> thirdPassInputReferences(1);
        //thirdPassInputReferences[0] = { 1, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL };
        ////m_deferredLastSubpass.createDescription(thirdPassInputReferences, secondPassColorReferences, {}, {attachments[0]});

        //Set subpass dependencies.
        m_gBufferPass.setSubpassDependency(VK_SUBPASS_EXTERNAL, 0, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                                           VK_ACCESS_MEMORY_READ_BIT, VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, 
                                           VK_DEPENDENCY_BY_REGION_BIT);

        m_compositionPass.setSubpassDependency(0, 1, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
                                               VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT, VK_DEPENDENCY_BY_REGION_BIT);

        //Build the render pipeline. It will initialize all the subpasse pipelines etc.
        const RavenVulkanContext& context = DelegateManager::getInstance().getVulkanContextDelegates().getVulkanContextFunc.Invoke();
        std::vector<BaseSubpass*> subpasses = { &m_gBufferPass, &m_compositionPass };

        m_deferredPipeline.setRenderPipelineName("deferred");
        std::vector<VkClearValue> clearValues(5);
        clearValues[0].color = { 1.0f, 1.0f, 1.0f, 1.0f };
        clearValues[1].color = { 1.0f, 1.0f, 1.0f, 1.0f };
        clearValues[2].color = { 1.0f, 1.0f, 1.0f, 1.0f };
        clearValues[3].color = { 1.0f, 1.0f, 1.0f, 1.0f };
        clearValues[4].depthStencil = { 1.0f, 0 };

        m_deferredPipeline.initialize(m_device, RenderTargetType::SWAPCHAIN, subpasses, attachments, clearValues, nullptr);
        m_mainRenderingPipelines.push_back(&m_deferredPipeline);
    }

    void RenderSystem::createForwardRenderPipeline()
    {
        const Swapchain& swapchain = DelegateManager::getInstance().getRendererDelegates().getSwapchainFunc.Invoke();

        std::vector<VkAttachmentDescription> attachments(2);

        // Color attachment
        attachments[0].format = swapchain.swapchainImageFormat;
        attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
        attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        attachments[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

        // Depth attachment
        attachments[1].format = VK_FORMAT_D32_SFLOAT;
        attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
        attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        attachments[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        attachments[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        std::vector<VkClearValue> clearValues(2);
        clearValues[0].color = { 1.0f, 1.0f, 1.0f, 1.0f };
        clearValues[1].depthStencil = { 1.0f,0 };

        m_forwardPipeline.setRenderPipelineName("forward");

        m_forwardLightPass.setSubpassDependency(VK_SUBPASS_EXTERNAL, 0, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                                                VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, 0,
                                                VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);

        std::vector<VkAttachmentReference> colorReferences(1);
        colorReferences[0] = { 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };

        std::vector<VkAttachmentReference> depthReferences(1);
        depthReferences[0] = { 1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL };
        m_forwardLightPass.createSubpassDescription({}, colorReferences, depthReferences, attachments, {});

        std::vector<BaseSubpass*> defaultSubpasses = { &m_forwardLightPass };

        m_forwardPipeline.initialize(m_device, RenderTargetType::SWAPCHAIN, defaultSubpasses, attachments, clearValues, nullptr);
        m_mainRenderingPipelines.push_back(&m_forwardPipeline);
    }

    void RenderSystem::createTerrainPipeline()
    {
        std::vector<VkAttachmentDescription> attachments(2);

        //Main swapchain attachment.
        attachments[0].format = m_swapchain.swapchainImageFormat;
        attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
        attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
        attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[0].initialLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        attachments[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

        // Depth attachment
        attachments[1].format = VK_FORMAT_D32_SFLOAT;
        attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
        attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
        attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        attachments[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[1].initialLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
        attachments[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        std::vector<VkClearValue> clearValues(2);
        clearValues[0].color = { 1.0f, 1.0f, 1.0f, 1.0f };
        clearValues[1].depthStencil = { 1.0f,0 };

        m_terrainPipeline.setRenderPipelineName("terrain");

        TerrainPass* terrainPass = static_cast<TerrainPass*>(m_availableSubpasses.at("terrainPass"));
        assert(terrainPass);

        terrainPass->setSubpassDependency(VK_SUBPASS_EXTERNAL, 0, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, 0,
            VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);

        std::vector<VkAttachmentReference> colorReferences(1);
        colorReferences[0] = { 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };

        std::vector<VkAttachmentReference> depthReferences(1);
        depthReferences[0] = { 1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL };
        terrainPass->createSubpassDescription({}, colorReferences, depthReferences, attachments, {});

        std::string targetPipeline = "forward";
#ifdef RAVEN_DEFERRED_LIGHTING
        targetPipeline = "deferred";
#endif
        //Get the depth from previous pipeline and send it as a source for the deferred pipeline. This so that 
        //deferred pipeline doesn't clear objects that should be visible.
        RenderTarget previousPipelineTarget;
        for (auto& pipeline : m_mainRenderingPipelines)
        {
            if (pipeline->getName() == targetPipeline)
            {
                previousPipelineTarget = pipeline->getRenderTarget();
                break;
            }
        }

        std::vector<BaseSubpass*> terrainPasses = { terrainPass };
        m_terrainPipeline.initialize(m_device, RenderTargetType::SWAPCHAIN, terrainPasses, attachments, clearValues, &previousPipelineTarget.targetDepthAttachments[0].imageView);
        m_mainRenderingPipelines.push_back(&m_terrainPipeline);
    }

    void RenderSystem::createHighlightPipeline()
    {
        std::vector<VkAttachmentDescription> attachments(2);

        //Main swapchain attachment.
        attachments[0].format = m_swapchain.swapchainImageFormat;
        attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
        attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
        attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[0].initialLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        attachments[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

        // Depth attachment
        attachments[1].format = VK_FORMAT_D32_SFLOAT;
        attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
        attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
        attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        attachments[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[1].initialLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
        attachments[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        std::vector<VkClearValue> clearValues(2);
        clearValues[0].color = { 1.0f, 1.0f, 1.0f, 0.0f };
        clearValues[1].depthStencil = { 1.0f,0 };

        //Highlight Pipeline
        m_highlightPipeline.setRenderPipelineName("highlight");
        HighlightPass* highlightPass = static_cast<HighlightPass*>(m_availableSubpasses.at("highlightPass"));
        assert(highlightPass);

        highlightPass->setSubpassDependency(VK_SUBPASS_EXTERNAL, 0, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, 0,
            VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);

        std::vector<VkAttachmentReference> colorReferences(1);
        colorReferences[0] = { 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };

        std::vector<VkAttachmentReference> depthReferences(1);
        depthReferences[0] = { 1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL };
        highlightPass->createSubpassDescription({}, colorReferences, depthReferences, attachments, {});

        std::vector<BaseSubpass*> highlightSubpasses = { highlightPass };

        std::string targetPipeline = "forward";
#ifdef RAVEN_DEFERRED_LIGHTING
        targetPipeline = "deferred";
#endif

        //Get the depth from previous Pipeline and send it as a source for the deferred Pipeline. This so that 
        //deferred Pipeline doesn't clear objects that should be visible.
        RenderTarget previousPipelineTarget;
        for (auto& pipeline : m_mainRenderingPipelines)
        {
            if (pipeline->getName() == targetPipeline)
            {
                previousPipelineTarget = pipeline->getRenderTarget();
                break;
            }
        }

        m_highlightPipeline.initialize(m_device, RenderTargetType::SWAPCHAIN, highlightSubpasses, attachments, clearValues, &previousPipelineTarget.targetDepthAttachments[0].imageView);
        m_mainRenderingPipelines.push_back(&m_highlightPipeline);
    }

    void RenderSystem::createUnlitPipeline()
    {
        std::vector<VkAttachmentDescription> attachments(2);

        //Main swapchain attachment.
        attachments[0].format = m_swapchain.swapchainImageFormat;
        attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
        attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        attachments[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

        // Depth attachment
        attachments[1].format = VK_FORMAT_D32_SFLOAT;
        attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
        attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        attachments[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        std::vector<VkClearValue> clearValues(2);
        clearValues[0].color = { 1.0f, 1.0f, 1.0f, 1.0f };
        clearValues[1].depthStencil = { 1.0f,0 };

        //Default Pipeline
        m_unlitPipeline.setRenderPipelineName("default");
        UnlitPass* defaultPass = static_cast<UnlitPass*>(m_availableSubpasses.at("unlitPass"));
        assert(defaultPass);

        defaultPass->setSubpassDependency(VK_SUBPASS_EXTERNAL, 0, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, 0,
            VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);

        std::vector<VkAttachmentReference> colorReferences(1);
        colorReferences[0] = { 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };

        std::vector<VkAttachmentReference> depthReferences(1);
        depthReferences[0] = { 1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL };
        defaultPass->createSubpassDescription({}, colorReferences, depthReferences, attachments, {});

        std::vector<BaseSubpass*> defaultSubpasses = { defaultPass };

        m_unlitPipeline.initialize(m_device, RenderTargetType::SWAPCHAIN, defaultSubpasses, attachments, clearValues, nullptr);
        m_mainRenderingPipelines.push_back(&m_unlitPipeline);
    }

    //TODO:REMOVE!
    void RenderSystem::setupLights(const RavenBuffer& lightBuffer)
    {
        //TODO:Streamline this since it's a bit hacky. Composition pass uses input attachments + uniform buffers which need to be written.
        //I fetch all the attachments from the render pipeline and send them to the composition pass so that it can use them to update 
        //the descriptors.
#ifdef RAVEN_DEFERRED_LIGHTING
        for (auto& pipeline : m_mainRenderingPipelines)
        {
            if (pipeline->getName() == "deferred")
            {
                const RenderTarget& deferredTarget = pipeline->getRenderTarget();
                m_compositionPass.initializeDescriptorSet(m_device, deferredTarget.targetImageAttachments, lightBuffer);
                break;
            }
        }
#elif RAVEN_FORWARD_LIGHTING
        for (auto& pipeline : m_mainRenderingPipelines)
        {
            if (pipeline->getName() == "forward")
            {
                m_forwardLightPass.initializeDescriptorSet(m_device, lightBuffer);
                break;
            }
    }
#else
        std::cerr << "No lights defined!\n";
#endif
    }

    void RenderSystem::createMeshPipeline()
    {
        std::vector<VkAttachmentDescription> attachments(2);

        //Main swapchain attachment.
        attachments[0].format = m_swapchain.swapchainImageFormat;
        attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
        attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
        attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[0].initialLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        attachments[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

        // Depth attachment
        attachments[1].format = VK_FORMAT_D32_SFLOAT;
        attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
        attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
        attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        attachments[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[1].initialLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
        attachments[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        std::vector<VkClearValue> clearValues(2);
        clearValues[0].color = { 1.0f, 1.0f, 1.0f, 1.0f };
        clearValues[1].depthStencil = { 1.0f,0 };

        m_wireframePipeline.setRenderPipelineName("mesh");

        MeshPass* meshPass = static_cast<MeshPass*>(m_availableSubpasses.at("meshPass"));
        assert(meshPass);

        meshPass->setSubpassDependency(VK_SUBPASS_EXTERNAL, 0, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                                      VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, 0,
                                      VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);

        std::vector<VkAttachmentReference> colorReferences(1);
        colorReferences[0] = { 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };

        std::vector<VkAttachmentReference> depthReferences(1);
        depthReferences[0] = { 1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL };
        meshPass->createSubpassDescription({}, colorReferences, depthReferences, attachments, {});

        std::string targetPipeline = "forward";
#ifdef RAVEN_DEFERRED_LIGHTING
        targetPipeline = "deferred";
#endif

        //Get the depth from previous Pipeline and send it as a source for the deferred Pipeline. This so that 
        //deferred Pipeline doesn't clear objects that should be visible.
        RenderTarget previousPipelineTarget;
        for (auto& pipeline : m_mainRenderingPipelines)
        {
            if (pipeline->getName() == targetPipeline)
            {
                previousPipelineTarget = pipeline->getRenderTarget();
                break;
            }
        }

        std::vector<BaseSubpass*> meshPasses = { meshPass };
        m_wireframePipeline.initialize(m_device, RenderTargetType::SWAPCHAIN, meshPasses, attachments, clearValues, &previousPipelineTarget.targetDepthAttachments[0].imageView);

        m_mainRenderingPipelines.push_back(&m_wireframePipeline);
    }
}