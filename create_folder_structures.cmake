function(create_folder_structures TARGET)

	get_target_property(SOURCES ${TARGET} SOURCES)
	set(ROOT_DIRECTORY "${CMAKE_CURRENT_LIST_DIR}")

	foreach(FILE IN ITEMS ${SOURCES})

		get_filename_component(FILE_DIRECTORY "${FILE}" PATH)
		get_filename_component(FILE_EXTENSION "${FILE}" EXT)

		if ((${FILE_EXTENSION} STREQUAL ".h") OR (${FILE_EXTENSION} STREQUAL ".hpp"))
			
			file(RELATIVE_PATH FILE_RELATIVE_DIRECTORY "${ROOT_DIRECTORY}/inc" "${FILE_DIRECTORY}")
			string(REPLACE "/" "\\" GROUP_NAME "${FILE_RELATIVE_DIRECTORY}")
			source_group("Headers\\${GROUP_NAME}" FILES "${FILE}")

		else ((${FILE_EXTENSION} STREQUAL ".c") OR (${FILE_EXTENSION} STREQUAL ".cpp"))

			file(RELATIVE_PATH FILE_RELATIVE_DIRECTORY "${ROOT_DIRECTORY}/src" "${FILE_DIRECTORY}")
			string(REPLACE "/" "\\" GROUP_NAME "${FILE_RELATIVE_DIRECTORY}")
			source_group("Sources\\${GROUP_NAME}" FILES "${FILE}")

		endif()

	endforeach()

endfunction()
