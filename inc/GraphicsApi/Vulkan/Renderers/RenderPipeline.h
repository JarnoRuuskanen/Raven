#ifndef RAVEN_RENDER_PIPELINE_H_INCLUDED
#define RAVEN_RENDER_PIPELINE_H_INCLUDED

#include <GraphicsApi/Vulkan/Subpasses/BaseSubpass.h>
#include <EntityComponentSystem/RavenComponents/Renderable.h>
#include "GraphicsApi/Vulkan/VulkanObjects/RenderTarget.h"

namespace Raven
{
    //Renderpipeline is essentially a collection of subpasses aka a Vulkan renderpass.
    class RenderPipeline
    {
    public:
        RenderPipeline() : m_currentTargetType(RenderTargetType::SWAPCHAIN), 
                           m_descriptorPool(VK_NULL_HANDLE), 
                           m_extent(VkExtent2D{0,0}), 
                           m_renderPass(VK_NULL_HANDLE),
                           m_signalTimelineValue(0),
                           m_waitForTimelineValue(0),
                           m_submitInfo({}),
                           m_swapchainImageFormat(VK_FORMAT_UNDEFINED),
                           m_swapchainLength(0){}
        ~RenderPipeline();
        bool initialize(const VkDevice& device, RenderTargetType type, const std::vector<BaseSubpass*>& subpasses,
            const std::vector<VkAttachmentDescription>& renderPassAttachments, const std::vector<VkClearValue>& attachmentClearValues, const VkImageView* depthFromPreviousPass);
        //Executes all the subpasses that have been registered for this pipeline.
        bool draw(VkCommandBuffer cmdBuffer, uint32_t frameIndex);
        //TODO: Remove hard coded buffer index.
        const VkCommandBuffer& getCommandBufferForFrame(uint32_t frameIndex) const { return m_commandBuffers[frameIndex].buffers[0]; }
        const VkCommandPool& getCommandPoolForFrame(uint32_t frameIndex) const { return m_commandBuffers[frameIndex].pool; }
        void registerDataForDrawing(const VkDevice& device, const std::vector<PushConstantData*>& pushConstants, Renderable* renderable);
        void removeRenderableFromRenderPipeline(const VkDevice& device, Renderable* renderable);
        void clearAllRenderables(const VkDevice& device);
        inline void setRenderPipelineName(const std::string& name) { assert(!name.empty());  m_renderPipelineName = name; }
        const std::string& getName() const { return m_renderPipelineName; }
        void setTimelineWaitAndSignalValue(uint64_t waitValue, uint64_t signalValue);
        const VkTimelineSemaphoreSubmitInfo& getTimelineSubmitInfo() const { return m_submitInfo; }
        bool setRenderTarget(RenderTarget& target, const RenderTargetType& targetType, const VkImageView* depthFromPreviousPass);
        const RenderTarget& getRenderTarget() const { return m_renderTarget; }
        void destroyPipeline(const VkDevice& device);
    private:
        bool initializeRenderPass(const VkDevice& device, const std::vector<BaseSubpass*> &subpasses);
        bool initializeSubpassPipelines(const VkDevice& device, const std::vector<BaseSubpass*> &subpasses);
        bool allocateDescriptorSetsForRenderable(const VkDevice& device, Renderable*& renderable);
        void storePushConstantPointers(const std::vector<PushConstantData*>& pushConstants);
        void gatherSubpassDescriptorSetLayoutBindings(const std::vector<BaseSubpass*>& subpasses);
        void generateDescriptorPool(const VkDevice& device);
        void gatherSubpassAttachmentInfo(const std::vector<BaseSubpass*>& subpasses);
        void createCommandPoolAndBuffers(const VkDevice& device);
        void resolveRenderPassAttachmentReferences();
        bool createRenderTargetAttachments(const VkDevice& device, const VkImageView* depthFromPreviousPass);
        bool createFramebuffers(const VkDevice& device, const VkImageView* depthFromPreviousPass);
        //Not yet sure if this should be created inside the pipeline or if it should be owned and set from outside.
        bool createRenderTarget(const VkDevice& device, const VkImageView* depthFromPreviousPass);
        void destroyAttachments();

        std::vector<BaseSubpass*> m_subPasses;
        std::vector<VkSubpassDescription> m_subpassDescriptions;
        std::vector<VkSubpassDependency> m_subpassDependencies;
        //Data that has been registered for rendering with this pipeline.
        //TODO: Should ECS -variables bleed outside EntityComponentSystem?
        std::vector<Renderable*> m_renderables;
        std::vector<std::vector<VkDescriptorSet>> m_renderableDescriptorSets; //This has to match with the m_renderables.
        std::vector<std::vector<PushConstantData*>> m_renderablePushConstants;
        VkRenderPass m_renderPass;
        VkExtent2D m_extent;
        std::string m_renderPipelineName;

        VkDescriptorPool m_descriptorPool;
        //Gathered across all subpasses.
        std::vector<VkDescriptorSetLayoutBinding> m_subpassDescriptorSetLayoutBindings;

        std::vector<VkAttachmentReference> m_renderPassColorReferences;
        std::vector<VkAttachmentReference> m_renderPassDepthReferences;
        std::vector<VkAttachmentDescription> m_renderPassAttachments;

        std::vector<PoolAndBuffers> m_commandBuffers;

        uint32_t m_swapchainLength;
        VkFormat m_swapchainImageFormat;

        //What timeline values will be waited and signaled by this renderpass.
        VkTimelineSemaphoreSubmitInfo m_submitInfo;
        uint64_t m_waitForTimelineValue;
        uint64_t m_signalTimelineValue;

        RenderTarget m_renderTarget;
        //You can either render offscreen or render the pipeline results to swapchain.
        RenderTargetType m_currentTargetType;
        std::vector<VkClearValue> m_clearValues;
    };
}

#endif