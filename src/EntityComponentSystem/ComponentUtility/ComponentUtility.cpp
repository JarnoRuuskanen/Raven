#include "EntityComponentSystem/RavenComponents/ComponentUtility/ComponentUtility.h"
#include "EntityComponentSystem/RavenComponents/Renderable.h"
#include "EntityComponentSystem/RavenComponents/Mesh.h"
#include "EntityComponentSystem/RavenComponents/Transform.h"
#include "EntityComponentSystem/RavenComponents/Tag.h"
#include "EntityComponentSystem/RavenComponents/Light.h"
#include "EntityComponentSystem/RavenComponents/Camera.h"
#include "EntityComponentSystem/RavenComponents/Material.h"

namespace Raven
{
    namespace ComponentUtility
    {
        std::string getComponentName(ComponentId id)
        {
            switch(id)
            {
                case Renderable::ID:
                {
                    return "Renderable";
                }
                case Mesh::ID:
                {
                    return "Mesh";
                }
                case Transform::ID:
                {
                    return "Transform";
                }
                case Tag::ID:
                {
                    return "Tag";
                }
                case Light::ID:
                {
                    return "Light";
                }
                case Camera::ID:
                {
                    return "Camera";
                }
                case Material::ID:
                {
                    return "Material";
                }
                default:
                    return "No such component";
            }
        }

        ComponentId getComponentIdFromString(const std::string& componentString)
        {
            if(componentString == "Renderable")
            {
                return Renderable::ID;
            }
            if(componentString == "Mesh")
            {
                return Mesh::ID;
            }
            if(componentString == "Transform")
            {
                return Transform::ID;
            }
            if(componentString == "Tag")
            {
                return Tag::ID;
            }
            if(componentString == "Light")
            {
                return Light::ID;
            }
            if(componentString == "Camera")
            {
                return Camera::ID;
            }
            if(componentString == "Material")
            {
                return Material::ID;
            }
            return Component::ID;
        }
    }
}