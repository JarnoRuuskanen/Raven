#ifndef RAVEN_LIGHT_SYSTEM_H_INCLUDED
#define RAVEN_LIGHT_SYSTEM_H_INCLUDED

#include "EntityComponentSystem/RavenSystems/System.h"
#include "EntityComponentSystem/RavenComponents/Transform.h"
#include "EntityComponentSystem/RavenComponents/Light.h"
#include "GraphicsApi/Vulkan/VulkanObjects/RavenBuffer.h"

#include "Utility/DelegateManager.h"

namespace Raven
{
	class LightSystem : public System<Transform, Light>
	{
		using BaseType = System<Transform, Light>;
	public:
		LightSystem(EntityManager* pGameWorld);
		// Inherited via System
		void update(const Timer& timer) override;
		inline void setViewTransform(Transform* transform) { assert(transform);  m_viewTransform = transform; }
		const RavenBuffer& getLightBuffer() { return m_lightsUniformBuffer; }
	private:
		void entityAddedToSystem(Entity* entity) override;
		//Lightsystem needs to know the position of the viewer.
		Transform* m_viewTransform;
		//Lights are kept in a uniform buffer and the buffer is updated with memcpy if 
		//there are changes.
		RavenBuffer m_lightsUniformBuffer;

		struct
		{
			glm::vec4 viewPosition;
			LightUniformBufferObject lights[2];
		} m_uboLights;
		VulkanResourceManagerDelegates m_vulkanResourceManagerDelegates;
	};
}

#endif